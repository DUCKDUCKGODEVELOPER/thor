package threads.magnet.torrent;

import java.util.concurrent.atomic.AtomicBoolean;

import threads.magnet.data.DataDescriptor;

public final class TorrentDescriptor {

    private final AtomicBoolean active = new AtomicBoolean();
    // !! this can be null in case with magnets (and in the beginning of processing) !!
    private volatile DataDescriptor dataDescriptor;

    TorrentDescriptor() {
    }

    public boolean isActive() {
        return active.get();
    }

    public void start() {
        active.set(true);
    }


    public void stop() {
        active.set(false);
    }


    public DataDescriptor getDataDescriptor() {
        return dataDescriptor;
    }

    void setDataDescriptor(DataDescriptor dataDescriptor) {
        this.dataDescriptor = dataDescriptor;
    }

}
