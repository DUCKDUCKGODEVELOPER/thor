package threads.magnet.utils;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public interface Functional {

    static <T> T tap(T obj, Consumer<T> c) {
        c.accept(obj);
        return obj;
    }

    static <T, E extends Throwable> T tapThrow(T obj, ThrowingConsumer<T, E> c) throws E {
        c.accept(obj);
        return obj;
    }

    static <T> T unchecked(ThrowingSupplier<? extends T, ?> f) {
        try {
            return f.get();
        } catch (Throwable e) {
            throwAsUnchecked(e);
            return null;
        }
    }


    static <IN, OUT, EX extends Throwable> Function<IN, OUT> castOrThrow(Class<OUT> type, Function<IN, EX> ex) {
        return (in) -> {
            if (!type.isInstance(in))
                throwAsUnchecked(ex.apply(in));
            return type.cast(in);
        };
    }

    private static void throwAsUnchecked(Throwable t) {
        Thrower.asUnchecked(t);
    }

    @SuppressWarnings("unchecked")
    // a supertype of the expected T can be passed to allow type-inference to match erased generics
    static <K, T> Optional<T> typedGet(Map<? super K, ?> map, K key, Class<? super T> clazz) {
        return (Optional<T>) Optional.ofNullable(map.get(key)).filter(clazz::isInstance).map(clazz::cast);
    }


    interface ThrowingConsumer<T, E extends Throwable> {
        void accept(T arg) throws E;
    }

    @FunctionalInterface
    interface ThrowingSupplier<T, E extends Throwable> {
        T get() throws E;
    }


    class Thrower {

        @SuppressWarnings("unchecked")
        private static <T extends Throwable> void asUnchecked(Throwable t) throws T {
            throw (T) t;
        }
    }


}
