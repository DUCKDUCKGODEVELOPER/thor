package threads.magnet.bencoding;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;

/**
 * BEncoded dictionary.
 *
 * @since 1.0
 */
public record BEMap(byte[] content, Map<String, BEObject> value) implements BEObject {

    /**
     * @param content Binary representation of this dictionary, as read from source.
     * @param value   Parsed value
     * @since 1.0
     */
    public BEMap(byte[] content, Map<String, BEObject> value) {
        this.content = content;
        this.value = Collections.unmodifiableMap(value);
    }

    @Override
    public BEType getType() {
        return BEType.MAP;
    }

    @Override
    public void writeTo(OutputStream out) throws IOException {
        BEEncoder.encode(this, out);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BEMap)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        return value.equals(((BEMap) obj).value());
    }

    @NonNull
    @Override
    public String toString() {
        return value.toString();
    }
}
