package threads.magnet.protocol;

import androidx.annotation.NonNull;

import threads.magnet.protocol.handler.PortMessageHandler;

public record Port(int port) implements Message {

    /**
     *
     */
    public Port {

        if (port < 0 || port > 65535) {
            throw new InvalidMessageException("Invalid argument: port (" + port + ")");
        }

    }

    /**
     * @since 1.1
     */
    @Override
    public int port() {
        return port;
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "] port {" + port + "}";
    }

    @Override
    public Integer getMessageId() {
        return PortMessageHandler.PORT_ID;
    }
}
