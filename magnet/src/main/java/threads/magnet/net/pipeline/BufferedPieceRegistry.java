package threads.magnet.net.pipeline;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import threads.magnet.net.buffer.BufferedData;

public final class BufferedPieceRegistry extends ConcurrentHashMap<Long, BufferedData> {


    private static long zip(int pieceIndex, int offset) {
        return (((long) pieceIndex) << 32) + offset;
    }

    public boolean addBufferedPiece(int pieceIndex, int offset, BufferedData buffer) {
        if (pieceIndex < 0) {
            throw new IllegalArgumentException("Illegal piece index: " + pieceIndex);
        }
        Objects.requireNonNull(buffer);

        BufferedData existing = putIfAbsent(zip(pieceIndex, offset), buffer);
        return (existing == null);
    }

    public BufferedData getBufferedPiece(int pieceIndex, int offset) {
        return remove(zip(pieceIndex, offset));
    }
}
