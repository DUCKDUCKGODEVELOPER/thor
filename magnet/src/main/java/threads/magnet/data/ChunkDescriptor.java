package threads.magnet.data;

public record ChunkDescriptor(DataRange data, BlockSet blockSet,
                              byte[] checksum) implements BlockSet {

    byte[] getChecksum() {
        return checksum;
    }


    public DataRange getData() {
        return data;
    }

    @Override
    public int blockCount() {
        return blockSet.blockCount();
    }

    @Override
    public long blockSize() {
        return blockSet.blockSize();
    }

    @Override
    public long lastBlockSize() {
        return blockSet.lastBlockSize();
    }

    @Override
    public boolean isPresent(int blockIndex) {
        return blockSet.isPresent(blockIndex);
    }

    @Override
    public boolean isComplete() {
        return blockSet.isComplete();
    }


    @Override
    public void clear() {
        blockSet.clear();
    }
}
