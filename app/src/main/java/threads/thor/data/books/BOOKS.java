package threads.thor.data.books;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

public class BOOKS {

    private static volatile BOOKS INSTANCE = null;
    private final BookmarkDatabase bookmarkDatabase;


    private BOOKS(BookmarkDatabase bookmarkDatabase) {
        this.bookmarkDatabase = bookmarkDatabase;
    }

    @NonNull
    private static BOOKS createBooks(@NonNull BookmarkDatabase bookmarkDatabase) {
        return new BOOKS(bookmarkDatabase);
    }

    public static BOOKS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BOOKS.class) {
                if (INSTANCE == null) {
                    BookmarkDatabase pageDatabase = Room.databaseBuilder(context,
                                    BookmarkDatabase.class,
                                    BookmarkDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().
                            build();

                    INSTANCE = BOOKS.createBooks(pageDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public BookmarkDatabase bookmarkDatabase() {
        return bookmarkDatabase;
    }
}
