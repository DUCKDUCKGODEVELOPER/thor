package threads.thor.data.tabs;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Tab.class}, version = 1, exportSchema = false)
public abstract class TabsDatabase extends RoomDatabase {

    public abstract TabDao tabDao();

}
