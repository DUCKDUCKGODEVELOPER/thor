package threads.thor.data.tabs;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

public class TABS {
    private static volatile TABS INSTANCE = null;

    private final TabsDatabase tabsDatabase;

    private TABS(TabsDatabase tabsDatabase) {
        this.tabsDatabase = tabsDatabase;
    }

    public static TABS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (TABS.class) {
                if (INSTANCE == null) {
                    TabsDatabase tabsDatabase =
                            Room.inMemoryDatabaseBuilder(context,
                                            TabsDatabase.class).
                                    allowMainThreadQueries().build();
                    INSTANCE = new TABS(tabsDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public TabsDatabase tabsDatabase() {
        return tabsDatabase;
    }

}
