package threads.thor.data.tabs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TabDao {

    @Query("SELECT * FROM Tab")
    LiveData<List<Tab>> getLiveDataTabs();

    @Query("SELECT idx FROM Tab")
    LiveData<List<Long>> getLiveDataIdxs();

    @Query("SELECT * FROM Tab WHERE idx = :idx")
    Tab getTab(long idx);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTab(Tab tab);

    @Delete
    void deleteTab(Tab tab);

    @Query("UPDATE Tab SET title = :title, uri = :uri WHERE idx = :idx")
    void updateTab(long idx, String title, String uri);

    @Query("UPDATE Tab SET image = :image WHERE idx = :idx")
    void updateTabIcon(long idx, byte[] image);

    @Query("SELECT EXISTS(SELECT * FROM Tab)")
    boolean hasTabs();
}
