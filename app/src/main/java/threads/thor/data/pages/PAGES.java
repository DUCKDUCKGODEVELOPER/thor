package threads.thor.data.pages;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import tech.lp2p.core.Cid;
import tech.lp2p.core.PeerId;


public final class PAGES {

    private static volatile PAGES INSTANCE = null;

    private final PageDatabase pageDatabase;

    private PAGES(PageDatabase pageDatabase) {
        this.pageDatabase = pageDatabase;
    }

    public static PAGES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PAGES.class) {
                if (INSTANCE == null) {
                    PageDatabase pageDatabase = Room.databaseBuilder(context,
                                    PageDatabase.class,
                                    PageDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new PAGES(pageDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public void store(@NonNull Page page) {
        pageDatabase.pageDao().insertPage(page);
    }

    @Nullable
    public Page page(@NonNull PeerId peerId) {
        return pageDatabase.pageDao().getPage(peerId);
    }


    public void clear() {
        pageDatabase.clearAllTables();
    }


    @Nullable
    public Cid cid(@NonNull PeerId peerId) {
        return pageDatabase.pageDao().getCid(peerId);
    }

}
