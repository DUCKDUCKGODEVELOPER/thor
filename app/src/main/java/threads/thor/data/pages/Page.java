package threads.thor.data.pages;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import tech.lp2p.core.Cid;
import tech.lp2p.core.PeerId;

@Entity
public record Page(
        @PrimaryKey @ColumnInfo(name = "peerId") @NonNull PeerId peerId,
        @ColumnInfo(name = "cid") Cid cid) {

    public Page(@NonNull PeerId peerId, @NonNull Cid cid) {
        this.peerId = peerId;
        this.cid = cid;
    }

    @Override
    @NonNull
    public PeerId peerId() {
        return peerId;
    }

    @Override
    public Cid cid() {
        return cid;
    }

}