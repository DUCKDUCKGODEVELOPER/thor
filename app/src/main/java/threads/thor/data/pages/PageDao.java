package threads.thor.data.pages;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import tech.lp2p.core.Cid;
import tech.lp2p.core.PeerId;


@Dao
public interface PageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPage(Page page);

    @Query("SELECT * FROM Page WHERE peerId = :peerId")
    Page getPage(PeerId peerId);

    @Query("Select cid From Page WHERE peerId = :peerId")
    Cid getCid(PeerId peerId);

}
