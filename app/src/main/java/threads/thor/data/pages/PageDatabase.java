package threads.thor.data.pages;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.thor.utils.RoomConverters;


@androidx.room.Database(entities = {Page.class}, version = 4, exportSchema = false)
@TypeConverters({RoomConverters.class})
public abstract class PageDatabase extends RoomDatabase {

    public abstract PageDao pageDao();

}
