package threads.thor.model.data;

import android.content.Context;
import android.os.Environment;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.Objects;

import threads.thor.LogUtils;

public class DATA {
    private static final String TAG = DATA.class.getSimpleName();
    private static volatile DATA INSTANCE = null;
    @NonNull
    private final File downloads;

    private DATA(@NonNull Context context) {
        downloads = Objects.requireNonNull(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS));
    }

    public static void deleteFile(@NonNull File root, boolean deleteWhenDir) {
        try {
            if (root.isDirectory()) {
                File[] files = root.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory()) {
                            deleteFile(file, true);
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        } else {
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        }
                    }
                }
                if (deleteWhenDir) {
                    boolean result = root.delete();
                    if (!result) {
                        LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                    }
                }
            } else {
                boolean result = root.delete();
                if (!result) {
                    LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public static DATA getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (DATA.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DATA(context);
                }
            }
        }
        return INSTANCE;
    }

    public File downloadsDir() {
        if (!downloads.exists()) {
            boolean result = downloads.mkdir();
            if (!result) {
                throw new RuntimeException("image directory does not exists");
            }
        }
        return downloads;
    }

    public void cleanDataDir() {
        deleteFile(downloadsDir(), false);
    }
}
