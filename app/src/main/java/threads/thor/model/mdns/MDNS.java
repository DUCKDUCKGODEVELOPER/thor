package threads.thor.model.mdns;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Server;
import threads.thor.LogUtils;


public class MDNS {
    public static final String ALPNS = "alpns";
    public static final String MDNS_SERVICE = "_p2p._udp."; // default libp2p [or kubo] service name

    @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    private static final String TAG = MDNS.class.getName();
    private static final ALPN[] EMPTY = new ALPN[0];
    private final NsdManager nsdManager;
    private final String mdnsName;
    private final RegistrationService registrationService;
    private volatile DiscoveryService discoveryService;
    private volatile boolean serviceStarted = false;

    private MDNS(NsdManager nsdManager, String mdnsName) {
        this.nsdManager = nsdManager;
        this.mdnsName = mdnsName;
        this.registrationService = new RegistrationService();
    }

    public static MDNS mdns(Context context, String mdnsName) {
        NsdManager nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        Objects.requireNonNull(nsdManager);
        return new MDNS(nsdManager, mdnsName);
    }

    public static MDNS mdns(Context context) {
        NsdManager nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        Objects.requireNonNull(nsdManager);
        return new MDNS(nsdManager, MDNS_SERVICE);
    }

    public void startService(Server server) {
        PeerId peerId = server.self();
        String ownServiceName = peerId.toString();


        NsdServiceInfo serviceInfo = new NsdServiceInfo();

        serviceInfo.setAttribute(ALPNS, ALPN.lite.name() + ";" + ALPN.libp2p.name());

        serviceInfo.setServiceName(ownServiceName);
        serviceInfo.setServiceType(mdnsName);
        serviceInfo.setPort(server.port());

        nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationService);

        serviceStarted = true;
    }

    public void startDiscovery(Consumer<Peer> consumer) {
        try {
            this.discoveryService = new DiscoveryService(nsdManager, consumer);

            nsdManager.discoverServices(mdnsName,
                    NsdManager.PROTOCOL_DNS_SD, discoveryService);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void stop() {
        try {
            if (serviceStarted) {
                nsdManager.unregisterService(registrationService);
                serviceStarted = false;
            }
            if (discoveryService != null) {
                nsdManager.stopServiceDiscovery(discoveryService);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable); // not ok when fails
        }
    }


    private record DiscoveryService(@NonNull NsdManager nsdManager,
                                    @NonNull Consumer<Peer> consumer)
            implements NsdManager.DiscoveryListener {
        private static final String TAG = DiscoveryService.class.getSimpleName();


        private static ALPN[] evaluateAlpns(
                @NonNull NsdServiceInfo serviceInfo) {
            try {
                Map<String, byte[]> attributes = serviceInfo.getAttributes();
                byte[] bytes = attributes.get(ALPNS);
                if (bytes != null && bytes.length > 0) {
                    String alpns = new String(bytes);
                    String[] alpnArray = alpns.split(";");
                    ALPN[] result = new ALPN[alpnArray.length];
                    for (int i = 0; i < alpnArray.length; i++) {
                        result[i] = ALPN.valueOf(alpnArray[i]);
                    }
                    return result;
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // not ok when fails
            }
            return EMPTY;
        }

        @Override
        public void onStartDiscoveryFailed(String serviceType, int errorCode) {
            LogUtils.error(TAG, "onStartDiscoveryFailed " + serviceType);
        }

        @Override
        public void onStopDiscoveryFailed(String serviceType, int errorCode) {
            LogUtils.error(TAG, "onStopDiscoveryFailed " + serviceType);
        }

        @Override
        public void onDiscoveryStarted(String serviceType) {
            LogUtils.error(TAG, "onDiscoveryStarted " + serviceType);
        }

        @Override
        public void onDiscoveryStopped(String serviceType) {
            LogUtils.error(TAG, "onDiscoveryStopped " + serviceType);
        }

        @Override
        public void onServiceFound(NsdServiceInfo serviceInfo) {
            LogUtils.error(TAG, "onServiceFound " + serviceInfo);

            if (Build.VERSION.SDK_INT >= 34) {
                //noinspection AnonymousInnerClass
                nsdManager.registerServiceInfoCallback(serviceInfo, EXECUTOR,
                        new NsdManager.ServiceInfoCallback() {
                            @Override
                            public void onServiceInfoCallbackRegistrationFailed(int i) {
                                LogUtils.error(TAG, "onServiceInfoCallbackRegistrationFailed " + i);
                            }

                            @Override
                            public void onServiceUpdated(@NonNull NsdServiceInfo nsdServiceInfo) {
                                evaluateHosts(nsdServiceInfo);
                            }

                            @Override
                            public void onServiceLost() {
                                LogUtils.error(TAG, "onServiceLost");
                            }

                            @Override
                            public void onServiceInfoCallbackUnregistered() {
                                LogUtils.error(TAG, "onServiceInfoCallbackUnregistered");
                            }
                        });
            } else {
                //noinspection AnonymousInnerClass
                nsdManager.resolveService(serviceInfo, new NsdManager.ResolveListener() {

                    @Override
                    public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {
                        LogUtils.error(TAG, "onResolveFailed " + nsdServiceInfo.toString());
                    }

                    @Override
                    public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
                        evaluateHost(nsdServiceInfo);
                    }
                });
            }
        }

        @Override
        public void onServiceLost(NsdServiceInfo serviceInfo) {
            LogUtils.info(TAG, "onServiceLost " + serviceInfo.toString());
        }

        private void evaluateHost(@NonNull NsdServiceInfo serviceInfo) {
            try {
                PeerId decodedPeerId = Lite.decodePeerId(serviceInfo.getServiceName());

                ALPN[] alpns = evaluateAlpns(serviceInfo);

                InetAddress inetAddress = serviceInfo.getHost();

                Peeraddr peeraddr = Peeraddr.create(decodedPeerId,
                        new InetSocketAddress(inetAddress, serviceInfo.getPort()));
                consumer.accept(new Peer(peeraddr, alpns));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // is ok, fails for kubo
            }
        }

        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private void evaluateHosts(@NonNull NsdServiceInfo serviceInfo) {
            try {
                LogUtils.error(TAG, serviceInfo.toString());

                PeerId decodedPeerId = Lite.decodePeerId(serviceInfo.getServiceName());
                ALPN[] alpns = evaluateAlpns(serviceInfo);

                Peeraddrs peeraddrs = new Peeraddrs();
                for (InetAddress inetAddress : serviceInfo.getHostAddresses()) {
                    peeraddrs.add(Peeraddr.create(decodedPeerId,
                            new InetSocketAddress(inetAddress, serviceInfo.getPort())));
                }
                if (!peeraddrs.isEmpty()) {
                    Peeraddr peeraddr = Peeraddrs.reduceToOne(peeraddrs);
                    Objects.requireNonNull(peeraddr);
                    consumer.accept(new Peer(peeraddr, alpns));
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // is ok, fails for kubo
            }
        }

    }

    public record Peer(Peeraddr peeraddr, ALPN[] alpns) {
    }

    public static class RegistrationService implements NsdManager.RegistrationListener {
        private static final String TAG = RegistrationService.class.getSimpleName();

        @Override
        public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            LogUtils.error(TAG, "onRegistrationFailed : " + errorCode);
        }

        @Override
        public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            LogUtils.error(TAG, "onUn-RegistrationFailed : " + errorCode);
        }

        @Override
        public void onServiceRegistered(NsdServiceInfo serviceInfo) {
            LogUtils.error(TAG, "onServiceRegistered : " + serviceInfo.getServiceName());
        }

        @Override
        public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
            LogUtils.error(TAG, "onServiceUnregistered : " + serviceInfo.getServiceName());
        }
    }

}
