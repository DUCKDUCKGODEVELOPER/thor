package threads.thor.model.peers;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import tech.lp2p.core.Peeraddr;

@Entity
public record Peer(@PrimaryKey @ColumnInfo(name = "id") int id,
                   @NonNull @ColumnInfo(name = "peeraddr") Peeraddr peeraddr) {

    @NonNull
    public static Peer create(@NonNull Peeraddr peeraddr) {

        return new Peer(peeraddr.hashCode(), peeraddr);
    }

    @Override
    public int id() {
        return id;
    }

    @NonNull
    public Peeraddr peeraddr() {
        return peeraddr;
    }


}
