package threads.thor.model.peers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import java.util.List;

import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;


public final class PEERS implements PeerStore {

    private static volatile PEERS INSTANCE = null;
    private final PeerDatabase bootstrapDatabase;

    private PEERS(PeerDatabase bootstrapDatabase) {
        this.bootstrapDatabase = bootstrapDatabase;
    }

    public static PEERS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PEERS.class) {
                if (INSTANCE == null) {
                    PeerDatabase bootstrapDatabase = Room.databaseBuilder(context, PeerDatabase.class,
                                    PeerDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new PEERS(bootstrapDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    @NonNull
    public List<Peeraddr> randomPeeraddrs(int limit) {
        return bootstrapDatabase.bootstrapDao().randomPeers(limit);
    }


    @Override
    public void storePeeraddr(@NonNull Peeraddr peeraddr) {
        bootstrapDatabase.bootstrapDao().insertPeer(Peer.create(peeraddr));
    }


    @Override
    public void removePeeraddr(@NonNull Peeraddr peeraddr) {
        bootstrapDatabase.bootstrapDao().removePeer(Peer.create(peeraddr));
    }

}
