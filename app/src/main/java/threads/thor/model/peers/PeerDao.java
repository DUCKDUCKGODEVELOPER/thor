package threads.thor.model.peers;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import tech.lp2p.core.Peeraddr;


@Dao
public interface PeerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPeer(Peer peer);

    @Query("SELECT peeraddr FROM Peer ORDER BY RANDOM() LIMIT :limit")
    List<Peeraddr> randomPeers(int limit);

    @Delete
    void removePeer(Peer peer);
}
