package threads.thor.model.peers;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import tech.lp2p.store.RoomConverters;

@androidx.room.Database(entities = {Peer.class}, version = 2, exportSchema = false)
@TypeConverters({RoomConverters.class})
public abstract class PeerDatabase extends RoomDatabase {

    public abstract PeerDao bootstrapDao();

}
