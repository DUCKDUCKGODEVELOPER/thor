package threads.thor.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.webkit.WebResourceResponse;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Info;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.pages.PAGES;
import threads.thor.data.pages.Page;
import threads.thor.model.peers.PEERS;
import threads.thor.utils.MimeTypeService;

public class API {
    public static final String INDEX_HTML = "index.html";
    public static final int CLICK_OFFSET = 500;
    public static final String MAGNET_SCHEME = "magnet";
    public static final String PNS_SCHEME = "pns";
    public static final String HTTPS_SCHEME = "https";
    public static final String HTTP_SCHEME = "http";
    public static final String FILE_SCHEME = "file";
    public static final String TAB = "tab";
    public static final Payload PNS_RECORD = new Payload("pns-record", 20);

    private static final String TAG = API.class.getSimpleName();
    private static final int QR_CODE_SIZE = 250;
    @NonNull
    private static final ConcurrentHashMap<PeerId, Cid> resolves = new ConcurrentHashMap<>();
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    private static final Pattern CONTENT_DISPOSITION_PATTERN =
            Pattern.compile("attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1(\"?);",
                    Pattern.CASE_INSENSITIVE);
    private static volatile API INSTANCE = null;
    @NonNull
    private final ConcurrentHashMap<PeerId, Peeraddr> locals = new ConcurrentHashMap<>();
    @NonNull
    private final ConcurrentHashMap<PeerId, Connection> swarm = new ConcurrentHashMap<>();
    @NonNull
    private final PAGES pages;
    @NonNull
    private final Lite lite;
    @NonNull
    private final Session session;

    private API(@NonNull Context context) throws Exception {
        Payloads payloads = new Payloads();
        payloads.put(PNS_RECORD.type(), PNS_RECORD);
        Lite.Settings settings = Lite.createSettings(
                Lite.generateKeyPair(), bootstrap(), "thor/1.0.0/", payloads);
        lite = new Lite(settings);

        pages = PAGES.getInstance(context);
        BLOCKS blocks = BLOCKS.getInstance(context);
        PEERS peers = PEERS.getInstance(context);
        session = lite.createSession(blocks, peers); // default global session
    }


    public static byte[] bytes(@Nullable Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Bitmap copy = bitmap.copy(bitmap.getConfig(), true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        copy.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        copy.recycle();
        return byteArray;
    }

    public static API getInstance(@NonNull Context context) throws Exception {

        if (INSTANCE == null) {
            synchronized (API.class) {
                if (INSTANCE == null) {
                    INSTANCE = new API(context);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static String getUriTitle(@NonNull Uri uri) {

        if (Objects.equals(uri.getScheme(), PNS_SCHEME)) {
            List<String> paths = uri.getPathSegments();
            if (!paths.isEmpty()) {
                return paths.get(paths.size() - 1);
            } else {
                String host = uri.getHost();
                if (host != null && !host.isBlank()) {
                    return host;
                } else {
                    return "";
                }
            }
        } else {
            String host = uri.getHost();
            if (host != null && !host.isBlank()) {
                return host;
            } else {
                List<String> paths = uri.getPathSegments();
                if (!paths.isEmpty()) {
                    return paths.get(paths.size() - 1);
                } else {
                    return "";
                }
            }
        }

    }

    @DrawableRes
    public static int getImageResource(@NonNull Uri uri) {
        if (Objects.equals(uri.getScheme(), API.FILE_SCHEME)) {
            return R.drawable.bookmark_mht; // it can only be mht right now
        } else if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {
            return R.drawable.bookmark_pns;
        } else {
            return R.drawable.bookmark;
        }
    }

    public static Bitmap getBitmap(@NonNull String content) {
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE,
                    QR_CODE_SIZE, QR_CODE_SIZE);
            return createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static Bitmap createBitmap(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static WebResourceResponse createEmptyResource() {
        return new WebResourceResponse(MimeTypeService.PLAIN_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream("".getBytes()));
    }

    public static WebResourceResponse createErrorMessage(@NonNull Throwable throwable) {
        LogUtils.error(TAG, throwable);
        String message = throwable.getMessage();
        if (message == null || message.isEmpty()) {
            message = throwable.getClass().getSimpleName();
        }
        String report = generateErrorHtml(message);
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(report.getBytes()));
    }

    public static WebResourceResponse createErrorMessage(@NonNull String message) {
        String report = generateErrorHtml(message);
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(report.getBytes()));
    }

    private static String generateErrorHtml(@NonNull String message) {
        return "<html>" + "<head>" + MimeTypeService.META +
                "<title>" + "Error" + "</title>" +
                "</head>\n" + MimeTypeService.STYLE +
                "<body><div <div>" + message + "</div></body></html>";
    }

    @NonNull
    private static String getMimeType(@NonNull Info info) {

        if (info.isDir()) {
            return MimeTypeService.DIR_MIME_TYPE;
        }

        return MimeTypeService.OCTET_MIME_TYPE;
    }

    @Nullable
    private static String getHost(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), PNS_SCHEME)) {
                return uri.getHost();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    @NonNull
    private static WebResourceResponse getContentResponse(@NonNull Connection connection,
                                                          @NonNull Cid cid,
                                                          @NonNull String mimeType)
            throws Exception {

        try (InputStream in = Lite.getInputStream(connection, cid)) {

            Map<String, String> responseHeaders = new HashMap<>();

            return new WebResourceResponse(mimeType, StandardCharsets.UTF_8.name(), 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }
    }

    @NonNull
    public static String getMimeType(@NonNull Uri uri, @NonNull Info info) {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            String name = paths.get(paths.size() - 1);
            String mimeType = MimeTypeService.getMimeType(name);
            if (!mimeType.equals(MimeTypeService.OCTET_MIME_TYPE)) {
                return mimeType;
            } else {
                return getMimeType(info);
            }
        } else {
            return getMimeType(info);
        }

    }

    private static void addResolves(@NonNull PeerId peerId,
                                    @NonNull Cid root) {
        resolves.put(peerId, root);
    }

    public static WebResourceResponse createRedirectMessage(@NonNull Uri uri) {
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(("<!DOCTYPE HTML>\n" +
                "<html lang=\"en-US\">\n" +
                "    <head>\n" + MimeTypeService.META +
                "        <meta http-equiv=\"refresh\" content=\"0; url=" + uri + "\">\n" +
                "        <title>Page Redirection</title>\n" +
                "    </head>\n" + MimeTypeService.STYLE +
                "    <body>\n" +
                "        Automatically redirected to the <a style=\"word-wrap: break-word;\" href='" + uri + "'>" + uri + "</a> location\n" +
                "</html>").getBytes()));
    }

    private static String generateDirectoryHtml(@NonNull Session session,
                                                @NonNull Cid root,
                                                @NonNull Uri uri,
                                                @NonNull List<String> paths,
                                                @Nullable List<Info> infos) {
        String title = "";

        try {
            title = Lite.info(session, root).name();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (paths.isEmpty()) {
            if (title == null || title.isBlank()) {
                title = uri.getHost();
            }
        } else {
            title = paths.get(paths.size() - 1); // get last
        }


        StringBuilder answer = new StringBuilder("<html>" + "<head>" + MimeTypeService.META +
                "<title>" + title + "</title>");

        answer.append("</head>");
        answer.append(MimeTypeService.STYLE);
        answer.append("<body>");

        if (infos != null) {
            if (!infos.isEmpty()) {
                answer.append("<form><table  width=\"100%\" style=\"border-spacing: 8px;\">");

                // folder up
                if (!paths.isEmpty()) {
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (int i = 0; i < paths.size() - 1; i++) {
                        String path = paths.get(i);
                        builder.appendPath(path);
                    }

                    Uri linkUri = builder.build();
                    answer.append("<tr>");
                    answer.append("<td align=\"center\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(MimeTypeService.FOLDER_UP);
                    answer.append("</a>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("..");
                    answer.append("</td>");
                    answer.append("<td/>");
                    answer.append("<td/>");
                    answer.append("</td>");
                    answer.append("</tr>");
                }


                for (Info info : infos) {

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (String path : paths) {
                        builder.appendPath(path);
                    }
                    builder.appendPath(info.name());
                    builder.appendQueryParameter("download", "0");
                    Uri linkUri = builder.build();
                    answer.append("<tr>");

                    answer.append("<td>");
                    answer.append(MimeTypeService.getSvgResource(info));
                    answer.append("</td>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(info.name());
                    answer.append("</a>");
                    answer.append("</td>");

                    answer.append("<td>");
                    answer.append(getFileSize(info.size()));
                    answer.append("</td>");

                    answer.append("<td align=\"center\">");
                    String text = "<button style=\"float:none!important;display:inline;\" " +
                            "name=\"download\" value=\"1\" formenctype=\"text/plain\" " +
                            "formmethod=\"get\" type=\"submit\" formaction=\"" +
                            linkUri + "\">" + MimeTypeService.getSvgDownload() + "</button>";
                    answer.append(text);
                    answer.append("</td>");
                    answer.append("</tr>");
                }
                answer.append("</table></form>");
            }

        }

        ZonedDateTime zoned = ZonedDateTime.now();
        DateTimeFormatter pattern = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

        answer.append("</body><div class=\"footer\">")
                .append("<p>")
                .append(zoned.format(pattern))
                .append("</p>")
                .append("</div></html>");


        return answer.toString();
    }

    private static String getFileSize(long size) {

        String fileSize;

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize.concat(" B");
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize.concat(" KB");
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize.concat(" MB");
        }
    }

    @NonNull
    private static Uri redirect(@NonNull Connection connection,
                                @NonNull Uri uri, @NonNull Cid root,
                                @NonNull List<String> paths)
            throws Exception {

        Info info = Lite.resolvePath(connection, root, paths);
        if (info instanceof Dir dir) {
            boolean exists = Lite.hasChild(connection, dir, INDEX_HTML);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }

    public static void cleanupResolver(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), PNS_SCHEME)) {
                String host = getHost(uri);
                if (host != null) {
                    PeerId peerId = Lite.decodePeerId(host);
                    resolves.remove(peerId);
                }
            }
        } catch (Throwable ignore) {
            // ignore common failure
        }
    }

    public static boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return false;
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
        return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    @NonNull
    public static Peeraddrs bootstrap() {
        // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
        Peeraddrs peeraddrs = new Peeraddrs();
        peeraddrs.add(Lite.createPeeraddr(
                "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                "104.131.131.82", 4001));
        return peeraddrs;
    }

    @NonNull
    public static Uri redirectUri(@NonNull Connection connection, @NonNull Cid root,
                                  @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();
        return redirect(connection, uri, root, paths);
    }

    @Nullable
    public static Uri downloadsUri(Context context, String mimeType, String name, String path) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Downloads.DISPLAY_NAME, name);
        contentValues.put(MediaStore.Downloads.MIME_TYPE, mimeType);
        contentValues.put(MediaStore.Downloads.RELATIVE_PATH, path);

        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);
    }

    @Nullable
    public static String parseContentDisposition(String contentDisposition) {
        try {
            Matcher m = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
            if (m.find()) {
                return m.group(2);
            }
        } catch (IllegalStateException ex) {
            // This function is defined as returning null when it can't parse the header
        }
        return null;
    }

    @NonNull
    public static String getFileName(@NonNull Context context, @NonNull Uri uri) {
        String filename = null;

        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            filename = cursor.getString(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (filename == null) {
            filename = uri.getLastPathSegment();
        }

        if (filename == null) {
            filename = "file_name_not_detected";
        }

        return filename;
    }

    public static boolean downloadActive(@Nullable String url) {
        if (url != null && !url.isEmpty()) {
            Uri uri = Uri.parse(url);
            return Objects.equals(uri.getScheme(), API.PNS_SCHEME) ||
                    Objects.equals(uri.getScheme(), API.HTTP_SCHEME) ||
                    Objects.equals(uri.getScheme(), API.HTTPS_SCHEME);
        }
        return false;
    }

    @NonNull
    private WebResourceResponse getContentResponse(@NonNull Cid cid, @NonNull String mimeType)
            throws Exception {

        try (InputStream in = Lite.getInputStream(session, cid)) {

            Map<String, String> responseHeaders = new HashMap<>();

            return new WebResourceResponse(mimeType, StandardCharsets.UTF_8.name(), 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }
    }

    @NonNull
    private Uri redirect(@NonNull Uri uri, @NonNull Cid root, @NonNull List<String> paths)
            throws Exception {

        Info info = Lite.resolvePath(session, root, paths);
        if (info instanceof Dir dir) {
            boolean exists = Lite.hasChild(session, dir, INDEX_HTML);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }

    @NonNull
    public Uri redirectUri(@NonNull Cid root, @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();
        return redirect(uri, root, paths);
    }

    @NonNull
    public WebResourceResponse response(@NonNull Connection connection, @NonNull Cid root,
                                        @NonNull Uri uri)
            throws Exception {
        List<String> paths = uri.getPathSegments();

        Info info = Lite.resolvePath(connection, root, paths); // is resolved
        if (info instanceof Dir dir) {
            List<Info> childs = Lite.childs(session, dir);
            String answer = generateDirectoryHtml(session, root, uri, paths, childs);
            return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                    StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));

        } else {
            String mimeType = getMimeType(uri, info);
            return getContentResponse(connection, info.cid(), mimeType);
        }

    }

    @NonNull
    public WebResourceResponse response(@NonNull Cid root, @NonNull Uri uri)
            throws Exception {
        List<String> paths = uri.getPathSegments();

        Info info = Lite.resolvePath(session, root, paths); // is resolved
        if (info instanceof Dir dir) {
            List<Info> childs = Lite.childs(session, dir);
            String answer = generateDirectoryHtml(session, root, uri, paths, childs);
            return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                    StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));

        } else {
            String mimeType = getMimeType(uri, info);
            return getContentResponse(info.cid(), mimeType);
        }

    }

    @Nullable
    public Cid resolveName(@NonNull PeerId peerId) {
        Page page = pages.page(peerId);
        if (page != null) {
            return page.cid();
        }
        return null;
    }

    @NonNull
    public Cid resolveName(@NonNull Connection connection, @NonNull PeerId peerId) throws Exception {

        Cid resolved = resolves.get(peerId);
        if (resolved != null) {
            return resolved;
        }

        Envelope envelope = Lite.pullEnvelope(session, connection, PNS_RECORD);
        Objects.requireNonNull(envelope);
        if (!Objects.equals(envelope.peerId(), peerId)) {
            throw new Exception("Envelope not from requested peer");
        }
        Cid entry = envelope.cid();

        pages.store(new Page(peerId, entry));
        addResolves(peerId, entry);
        return entry;
    }


    @NonNull
    public PeerId self() {
        return lite.self();
    }

    @NonNull
    public Connection connect(@NonNull Peeraddr peeraddr) throws Exception {
        Connection connection = swarm.get(peeraddr.peerId());
        if (connection != null) {
            if (connection.isConnected()) {
                return connection;
            } else {
                swarm.remove(peeraddr.peerId());
            }
        }
        connection = Lite.dial(session, peeraddr, Parameters.create(ALPN.lite, true));
        swarm.put(peeraddr.peerId(), connection);
        return connection;
    }

    @NonNull
    public Connection connect(@NonNull PeerId peerId, @NonNull Cancellable cancellable) throws Exception {
        Connection connection = swarm.get(peerId);
        if (connection != null) {
            if (connection.isConnected()) {
                return connection;
            } else {
                swarm.remove(peerId);
            }
        }
        synchronized (peerId.toString().intern()) {
            connection = Lite.dial(session, peerId, Parameters.create(ALPN.lite, false),
                    cancellable);
            swarm.put(peerId, connection);
            return connection;

        }
    }

    @NonNull
    public Session createSession(BLOCKS blocks, PEERS peers) {
        return lite.createSession(blocks, peers);
    }

    public boolean hasConnection(PeerId peerId) {
        Connection connection = swarm.get(peerId);
        if (connection != null) {
            if (connection.isConnected()) {
                return true;
            } else {
                swarm.remove(peerId);
            }
        }
        return false;
    }

    public void local(Peeraddr peeraddr) {
        locals.put(peeraddr.peerId(), peeraddr);
    }

    @NonNull
    public Peeraddr local(PeerId peerId) {
        return Objects.requireNonNull(locals.get(peerId));
    }

    public boolean hasLocal(PeerId peerId) {
        return locals.containsKey(peerId);
    }
}
