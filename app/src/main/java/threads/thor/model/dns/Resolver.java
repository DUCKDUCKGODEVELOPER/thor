package threads.thor.model.dns;

import android.net.DnsResolver;

import androidx.annotation.NonNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import threads.magnet.LogUtils;


public interface Resolver {
    String DNS_ADDR = "dnsaddr=";
    String DNS_LINK = "dnslink=";
    String TAG = Resolver.class.getSimpleName();
    ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    Random RANDOM = new Random();
    int UDP_PAYLOAD_SIZE = 1024;
    int REQUEST_TIMEOUT = 1; // in seconds

    @NonNull
    static String resolveDnsLink(@NonNull String host) {

        Set<String> txtRecords = retrieveTxtRecords("_dnslink.".concat(host));
        for (String txtRecord : txtRecords) {
            if (txtRecord.startsWith(DNS_LINK)) {
                return txtRecord.replaceFirst(DNS_LINK, "");
            }
        }
        return "";
    }


    @NonNull
    static Set<Peeraddr> resolveDnsaddr(@NonNull String host, @NonNull PeerId peerId) {
        Set<Peeraddr> peeraddrs = new HashSet<>();

        try {
            Set<Peeraddr> addresses = resolveDnsaddrHost(host);
            for (Peeraddr addr : addresses) {
                PeerId cmpPeerId = addr.peerId();
                if (Objects.equals(cmpPeerId, peerId)) {
                    peeraddrs.add(addr);
                }
            }
        } catch (Throwable ignore) {
        }
        return peeraddrs;
    }


    @NonNull
    static Set<Peeraddr> resolveDnsaddrHost(@NonNull String host) {
        return resolveDnsAddressInternal(host, new HashSet<>());
    }

    @NonNull
    private static Set<Peeraddr> resolveDnsAddressInternal(@NonNull String host,
                                                           @NonNull Set<String> hosts) {
        Set<Peeraddr> multiAddresses = new HashSet<>();
        // recursion protection
        if (hosts.contains(host)) {
            return multiAddresses;
        }
        hosts.add(host);

        Set<String> txtRecords = retrieveTxtRecords("_." + host);

        for (String txtRecord : txtRecords) {
            try {
                threads.thor.LogUtils.error(TAG, txtRecord);
                if (txtRecord.startsWith(DNS_ADDR)) {
                    String testRecordReduced = txtRecord.replaceFirst(DNS_ADDR, "");
                    if (testRecordReduced.startsWith("/dnsaddr/")) {
                        String rest = testRecordReduced.replaceFirst("/dnsaddr/", "");
                        String[] tokens = rest.split("/");
                        resolveDnsAddressInternal(tokens[0], hosts);
                    } else {
                        Peeraddr peeraddr = Peeraddr.create(testRecordReduced);
                        multiAddresses.add(peeraddr);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.warning(TAG, "Not supported " + txtRecord);
            }
        }
        return multiAddresses;
    }


    @NonNull
    private static Set<String> retrieveTxtRecords(@NonNull String host) {
        Set<String> txtRecords = ConcurrentHashMap.newKeySet();
        long start = System.currentTimeMillis();
        CountDownLatch finishFirst = new CountDownLatch(1);

        DnsQuestion question = DnsQuestion.create(DnsName.from(host));
        DnsMessage query = buildMessage(question).build();
        //noinspection AnonymousInnerClass
        DnsResolver.getInstance().rawQuery(null, query.serialize(),
                DnsResolver.FLAG_EMPTY, EXECUTOR_SERVICE, null,
                new DnsResolver.Callback<>() {
                    @Override
                    public void onAnswer(@NonNull byte[] bytes, int i) {

                        LogUtils.error(TAG, host + " " + new String(bytes) + " " + i);
                        try {
                            DnsMessage dnsMessage = DnsMessage.parse(bytes);
                            for (DnsRecord dnsRecord : dnsMessage.answerSection()) {
                                DnsData payload = dnsRecord.getPayload();
                                if (payload instanceof DnsData.TXT text) {
                                    txtRecords.add(text.getText());
                                } else {
                                    LogUtils.error(TAG, host + " " + payload.toString());
                                }
                            }

                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, host + " " + throwable.getMessage());
                        } finally {
                            finishFirst.countDown();
                        }
                    }

                    @Override
                    public void onError(@NonNull DnsResolver.DnsException dnsException) {
                        LogUtils.error(TAG, host + " " + dnsException);
                        finishFirst.countDown();
                    }
                });


        try {
            boolean finished = finishFirst.await(REQUEST_TIMEOUT, TimeUnit.SECONDS);
            if (LogUtils.isDebug()) {
                LogUtils.error(TAG, "finished " + finished + " " +
                        (System.currentTimeMillis() - start) + "[ms]");
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable.getClass().getSimpleName()); // is ok, when it is InterruptedException
        }
        return txtRecords;
    }

    private static DnsMessage.Builder buildMessage(DnsQuestion question) {
        DnsMessage.Builder message = DnsMessage.builder();
        message.setQuestion(question);
        message.setId(RANDOM.nextInt());
        message.setRecursionDesired();
        message.getEdnsBuilder().setUdpPayloadSize(UDP_PAYLOAD_SIZE).setDnssecOk(false);
        return message;
    }


}
