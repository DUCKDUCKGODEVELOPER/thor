package threads.thor.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

import threads.thor.data.books.Bookmark;


final class BookmarkDiffCallback extends DiffUtil.Callback {
    private final List<Bookmark> mOldList;
    private final List<Bookmark> mNewList;

    BookmarkDiffCallback(List<Bookmark> messages, List<Bookmark> messageThreads) {
        this.mOldList = messages;
        this.mNewList = messageThreads;
    }

    private static boolean sameContent(@NonNull Bookmark t, @NonNull Bookmark o) {

        if (t == o) return true;
        return t.timestamp() == o.timestamp() &&
                Objects.equals(t.uri(), o.uri()) &&
                Objects.equals(t.title(), o.title());
    }


    private static boolean areItemsTheSame(@NonNull Bookmark t, @NonNull Bookmark o) {
        return Objects.equals(t.uri(), o.uri());
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return sameContent(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

}
