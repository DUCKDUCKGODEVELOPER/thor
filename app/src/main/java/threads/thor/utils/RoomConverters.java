package threads.thor.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import threads.thor.LogUtils;

public class RoomConverters {


    @NonNull
    @TypeConverter
    public static Peeraddr fromArrayToPeeraddr(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data can not be null");
        }


        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(data)) {
            try (ObjectInputStream objectInputStream =
                         new ObjectInputStream(inputStream)) {
                return (Peeraddr) objectInputStream.readObject();
            }

        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Peeraddr peeraddr) {
        if (peeraddr == null) {
            throw new IllegalStateException("multiaddr can not be null");
        }

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
                objectOutputStream.writeObject(peeraddr);
                return outputStream.toByteArray();
            }
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }

    }


    @Nullable
    @TypeConverter
    public static Cid fromArrayToCid(byte[] data) {
        if (data == null) {
            return null;
        }
        try {
            return Keys.decodeCid(data);
        } catch (Throwable throwable) {
            LogUtils.error(RoomConverters.class.getSimpleName(), throwable);
            return null;
        }
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            return null;
        }
        return cid.encoded();
    }


    @NonNull
    @TypeConverter
    public static PeerId fromArrayToPeerId(byte[] data) {
        return PeerId.create(data);
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(PeerId peerId) {
        return peerId.multihash();
    }
}
