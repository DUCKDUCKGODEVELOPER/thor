package threads.thor.work;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Info;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.model.API;
import threads.thor.model.peers.PEERS;
import threads.thor.utils.MimeTypeService;

public final class DownloadContentWorker extends Worker {

    private static final String TAG = DownloadContentWorker.class.getSimpleName();

    private static final String URI = "uri";
    private final AtomicBoolean success = new AtomicBoolean(true);

    /**
     * @noinspection WeakerAccess
     */
    public DownloadContentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri content) {

        Data.Builder data = new Data.Builder();
        data.putString(URI, content.toString());

        return new OneTimeWorkRequest.Builder(DownloadContentWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri content) {
        WorkManager.getInstance(context).enqueue(getWork(content));
    }

    @NonNull
    @Override
    public Result doWork() {


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... ");

        try {
            API api = API.getInstance(getApplicationContext());

            BLOCKS blocks = BLOCKS.getInstance(getApplicationContext());
            PEERS peers = PEERS.getInstance(getApplicationContext());
            try (Session session = api.createSession(blocks, peers)) {

                Uri uri = Uri.parse(getInputData().getString(URI));

                String name = API.getUriTitle(uri);

                setForegroundAsync(createForegroundInfo(name, 0));

                PeerId peerId = Lite.decodePeerId(
                        Objects.requireNonNull(uri.getHost()));
                Connection connection = api.connect(peerId, this::isStopped);
                Cid root = api.resolveName(connection, peerId);

                Info info = Lite.resolvePath(connection, root, uri.getPathSegments());
                Objects.requireNonNull(info); // is resolved

                Stack<String> paths = new Stack<>();
                paths.push(name);
                if (info instanceof Dir dir) {
                    Objects.requireNonNull(dir);
                    download(connection, session, paths, dir);
                } else {
                    download(connection, paths, info.cid());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private void download(@NonNull Connection connection,
                          @NonNull Stack<String> paths,
                          @NonNull Cid cid) {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start [" + (System.currentTimeMillis() - start) + "]...");


        String name = paths.pop();
        Objects.requireNonNull(name);

        String mimeType = MimeTypeService.getMimeType(name);
        String path = Environment.DIRECTORY_DOWNLOADS + File.separator + String.join((File.separator), paths);

        Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name, path);
        Objects.requireNonNull(downloads);
        ContentResolver contentResolver = getApplicationContext().getContentResolver();


        try (OutputStream os = contentResolver.openOutputStream(downloads)) {
            Objects.requireNonNull(os, "Failed to open output stream.");
            Lite.fetchToOutputStream(connection, os, cid, progress -> setForegroundAsync(
                    createForegroundInfo(name, progress)));

        } catch (Throwable throwable) {
            success.set(false);
            contentResolver.delete(downloads, null, null);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

    }

    private void evalLinks(@NonNull Connection connection,
                           @NonNull Session session, @NonNull Stack<String> paths,
                           @NonNull List<Info> childs) throws Exception {

        for (Info child : childs) {
            if (!isStopped()) {
                Cid cid = child.cid(); // not yet resolved
                paths.push(child.name());
                Info info = Lite.resolvePath(connection, cid, Collections.emptyList());
                if (info instanceof Dir dir) { // now it is resolved
                    Objects.requireNonNull(dir);
                    download(connection, session, paths, dir);
                } else {
                    download(connection, paths, cid);
                }
            }
        }

    }


    private void download(@NonNull Connection connection,
                          @NonNull Session session,
                          @NonNull Stack<String> paths,
                          @NonNull Dir dir) throws Exception {

        List<Info> childs = Lite.childs(session, dir);

        evalLinks(connection, session, paths, childs);

    }

    @NonNull
    private ForegroundInfo createForegroundInfo(@NonNull String name, int progress) {

        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);

        PendingIntent cancelPendingIntent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent intent = InitApplication.getDownloadsIntent();

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                cancelPendingIntent).build();

        builder.setContentTitle(name)
                .setSubText(progress + "%")
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true)
                .setOngoing(true);


        Notification notification = builder.build();

        int notificationId = getId().hashCode();
        return new ForegroundInfo(notificationId, notification,
                ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC);
    }
}
