package threads.thor.work;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import tech.lp2p.core.Progress;
import tech.lp2p.utils.Utils;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.model.API;

public final class DownloadFileWorker extends Worker {

    private static final String SIZE = "size";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String URI = "uri";
    private static final String TAG = DownloadFileWorker.class.getSimpleName();

    /**
     * @noinspection WeakerAccess
     */
    public DownloadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri source, @NonNull String filename,
                                              @NonNull String mimeType, long size) {
        Data.Builder data = new Data.Builder();
        data.putString(NAME, filename);
        data.putString(TYPE, mimeType);
        data.putLong(SIZE, size);
        data.putString(URI, source.toString());

        return new OneTimeWorkRequest.Builder(DownloadFileWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri source,
                                @NonNull String filename, @NonNull String mimeType, long size) {
        WorkManager.getInstance(context).enqueue(getWork(source, filename, mimeType, size));
    }


    private static void downloadUrl(URL urlCon, OutputStream os,
                                    Progress progress, long size) throws IOException {
        HttpURLConnection.setFollowRedirects(false);

        HttpURLConnection huc = (HttpURLConnection) urlCon.openConnection();

        huc.setReadTimeout(30000); // 30 sec
        huc.connect();

        try (InputStream is = huc.getInputStream()) {
            Utils.copy(is, os, progress, size);
        }
    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... ");

        try {
            long size = getInputData().getLong(SIZE, 0);
            String name = getInputData().getString(NAME);
            Objects.requireNonNull(name);
            String mimeType = getInputData().getString(TYPE);
            Objects.requireNonNull(mimeType);

            String url = getInputData().getString(URI);
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);

            URL urlCon = new URL(uri.toString());

            setForegroundAsync(createForegroundInfo(name, 0));

            Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name,
                    Environment.DIRECTORY_DOWNLOADS);
            Objects.requireNonNull(downloads);
            ContentResolver contentResolver = getApplicationContext().getContentResolver();

            try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                Objects.requireNonNull(os, "Failed to open output stream.");
                downloadUrl(urlCon, os, progress -> setForegroundAsync(
                        createForegroundInfo(name, progress)), size);

            } catch (Throwable unused) {
                contentResolver.delete(downloads, null, null);
                return Result.success();
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }

    @NonNull
    private ForegroundInfo createForegroundInfo(@NonNull String name, int progress) {
        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);

        PendingIntent cancelPendingIntent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent intent = InitApplication.getDownloadsIntent();

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                cancelPendingIntent).build();

        builder.setContentTitle(name)
                .setSubText(progress + "%")
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true)
                .setOngoing(true);


        Notification notification = builder.build();
        int notificationId = getId().hashCode();
        return new ForegroundInfo(notificationId, notification,
                ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC);
    }
}
