package threads.thor.state;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.webkit.WebResourceResponse;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.utils.Utils;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.books.BOOKS;
import threads.thor.data.books.Bookmark;
import threads.thor.data.books.BookmarkDatabase;
import threads.thor.data.tabs.TABS;
import threads.thor.data.tabs.Tab;
import threads.thor.data.tabs.TabsDatabase;
import threads.thor.model.API;
import threads.thor.model.data.DATA;
import threads.thor.utils.MimeTypeService;

public class StateModel extends AndroidViewModel {
    private static final String TAG = StateModel.class.getSimpleName();
    private static final String TAB_KEY = "tabKey";
    @NonNull
    private final TabsDatabase tabsDatabase;
    @NonNull
    private final BookmarkDatabase bookmarkDatabase;
    @NonNull
    private final DATA data;
    @Nullable
    private final API api;
    @NonNull
    private final MutableLiveData<Uri> uri = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> permission = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> error = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> warning = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<Boolean> online = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<Bookmark> removed = new MutableLiveData<>(null);


    public StateModel(@NonNull Application application) {
        super(application);
        tabsDatabase = TABS.getInstance(application).tabsDatabase();
        bookmarkDatabase = BOOKS.getInstance(application).bookmarkDatabase();
        data = DATA.getInstance(getApplication());
        API apiTest = null;
        try {
            apiTest = API.getInstance(getApplication());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        this.api = apiTest;
    }

    @NonNull
    private static String getTitle(@NonNull String url, @Nullable String webViewTitle) {
        try {
            String title = webViewTitle;
            if (title == null) {
                title = Uri.parse(url).getAuthority();
            }
            Objects.requireNonNull(title);
            return title;
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return "";
    }

    @NonNull
    public Uri downloadMht(@NonNull Uri uri, @NonNull String filename)
            throws Exception {

        ContentResolver contentResolver = getApplication().getContentResolver();

        File file = new File(data.downloadsDir(), filename);
        try (InputStream is = contentResolver.openInputStream(uri)) {
            try (FileOutputStream os = new FileOutputStream(file)) {
                Utils.copy(is, os);
                return Uri.parse(API.FILE_SCHEME + "://" +
                        file.getAbsolutePath());
            }
        }
    }

    public LiveData<List<Bookmark>> bookmarks() {
        return bookmarkDatabase.bookmarkDao().getLiveDataBookmarks();
    }

    public LiveData<List<Tab>> tabs() {
        return tabsDatabase.tabDao().getLiveDataTabs();
    }

    public LiveData<List<Long>> idxs() {
        return tabsDatabase.tabDao().getLiveDataIdxs();
    }

    @NonNull
    public MutableLiveData<Uri> uri() {
        return uri;
    }

    public void uri(Uri uri) {
        uri().postValue(uri);
    }

    @NonNull
    public MutableLiveData<Boolean> online() {
        return online;
    }

    public void online(boolean online) {
        online().postValue(online);
    }

    public MutableLiveData<String> error() {
        return error;
    }

    public MutableLiveData<String> warning() {
        return warning;
    }

    public MutableLiveData<Bookmark> removed() {
        return removed;
    }

    public void removed(@Nullable Bookmark bookmark) {
        removed.postValue(bookmark);
    }

    public MutableLiveData<String> permission() {
        return permission;
    }

    public void permission(@Nullable String content) {
        permission().postValue(content);
    }

    public void error(@Nullable String content) {
        error().postValue(content);
    }

    public void warning(@Nullable String content) {
        warning().postValue(content);
    }

    @Nullable
    public Bookmark bookmark(@NonNull String url) {
        return bookmarkDatabase.bookmarkDao().getBookmark(url);
    }

    public boolean hasBookmark(@Nullable String url) {
        if (url != null && !url.isEmpty()) {
            return bookmark(url) != null;
        } else {
            return false;
        }
    }

    public void bookmark(@NonNull String url,
                         @Nullable String title,
                         @Nullable Bitmap bitmap) {
        try {
            Bookmark bookmark = bookmark(url);
            if (bookmark != null) {
                removeBookmark(bookmark);
            } else {
                bookmark = Bookmark.createBookmark(url,
                        getTitle(url, title), bitmap);
                bookmarkDatabase.bookmarkDao().insertBookmark(bookmark);

                String msg = bookmark.title();
                if (msg.isEmpty()) {
                    msg = url;
                }
                warning(super.getApplication().getString(R.string.bookmark_added, msg));
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void downloadMht(String value, String display) {

        if (value == null) {
            error(getApplication().getString(R.string.failure_download_file));
        } else {
            try {
                ContentResolver contentResolver = getApplication().getContentResolver();
                File file = new File(value);

                Uri downloads = API.downloadsUri(getApplication(),
                        MimeTypeService.MHT_MIME_TYPE, display,
                        Environment.DIRECTORY_DOWNLOADS);

                Objects.requireNonNull(downloads, "");
                try (FileInputStream is = new FileInputStream(file)) {
                    try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                        Objects.requireNonNull(os, "Failed to open output stream.");
                        Utils.copy(is, os);

                        warning(getApplication().getString(R.string.download_file) +
                                " \"" + display + "\"");
                    }
                } catch (Throwable throwable) {
                    contentResolver.delete(downloads, null, null);
                    throw throwable;
                } finally {
                    file.deleteOnExit();
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }

    public void removeBookmark(Bookmark bookmark) {
        bookmarkDatabase.bookmarkDao().removeBookmark(bookmark);
        removed(bookmark);
    }

    public void createTab(Bookmark bookmark) {
        Tab tab = Tab.createTab(bookmark.title(), bookmark.uri(),
                bookmark.icon());
        tabsDatabase.tabDao().insertTab(tab);
    }

    public void clearTabs() {
        tabsDatabase.clearAllTables();
    }

    public boolean hasNoTabs() {
        return !tabsDatabase.tabDao().hasTabs();
    }

    public void removeTab(@NonNull Tab tab) {
        tabsDatabase.tabDao().deleteTab(tab);
    }

    @Nullable
    public Tab tab(long idx) {
        return tabsDatabase.tabDao().getTab(idx);
    }

    public void updateTab(long tabIdx, @NonNull String title, @NonNull String uri) {
        tabsDatabase.tabDao().updateTab(tabIdx, title, uri);
    }

    public void updateTabIcon(long tabIdx, @Nullable Bitmap bitmap) {
        tabsDatabase.tabDao().updateTabIcon(tabIdx, API.bytes(bitmap));
    }

    public void createTab(@NonNull String title, @NonNull Uri uri, @Nullable Bitmap bitmap) {
        Tab tab = Tab.createTab(title, uri.toString(), API.bytes(bitmap));
        tabsDatabase.tabDao().insertTab(tab);
    }

    public void restoreBookmark(Bookmark bookmark) {
        bookmarkDatabase.bookmarkDao().insertBookmark(bookmark);
    }

    public WebResourceResponse response(@NonNull Uri uri, @NonNull Cancellable cancellable) {
        if (api == null) { // pns service not running
            return API.createErrorMessage(getApplication().getString(R.string.pns_service_error));
        }
        try {
            // when offline or no public ipv6 check if it is possible to load previous site
            PeerId peerId = Lite.decodePeerId(
                    Objects.requireNonNull(uri.getHost()));

            // check if local is available
            if (api.hasLocal(peerId)) {
                Peeraddr peeraddr = api.local(peerId);
                Connection connection = api.connect(peeraddr);
                Cid root = api.resolveName(connection, peerId);
                Uri redirectUri = API.redirectUri(connection, root,
                        uri);
                if (!Objects.equals(uri, redirectUri)) {
                    return API.createRedirectMessage(redirectUri);
                }
                return api.response(connection, root, redirectUri);
            }

            if (API.isNetworkConnected(getApplication())) {

                boolean hasConnection = api.hasConnection(peerId);
                if (!hasConnection) {
                    // check if any IPv6 addresses are available (site local and public)
                    if (Network.publicAddresses().isEmpty()) {
                        return API.createErrorMessage(
                                getApplication().getString(R.string.pns_service_limitation_ip));
                    }
                }
                if (!hasConnection) {
                    warning(getApplication().getString(R.string.connect_to, peerId.toString()));
                }
                Connection connection = api.connect(peerId, cancellable);
                Cid root = api.resolveName(connection, peerId);
                Uri redirectUri = API.redirectUri(connection, root,
                        uri);
                if (!Objects.equals(uri, redirectUri)) {
                    return API.createRedirectMessage(redirectUri);
                }
                return api.response(connection, root, redirectUri);
            } else {
                // when offline check if it is possible to load previous site
                Cid root = api.resolveName(peerId);
                if (root == null) {
                    return API.createErrorMessage(
                            getApplication().getString(R.string.no_offline_page));
                } else {
                    Uri redirectUri = api.redirectUri(root, uri);
                    if (!Objects.equals(uri, redirectUri)) {
                        return API.createRedirectMessage(redirectUri);
                    }
                    return api.response(root, redirectUri);
                }
            }
        } catch (Throwable throwable) {
            if (cancellable.isCancelled()) {
                return API.createEmptyResource();
            }
            return API.createErrorMessage(throwable);
        }
    }

    @NonNull
    public Uri homepage() {
        String uri = InitApplication.getHomepage(getApplication());
        if (uri == null) {
            return InitApplication.getEngine(getApplication()).uri();
        }
        return Uri.parse(uri);
    }

    public void tabIdx(long tabIdx) {
        SharedPreferences sharedPref = getApplication().getSharedPreferences(
                InitApplication.APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(TAB_KEY, tabIdx);
        editor.apply();
    }


    public long tabIdx() {
        SharedPreferences sharedPref = getApplication().getSharedPreferences(
                InitApplication.APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getLong(TAB_KEY, 0L);
    }
}
