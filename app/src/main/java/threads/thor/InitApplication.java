package threads.thor;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.ArrayMap;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

import tech.lp2p.core.PeerId;
import threads.thor.model.API;
import threads.thor.model.mdns.MDNS;
import threads.thor.utils.AdBlocker;

public class InitApplication extends Application {

    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    public static final String APP_KEY = "AppKey";
    @NonNull
    private static final ArrayMap<String, Engine> SEARCH_ENGINES = new ArrayMap<>();
    private static final String TAG = InitApplication.class.getSimpleName();
    private static final String JAVASCRIPT_KEY = "javascriptKey";
    private static final String HOMEPAGE_KEY = "homepageKey";
    private static final String SEARCH_ENGINE_KEY = "searchEngineKey";

    private static final Engine DUCKDUCKGO = new Engine("DuckDuckGo",
            Uri.parse("https://start.duckduckgo.com/"), "https://duckduckgo.com/?q=");
    private static final Engine GOOGLE = new Engine("Google",
            Uri.parse("https://www.google.com/"), "https://www.google.com/search?q=");
    private static final Engine ECOSIA = new Engine("Ecosia",
            Uri.parse("https://www.ecosia.org/"), "https://www.ecosia.org/search?q=");
    private static final Engine BING = new Engine("Bing",
            Uri.parse("https://www.bing.com/"), "https://www.bing.com/search?q=");

    private volatile MDNS mdns;


    public static void setHomepage(Context context, @Nullable String uri) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(HOMEPAGE_KEY, uri);
        editor.apply();
    }

    @Nullable
    public static String getHomepage(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(HOMEPAGE_KEY, null);
    }

    public static void setJavascriptEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(JAVASCRIPT_KEY, auto);
        editor.apply();
    }

    public static boolean isJavascriptEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(JAVASCRIPT_KEY, true);

    }

    public static String getSearchEngine(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(SEARCH_ENGINE_KEY, "DuckDuckGo");
    }

    public static void setSearchEngine(@NonNull Context context, @NonNull String searchEngine) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SEARCH_ENGINE_KEY, searchEngine);
        editor.apply();
    }

    @NonNull
    public static Intent getDownloadsIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(Uri.parse(
                Environment.getExternalStorageDirectory().getPath()), "*/*");
        return intent;
    }

    @SuppressLint("SetJavaScriptEnabled")
    public static void setWebSettings(@NonNull WebView webView, boolean enableJavascript) {


        WebSettings settings = webView.getSettings();
        settings.setUserAgentString("Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")");

        settings.setJavaScriptEnabled(enableJavascript);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);


        settings.setSafeBrowsingEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(true);    // set to true for mht files
        settings.setLoadsImagesAutomatically(true);
        settings.setBlockNetworkLoads(false);
        settings.setBlockNetworkImage(false);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDatabaseEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setMediaPlaybackRequiresUserGesture(false); // set to false, required for camera permission
        settings.setSupportMultipleWindows(false);
        settings.setGeolocationEnabled(false);
    }

    @NonNull
    public static ArrayMap<String, Engine> getSearchEngines() {
        return SEARCH_ENGINES;
    }

    @NonNull
    public static Engine getEngine(@NonNull Context context) {
        String searchEngine = InitApplication.getSearchEngine(context);
        Engine engine = SEARCH_ENGINES.get(searchEngine);
        if (engine == null) {
            return DUCKDUCKGO;
        }
        return engine;
    }

    private static void createStorageChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mdns != null) {
            mdns.stop();
            mdns = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        createStorageChannel(getApplicationContext());

        SEARCH_ENGINES.put(DUCKDUCKGO.name, DUCKDUCKGO);
        SEARCH_ENGINES.put(GOOGLE.name, GOOGLE);
        SEARCH_ENGINES.put(ECOSIA.name, ECOSIA);
        SEARCH_ENGINES.put(BING.name, BING);

        try {
            API api = API.getInstance(getApplicationContext());
            mdns = MDNS.mdns(getApplicationContext());
            mdns.startDiscovery(peer -> {
                try {
                    PeerId peerId = peer.peeraddr().peerId();
                    if (!Objects.equals(peerId, api.self())) {
                        api.local(peer.peeraddr());
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            if (mdns != null) {
                mdns.stop();
                mdns = null;
            }
            LogUtils.error(TAG, throwable);
        }

        AdBlocker.init(getApplicationContext());

    }

    public record Engine(String name, Uri uri, String query) {
    }
}