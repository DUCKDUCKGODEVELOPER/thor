package threads.thor.ui;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.Comparator;
import java.util.Objects;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.books.Bookmark;
import threads.thor.state.StateModel;
import threads.thor.utils.BookmarksAdapter;

public class BookmarksFragment extends DialogFragment {

    public static final String TAG = BookmarksFragment.class.getSimpleName();
    private static final String TAB = "tab";
    private WindowSizeClass widthWindowSizeClass = WindowSizeClass.COMPACT;

    public static BookmarksFragment newInstance(long tabIdx) {
        BookmarksFragment bookmarksFragment = new BookmarksFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(TAB, tabIdx);
        bookmarksFragment.setArguments(bundle);
        return bookmarksFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booksmark, container, false);

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        long tabIdx = args.getLong(TAB);


        RecyclerView bookmarks = view.findViewById(R.id.bookmarks);
        Objects.requireNonNull(bookmarks);
        bookmarks.setLayoutManager(new LinearLayoutManager(requireContext()));

        StateModel stateModel =
                new ViewModelProvider(requireActivity()).get(StateModel.class);

        BookmarksAdapter mBookmarksAdapter = new BookmarksAdapter(
                new BookmarksAdapter.BookmarkListener() {
                    @Override
                    public void onClick(@NonNull Bookmark bookmark) {
                        try {
                            if (tabIdx == 0L) {
                                stateModel.createTab(bookmark);
                            } else {
                                stateModel.uri(Uri.parse(bookmark.uri()));
                            }
                            Objects.requireNonNull(getDialog()).dismiss();
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }

                    @Override
                    public void onDismiss(@NonNull Bookmark bookmark) {
                        stateModel.removeBookmark(bookmark);
                    }
                });
        bookmarks.setAdapter(mBookmarksAdapter);


        stateModel.bookmarks().observe(getViewLifecycleOwner(), (marks -> {
            try {
                if (marks != null) {
                    marks.sort(Comparator.comparing(Bookmark::timestamp).reversed());
                    mBookmarksAdapter.updateData(marks);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));

        stateModel.removed().observe(this, (bookmark) -> {
            try {
                if (bookmark != null) {
                    Snackbar snackbar = Snackbar.make(view,
                            getString(R.string.bookmark_removed, bookmark.title()),
                            Snackbar.LENGTH_LONG);
                    snackbar.setAction(getString(R.string.revert), (view1) -> {
                        try {
                            stateModel.restoreBookmark(bookmark);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            snackbar.dismiss();
                        }

                    });
                    snackbar.show();
                    stateModel.removed(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        return view;
    }

    private void computeWindowSizeClasses() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;


        if (widthDp < 600f) {
            widthWindowSizeClass = WindowSizeClass.COMPACT;
        } else {
            widthWindowSizeClass = WindowSizeClass.MEDIUM;
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        computeWindowSizeClasses();
        if (widthWindowSizeClass == WindowSizeClass.MEDIUM) {
            return new SideSheetDialog(requireContext());
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
            BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
            return dialog;
        }
    }

    public enum WindowSizeClass {COMPACT, MEDIUM}
}
