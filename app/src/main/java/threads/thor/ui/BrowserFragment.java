package threads.thor.ui;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.provider.DocumentsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.JsPromptResult;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textview.MaterialTextView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import tech.lp2p.core.Cancellable;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.tabs.Tab;
import threads.thor.model.API;
import threads.thor.model.data.DATA;
import threads.thor.state.StateModel;
import threads.thor.utils.AdBlocker;
import threads.thor.utils.HistoryAdapter;
import threads.thor.utils.MimeTypeService;
import threads.thor.work.BrowserResetWorker;
import threads.thor.work.DownloadContentWorker;
import threads.thor.work.DownloadFileWorker;
import threads.thor.work.DownloadMagnetWorker;

public class BrowserFragment extends Fragment {
    private static final String TAG = BrowserFragment.class.getSimpleName();
    private static final String ABOUT_SCHEME = "about";
    @NonNull
    private final Set<Uri> uris = ConcurrentHashMap.newKeySet();
    private WebView webView;
    private ValueCallback<Uri[]> mUploadMessage;
    private final ActivityResultLauncher<Intent> mFileChooserForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {

                if (mUploadMessage != null) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        Uri[] uris = null;
                        if (intent != null) {
                            String dataString = intent.getDataString();

                            if (dataString != null) {
                                uris = new Uri[]{Uri.parse(dataString)};
                            }
                        }
                        mUploadMessage.onReceiveValue(uris);
                    } else {
                        mUploadMessage.onReceiveValue(null);
                    }
                    mUploadMessage = null;
                }
            });
    private StateModel stateModel;
    private PermissionRequest mPermissionRequest;

    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    mPermissionRequest.grant(mPermissionRequest.getResources());
                } else {
                    mPermissionRequest.deny();
                }
            });
    private SwipeRefreshLayout swipeRefreshLayout;
    private CircularProgressIndicator progressIndicator;
    private long lastClickTime;
    private WindowSizeClass widthWindowSizeClass = WindowSizeClass.COMPACT;
    private long tabItem;
    private CoordinatorLayout root;
    private MaterialTextView tabTitle;
    private MaterialButton tabIcon;
    private MaterialButton nextPage;
    private MaterialButton previousPage;
    private final ActivityResultLauncher<Intent> mFolderRequestForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent intent = result.getData();

                    try {
                        Objects.requireNonNull(intent);

                        LogUtils.error(TAG, "Intent " + intent);
                        LogUtils.error(TAG, "Type " + intent.getType());

                        Uri uri = intent.getData();
                        Objects.requireNonNull(uri);


                        String filename = API.getFileName(requireContext(), uri);

                        LogUtils.error(TAG, uri.toString());
                        LogUtils.error(TAG, filename);

                        boolean isMht = filename.endsWith(".mht"); // this is still a hack

                        if (isMht) {
                            openUri(stateModel.downloadMht(uri, filename));
                        } else {
                            Intent viewIntent = new Intent(Intent.ACTION_VIEW, uri);
                            viewIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(viewIntent);
                        }
                    } catch (Throwable e) {
                        stateModel.warning(getString(R.string.no_activity_found_to_handle_uri));
                    }
                }
            });
    private MaterialButton reloadPage;
    private MaterialButton tabs;


    private static void tearDown(@NonNull Dialog dialog) {
        try {
            Thread.sleep(150);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            dialog.dismiss();
        }
    }

    private void doSearch(TextView textView) {
        String origin;
        try {
            origin = textView.getText().toString();
            if (!origin.isBlank()) {
                Uri uri = Uri.parse(origin);
                Objects.requireNonNull(uri);
                String scheme = uri.getScheme();
                if (!Objects.equals(scheme, API.PNS_SCHEME) &&
                        !Objects.equals(scheme, API.HTTP_SCHEME) &&
                        !Objects.equals(scheme, API.HTTPS_SCHEME)) {

                    InitApplication.Engine engine =
                            InitApplication.getEngine(requireContext());

                    uri = Uri.parse(engine.query() + origin);
                }

                openUri(uri);
            }
        } catch (Throwable throwable) {
            stateModel.error(throwable.getMessage());
        }
    }

    private void searchView() {
        try {
            final View form = ((LayoutInflater)
                    requireActivity().getSystemService(LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.search_view, null);

            TextView input = form.findViewById(R.id.search);
            TextView numbers = form.findViewById(R.id.numbers);
            MaterialButton previous = form.findViewById(R.id.action_previous);
            previous.setOnClickListener(v -> {
                try {
                    webView.findNext(false);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });
            MaterialButton next = form.findViewById(R.id.action_next);
            next.setOnClickListener(v -> {
                try {
                    webView.findNext(true);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });

            MaterialButton close = form.findViewById(R.id.search_close);
            input.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    webView.findAllAsync(editable.toString());
                }
            });

            numbers.setText("0/0");

            webView.setFindListener((activeMatchOrdinal, numberOfMatches, isDoneCounting) -> {
                try {
                    String result = activeMatchOrdinal + "/" + numberOfMatches;
                    numbers.setText(result);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });


            MaterialAlertDialogBuilder enterSearchViewDialog = new MaterialAlertDialogBuilder(
                    requireContext(), R.style.AppTheme)
                    .setBackgroundInsetStart(0)
                    .setBackgroundInsetEnd(0)
                    .setBackgroundInsetTop(0)
                    .setBackgroundInsetBottom(0)
                    .setView(form);

            AlertDialog dialog = enterSearchViewDialog.create();
            dialog.setOnDismissListener(dialog1 -> {
                webView.clearMatches();
                webView.setFindListener(null);
            });
            dialog.setCanceledOnTouchOutside(false);

            close.setOnClickListener(v -> dialog.cancel());


            Window window = dialog.getWindow();
            Objects.requireNonNull(window);
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.TOP;
            window.setAttributes(lp);

            dialog.show();

            input.setOnEditorActionListener((v, actionId, event) -> {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    try {
                        InputMethodManager imm = (InputMethodManager)
                                requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    handled = true;
                }
                return handled;
            });

            input.requestFocus();


        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    private void searchWeb() {
        try {
            final View form = ((LayoutInflater)
                    requireActivity().getSystemService(LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.search_web, null);

            TextView input = form.findViewById(R.id.search_term);
            MaterialButton close = form.findViewById(R.id.search_term_close);

            MaterialAlertDialogBuilder enterUriDialog = new MaterialAlertDialogBuilder(
                    requireContext(), R.style.AppTheme)
                    .setBackgroundInsetStart(0)
                    .setBackgroundInsetEnd(0)
                    .setBackgroundInsetTop(0)
                    .setBackgroundInsetBottom(0)
                    .setView(form);


            AlertDialog dialog = enterUriDialog.create();

            close.setOnClickListener(v -> dialog.cancel());

            Window window = dialog.getWindow();
            Objects.requireNonNull(window);
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.TOP;

            window.setAttributes(lp);

            dialog.show();

            input.setOnEditorActionListener((v, actionId, event) -> {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    doSearch(v);
                    dialog.dismiss();
                    handled = true;
                }
                return handled;
            });
            input.requestFocus();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private int numUris() {
        return uris.size();
    }

    private void detachUri(@NonNull Uri uri) {
        uris.remove(uri);
    }

    private void attachUri(@NonNull Uri uri) {
        uris.add(uri);
    }

    private void computeWindowSizeClasses() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;


        if (widthDp < 600f) {
            widthWindowSizeClass = WindowSizeClass.COMPACT;
        } else {
            widthWindowSizeClass = WindowSizeClass.MEDIUM;
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_browser, container, false);
    }

    public void updateTitle(String title, String url) {
        try {
            this.tabTitle.setText(title);
            stateModel.updateTab(tabItem, title, url);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void updateFavicon(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), API.FILE_SCHEME)) {
                tabIcon.setIconResource(R.drawable.mht); // it can only be mht right now
                stateModel.updateTabIcon(tabItem, null);
            } else if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {
                tabIcon.setIconResource(R.drawable.pns);
                stateModel.updateTabIcon(tabItem, null);
            } else {
                tabIcon.setIconResource(R.drawable.https);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void showDocumentation() {
        try {
            String uri = "https://gitlab.com/lp2p/thor";

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri),
                    requireContext(), MainActivity.class);
            startActivity(intent);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showSettings() {
        try {
            Dialog settingsDialog = getDialog();

            settingsDialog.setContentView(R.layout.fragment_settings);


            SwitchMaterial enableJavascript = settingsDialog.findViewById(
                    R.id.enable_javascript);
            Objects.requireNonNull(enableJavascript);
            enableJavascript.setChecked(InitApplication.isJavascriptEnabled(requireContext()));
            enableJavascript.setOnCheckedChangeListener((buttonView, isChecked) ->
                    InitApplication.setJavascriptEnabled(requireContext(), isChecked)
            );

            ArrayMap<String, InitApplication.Engine> searchEngines = InitApplication.getSearchEngines();

            MaterialAutoCompleteTextView search_engine_menu =
                    settingsDialog.findViewById(R.id.search_engine_menu);


            //noinspection SimplifyStreamApiCallChains
            String[] items = searchEngines.keySet().stream().toArray(String[]::new);
            search_engine_menu.setSimpleItems(items);

            String searchEngine = InitApplication.getSearchEngine(requireContext());
            int pos = searchEngines.indexOfKey(searchEngine);

            search_engine_menu.setListSelection(pos);
            search_engine_menu.setText(searchEngine, false);


            search_engine_menu.setOnItemClickListener((parent, view, position, id) -> {
                try {
                    InitApplication.setSearchEngine(requireContext(),
                            searchEngines.keyAt(position));

                    search_engine_menu.dismissDropDown();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });


            settingsDialog.show();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void createPrintJob() {
        PrintManager printManager = (PrintManager) requireContext()
                .getSystemService(Context.PRINT_SERVICE);
        String jobName = getString(R.string.app_name);
        PrintDocumentAdapter printDocumentAdapter = webView.createPrintDocumentAdapter(jobName);
        printManager.print(jobName, printDocumentAdapter,
                new PrintAttributes.Builder().build());
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            computeWindowSizeClasses();
            visibilityActions();
            updateActions();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void visibilityActions() {
        boolean expandedScreen = widthWindowSizeClass == WindowSizeClass.MEDIUM;

        if (expandedScreen) {
            previousPage.setVisibility(View.VISIBLE);
        } else {
            previousPage.setVisibility(View.GONE);
        }

        if (expandedScreen) {
            nextPage.setVisibility(View.VISIBLE);
        } else {
            nextPage.setVisibility(View.GONE);
        }

        if (expandedScreen) {
            reloadPage.setVisibility(View.VISIBLE);
        } else {
            reloadPage.setVisibility(View.GONE);
        }
    }

    private void updateActions() {
        previousPage.setEnabled(webView.canGoBack());
        nextPage.setEnabled(webView.canGoForward());
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
    }


    public boolean onBackPressedCheck() {
        if (webView.canGoBack()) {
            previousPage();
            return true;
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    private void clearWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.clearFormData();

    }


    private void contentDownloader(@NonNull Uri uri) {

        if (!isResumed()) {
            return;
        }

        NotificationManager notificationManager = (NotificationManager)
                requireContext().getSystemService(NOTIFICATION_SERVICE);

        if (!notificationManager.areNotificationsEnabled()) {
            stateModel.permission(getString(R.string.notification_permission_required));
            return;
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download);
        builder.setMessage(API.getUriTitle(uri));

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {
            DownloadContentWorker.download(requireContext(), uri);

            progressIndicator.setVisibility(View.INVISIBLE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.INVISIBLE);
                    dialog.cancel();
                });
        builder.show();


    }

    private void magnetDownloader(@NonNull Uri uri) {

        if (!isResumed()) {
            return;
        }

        NotificationManager notificationManager = (NotificationManager)
                requireContext().getSystemService(NOTIFICATION_SERVICE);

        if (!notificationManager.areNotificationsEnabled()) {
            stateModel.permission(getString(R.string.notification_permission_required));
            return;
        }

        MagnetUri magnetUri = MagnetUriParser.parse(uri.toString());

        String name = uri.toString();
        if (magnetUri.getDisplayName().isPresent()) {
            name = magnetUri.getDisplayName().get();
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download);
        builder.setMessage(name);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadMagnetWorker.download(requireContext(), uri);

            progressIndicator.setVisibility(View.INVISIBLE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.INVISIBLE);
                    dialog.cancel();
                });
        builder.show();

    }

    private void fileDownloader(@NonNull Uri uri, @NonNull String filename,
                                @NonNull String mimeType, long size) {

        if (!isResumed()) {
            return;
        }
        NotificationManager notificationManager = (NotificationManager)
                requireContext().getSystemService(NOTIFICATION_SERVICE);

        if (!notificationManager.areNotificationsEnabled()) {
            stateModel.permission(getString(R.string.notification_permission_required));
            return;
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadFileWorker.download(requireContext(), uri, filename, mimeType, size);

            progressIndicator.setVisibility(View.INVISIBLE);

        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.INVISIBLE);
                    dialog.cancel();
                });
        builder.show();
    }

    private void reloadPage() {

        try {
            progressIndicator.setVisibility(View.INVISIBLE);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            API.cleanupResolver(Uri.parse(webView.getUrl()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            webView.reload();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void openUri(@NonNull Uri uri) {
        try {
            progressIndicator.setVisibility(View.VISIBLE);

            updateFavicon(uri);

            if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {
                API.cleanupResolver(uri);
                attachUri(uri);
            }
            webView.stopLoading();
            LogUtils.error(TAG, "Load " + uri);
            webView.loadUrl(uri.toString());
            updateActions();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void download() {
        try {
            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {
                    contentDownloader(uri);
                } else if (Objects.equals(uri.getScheme(), API.HTTP_SCHEME) ||
                        Objects.equals(uri.getScheme(), API.HTTPS_SCHEME)) {
                    try {
                        String name = webView.getTitle();
                        if (name == null || name.isEmpty()) {
                            name = "unknown";
                        }
                        name = name.replace("\n", "")
                                .replace("\r", "")
                                .replace(".", "");
                        String display = name.concat(".mht");


                        DATA data = DATA.getInstance(requireContext());
                        webView.saveWebArchive(data.downloadsDir().getAbsolutePath(),
                                true, value -> stateModel.downloadMht(value, display));

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void share() {
        try {
            Uri uri = Uri.parse(webView.getUrl());

            ComponentName[] names = {new ComponentName(requireContext(), MainActivity.class)};

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
            intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
            intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


            Intent chooser = Intent.createChooser(intent, getText(R.string.share));
            chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
            startActivity(chooser);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void previousPage() {
        try {
            webView.stopLoading();
            webView.goBack();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void nextPage() {
        try {
            webView.stopLoading();
            webView.goForward();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private Dialog getDialog() {
        if (widthWindowSizeClass == WindowSizeClass.MEDIUM) {
            return new SideSheetDialog(requireContext());
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
            BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
            return dialog;
        }
    }

    private void history() {
        try {
            Dialog historyDialog = getDialog();
            historyDialog.setContentView(R.layout.fragment_history);

            RecyclerView history = historyDialog.findViewById(R.id.history);
            Objects.requireNonNull(history);

            LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
            layoutManager.setReverseLayout(true);
            history.setLayoutManager(layoutManager);
            HistoryAdapter mHistoryAdapter = new HistoryAdapter(item -> {

                try {
                    openUri(Uri.parse(item.getUrl()));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    historyDialog.dismiss();
                }
            }, webView.copyBackForwardList());
            history.setAdapter(mHistoryAdapter);
            historyDialog.show();

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        tabItem = args.getLong(API.TAB);
        stateModel = new ViewModelProvider(requireActivity()).get(StateModel.class);

        computeWindowSizeClasses();

        AtomicBoolean pageFinished = new AtomicBoolean();
        root = view.findViewById(R.id.browser_root);
        webView = view.findViewById(R.id.web_view);
        tabIcon = view.findViewById(R.id.tab_icon);
        tabTitle = view.findViewById(R.id.tab_title);

        AppBarLayout actionMenuBar = view.findViewById(R.id.action_menu_bar);


        MaterialButton home = view.findViewById(R.id.action_home);
        home.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            try {
                openUri(stateModel.homepage());
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        MaterialCardView site = view.findViewById(R.id.action_site);
        site.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            searchWeb();
        });

        tabIcon.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            InfoFragment.newInstance(
                            webView.getUrl(), webView.getTitle(), webView.getFavicon())
                    .show(getParentFragmentManager(), InfoFragment.TAG);

        });

        tabs = view.findViewById(R.id.action_tabs);
        tabs.setOnClickListener(v -> ((MainActivity) requireActivity()).openTabsDialog());


        // back page
        previousPage = view.findViewById(R.id.action_previous_page);
        previousPage.setOnClickListener(v -> previousPage());


        // next page
        nextPage = view.findViewById(R.id.action_next_page);
        nextPage.setOnClickListener(v -> nextPage());


        // reload page
        reloadPage = view.findViewById(R.id.action_reload_page);
        reloadPage.setOnClickListener(v -> reloadPage());


        MaterialButton bookmarks = view.findViewById(R.id.action_bookmarks);
        bookmarks.setOnClickListener(v -> {
            try {
                BookmarksFragment.newInstance(tabItem)
                        .show(getParentFragmentManager(), BookmarksFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        MaterialButton actionOverflow = view.findViewById(R.id.action_overflow);
        actionOverflow.setOnClickListener(v -> {
            try {
                LayoutInflater inflater = (LayoutInflater)
                        requireContext().getSystemService(LAYOUT_INFLATER_SERVICE);


                View menuOverflow = inflater.inflate(
                        R.layout.menu_overflow, root, false);

                SideSheetDialog dialog = new SideSheetDialog(requireContext());
                dialog.setContentView(menuOverflow);
                dialog.show();


                MaterialButton actionNextPage = menuOverflow.findViewById(R.id.action_next_page);

                if (widthWindowSizeClass == WindowSizeClass.MEDIUM) {
                    actionNextPage.setVisibility(View.GONE);
                } else {
                    actionNextPage.setVisibility(View.VISIBLE);
                }


                actionNextPage.setEnabled(webView.canGoForward());

                actionNextPage.setOnClickListener(v1 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        nextPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                MaterialButton actionBookmark = menuOverflow.findViewById(R.id.action_bookmark);
                actionBookmark.setEnabled(pageFinished.get());
                boolean hasBookmark = stateModel.hasBookmark(webView.getUrl());
                if (hasBookmark) {
                    actionBookmark.setIconResource(R.drawable.star);
                } else {
                    actionBookmark.setIconResource(R.drawable.star_outline);
                }

                actionBookmark.setOnClickListener(v1 -> {

                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        stateModel.bookmark(Objects.requireNonNull(webView.getUrl()),
                                webView.getTitle(),
                                webView.getFavicon());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                Button actionFindPage = menuOverflow.findViewById(R.id.action_find_page);

                actionFindPage.setOnClickListener(v12 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        searchView();
                        //findPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionDownload = menuOverflow.findViewById(R.id.action_download);

                actionDownload.setEnabled(API.downloadActive(webView.getUrl()));

                actionDownload.setOnClickListener(v13 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        download();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionShare = menuOverflow.findViewById(R.id.action_share);
                actionShare.setEnabled(pageFinished.get());
                actionShare.setOnClickListener(v14 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        share();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionInformation = menuOverflow.findViewById(R.id.action_information);

                actionInformation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        InfoFragment.newInstance(
                                        webView.getUrl(), webView.getTitle(), webView.getFavicon())
                                .show(getParentFragmentManager(), InfoFragment.TAG);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionHistory = menuOverflow.findViewById(R.id.action_history);
                actionHistory.setOnClickListener(v16 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        history();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionEditUri = menuOverflow.findViewById(R.id.action_edit_uri);
                actionEditUri.setOnClickListener(v15 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        searchWeb();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });


                TextView actionPrint = menuOverflow.findViewById(R.id.action_print);
                actionPrint.setOnClickListener(v17 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        try {
                            createPrintJob();
                        } catch (Throwable e) {
                            stateModel.warning(
                                    getString(R.string.no_activity_found_to_handle_uri));
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionDownloads = menuOverflow.findViewById(R.id.action_downloads);
                actionDownloads.setOnClickListener(v17 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        try {
                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("*/*");
                            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, false);
                            intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, Uri.parse(
                                    Environment.getExternalStorageDirectory().getPath()));

                            mFolderRequestForResult.launch(intent);
                        } catch (Throwable throwable) {
                            stateModel.warning(
                                    getString(R.string.no_activity_found_to_handle_uri));
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionCleanup = menuOverflow.findViewById(R.id.action_cleanup);
                actionCleanup.setOnClickListener(v18 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        try {
                            // clear web view data
                            clearWebView();

                            // Clear data and cookies
                            BrowserResetWorker.reset(requireContext());
                        } finally {
                            stateModel.warning(getString(R.string.browser_cleanup_message));
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionSettings = menuOverflow.findViewById(R.id.action_settings);
                actionSettings.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showSettings();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionDocumentation = menuOverflow.findViewById(R.id.action_documentation);
                actionDocumentation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showDocumentation();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        progressIndicator = view.findViewById(R.id.progress_circle);
        progressIndicator.setVisibility(View.INVISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swipe_container);

        actionMenuBar.addOnOffsetChangedListener((appBarLayout, i) ->
                swipeRefreshLayout.setEnabled(i == 0));

        MaterialTextView offline_mode = view.findViewById(R.id.offline_mode);


        InitApplication.setWebSettings(webView,
                InitApplication.isJavascriptEnabled(requireContext()));

        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, false);


        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                swipeRefreshLayout.setRefreshing(true);
                reloadPage();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                swipeRefreshLayout.setRefreshing(false);
            }
        });


        CustomWebChromeClient mCustomWebChromeClient =
                new CustomWebChromeClient(requireActivity(), this);
        webView.setWebChromeClient(mCustomWebChromeClient);


        webView.setDownloadListener((url, userAgent, contentDisposition, mimeType, contentLength) -> {

            try {
                Uri uri = Uri.parse(url);
                String filename = API.parseContentDisposition(contentDisposition);
                if (filename == null || filename.isEmpty()) {
                    filename = URLUtil.guessFileName(url, contentDisposition, mimeType);
                }

                if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {
                    String res = uri.getQueryParameter("download");
                    if (Objects.equals(res, "0")) {
                        try {
                            stateModel.warning(getString(R.string.browser_handle_file, filename));
                        } finally {
                            progressIndicator.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        contentDownloader(uri);
                    }
                } else {
                    fileDownloader(uri, filename, mimeType, contentLength);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        stateModel.online().observe(getViewLifecycleOwner(), online -> {
            try {
                if (online != null) {
                    webView.setNetworkAvailable(online);

                    if (online) {
                        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
                    } else {
                        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ONLY);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        stateModel.tabs().observe(getViewLifecycleOwner(), tabs -> {
            if (tabs != null) {
                int size = tabs.size();
                int resId = R.drawable.numeric_9_plus_box_multiple_outline;
                if (size < 10) {
                    switch (size) {
                        case 0 -> resId = R.drawable.numeric_0_box_multiple_outline;
                        case 1 -> resId = R.drawable.numeric_1_box_multiple_outline;
                        case 2 -> resId = R.drawable.numeric_2_box_multiple_outline;
                        case 3 -> resId = R.drawable.numeric_3_box_multiple_outline;
                        case 4 -> resId = R.drawable.numeric_4_box_multiple_outline;
                        case 5 -> resId = R.drawable.numeric_5_box_multiple_outline;
                        case 6 -> resId = R.drawable.numeric_6_box_multiple_outline;
                        case 7 -> resId = R.drawable.numeric_7_box_multiple_outline;
                        case 8 -> resId = R.drawable.numeric_8_box_multiple_outline;
                        case 9 -> resId = R.drawable.numeric_9_box_multiple_outline;
                    }
                }
                this.tabs.setIconResource(resId);
            }
        });

        stateModel.online().observe(getViewLifecycleOwner(), (online) -> {
            try {
                if (online != null) {
                    if (online) {
                        offline_mode.setVisibility(View.GONE);
                    } else {
                        offline_mode.setVisibility(View.VISIBLE);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        webView.setWebViewClient(new WebViewClient() {

            private final Map<Uri, Boolean> loadedUrls = new HashMap<>();
            private final AtomicReference<String> host = new AtomicReference<>();


            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request,
                                            WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                LogUtils.error(TAG, "onReceivedHttpError " + errorResponse.getReasonPhrase());
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                LogUtils.info(TAG, "onReceivedSslError " + error.toString());
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                LogUtils.info(TAG, "onPageCommitVisible " + url);
                progressIndicator.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {

                try {

                    WebViewDatabase database = WebViewDatabase.getInstance(requireContext());
                    String[] data = database.getHttpAuthUsernamePassword(host, realm);


                    String storedName = null;
                    String storedPass = null;

                    if (data != null) {
                        storedName = data[0];
                        storedPass = data[1];
                    }

                    LayoutInflater inflater = (LayoutInflater)
                            requireActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                    final View form = inflater.inflate(R.layout.http_auth_request, null);


                    final EditText usernameInput = form.findViewById(R.id.user_name);
                    final EditText passwordInput = form.findViewById(R.id.password);

                    if (storedName != null) {
                        usernameInput.setText(storedName);
                    }

                    if (storedPass != null) {
                        passwordInput.setText(storedPass);
                    }

                    MaterialAlertDialogBuilder authDialog = new MaterialAlertDialogBuilder(
                            requireContext())
                            .setTitle(R.string.authentication)
                            .setView(form)
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> {

                                String username = usernameInput.getText().toString();
                                String password = passwordInput.getText().toString();

                                database.setHttpAuthUsernamePassword(host, realm, username, password);

                                handler.proceed(username, password);
                                dialog.dismiss();
                            })

                            .setNegativeButton(android.R.string.cancel, (dialog, whichButton) -> {
                                dialog.dismiss();
                                view.stopLoading();
                                handler.cancel();
                            });


                    authDialog.show();
                    return;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                super.onReceivedHttpAuthRequest(view, handler, host, realm);
            }


            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                LogUtils.info(TAG, "onLoadResource : " + url);
            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
                super.doUpdateVisitedHistory(view, url, isReload);
                LogUtils.info(TAG, "doUpdateVisitedHistory : " + url + " " + isReload);
            }

            @Override
            public void onPageStarted(WebView view, String uri, Bitmap favicon) {
                LogUtils.info(TAG, "onPageStarted : " + uri);
                updateFavicon(Uri.parse(uri));
                progressIndicator.setVisibility(View.VISIBLE);
                pageFinished.set(false);
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                LogUtils.info(TAG, "onPageFinished : " + url);

                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {

                    try {
                        if (numUris() == 0) {
                            progressIndicator.setVisibility(View.INVISIBLE);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                } else {
                    progressIndicator.setVisibility(View.INVISIBLE);
                }

                // deactivate when download when uri is file otherwise download possible
                pageFinished.set(!Objects.equals(uri.getScheme(), API.FILE_SCHEME));
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                LogUtils.error(TAG, "onReceivedError " + view.getUrl() + " " +
                        error.getDescription() + " " + error.getErrorCode());
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                try {
                    Uri uri = request.getUrl();

                    LogUtils.error(TAG, "shouldOverrideUrlLoading : " + uri);

                    if (Objects.equals(uri.getScheme(), ABOUT_SCHEME)) {
                        LogUtils.error(TAG, "about scheme " + uri);
                        return true;
                    } else if (Objects.equals(uri.getScheme(), API.MAGNET_SCHEME)) {
                        magnetDownloader(uri);
                        return true;
                    } else if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {

                        String res = uri.getQueryParameter("download");
                        if (Objects.equals(res, "1")) {
                            contentDownloader(uri);
                            return true;
                        }

                        progressIndicator.setVisibility(View.VISIBLE);
                        return false;
                    }
                    if (Objects.equals(uri.getScheme(), API.HTTP_SCHEME) ||
                            Objects.equals(uri.getScheme(), API.HTTPS_SCHEME)) {
                        return false;
                    } else {
                        // all other stuff
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } catch (Throwable ignore) {
                            LogUtils.error(TAG, "Not  handled uri " + uri);
                        }
                        return true;
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                return false;

            }


            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                Uri uri = request.getUrl();

                LogUtils.error(TAG, "shouldInterceptRequest : " + uri.toString());

                host.set(uri.getHost());
                if (Objects.equals(uri.getScheme(), API.HTTP_SCHEME) ||
                        Objects.equals(uri.getScheme(), API.HTTPS_SCHEME)) {
                    boolean advertisement = false;
                    if (!loadedUrls.containsKey(uri)) {
                        advertisement = AdBlocker.isAd(uri);
                        loadedUrls.put(uri, advertisement);
                    } else {
                        Boolean value = loadedUrls.get(uri);
                        if (value != null) {
                            advertisement = value;
                        }
                    }

                    if (advertisement) {
                        try {
                            return API.createEmptyResource();
                        } catch (Throwable throwable) {
                            return null;
                        }
                    } else {
                        return null;
                    }

                } else if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {

                    try {

                        attachUri(uri);

                        Cancellable cancellable = () -> Thread.currentThread().isInterrupted();

                        try {
                            return stateModel.response(uri, cancellable);

                        } catch (Throwable throwable) {
                            if (cancellable.isCancelled()) {
                                return API.createEmptyResource();
                            }
                            return API.createErrorMessage(throwable);
                        } finally {
                            detachUri(uri);
                        }
                    } catch (Throwable throwable) {
                        return API.createErrorMessage(throwable);
                    }
                }
                return null;
            }
        });

        visibilityActions(); // check of actions are visible

        try {

            Tab tab = stateModel.tab(tabItem);
            Objects.requireNonNull(tab);
            String url = tab.uri();
            Objects.requireNonNull(url);

            updateTitle(tab.title(), url);

            Uri uri = Uri.parse(url);
            openUri(uri);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState);
        }
    }

    public enum WindowSizeClass {COMPACT, MEDIUM}


    private class CustomWebChromeClient extends WebChromeClient {

        private static final String TAG = CustomWebChromeClient.class.getSimpleName();
        @NonNull
        private final Activity activity;
        private final BrowserFragment browserFragment;
        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;

        public CustomWebChromeClient(@NonNull Activity activity,
                                     @NonNull BrowserFragment browserFragment) {
            this.activity = activity;
            this.browserFragment = browserFragment;
        }

        private static String permission(PermissionRequest request) {
            LogUtils.error(TAG, request.getOrigin().toString());
            String[] resources = request.getResources();
            if (resources == null || resources.length == 0) {
                return "";
            }


            String resource = resources[0];
            return switch (resource) {
                case "android.webkit.resource.AUDIO_CAPTURE" -> Manifest.permission.RECORD_AUDIO;
                case "android.webkit.resource.VIDEO_CAPTURE" -> Manifest.permission.CAMERA;
                default -> "";
            };
        }

        @Override
        public void onHideCustomView() {
            ((FrameLayout) activity.getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            showSystemUI();
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, android.os.Message resultMsg) {
            WebView.HitTestResult result = view.getHitTestResult();
            String data = result.getExtra();
            Context context = view.getContext();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data), context, MainActivity.class);
            context.startActivity(browserIntent);
            return false;
        }

        @Override
        public void onRequestFocus(WebView view) {
            LogUtils.error(TAG, "onRequestFocus " + view.getUrl());
        }

        @Override
        public void onCloseWindow(WebView window) {
            LogUtils.error(TAG, "onCloseWindow ");
        }

        @Override
        public boolean onJsPrompt(WebView view, String url, String message,
                                  String defaultValue, JsPromptResult result) {
            LogUtils.error(TAG, "onJsPrompt ");
            return false;
        }

        @Override
        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
            this.mCustomView = paramView;
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout) activity.getWindow().getDecorView())
                    .addView(this.mCustomView, new FrameLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            hideSystemUI();

        }

        private void hideSystemUI() {
            WindowCompat.setDecorFitsSystemWindows(activity.getWindow(), false);
            WindowInsetsControllerCompat controller = new WindowInsetsControllerCompat(
                    activity.getWindow(), this.mCustomView);
            controller.hide(WindowInsetsCompat.Type.systemBars());
            controller.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
        }

        private void showSystemUI() {
            WindowCompat.setDecorFitsSystemWindows(activity.getWindow(), true);
            new WindowInsetsControllerCompat(activity.getWindow(), activity.getWindow().getDecorView())
                    .show(WindowInsetsCompat.Type.systemBars());
        }

        @Override
        public Bitmap getDefaultVideoPoster() {
            return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            LogUtils.error(TAG, "onReceivedTitle " + title + " " + view.getUrl());
            browserFragment.updateTitle(title, view.getUrl());
        }


        @Override
        public void onReceivedTouchIconUrl(WebView view, String url,
                                           boolean precomposed) {
            LogUtils.error(TAG, "onReceivedTouchIconUrl");
        }

        @Override
        public void onReceivedIcon(WebView view, Bitmap icon) {
            LogUtils.error(TAG, "onReceivedIcon " + view.getUrl());

            stateModel.updateTabIcon(tabItem, icon);
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback,
                                         FileChooserParams fileChooserParams) {

            if (mUploadMessage != null) {
                mUploadMessage.onReceiveValue(null);
            }
            mUploadMessage = filePathCallback;

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");

            mFileChooserForResult.launch(intent);

            return true;
        }

        @Override
        public void onPermissionRequest(PermissionRequest request) {

            String permission = permission(request);
            if (permission.isEmpty()) {
                request.deny();
                return;
            }

            if (Objects.equals(Manifest.permission.CAMERA, permission)) {
                if (!activity.getPackageManager().hasSystemFeature(
                        PackageManager.FEATURE_CAMERA_ANY)) {
                    request.deny();
                    return;
                }
            }

            mPermissionRequest = request;
            requestPermissionLauncher.launch(permission);
        }

        @Override
        public void onPermissionRequestCanceled(PermissionRequest request) {
            super.onPermissionRequestCanceled(request);
            LogUtils.error(TAG, "onPermissionRequestCanceled " + request.toString());
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            LogUtils.error(TAG, "onConsoleMessage " + consoleMessage.message());
            return super.onConsoleMessage(consoleMessage);
        }

        @Override
        public View getVideoLoadingProgressView() {
            LogUtils.error(TAG, "getVideoLoadingProgressView ");
            return null;
        }

        @Override
        public void getVisitedHistory(ValueCallback<String[]> callback) {
            LogUtils.error(TAG, "getVisitedHistory ");
        }
    }
}
