package threads.thor.ui;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import tech.lp2p.Lite;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.tabs.Tab;
import threads.thor.model.API;
import threads.thor.state.StateModel;
import threads.thor.utils.LongDiffCallback;
import threads.thor.utils.MimeTypeService;
import threads.thor.utils.TabsAdapter;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private final ActivityResultLauncher<String> mPermissionNotificationResult = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            result -> {
            });
    private StateModel stateModel;
    private ViewPager2 browserPager;
    private ViewPagerFragmentAdapter browserAdapter;
    private Dialog tabsDialog;

    private void notificationRequest() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    android.Manifest.permission.POST_NOTIFICATIONS)
                    != PackageManager.PERMISSION_GRANTED) {
                mPermissionNotificationResult.launch(Manifest.permission.POST_NOTIFICATIONS);
            }
        }
    }


    private void openTab(@NonNull Uri uri) {
        LogUtils.error(TAG, "openTab " + uri);
        try {
            stateModel.createTab(API.getUriTitle(uri), uri, null);
            closeTabsDialog();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void updateTab(@NonNull Uri uri) {

        long tabIdx = stateModel.tabIdx();
        LogUtils.error(TAG, "updateTab " + tabIdx + " " + uri);
        try {
            if (stateModel.hasNoTabs() || tabIdx == 0L) {
                openTab(uri);
            } else {
                stateModel.updateTab(tabIdx, API.getUriTitle(uri), uri.toString());
                closeTabsDialog();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    void openTabsDialog() {

        tabsDialog = new SideSheetDialog(this);
        tabsDialog.setContentView(R.layout.fragment_tabs);

        RecyclerView recyclerView = tabsDialog.findViewById(R.id.tabs_recycler_view);
        Objects.requireNonNull(recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(linearLayoutManager);

        TabsAdapter tabsAdapter = new TabsAdapter(new TabsAdapter.ViewHolderListener() {
            @Override
            public void onItemClicked(int position) {
                try {
                    browserPager.setCurrentItem(position, true);
                    closeTabsDialog();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void onItemDismiss(Tab tab) {
                stateModel.removeTab(tab);
            }

            @Override
            public boolean isSelected(int position) {
                return browserPager.getCurrentItem() == position;
            }
        });

        recyclerView.setAdapter(tabsAdapter);


        stateModel.tabs().observe(this, tabs -> {
            if (tabs != null) {
                tabsAdapter.updateData(tabs);
                int position = browserPager.getCurrentItem();
                linearLayoutManager.scrollToPosition(position);
            }
        });


        MaterialToolbar gridToolbar = tabsDialog.findViewById(R.id.tabs_toolbar);
        Objects.requireNonNull(gridToolbar);
        gridToolbar.getMenu().findItem(R.id.add_tab).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.add_tab) {
                try {
                    openTab(stateModel.homepage());
                    return true;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
            return false;
        });

        gridToolbar.getMenu().findItem(R.id.close_tabs).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.close_tabs) {
                stateModel.clearTabs();
                return true;
            }
            return false;
        });

        tabsDialog.setOnDismissListener(dialog -> {
            if (stateModel.hasNoTabs()) {
                openTab(stateModel.homepage());
            }
        });

        tabsDialog.show();
    }

    private void closeTabsDialog() {
        if (tabsDialog != null) {
            tabsDialog.dismiss();
            tabsDialog = null;
        }
    }

    @Nullable
    private BrowserFragment getBrowserFragment() {
        long tabItem = browserAdapter.getItemId(browserPager.getCurrentItem());
        if (tabItem > 0L) {
            Optional<Fragment> result = getSupportFragmentManager().getFragments()
                    .stream()
                    .filter(it -> it instanceof BrowserFragment && it.getArguments() != null
                            && it.getArguments().getLong(API.TAB) == tabItem)
                    .findFirst();
            return (BrowserFragment) result.orElse(null);
        }
        return null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LogUtils.error(TAG, "onCreate");

        stateModel = new ViewModelProvider(this).get(StateModel.class);

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                BrowserFragment browserFragment = getBrowserFragment();
                if (browserFragment != null) {
                    boolean result = browserFragment.onBackPressedCheck();
                    if (result) {
                        return;
                    }
                }
                finish();
            }
        });


        browserPager = findViewById(R.id.browser_pager);


        browserAdapter = new ViewPagerFragmentAdapter(
                getSupportFragmentManager(), getLifecycle());
        browserPager.setAdapter(browserAdapter);
        browserPager.setUserInputEnabled(false);
        browserPager.setOffscreenPageLimit(20);
        browserPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                stateModel.tabIdx(browserAdapter.getItemId(position));
            }
        });


        stateModel.uri().observe(this, uri -> {
            try {
                if (uri != null) {
                    BrowserFragment browserFragment = getBrowserFragment();
                    if (browserFragment != null) {
                        browserFragment.openUri(uri);
                    }
                    stateModel.uri(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        stateModel.idxs().observe(this, idxs -> {
            if (idxs != null) {
                int count = browserAdapter.getItemCount();
                browserAdapter.updateData(idxs);

                if (idxs.size() > count) {
                    browserPager.setCurrentItem(idxs.size(), true);
                }

            }
        });

        stateModel.error().observe(this, (content) -> {
            try {
                if (content != null) {
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(browserPager, content, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    stateModel.error(null);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        stateModel.warning().observe(this, (content) -> {
            try {
                if (content != null) {
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(browserPager, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                    stateModel.warning(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        stateModel.permission().observe(this, (content) -> {
            try {
                if (content != null) {
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(browserPager, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.settings, view -> {
                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" +
                                    view.getContext().getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                        });
                        snackbar.show();

                    }
                    stateModel.permission(null);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        stateModel.removed().observe(this, (bookmark) -> {
            try {
                if (bookmark != null) {
                    Snackbar snackbar = Snackbar.make(browserPager,
                            getString(R.string.bookmark_removed, bookmark.title()),
                            Snackbar.LENGTH_LONG);
                    snackbar.setAction(getString(R.string.revert), (view) -> {
                        try {
                            stateModel.restoreBookmark(bookmark);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            snackbar.dismiss();
                        }

                    });
                    snackbar.show();
                    stateModel.removed(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        Intent intent = getIntent();

        if (!handleIntents(intent)) {
            try {
                if (stateModel.hasNoTabs()) {
                    openTab(stateModel.homepage());
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        stateModel.online(API.isNetworkConnected(getApplicationContext()));

        notificationRequest();
    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntents(intent);
    }


    private boolean handleIntents(Intent intent) {

        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {
                updateTab(uri);
                return true;
            }
        }

        if (Intent.ACTION_SEND.equals(action)) {
            if (Objects.equals(intent.getType(), MimeTypeService.PLAIN_MIME_TYPE)) {
                String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                return doSearch(text);
            }
        }

        return false;
    }


    private boolean doSearch(@Nullable String query) {
        try {
            if (query != null && !query.isEmpty()) {
                Uri uri = Uri.parse(query);
                String scheme = uri.getScheme();
                if (Objects.equals(scheme, API.PNS_SCHEME) ||
                        Objects.equals(scheme, API.HTTP_SCHEME) ||
                        Objects.equals(scheme, API.HTTPS_SCHEME)) {
                    updateTab(uri);
                } else {
                    try {
                        // in case the search is an pns string
                        updateTab(Uri.parse(API.PNS_SCHEME + "://" +
                                Lite.decodePeerId(query).toBase36()));
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        networkCallback();
    }


    private void networkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);

            ConnectivityManager.NetworkCallback networkCallback =
                    new ConnectivityManager.NetworkCallback() {
                        @Override
                        public void onAvailable(@NonNull Network network) {
                            super.onAvailable(network);
                            stateModel.online(true);
                        }

                        @Override
                        public void onLost(@NonNull Network network) {
                            super.onLost(network);
                            stateModel.online(false);
                        }

                    };

            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .build();

            connectivityManager.registerNetworkCallback(networkRequest, networkCallback);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    static class ViewPagerFragmentAdapter extends FragmentStateAdapter {
        private final List<Long> tabs = new ArrayList<>();


        ViewPagerFragmentAdapter(@NonNull FragmentManager fragmentManager,
                                 @NonNull Lifecycle lifecycle) {
            super(fragmentManager, lifecycle);
        }

        @Override
        public long getItemId(int position) {
            if (position < 0 || position >= getItemCount()) {
                return 0;
            }
            return tabs.get(position);
        }

        @Override
        public boolean containsItem(long tabIdx) {
            for (Long tab : tabs) {
                if (tab == tabIdx) {
                    return true;
                }
            }
            return false;
        }


        void updateData(@NonNull List<Long> tabs) {
            final LongDiffCallback diffCallback = new LongDiffCallback(this.tabs, tabs);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

            this.tabs.clear();
            this.tabs.addAll(tabs);
            diffResult.dispatchUpdatesTo(this);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            long tab = tabs.get(position);
            BrowserFragment browserFragment = new BrowserFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(API.TAB, tab);
            browserFragment.setArguments(bundle);
            return browserFragment;
        }

        @Override
        public int getItemCount() {
            return tabs.size();
        }

    }
}