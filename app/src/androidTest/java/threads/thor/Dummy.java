package threads.thor;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Session;
import tech.lp2p.lite.LiteHost;
import tech.lp2p.lite.LiteSession;
import tech.lp2p.store.Block;
import tech.lp2p.store.BlockStoreDatabase;
import threads.thor.model.API;

public final class Dummy {

    @NonNull
    private final LiteHost host;
    @NonNull
    private final BlockStoreCache blockStore;
    private final PeerStore peerStore = new DummyPeerStore();

    private Dummy(@NonNull Context context) throws Exception {
        this.blockStore = BlockStoreCache.createInstance(context);
        this.host = new LiteHost(Lite.generateKeyPair(), API.bootstrap(), TestEnv.AGENT,
                new Payloads());
    }

    @NonNull
    public static Dummy getInstance(@NonNull Context context) throws Exception {
        return new Dummy(context);
    }

    @NonNull
    public PeerId self() {
        return host.self();
    }

    void clearDatabase() {
        blockStore.close();
    }

    @NonNull
    public Session createSession() {
        return new LiteSession(blockStore, peerStore, host);
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore) {
        return new LiteSession(blockStore, peerStore, host);
    }

    private static class DummyPeerStore implements PeerStore {
        @Override
        public List<Peeraddr> randomPeeraddrs(int limit) {
            return Collections.emptyList();
        }


        @Override
        public void storePeeraddr(@NonNull Peeraddr peeraddr) {

        }

        @Override
        public void removePeeraddr(@NonNull Peeraddr peeraddr) {

        }
    }


    public static final class BlockStoreCache implements BlockStore, AutoCloseable {

        private static final String TAG = BlockStoreCache.class.getSimpleName();
        private final BlockStoreDatabase blocksStoreDatabase;
        private final String name;
        private final Context context;

        private BlockStoreCache(Context context, BlockStoreDatabase blocksStoreDatabase, String name) {
            this.context = context;
            this.blocksStoreDatabase = blocksStoreDatabase;
            this.name = name;
        }

        public static BlockStoreCache createInstance(@NonNull Context context) {

            UUID uuid = UUID.randomUUID();
            BlockStoreDatabase blocksStoreDatabase = Room.databaseBuilder(context,
                            BlockStoreDatabase.class,
                            uuid.toString()).
                    allowMainThreadQueries().
                    fallbackToDestructiveMigration().build();
            return new BlockStoreCache(context, blocksStoreDatabase, uuid.toString());

        }


        public void clear() {
            blocksStoreDatabase.clearAllTables();
        }


        @Override
        public boolean hasBlock(@NonNull Cid cid) {
            return blocksStoreDatabase.blockDao().hasBlock(cid);
        }

        @Nullable
        @Override
        public byte[] getBlock(@NonNull Cid cid) {
            return blocksStoreDatabase.blockDao().getData(cid);
        }

        @Override
        public void storeBlock(@NonNull Cid cid, @NonNull byte[] block) {
            blocksStoreDatabase.blockDao().insertBlock(new Block(cid, block));
        }

        @Override
        public void deleteBlock(@NonNull Cid cid) {
            blocksStoreDatabase.blockDao().deleteBlock(cid);
        }

        @NonNull
        public String getName() {
            return name;
        }

        @Override
        public void close() {
            clear();
            boolean success = context.deleteDatabase(name);
            LogUtils.info(TAG, "Delete success " + success + " of database " + name);

        }
    }
}
