package threads.thor;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Peeraddr;
import threads.thor.model.dns.Resolver;

public class ResolveTest {
    private static final String TAG = ResolveTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_dnsLink() throws Exception {

        if (!TestEnv.isNetworkConnected(context)) {
            LogUtils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }
        String link = Resolver.resolveDnsLink("blog.ipfs.io");
        TestCase.assertNotNull(link);
        TestCase.assertFalse(link.isEmpty());
        Cid cid = Lite.decodeCid(link.replace("/ipfs/", ""));
        assertNotNull(cid);
    }

    @Test
    public void test_dnsAddress() throws Exception {

        if (!TestEnv.isNetworkConnected(context)) {
            LogUtils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Set<Peeraddr> addresses = Resolver.resolveDnsaddr("bootstrap.libp2p.io",
                Lite.decodePeerId("QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN"));
        assertNotNull(addresses);
        assertFalse(addresses.isEmpty());
    }

    @Test
    public void test_dnsAddress2() throws Exception {

        Set<Peeraddr> addresses = Resolver.resolveDnsaddr("ipfs.jamonbread.tech",
                Lite.decodePeerId("12D3KooWHnwgjLJjDgBVBxFJXGJYzrwM9RrsYi9KqJYDNoaGYBhC"));
        assertNotNull(addresses);
        assertTrue(addresses.isEmpty());
    }
}
