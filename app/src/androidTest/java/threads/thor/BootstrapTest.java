package threads.thor;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import threads.thor.model.peers.PEERS;


public class BootstrapTest {
    private static final String TAG = BootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_bootstrap() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            LogUtils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }


        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            for (Peeraddr peeraddr : session.bootstrap()) {
                LogUtils.error(TAG, "Routing Peer " + peeraddr.toString());

                Connection connection;
                try {
                    connection = Lite.dial(session, peeraddr, Lite.connectionParameters(ALPN.libp2p));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, "Connection failed " +
                            throwable.getClass().getSimpleName());
                    continue;
                }
                try {
                    assertNotNull(connection);
                    Identify info = Lite.identify(connection);
                    assertNotNull(info);
                    LogUtils.error(TAG, info.toString());

                    if (!info.hasProtocol(Protocol.DHT_PROTOCOL.name())) {
                        LogUtils.error(TAG, "Error has no DHT protocol !!!");
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, "PeerInfo failed " +
                            throwable.getClass().getSimpleName());
                }
            }
        }
    }

    @Test
    public void test_bootstrap_peers() throws Exception {

        if (!TestEnv.isNetworkConnected(context)) {
            LogUtils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }


        Set<Peeraddr> bootstrap = TestEnv.bootstrap();
        assertNotNull(bootstrap);


        for (Peeraddr address : bootstrap) {
            LogUtils.error(TAG, "Bootstrap " + address.toString());
        }
        assertTrue(bootstrap.size() >= 4);
    }

}
