# Privacy Policy

## Data Protection

As an application provider, we take the protection of all personal data very seriously.
All personal information is treated confidentially and in accordance with the legal requirements,
regulations, as explained in this privacy policy.

This app is designed so that the user do not have to enter any personal data. Never will data
collected by us, and especially not passed to third parties. The users behaviour is also not
analyzed by this application.

The user is responsible what kind of data it is retrieved from the network.
This kind of information is also not tracked by this application.

## Android Permissions

This section describes briefly the Android permissions are required.

- **Internet** Obvious basic requirement in order for the application to work properly.
- **Access Network State** Required to evaluate the current network state.
- **Foreground Service Data Sync** The foreground service permission is required to download data
  over a longer period of time.
- **Post Notification** Required to notify the user when data is downloaded with the progress of the
  operation.
- **Camera** Required for websites to function.
- **Record Audio** Required for websites to function.
- **Modify Audio Settings** Required for websites to function.

### Contact Information

<p>Remmer Wilts, Dr. Munderloh Str.10, 27798 Wüsting, Germany</p>