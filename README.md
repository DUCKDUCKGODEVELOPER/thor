#sudo su
# Thor

The **Thor** browser is a web browser with the focus on decentralized technologies.
It supports the **pns** (a subset of **ipns**) and the **magnet** protocol.

In addition to the protocols enhancements it focus on usability.
To improve the usability of the browser, an **Ad-blocker** is integrated.
It based on the information which are coming
from [pgl.yoyo.org](https://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml&showintro=0&mimetype=plaintext)

## General

The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/threads.thor/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
alt="Get it on Google Play"
height="80">](https://play.google.com/store/apps/details?id=threads.thor)

## PNS

The browser runs an PNS service in the background, which will be started automatically when
the browser detects the first access to.

The service itself is configured in client mode, that means that you are only be able
to download data, but not providing data to others.

A **PNS** server implementation is available as an android
application [Odin](https://gitlab.com/lp2p/odin/).

## Settings

This application based on the WebKit API (like Chrome, Brave, etc). This section just gives
a brief overview of the settings which have been made for the browser.
This information is probably only useful for people with technical background.

General Browser Settings:

```
WebSettings settings = webView.getSettings();
settings.setUserAgentString("Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")");


settings.setJavaScriptEnabled(enableJavascript);
settings.setJavaScriptCanOpenWindowsAutomatically(false);

settings.setSafeBrowsingEnabled(true);
settings.setAllowContentAccess(false);
settings.setAllowFileAccess(true);  // set to true for mht files 
settings.setLoadsImagesAutomatically(true);
settings.setBlockNetworkLoads(false);
settings.setBlockNetworkImage(false);
settings.setDomStorageEnabled(true);
settings.setCacheMode(WebSettings.LOAD_DEFAULT);
settings.setDatabaseEnabled(true);
settings.setSupportZoom(true);
settings.setBuiltInZoomControls(true);
settings.setDisplayZoomControls(false);
settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
settings.setUseWideViewPort(true);
settings.setLoadWithOverviewMode(true);
settings.setMediaPlaybackRequiresUserGesture(false);  // set to false, required for camera permission
settings.setSupportMultipleWindows(false);
settings.setGeolocationEnabled(false);
```

### Cookies

The application accept all cookies, except third party cookies.

```
CookieManager.getInstance().setAcceptThirdPartyCookies(mWebView, false);
```

## Links

[Privacy Policy](https://gitlab.com/lp2p/thor/-/blob/master/POLICY.md)
<br/>
[Apache License](https://gitlab.com/lp2p/thor/-/blob/master/LICENSE)
