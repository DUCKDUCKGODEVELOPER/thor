package tech.lp2p.store;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = {Block.class}, version = 2, exportSchema = false)
@TypeConverters({RoomConverters.class})
public abstract class BlockStoreDatabase extends RoomDatabase {

    public abstract BlockStoreDao blockDao();

}
