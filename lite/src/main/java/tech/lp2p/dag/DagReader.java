package tech.lp2p.dag;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import merkledag.pb.Merkledag;
import unixfs.pb.Unixfs;

public record DagReader(@NonNull DagVisitor dagVisitor, @NonNull DagWalker dagWalker,
                        @NonNull AtomicInteger left, long size) {

    public static DagReader create(@NonNull Merkledag.PBNode node) throws Exception {
        long size = 0;

        Unixfs.Data unixData = DagReader.getData(node);

        switch (unixData.getType()) {
            case Raw, File -> size = unixData.getFilesize();
        }


        DagWalker dagWalker = DagWalker.createWalker(node);
        return new DagReader(DagVisitor.createDagVisitor(dagWalker.root()), dagWalker,
                new AtomicInteger(0), size);

    }

    @Nullable
    public static Merkledag.PBLink getLinkByName(@NonNull Merkledag.PBNode node,
                                                 @NonNull String name) {

        for (Merkledag.PBLink link : node.getLinksList()) {
            if (Objects.equals(link.getName(), name)) {
                return link;
            }
        }
        return null;

    }

    @NonNull
    public static Unixfs.Data getData(@NonNull Merkledag.PBNode node) throws Exception {
        return Unixfs.Data.parseFrom(node.getData().toByteArray());
    }

    private static ByteString readUnixNodeData(@NonNull Unixfs.Data unixData, int position) {

        return switch (unixData.getType()) {
            case Directory, File, Raw -> unixData.getData().substring(position);
            default -> throw new IllegalStateException("found %s node in unexpected place " +
                    unixData.getType().name());
        };
    }

    public void seek(@NonNull DagFetch dagFetch, int offset) throws Exception {
        DagWalker.Result result = dagWalker.seek(dagFetch, offset);
        this.left.set(result.left());
        this.dagVisitor.reset(result.stack());
    }

    @NonNull
    public ByteString loadNextData(@NonNull DagFetch dagFetch) throws Exception {

        int left = this.left.getAndSet(0);
        if (left > 0) {
            DagStage dagStage = dagVisitor.peekStage();
            Merkledag.PBNode node = dagStage.node();

            if (node.getLinksCount() == 0) {
                return readUnixNodeData(DagReader.getData(node), left);
            }
        }

        while (true) {
            DagStage dagStage = dagWalker.next(dagFetch, dagVisitor);
            if (dagStage == null) {
                // done nothing left
                return ByteString.EMPTY;
            }

            Merkledag.PBNode node = dagStage.node();
            if (node.getLinksCount() > 0) {
                continue;
            }

            return readUnixNodeData(DagReader.getData(node), 0);
        }
    }
}
