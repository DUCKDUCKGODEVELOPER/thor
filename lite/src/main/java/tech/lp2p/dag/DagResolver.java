package tech.lp2p.dag;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Keys;


public interface DagResolver {

    @NonNull
    static Info resolvePath(@NonNull DagFetch dagFetch, @NonNull Cid root,
                            @NonNull List<String> path) throws Exception {

        Pair<Cid, List<String>> resolved = resolveToLastNode(dagFetch, root, path);
        Cid cid = resolved.first;
        Objects.requireNonNull(cid);
        // make sure it is resolved
        Merkledag.PBNode node = DagService.getNode(dagFetch, cid);
        Objects.requireNonNull(node);
        return DagStream.info(cid, node);
    }

    @NonNull
    static Info resolvePath(@NonNull BlockStore blockStore, @NonNull Cid root,
                            @NonNull List<String> path) throws Exception {

        Pair<Cid, List<String>> resolved = resolveToLastNode(blockStore, root, path);
        Cid cid = resolved.first;
        Objects.requireNonNull(cid);
        // make sure it is resolved
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);
        Objects.requireNonNull(node);
        return DagStream.info(cid, node);
    }

    @NonNull
    private static Pair<Cid, List<String>> resolveToLastNode(@NonNull BlockStore blockStore,
                                                             @NonNull Cid root,
                                                             @NonNull List<String> path)
            throws Exception {

        if (path.isEmpty()) {
            return Pair.create(root, Collections.emptyList());
        }

        Cid cid = root;
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);

        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            Objects.requireNonNull(lnk, name + " not found");
            cid = Keys.decodeCid(lnk.getHash().toByteArray());
            node = DagService.getNode(blockStore, cid);
        }

        return Pair.create(cid, Collections.emptyList());

    }

    @NonNull
    private static Pair<Cid, List<String>> resolveToLastNode(@NonNull DagFetch dagFetch,
                                                             @NonNull Cid root,
                                                             @NonNull List<String> path)
            throws Exception {

        if (path.isEmpty()) {
            return Pair.create(root, Collections.emptyList());
        }

        Cid cid = root;
        Merkledag.PBNode node = DagService.getNode(dagFetch, cid);

        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            Objects.requireNonNull(lnk, name + " not found");
            cid = Keys.decodeCid(lnk.getHash().toByteArray());
            node = DagService.getNode(dagFetch, cid);
        }

        return Pair.create(cid, Collections.emptyList());

    }

}
