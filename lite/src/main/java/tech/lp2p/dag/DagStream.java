package tech.lp2p.dag;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import unixfs.pb.Unixfs;


public interface DagStream {


    @NonNull
    static Info info(@NonNull BlockStore blockStore, @NonNull Cid cid) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);
        Objects.requireNonNull(node);
        return info(cid, node);
    }

    @NonNull
    static Info info(Cid cid, Merkledag.PBNode node) throws Exception {
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() == unixfs.pb.Unixfs.Data.DataType.Directory) {
            return new Dir(cid, unixData.getFilesize(), unixData.getName());
        }
        if (unixData.getType() == Unixfs.Data.DataType.File) {
            return new Fid(cid, unixData.getFilesize(), unixData.getName());
        }
        if (unixData.getType() == Unixfs.Data.DataType.Raw) {
            return new Raw(cid, unixData.getFilesize());
        }
        throw new IllegalStateException();
    }

    @NonNull
    static Dir createEmptyDirectory(@NonNull Session session, @NonNull String name) throws Exception {
        return DagService.createEmptyDirectory(session, name);
    }

    @NonNull
    static Dir addToDirectory(@NonNull Session session,
                              @NonNull Cid directory,
                              @NonNull Info info) throws Exception {

        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.addChild(session, dirNode, info);

    }

    @NonNull
    static Dir updateDirectory(@NonNull Session session, @NonNull Cid directory,
                               @NonNull Info info) throws Exception {
        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.updateDirectory(session, dirNode, info);

    }

    @NonNull
    static Dir createDirectory(@NonNull Session session,
                               @NonNull String name,
                               @NonNull List<Info> links) throws Exception {
        return DagService.createDirectory(session, name, links);
    }

    @NonNull
    static Dir renameDirectory(@NonNull Session session,
                               @NonNull Cid directory,
                               @NonNull String name) throws Exception {
        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.renameDirectory(session, dirNode, name);
    }

    @NonNull
    static Dir removeFromDirectory(@NonNull Session session, @NonNull Cid directory,
                                   @NonNull Info info) throws Exception {
        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.removeChild(session, dirNode, info);
    }

    @NonNull
    static List<Cid> blocks(@NonNull Session session, @NonNull Cid cid) throws Exception {
        List<Cid> result = new ArrayList<>();

        byte[] block = session.blockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = DagService.node(block);
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Keys.decodeCid(link.getHash().toByteArray());
                result.add(child);
                result.addAll(blocks(session, child));
            }
        }
        return result;
    }

    static void removeBlocks(@NonNull Session session, @NonNull Cid cid) throws Exception {
        byte[] block = session.blockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = DagService.node(block);
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Keys.decodeCid(link.getHash().toByteArray());
                removeBlocks(session, child);
            }
            session.blockStore().deleteBlock(cid);
        }
    }

    static List<Info> childs(@NonNull BlockStore blockStore, @NonNull Dir dir) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new IllegalStateException("invalid node");
        }
        List<Info> result = new ArrayList<>();
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            String name = link.getName();
            Merkledag.PBLinkType type = link.getType();
            Cid cid = Keys.decodeCid(link.getHash().toByteArray());
            if (type != null) {
                switch (type) {
                    case Dir -> result.add(new Dir(cid, link.getTsize(), name));
                    case File -> result.add(new Fid(cid, link.getTsize(), name));
                }
            }
        }
        return result;
    }

    static List<Raw> raws(@NonNull BlockStore blockStore, @NonNull Fid fid) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, fid.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() != Unixfs.Data.DataType.File) {
            throw new IllegalStateException("invalid node");
        }
        List<Raw> result = new ArrayList<>();
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            result.add(new Raw(Keys.decodeCid(link.getHash().toByteArray()), link.getTsize()));
        }
        return result;
    }

    static boolean hasChild(@NonNull DagFetch dagFetch, @NonNull Dir dir,
                            @NonNull String name) throws Exception {
        Merkledag.PBNode node = DagService.getNode(dagFetch, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() != Unixfs.Data.DataType.Directory) {
            throw new IllegalStateException("invalid node");
        }
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    static boolean hasChild(@NonNull BlockStore blockStore, @NonNull Dir dir,
                            @NonNull String name) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() != Unixfs.Data.DataType.Directory) {
            throw new IllegalStateException("invalid node");
        }
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    @NonNull
    static Fid readInputStream(@NonNull Session session,
                               @NonNull String name,
                               @NonNull DagInputStream dagInputStream) throws Exception {

        return DagService.createFromStream(session, name, dagInputStream);
    }

}
