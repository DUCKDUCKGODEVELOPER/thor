package tech.lp2p.dag;

import androidx.annotation.NonNull;

import merkledag.pb.Merkledag;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;

public interface DagFetch {

    @NonNull
    Merkledag.PBNode getBlock(Cid cid) throws Exception;

    @NonNull
    BlockStore blockStore();

}
