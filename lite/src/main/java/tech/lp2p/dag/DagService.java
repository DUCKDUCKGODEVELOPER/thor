package tech.lp2p.dag;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Multicodec;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import unixfs.pb.Unixfs;

public interface DagService {

    @NonNull
    private static Merkledag.PBNode createRaw(byte[] bytes) {
        if (bytes.length > Lite.CHUNK_SIZE) {
            throw new IllegalStateException();
        }
        Unixfs.Data.Builder data = Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                .setFilesize(bytes.length)
                .setData(ByteString.copyFrom(bytes));

        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(data.build().toByteArray()));

        return pbn.build();
    }

    static DirectoryNode createDirectory(@NonNull String name, @NonNull List<Info> infos) {
        Unixfs.Data.Builder builder = Unixfs.Data.newBuilder()
                .setName(name)
                .setType(Unixfs.Data.DataType.Directory);
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        long fileSize = 0;
        for (Info info : infos) {

            if (info instanceof Raw) {
                throw new IllegalStateException("Raw type can not be added to directory");
            }

            long size = info.size();
            fileSize = fileSize + size;
            builder.addBlocksizes(size);


            Merkledag.PBLink.Builder lnb = createLink(info);

            pbn.addLinks(lnb.build());
        }
        builder.setFilesize(fileSize);
        byte[] unixData = builder.build().toByteArray();
        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), fileSize, name);

    }

    static DirectoryNode createDirectory(@NonNull String name) {
        byte[] unixData = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory)
                .setName(name)
                .build().toByteArray();
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), 0, name);
    }

    static Unixfs.Data info(@NonNull Merkledag.PBNode node) throws Exception {
        return DagReader.getData(node);
    }

    @NonNull
    static Merkledag.PBNode getNode(@NonNull DagFetch dagFetch,
                                    @NonNull Cid cid) throws Exception {
        byte[] block = dagFetch.blockStore().getBlock(cid);
        if (block != null) {
            return node(block);
        }
        return dagFetch.getBlock(cid);
    }

    @NonNull
    static Merkledag.PBNode getNode(@NonNull BlockStore blockStore,
                                    @NonNull Cid cid) throws Exception {
        byte[] block = blockStore.getBlock(cid);
        if (block == null) {
            throw new Exception("Cid not is not available in block store");
        }
        return node(block);
    }

    @NonNull
    static Merkledag.PBNode node(byte[] block) {
        try {
            return Merkledag.PBNode.parseFrom(block);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    static Raw createRaw(@NonNull Session session, byte[] data) throws Exception {
        Merkledag.PBNode node = DagService.createRaw(data);
        byte[] bytes = node.toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        session.blockStore().storeBlock(cid, bytes);
        return new Raw(cid, data.length);
    }


    static Dir createDirectory(@NonNull Session session,
                               @NonNull String name,
                               @NonNull List<Info> links) throws Exception {
        DagService.DirectoryNode directory = DagService.createDirectory(name, links);
        byte[] bytes = directory.node().toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        session.blockStore().storeBlock(cid, bytes);
        return new Dir(cid, directory.size(), directory.name());
    }

    static Dir createEmptyDirectory(@NonNull Session session, @NonNull String name) throws Exception {
        DagService.DirectoryNode directory = DagService.createDirectory(name);
        byte[] bytes = directory.node().toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        session.blockStore().storeBlock(cid, bytes);
        return new Dir(cid, directory.size(), directory.name());
    }

    static Cid createEmptyDirectory(@NonNull String name) throws Exception {
        DagService.DirectoryNode directory = DagService.createDirectory(name);
        byte[] bytes = directory.node().toByteArray();
        return Cid.generateCid(Multicodec.DAG_PB, bytes);
    }

    @NonNull
    static Dir addChild(@NonNull Session session,
                        @NonNull Merkledag.PBNode dirNode,
                        @NonNull Info info) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long dirSize = unixData.getFilesize();

        Merkledag.PBLink.Builder lnb = createLink(info);

        long newDirSize = dirSize + info.size();

        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.setFilesize(newDirSize);
        builder.addBlocksizes(info.size());

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        pbn.addLinks(lnb.build());
        Unixfs.Data updateUnixData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnixData.toByteArray()));

        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        session.blockStore().storeBlock(cid, bytes);

        return new Dir(cid, updateUnixData.getFilesize(), updateUnixData.getName());
    }

    private static Merkledag.PBLink.Builder createLink(@NonNull Info info) {
        if (info.isDir()) {
            return Merkledag.PBLink.newBuilder()
                    .setName(info.name())
                    .setType(Merkledag.PBLinkType.Dir)
                    .setHash(ByteString.copyFrom(info.cid().encoded()))
                    .setTsize(info.size());
        }
        if (info.isFid()) {
            return Merkledag.PBLink.newBuilder()
                    .setName(info.name())
                    .setType(Merkledag.PBLinkType.File)
                    .setHash(ByteString.copyFrom(info.cid().encoded()))
                    .setTsize(info.size());
        }
        if (info.isRaw()) {
            return Merkledag.PBLink.newBuilder()
                    .setHash(ByteString.copyFrom(info.cid().encoded()))
                    .setTsize(info.size());
        }
        throw new IllegalStateException("not supported link");
    }

    @NonNull
    static Dir updateDirectory(@NonNull Session session,
                               @NonNull Merkledag.PBNode dirNode,
                               @NonNull Info info) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long filesize = 0L;

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.clearBlocksizes();

        for (int index = 0; index < pbn.getLinksCount(); index++) {
            Merkledag.PBLink pbLink = dirNode.getLinks(index);
            if (Objects.equals(pbLink.getName(), info.name())) {
                pbn.removeLinks(index); // remove the update link when available
            } else {
                filesize = filesize + pbLink.getTsize();
                builder.addBlocksizes(pbLink.getTsize());
            }
        }

        // added the updated link
        Merkledag.PBLink.Builder lnb = createLink(info);
        pbn.addLinks(lnb);

        filesize = filesize + info.size();
        builder.addBlocksizes(info.size());
        // end update link

        builder.setFilesize(filesize);
        Unixfs.Data updateUnixData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnixData.toByteArray()));
        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        session.blockStore().storeBlock(cid, bytes);

        return new Dir(cid, updateUnixData.getFilesize(), updateUnixData.getName());
    }

    static Dir removeChild(@NonNull Session session,
                           @NonNull Merkledag.PBNode dirNode,
                           @NonNull Info info) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long filesize = 0L;

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.clearBlocksizes();

        for (int index = 0; index < pbn.getLinksCount(); index++) {
            Merkledag.PBLink link = dirNode.getLinks(index);
            if (Objects.equals(link.getName(), info.name())) {
                pbn.removeLinks(index);
            } else {
                filesize = filesize + link.getTsize();
                builder.addBlocksizes(link.getTsize());
            }
        }

        builder.setFilesize(filesize);
        Unixfs.Data updateUnixData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnixData.toByteArray()));

        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        session.blockStore().storeBlock(cid, bytes);

        return new Dir(cid, updateUnixData.getFilesize(), updateUnixData.getName());

    }

    @NonNull
    static Fid createFromStream(@NonNull Session session,
                                @NonNull String name,
                                @NonNull DagInputStream reader) throws Exception {
        return DagWriter.trickle(name, reader, session.blockStore());
    }

    static Dir renameDirectory(Session session, Merkledag.PBNode dirNode, String name) throws Exception {
        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }
        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.setName(name);
        Unixfs.Data updateUnitData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnitData.toByteArray()));

        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        session.blockStore().storeBlock(cid, bytes);

        return new Dir(cid, updateUnitData.getFilesize(), updateUnitData.getName());
    }

    record DirectoryNode(Merkledag.PBNode node, long size, String name) {
    }
}
