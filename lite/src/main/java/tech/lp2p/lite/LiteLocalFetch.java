package tech.lp2p.lite;

import androidx.annotation.NonNull;

import merkledag.pb.Merkledag;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagFetch;

public record LiteLocalFetch(Session session) implements DagFetch {
    @NonNull
    @Override
    public Merkledag.PBNode getBlock(Cid cid) throws Exception {
        throw new Exception("data not available");
    }

    @NonNull
    @Override
    public BlockStore blockStore() {
        return session.blockStore();
    }
}
