package tech.lp2p.lite;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import fetch.pb.Fetch;
import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Keys;
import tech.lp2p.quic.Stream;

public final class LiteFetchHandler implements Handler {

    private final BlockStore blockStore;

    public LiteFetchHandler(@NonNull BlockStore blockStore) {
        this.blockStore = blockStore;
    }

    @Override
    public void protocol(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        Fetch.FetchRequest request = Fetch.FetchRequest.parseFrom(data);
        Cid cid = Keys.decodeCid(request.getBlock().toByteArray());
        byte[] blockData = blockStore.getBlock(cid);
        if (blockData != null) {
            Fetch.FetchResponse response = Fetch.FetchResponse.newBuilder()
                    .setStatus(Fetch.FetchResponse.Status.Have)
                    .setData(ByteString.copyFrom(blockData)).build();

            stream.response(response, Lite.GRACE_PERIOD);

        } else {
            Fetch.FetchResponse response = Fetch.FetchResponse.newBuilder()
                    .setStatus(Fetch.FetchResponse.Status.DontHave).build();
            stream.response(response, Lite.DEFAULT_REQUEST_TIMEOUT);
        }
    }

    @Override
    public void closed(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

}
