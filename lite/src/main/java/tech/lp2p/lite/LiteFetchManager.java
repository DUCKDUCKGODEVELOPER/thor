package tech.lp2p.lite;


import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Objects;

import merkledag.pb.Merkledag;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Multicodec;
import tech.lp2p.dag.DagFetch;


public final class LiteFetchManager implements DagFetch {

    @NonNull
    private final Connection connection;
    @NonNull
    private final BlockStore blockStore;

    public LiteFetchManager(@NonNull Connection connection) {
        this.connection = connection;
        this.blockStore = connection.host().blockStore();
    }

    @Override
    @NonNull
    public Merkledag.PBNode getBlock(Cid cid) throws Exception {

        fetch.pb.Fetch.FetchRequest fetchRequest =
                fetch.pb.Fetch.FetchRequest.newBuilder().setBlock(
                        ByteString.copyFrom(cid.encoded())).build();

        fetch.pb.Fetch.FetchResponse fetchResponse = LiteService.fetch(connection, fetchRequest);

        if (fetchResponse.getStatus() == fetch.pb.Fetch.FetchResponse.Status.Have) {

            ByteString payloadData = fetchResponse.getData();

            if (cid.codec() == Multicodec.DAG_PB.codec()) {
                // only Multicodec.DAG_PB is supported here
                Cid cmp = Cid.generateCid(Multicodec.DAG_PB, payloadData.toByteArray());

                if (!Objects.equals(cmp, cid)) {
                    throw new Exception("Invalid Cid block returned");
                }
                Merkledag.PBNode node = Merkledag.PBNode.parseFrom(payloadData.toByteArray());

                blockStore.storeBlock(cid, node.toByteArray());
                return node;
            } else {
                throw new Exception("not supported multicodec");
            }
        }

        throw new Exception("Block not found");

    }

    @NonNull
    @Override
    public BlockStore blockStore() {
        return blockStore;
    }


}
