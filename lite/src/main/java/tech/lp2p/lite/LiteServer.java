package tech.lp2p.lite;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Key;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Reservations;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Server;
import tech.lp2p.dht.DhtHandler;
import tech.lp2p.dht.DhtKademlia;
import tech.lp2p.ident.IdentifyHandler;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.AlpnLibp2pResponder;
import tech.lp2p.quic.AlpnLiteResponder;
import tech.lp2p.quic.ApplicationProtocolConnectionFactory;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.ServerConnection;
import tech.lp2p.quic.ServerConnector;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamData;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.quic.TransportError;
import tech.lp2p.quic.Version;
import tech.lp2p.relay.RelayHopHandler;
import tech.lp2p.relay.RelayService;
import tech.lp2p.relay.RelayStopHandler;
import tech.lp2p.utils.Utils;

public final class LiteServer implements Server {
    private static final String TAG = LiteServer.class.getSimpleName();

    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    @NonNull
    private final Set<Connection> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector serverConnector;
    @NonNull
    private final Responder responderLibp2p;
    @NonNull
    private final Responder responderLite;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Function<Payload, Envelope> envelopeSupplier;
    @NonNull
    private final Consumer<Envelope> envelopeConsumer;
    @NonNull
    private final DhtKademlia dhtKademlia;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final Consumer<Reservation> reservationGain;
    @NonNull
    private final Consumer<Reservation> reservationLost;

    @NonNull
    private final Function<PeerId, Boolean> isGated;

    private LiteServer(@NonNull Server.Settings settings,
                       @NonNull LiteHost host,
                       @NonNull BlockStore blockStore,
                       @NonNull PeerStore peerStore,
                       @NonNull Consumer<Reservation> reservationGain,
                       @NonNull Consumer<Reservation> reservationLost,
                       @NonNull Function<PeerId, Boolean> isGated,
                       @NonNull Function<Payload, Envelope> envelopeSupplier,
                       @NonNull Consumer<Envelope> envelopeConsumer,
                       @NonNull DatagramSocket socket) {
        this.host = host;
        this.blockStore = blockStore;
        this.socket = socket;
        this.reservationGain = reservationGain;
        this.reservationLost = reservationLost;
        this.isGated = isGated;
        this.envelopeSupplier = envelopeSupplier;
        this.envelopeConsumer = envelopeConsumer;
        this.serverConnector = createServerConnector(socket);
        this.dhtKademlia = new DhtKademlia(this, peerStore);

        Limit limit = settings.limit();
        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        if (settings.dhtProtocolEnabled()) {
            libp2p.put(Protocol.DHT_PROTOCOL, new DhtHandler(dhtKademlia));
        }

        libp2p.put(Protocol.RELAY_PROTOCOL_HOP, new RelayHopHandler(this, limit));
        this.responderLibp2p = new Responder(libp2p);

        Protocols lite = new Protocols();
        lite.put(Protocol.LITE_PUSH_PROTOCOL, new LitePushHandler(this));
        lite.put(Protocol.LITE_PULL_PROTOCOL, new LitePullHandler(this));
        lite.put(Protocol.LITE_FETCH_PROTOCOL, new LiteFetchHandler(blockStore));
        this.responderLite = new Responder(lite);


    }

    private static int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    @NonNull
    public static Server startServer(@NonNull Server.Settings settings,
                                     @NonNull LiteHost host,
                                     @NonNull BlockStore blockStore,
                                     @NonNull PeerStore peerStore,
                                     @NonNull Consumer<Peeraddr> connectionGain,
                                     @NonNull Consumer<Peeraddr> connectionLost,
                                     @NonNull Consumer<Reservation> reservationGain,
                                     @NonNull Consumer<Reservation> reservationLost,
                                     @NonNull Function<PeerId, Boolean> isGated,
                                     @NonNull Function<Payload, Envelope> envelopeSupplier,
                                     @NonNull Consumer<Envelope> envelopeConsumer) {

        DatagramSocket socket = Utils.getSocket(settings.port());

        LiteServer server = new LiteServer(settings, host, blockStore, peerStore,
                reservationGain, reservationLost, isGated, envelopeSupplier,
                envelopeConsumer, socket);

        ServerConnector serverConnector = server.serverConnector;

        serverConnector.setClosedConsumer(connectionLost);
        ApplicationProtocolConnection protocolConnection =
                new ApplicationProtocolConnection(isGated, connectionGain);

        serverConnector.registerApplicationProtocol(ALPN.libp2p.name(), protocolConnection);

        serverConnector.registerApplicationProtocol(ALPN.lite.name(), protocolConnection);

        serverConnector.start();
        return server;
    }

    @NonNull
    public ServerConnector serverConnector() {
        return serverConnector;
    }

    void push(@NonNull Envelope envelope) {
        envelopeConsumer.accept(envelope);
    }

    @Override
    @NonNull
    public Responder responder(@NonNull ALPN alpn) {
        switch (alpn) {
            case lite -> {
                return responderLite;
            }
            case libp2p -> {
                return responderLibp2p;
            }
        }
        throw new IllegalStateException();
    }

    @NonNull
    @Override
    public KeyPair keyPair() {
        return host.keyPair();
    }

    @NonNull
    private ServerConnector createServerConnector(DatagramSocket socket) {
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.QUIC_version_1);

        Map<ALPN, Function<Stream, Consumer<StreamData>>> streamDataConsumers =
                Map.of(ALPN.libp2p,
                        quicStream -> AlpnLibp2pResponder.create(responderLibp2p),
                        ALPN.lite,
                        quicStream -> AlpnLiteResponder.create(responderLite));

        return new ServerConnector(this, socket, new LiteTrustManager(),
                host.certificate().certificate(),
                host.certificate().privateKey(), supportedVersions, streamDataConsumers);

    }

    @Nullable
    Envelope envelope(@NonNull Payload payload) {
        return envelopeSupplier.apply(payload);
    }

    @NonNull
    public DatagramSocket socket() {
        return socket;
    }

    @Override
    public void shutdown() {

        for (Connection connection : reservations) {
            connection.close(LiteErrorCode.SHUTDOWN);
        }
        reservations.clear();

        serverConnector.shutdown();
    }

    @Override
    public int port() {
        return socket.getLocalPort();
    }

    @Override
    public int numConnections() {
        return serverConnector.numConnections();
    }

    private void punching(Peeraddr peeraddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = peeraddr.inetAddress();
            int port = peeraddr.port();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            socket.send(datagram);
            Utils.debug(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(peeraddr);
            }

            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    public void holePunching(@NonNull Peeraddr peeraddr) {
        try {
            ExecutorService service = Executors.newSingleThreadExecutor();
            service.execute(() -> punching(peeraddr));
            boolean finished = service.awaitTermination(Lite.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    @Override
    public PeerId self() {
        return host.self();
    }

    @NonNull
    @Override
    public Peeraddrs bootstrap() {
        return host.bootstrap();
    }

    @NonNull
    @Override
    public Protocols protocols(@NonNull ALPN alpn) {
        return responder(alpn).protocols();
    }

    @NonNull
    @Override
    public Certificate certificate() {
        return host.certificate();
    }

    @NonNull
    public Reservations reservations() {
        Reservations result = new Reservations();
        for (Connection connection : reservations) {
            Reservation reservation = (Reservation) connection.getAttribute(
                    StreamRequester.RESERVATION);
            if (reservation != null) {
                result.add(reservation);
            } else {
                removeReservation(connection, null);
            }
        }
        return result;
    }


    @NonNull
    private Set<Reservation> reservations(Collection<Peeraddr> peeraddrs, Cancellable cancellable) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = new HashSet<>();
            Set<Connection> refreshRequired = new HashSet<>();
            // Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Connection connection : reservations) {
                Reservation reservation = (Reservation) connection.getAttribute(
                        StreamRequester.RESERVATION);
                if (reservation != null) {
                    relaysStillValid.add(reservation.peerId());  // still valid
                    if (reservation.limit().limited()) {
                        // update the limited connection
                        refreshRequired.add(connection);
                    }
                } else {
                    removeReservation(connection, null);
                }
            }

            ExecutorService service = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            boolean workToDo = !refreshRequired.isEmpty() || !peeraddrs.isEmpty();
            for (Connection connection : refreshRequired) {
                service.execute(() -> {
                    try {
                        if (cancellable.isCancelled()) {
                            return;
                        }
                        connection.setAttribute(StreamRequester.RESERVATION,
                                RelayService.hopReserve(connection, self()));
                    } catch (Throwable throwable) {
                        Reservation reservation = (Reservation) connection.getAttribute(
                                StreamRequester.RESERVATION);
                        removeReservation(connection, reservation);
                    }
                });

            }

            for (Peeraddr peeraddr : peeraddrs) {
                service.execute(() -> {
                    try {
                        if (cancellable.isCancelled()) {
                            return;
                        }
                        PeerId peerId = peeraddr.peerId();
                        Objects.requireNonNull(peerId);

                        if (!relaysStillValid.contains(peerId)) {
                            reservation(peeraddr);
                        }

                    } catch (Throwable throwable) {
                        Utils.error(TAG, throwable);
                    }
                });

            }

            if (workToDo) {
                service.shutdown();

                try {
                    while (!cancellable.isCancelled()) {
                        Thread.sleep(1000); // todo
                    }
                    service.shutdownNow();
                } catch (Throwable ignore) {
                }
            }

        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    @VisibleForTesting
    @Override
    @NonNull
    public Reservation reservation(Peeraddr peeraddr) throws Exception {

        Parameters parameters = Parameters.create(ALPN.libp2p, true);

        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        libp2p.put(Protocol.RELAY_PROTOCOL_STOP, new RelayStopHandler(this));

        Connection connection = ConnectionBuilder.connect(this, peeraddr,
                parameters, this.certificate(), new Responder(libp2p));
        connection.setCloseConsumer(connection1 -> {
            try {
                Reservation reservation = (Reservation) connection1.getAttribute(
                        StreamRequester.RESERVATION);
                removeReservation(connection1, reservation);
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable);
            }
        });
        try {
            Reservation reservation = RelayService.hopReserve(connection, self());
            connection.setAttribute(StreamRequester.RESERVATION, reservation);
            reservations.add(connection);
            reservationGain.accept(reservation);
            return reservation;
        } catch (Throwable throwable) {
            connection.close();
            throw throwable;
        }
    }

    public boolean hasReservations() {
        return !reservations.isEmpty();
    }

    public void reservations(Cancellable cancellable) {

        Set<Reservation> reservations = reservations(Collections.emptyList(), cancellable);

        Set<Peeraddr> relays = ConcurrentHashMap.newKeySet();
        for (Reservation reservation : reservations) {
            relays.add(reservation.peeraddr());
        }

        if (cancellable.isCancelled()) {
            return;
        }


        // fill up reservations [not yet enough]
        dhtKademlia.findClosestPeers(cancellable, peeraddr -> {

            if (relays.contains(peeraddr)) {
                return;
            }

            if (cancellable.isCancelled()) {
                return;
            }

            try {
                reservation(peeraddr);
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable);
            }
        }, self());

    }

    @NonNull
    public Peeraddrs peeraddrs() {
        return Peeraddr.peeraddrs(self(), port());
    }

    @NonNull
    public Identify identify() throws Exception {
        return IdentifyService.identify(IdentifyService.identify(
                host.keyPair(), host.agent(),
                protocols(ALPN.libp2p).keySet(), peeraddrs()), self());

    }

    @NonNull
    public Peeraddrs reservationPeeraddrs() {
        Peeraddrs result = new Peeraddrs();
        for (Reservation reservation : reservations()) {
            result.add(reservation.peeraddr());
        }
        return result;
    }

    public void provideKey(@NonNull Cancellable cancellable, @NonNull Consumer<Peeraddr> consumer,
                           @NonNull Key key) {
        dhtKademlia.provideKey(cancellable, this, consumer, key);
    }

    @Override
    public Reservation refreshReservation(Reservation reservation) throws Exception {
        for (Connection connection : reservations) {
            Reservation stored = (Reservation) connection.getAttribute(StreamRequester.RESERVATION);
            if (Objects.equals(stored, reservation)) {
                try {
                    Reservation refreshed = RelayService.hopReserve(connection, self());
                    connection.setAttribute(StreamRequester.RESERVATION, refreshed);
                    return refreshed;
                } catch (Throwable throwable) {
                    removeReservation(connection, stored);
                    throw throwable;
                }
            }
        }
        throw new Exception("Reservation is not valid anymore");
    }

    private void removeReservation(Connection connection, @Nullable Reservation reservation) {
        connection.removeAttribute(StreamRequester.RESERVATION);
        reservations.remove(connection);
        if (reservation != null) {
            reservationLost.accept(reservation);
        }
    }

    @Override
    public void closeReservation(Reservation reservation) {
        for (Connection connection : reservations) {
            Reservation stored = (Reservation) connection.getAttribute(StreamRequester.RESERVATION);
            if (Objects.equals(stored, reservation)) {
                removeReservation(connection, stored);
                connection.close(LiteErrorCode.CLOSE);
            }
        }
    }

    @Override
    public List<tech.lp2p.core.Connection> connections(PeerId peerId) {
        return new ArrayList<>(serverConnector().getConnections(peerId));
    }

    @Override
    public boolean isGated(PeerId peerId) {
        return isGated.apply(peerId);
    }

    @Override
    public boolean hasConnection(PeerId peerId) {
        return !serverConnector().getConnections(peerId).isEmpty();
    }

    @NonNull
    public String agent() {
        return host.agent();
    }

    @NonNull
    @Override
    public Payloads payloads() {
        return host.payloads();
    }

    @NonNull
    @Override
    public BlockStore blockStore() {
        return blockStore;
    }

    private static class ApplicationProtocolConnection extends ApplicationProtocolConnectionFactory {
        private final Function<PeerId, Boolean> isGated;
        private final Consumer<Peeraddr> connectConsumer;

        private ApplicationProtocolConnection(
                Function<PeerId, Boolean> isGated, Consumer<Peeraddr> connectConsumer) {
            this.isGated = isGated;
            this.connectConsumer = connectConsumer;
        }

        @Override
        public void createConnection(String protocol, ServerConnection connection) throws TransportError {
            PeerId remotePeerId;

            try {
                X509Certificate certificate = connection.remoteCertificate();
                Objects.requireNonNull(certificate);
                remotePeerId = Certificate.extractPeerId(certificate);
                Objects.requireNonNull(remotePeerId);
            } catch (Throwable throwable) {
                throw new TransportError(TransportError.Code.CRYPTO_ERROR,
                        throwable.getMessage());
            }

            // now the remote PeerId is available
            connection.setRemotePeerId(remotePeerId);

            if (isGated.apply(remotePeerId)) {
                throw new TransportError(LiteErrorCode.GATED.code(),
                        "Peer is gated " + remotePeerId);
            }

            try {
                connectConsumer.accept(connection.remotePeeraddr());
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable);
            }

        }
    }
}
