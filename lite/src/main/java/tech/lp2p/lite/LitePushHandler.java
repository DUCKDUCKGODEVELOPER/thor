package tech.lp2p.lite;

import androidx.annotation.NonNull;

import tech.lp2p.Lite;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Handler;
import tech.lp2p.quic.Stream;

public final class LitePushHandler implements Handler {

    private final LiteServer liteServer;

    LitePushHandler(@NonNull LiteServer liteServer) {
        this.liteServer = liteServer;
    }


    @Override
    public void protocol(Stream stream) throws Exception {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        Envelope envelope = LiteService.createEnvelope(liteServer, data);
        stream.response(Lite.DEFAULT_REQUEST_TIMEOUT); // finish stream
        liteServer.push(envelope);
    }

    @Override
    public void closed(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

}
