package tech.lp2p.lite;

import androidx.annotation.NonNull;

import java.util.Objects;

import record.pb.EnvelopeOuterClass;
import tech.lp2p.Lite;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Payload;
import tech.lp2p.quic.Stream;

public final class LitePullHandler implements Handler {
    private final LiteServer liteServer;

    LitePullHandler(@NonNull LiteServer liteServer) {
        this.liteServer = liteServer;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {

        EnvelopeOuterClass.Request request = EnvelopeOuterClass.Request.parseFrom(data);
        Payload payload;
        try {
            int payloadType = Payload.payloadType(
                    request.getPayloadType().toByteArray());
            payload = liteServer.payloads().get(payloadType);
            Objects.requireNonNull(payload);
        } catch (Throwable ignore) {
            // payloadType not known
            stream.response(Lite.DEFAULT_REQUEST_TIMEOUT);
            return;
        }
        Envelope envelope = liteServer.envelope(payload);
        if (envelope != null) {
            stream.response(LiteService.createEnvelope(liteServer, envelope),
                    Lite.DEFAULT_REQUEST_TIMEOUT);
        } else {
            stream.response(Lite.DEFAULT_REQUEST_TIMEOUT); // finish stream
        }

    }

    @Override
    public void closed(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

}
