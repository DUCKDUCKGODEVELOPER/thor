package tech.lp2p.lite;

import androidx.annotation.NonNull;

import java.security.KeyPair;
import java.util.function.Consumer;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Key;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Session;
import tech.lp2p.dht.DhtKademlia;
import tech.lp2p.ident.IdentifyHandler;


public final class LiteSession implements Session {
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final DhtKademlia routing;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Responder responderLibp2p;
    @NonNull
    private final Responder responderLite = new Responder(new Protocols());

    public LiteSession(@NonNull BlockStore blockStore,
                       @NonNull PeerStore peerStore,
                       @NonNull LiteHost host) {
        this.blockStore = blockStore;
        this.host = host;
        this.routing = new DhtKademlia(this, peerStore);

        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));

        this.responderLibp2p = new Responder(libp2p);

    }

    @NonNull
    public Responder responder(@NonNull ALPN alpn) {
        switch (alpn) {
            case lite -> {
                return responderLite;
            }
            case libp2p -> {
                return responderLibp2p;
            }
        }
        throw new IllegalStateException();
    }

    @NonNull
    @Override
    public KeyPair keyPair() {
        return host.keyPair();
    }


    @NonNull
    public Protocols protocols(@NonNull ALPN alpn) {
        return responder(alpn).protocols();
    }

    @NonNull
    @Override
    public BlockStore blockStore() {
        return blockStore;
    }

    @NonNull
    @Override
    public PeerId self() {
        return host.self();
    }

    @Override
    @NonNull
    public Peeraddrs findPeeraddrs(@NonNull Cancellable cancellable, @NonNull PeerId peerId) {
        return routing.findPeeraddrs(cancellable, peerId);
    }

    @Override
    public void providers(@NonNull Cancellable cancellable,
                          @NonNull Consumer<Peeraddr> consumer,
                          @NonNull Key key) {
        routing.providers(cancellable, consumer, key);
    }


    @NonNull
    @Override
    public Certificate certificate() {
        return host.certificate();
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Peeraddr> consumer, @NonNull PeerId peerId) {
        routing.findClosestPeers(cancellable, consumer, peerId);
    }

    @NonNull
    @Override
    public Peeraddrs bootstrap() {
        return host.bootstrap();
    }

    @Override
    public void close() {
        routing.close();
    }

    @NonNull
    @Override
    public Peeraddrs peeraddrs() {
        return new Peeraddrs(); // nothing to publish
    }


    @NonNull
    public String agent() {
        return host.agent();
    }

    @NonNull
    @Override
    public Payloads payloads() {
        return host.payloads();
    }
}

