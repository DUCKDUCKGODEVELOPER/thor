package tech.lp2p.lite;

import tech.lp2p.core.Handler;
import tech.lp2p.quic.Stream;

public final class LiteStreamHandler implements Handler {

    @Override
    public void protocol(Stream stream) {
        if (!stream.isInitiator()) {
            //stream.writeOutput(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL), false);
        }
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        throw new Exception("should not be invoked");
    }

    @Override
    public void closed(Stream stream) {
        // nothing to do here
    }

}
