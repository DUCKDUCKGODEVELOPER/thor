package tech.lp2p.lite;

public enum LiteErrorCode {

    PROTOCOL_NEGOTIATION_FAILED(200, "Protocol negotiation failed"),
    GATED(201, "Connection is gated"),
    CLOSE(202, "Connection is closed"),
    SHUTDOWN(203, "Shutdown the connection"),
    INTERNAL_ERROR(204, "Internal error occurs"),
    LIMIT_REACHED(205, "Limit reached"),
    STREAM_CLOSED(206, "Relayed stream closed");


    private final int code;
    private final String message;

    LiteErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int code() {
        return code;
    }

    public String message() {
        return message;
    }
}
