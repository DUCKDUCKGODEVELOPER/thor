package tech.lp2p.lite;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Objects;

import merkledag.pb.Merkledag;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.dag.DagReader;
import tech.lp2p.dag.DagService;

public record LiteReader(@NonNull DagReader dagReader, @NonNull DagFetch dagFetch) {

    @NonNull
    public static LiteReader getReader(@NonNull Connection connection, @NonNull Cid cid)
            throws Exception {
        LiteFetchManager fetchManager = new LiteFetchManager(connection);
        return LiteReader.createReader(fetchManager, cid);
    }

    @NonNull
    public static LiteReader getReader(@NonNull Session session, @NonNull Cid cid) throws Exception {
        return LiteReader.createReader(new LiteLocalFetch(session), cid);
    }

    public static LiteReader createReader(@NonNull DagFetch dagFetch, @NonNull Cid cid)
            throws Exception {

        Merkledag.PBNode top = DagService.getNode(dagFetch, cid);
        Objects.requireNonNull(top);
        DagReader dagReader = DagReader.create(top);

        return new LiteReader(dagReader, dagFetch);
    }

    @NonNull
    public ByteString loadNextData() throws Exception {
        return dagReader.loadNextData(dagFetch);
    }

    public void seek(int position) throws Exception {
        dagReader.seek(dagFetch, position);
    }

    public long getSize() {
        return this.dagReader.size();
    }
}
