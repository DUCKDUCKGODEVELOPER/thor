package tech.lp2p.lite;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.security.PublicKey;
import java.util.Objects;

import crypto.pb.Crypto;
import fetch.pb.Fetch;
import record.pb.EnvelopeOuterClass;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Host;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Payload;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Session;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.utils.Utils;

public interface LiteService {

    @NonNull
    static Fetch.FetchResponse fetch(@NonNull Connection connection,
                                     @NonNull Fetch.FetchRequest request) throws Exception {
        if (connection.alpn() != ALPN.lite) {
            throw new Exception("ALPN must be lite");
        }
        return Fetch.FetchResponse.parseFrom(StreamRequester.createStream(
                        (tech.lp2p.quic.Connection) connection,
                        Protocol.LITE_FETCH_PROTOCOL.size())
                .request(Utils.createMessage(Protocol.LITE_FETCH_PROTOCOL, request),
                        Lite.GRACE_PERIOD));
    }

    @Nullable
    static Envelope pullEnvelope(@NonNull Session session,
                                 @NonNull Connection connection,
                                 @NonNull Payload payload) throws Exception {
        if (connection.alpn() != ALPN.lite) {
            throw new Exception("ALPN must be lite");
        }

        EnvelopeOuterClass.Request msg = EnvelopeOuterClass.Request.newBuilder()
                .setPayloadType(ByteString.copyFrom(Payload.payloadType(payload))).build();

        byte[] data = StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.LITE_PULL_PROTOCOL.size())
                .request(Utils.createMessage(Protocol.LITE_PULL_PROTOCOL, msg),
                        Lite.DEFAULT_REQUEST_TIMEOUT);
        if (data.length > 0) {
            return LiteService.createEnvelope(session, data);
        }
        return null;

    }


    static void pushEnvelope(@NonNull Session session,
                             @NonNull Connection connection,
                             @NonNull Envelope envelope) throws Exception {
        if (connection.alpn() != ALPN.lite) {
            throw new Exception("ALPN must be lite");
        }
        EnvelopeOuterClass.Envelope msg = createEnvelope(session, envelope);

        StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.LITE_PUSH_PROTOCOL.size())
                .request(Utils.createMessage(Protocol.LITE_PUSH_PROTOCOL, msg),
                        Lite.DEFAULT_REQUEST_TIMEOUT);
    }

    @NonNull
    static Envelope createEnvelope(@NonNull Host host, @NonNull byte[] data) throws Exception {
        EnvelopeOuterClass.Envelope envelope = EnvelopeOuterClass.Envelope.parseFrom(data);
        Crypto.PublicKey publicKey = envelope.getPublicKey();
        PeerId peerId = Keys.fromPublicKey(publicKey);
        Keys keys = Keys.unmarshalPublicKey(publicKey);
        byte[] type = envelope.getPayloadType().toByteArray();
        int payloadType = Payload.payloadType(type);
        Payload payload = host.payloads().get(payloadType);
        Objects.requireNonNull(payload, "payload not known");
        Cid cid = Keys.decodeCid(envelope.getPayload().toByteArray());

        byte[] signature = envelope.getSignature().toByteArray();
        keys.verify(Payload.unsignedEnvelopePayload(payload, cid), signature);

        return new Envelope(peerId, cid, payload);
    }

    @NonNull
    static EnvelopeOuterClass.Envelope createEnvelope(@NonNull Host host,
                                                      @NonNull Envelope envelope)
            throws Exception {
        byte[] payload = Payload.unsignedEnvelopePayload(envelope.payload(), envelope.cid());
        byte[] signature = Keys.sign(host.keyPair().getPrivate(), payload);
        PublicKey publicKey = host.keyPair().getPublic();
        Crypto.PublicKey cryptoKey = Crypto.PublicKey.newBuilder()
                .setType(Crypto.KeyType.Ed25519)
                .setData(ByteString.copyFrom(publicKey.getEncoded()))
                .build();
        return EnvelopeOuterClass.Envelope.newBuilder()
                .setPublicKey(cryptoKey)
                .setPayloadType(ByteString.copyFrom(Payload.payloadType(envelope.payload())))
                .setPayload(ByteString.copyFrom(envelope.cid().encoded()))
                .setSignature(ByteString.copyFrom(signature))
                .build();
    }

}
