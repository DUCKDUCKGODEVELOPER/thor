package tech.lp2p.cert;

import java.io.IOException;

/**
 * Holding class for an X.509 Certificate structure.
 */
public final class X509CertificateHolder {

    private transient Certificate x509Certificate;

    /**
     * Create a X509CertificateHolder from the passed in ASN.1 structure.
     *
     * @param x509Certificate an ASN.1 Certificate structure.
     */
    X509CertificateHolder(Certificate x509Certificate) {
        init(x509Certificate);
    }

    private void init(Certificate x509Certificate) {
        this.x509Certificate = x509Certificate;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof X509CertificateHolder other)) {
            return false;
        }

        return this.x509Certificate.equals(other.x509Certificate);
    }

    /**
     * Return the ASN.1 encoding of this holder's certificate.
     *
     * @return a DER encoded byte array.
     * @throws IOException if an encoding cannot be generated.
     */
    public byte[] getEncoded() throws IOException {
        return x509Certificate.getEncoded();
    }
}
