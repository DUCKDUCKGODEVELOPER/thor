package tech.lp2p.cert;

/**
 * Holding class for a single Relative Distinguished Name (RDN).
 */
public final class RDN extends ASN1Object {
    private final ASN1Set values;

    private RDN(ASN1Set values) {
        this.values = values;
    }

    /**
     * Create a single valued RDN.
     *
     * @param oid   RDN payloadType.
     * @param value RDN value.
     */
    RDN(ASN1ObjectIdentifier oid, ASN1Encodable value) {
        ASN1EncodableVector v = new ASN1EncodableVector(2);

        v.add(oid);
        v.add(value);

        this.values = new DERSet(new DERSequence(v));
    }

    /**
     * Create a multi-valued RDN.
     *
     * @param aAndVs attribute payloadType/value pairs making up the RDN
     */
    RDN(AttributeTypeAndValue[] aAndVs) {
        this.values = new DERSet(aAndVs);
    }

    public static RDN getInstance(Object obj) {
        if (obj instanceof RDN) {
            return (RDN) obj;
        } else if (obj != null) {
            return new RDN(ASN1Set.getInstance(obj));
        }

        return null;
    }

    public boolean isMultiValued() {
        return this.values.size() > 1;
    }

    /**
     * Return the number of AttributeTypeAndValue objects in this RDN,
     *
     * @return size of RDN, greater than 1 if multi-valued.
     */
    public int size() {
        return this.values.size();
    }

    public AttributeTypeAndValue getFirst() {
        if (this.values.size() == 0) {
            return null;
        }

        return AttributeTypeAndValue.getInstance(this.values.getObjectAt(0));
    }

    public AttributeTypeAndValue[] getTypesAndValues() {
        AttributeTypeAndValue[] tmp = new AttributeTypeAndValue[values.size()];

        for (int i = 0; i != tmp.length; i++) {
            tmp[i] = AttributeTypeAndValue.getInstance(values.getObjectAt(i));
        }

        return tmp;
    }

    /**
     * <pre>
     * RelativeDistinguishedName ::=
     *                     SET OF AttributeTypeAndValue
     *
     * AttributeTypeAndValue ::= SEQUENCE {
     *        payloadType     AttributeType,
     *        value    AttributeValue }
     * </pre>
     *
     * @return this object as its ASN1Primitive payloadType
     */
    public ASN1Primitive toASN1Primitive() {
        return values;
    }
}
