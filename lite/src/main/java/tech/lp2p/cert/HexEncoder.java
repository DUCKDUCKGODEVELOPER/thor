package tech.lp2p.cert;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * A streaming Hex encoder.
 */
final class HexEncoder {
    private final byte[] encodingTable =
            {
                    (byte) '0', (byte) '1', (byte) '2', (byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
                    (byte) '8', (byte) '9', (byte) 'a', (byte) 'b', (byte) 'c', (byte) 'd', (byte) 'e', (byte) 'f'
            };

    /*
     * set up the decoding table.
     */
    private final byte[] decodingTable = new byte[128];

    HexEncoder() {
        initialiseDecodingTable();
    }


    private void initialiseDecodingTable() {
        Arrays.fill(decodingTable, (byte) 0xff);

        for (int i = 0; i < encodingTable.length; i++) {
            decodingTable[encodingTable[i]] = (byte) i;
        }

        decodingTable['A'] = decodingTable['a'];
        decodingTable['B'] = decodingTable['b'];
        decodingTable['C'] = decodingTable['c'];
        decodingTable['D'] = decodingTable['d'];
        decodingTable['E'] = decodingTable['e'];
        decodingTable['F'] = decodingTable['f'];
    }

    private int encode(byte[] inBuf, int inOff, int inLen, byte[] outBuf) {
        int inPos = inOff;
        int inEnd = inOff + inLen;
        int outPos = 0;

        while (inPos < inEnd) {
            int b = inBuf[inPos++] & 0xFF;

            outBuf[outPos++] = encodingTable[b >>> 4];
            outBuf[outPos++] = encodingTable[b & 0xF];
        }

        return outPos;
    }

    /**
     * encode the input data producing a Hex output stream.
     */
    public void encode(byte[] buf, int off, int len, OutputStream out)
            throws IOException {
        if (len < 0) {
            return;
        }

        byte[] tmp = new byte[72];
        int remaining = len;
        while (remaining > 0) {
            int inLen = Math.min(36, remaining);
            int outLen = encode(buf, off, inLen, tmp);
            out.write(tmp, 0, outLen);
            off += inLen;
            remaining -= inLen;
        }
    }

    byte[] decodeStrict(String str, int off, int len) throws IOException {
        if (null == str) {
            throw new NullPointerException("'str' cannot be null");
        }
        if (off < 0 || len < 0 || off > (str.length() - len)) {
            throw new IndexOutOfBoundsException("invalid offset and/or length specified");
        }
        if (0 != (len & 1)) {
            throw new IOException("a hexadecimal encoding must have an even number of characters");
        }

        int resultLen = len >>> 1;
        byte[] result = new byte[resultLen];

        int strPos = off;
        for (int i = 0; i < resultLen; ++i) {
            byte b1 = decodingTable[str.charAt(strPos++)];
            byte b2 = decodingTable[str.charAt(strPos++)];

            int n = (b1 << 4) | b2;
            if (n < 0) {
                throw new IOException("invalid characters encountered in Hex string");
            }

            result[i] = (byte) n;
        }
        return result;
    }
}
