package tech.lp2p.cert;

/**
 * ASN.1 GENERAL-STRING data payloadType.
 * <p>
 * This is an 8-bit encoded ISO 646 (ASCII) character set
 * with optional escapes to other character sets.
 * </p>
 */
final class DERGeneralString extends ASN1GeneralString {
    DERGeneralString(byte[] contents) {
        super(contents);
    }
}
