package tech.lp2p.cert;

/**
 * DER T61String (also the teletex string), try not to use this if you don't need to.
 * The standard support the encoding for this has been withdrawn.
 */
final class DERT61String extends ASN1T61String {
    DERT61String(byte[] contents) {
        super(contents);
    }
}
