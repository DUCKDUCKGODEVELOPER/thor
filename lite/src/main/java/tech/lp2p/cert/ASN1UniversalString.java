package tech.lp2p.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * ASN.1 UniversalString object - encodes UNICODE (ISO 10646) characters using 32-bit format. In Java we
 * have no way of representing this directly so we rely on byte arrays to carry these.
 */
public abstract class ASN1UniversalString extends ASN1Primitive implements ASN1String {

    private static final char[] table = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private final byte[] contents;

    ASN1UniversalString(byte[] contents) {
        this.contents = contents;
    }

    static ASN1UniversalString createPrimitive(byte[] contents) {
        return new DERUniversalString(contents);
    }

    private static void encodeHexByte(StringBuffer buf, int i) {
        buf.append(table[(i >>> 4) & 0xF]);
        buf.append(table[i & 0xF]);
    }

    private static void encodeHexDL(StringBuffer buf, int dl) {
        if (dl < 128) {
            encodeHexByte(buf, dl);
            return;
        }

        byte[] stack = new byte[5];
        int pos = 5;

        do {
            stack[--pos] = (byte) dl;
            dl >>>= 8;
        }
        while (dl != 0);

        int count = stack.length - pos;
        stack[--pos] = (byte) (0x80 | count);

        do {
            encodeHexByte(buf, stack[pos++]);
        }
        while (pos < stack.length);
    }

    public final String getString() {
        int dl = contents.length;
        StringBuffer buf = new StringBuffer(3 + 2 * (ASN1OutputStream.getLengthOfDL(dl) + dl));
        buf.append("#1C");
        encodeHexDL(buf, dl);

        for (byte content : contents) {
            encodeHexByte(buf, content);
        }

        return buf.toString();
    }

    @NonNull
    public String toString() {
        return getString();
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.UNIVERSAL_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1UniversalString that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }

}
