package tech.lp2p.cert;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.Iterator;

/**
 * ASN.1 <code>SEQUENCE</code> and <code>SEQUENCE OF</code> constructs.
 * <p>
 * DER form is always definite form length fields, while
 * BER support uses indefinite form.
 * <hr>
 * <p><b>X.690</b></p>
 * <p><b>8: Basic encoding rules</b></p>
 * <p><b>8.9 Encoding of a sequence value </b></p>
 * 8.9.1 The encoding of a sequence value shall be constructed.
 * <p>
 * <b>8.9.2</b> The contents octets shall consist of the complete
 * encoding of one data value from each of the types listed in
 * the ASN.1 definition of the sequence payloadType, in the order of
 * their appearance in the definition, unless the payloadType was referenced
 * with the keyword <b>OPTIONAL</b> or the keyword <b>DEFAULT</b>.
 * </p><p>
 * <b>8.9.3</b> The encoding of a data value may, but need not,
 * be present for a payloadType which was referenced with the keyword
 * <b>OPTIONAL</b> or the keyword <b>DEFAULT</b>.
 * If present, it shall appear in the encoding at the point
 * corresponding to the appearance of the payloadType in the ASN.1 definition.
 * </p><p>
 * <b>8.10 Encoding of a sequence-of value </b>
 * </p><p>
 * <b>8.10.1</b> The encoding of a sequence-of value shall be constructed.
 * <p>
 * <b>8.10.2</b> The contents octets shall consist of zero,
 * one or more complete encodings of data values from the payloadType listed in
 * the ASN.1 definition.
 * <p>
 * <b>8.10.3</b> The order of the encodings of the data values shall be
 * the same as the order of the data values in the sequence-of value to
 * be encoded.
 * </p>
 * <p><b>9: Canonical encoding rules</b></p>
 * <p><b>9.1 Length forms</b></p>
 * If the encoding is constructed, it shall employ the indefinite-length form.
 * If the encoding is primitive, it shall include the fewest length octets necessary.
 * [Contrast with 8.1.3.2 b).]
 *
 * <p><b>11: Restrictions on BER employed by both CER and DER</b></p>
 * <p><b>11.5 Set and sequence components with default value</b></p>
 * <p>
 * The encoding of a set value or sequence value shall not include
 * an encoding for any component value which is equal to
 * its default value.
 * </p>
 */
public abstract class ASN1Sequence extends ASN1Primitive implements Iterable<ASN1Encodable> {

    /**
     * @noinspection AnonymousInnerClass
     */
    private static final ASN1UniversalType TYPE = new ASN1UniversalType(ASN1Sequence.class) {
    };
    // NOTE: Only non-final to support LazyEncodedSequence
    ASN1Encodable[] elements;

    /**
     * Create an empty SEQUENCE
     */
    ASN1Sequence() {
        this.elements = ASN1EncodableVector.EMPTY_ELEMENTS;
    }

    /**
     * Create a SEQUENCE containing a vector of objects.
     *
     * @param elementVector the vector of objects to be put in the SEQUENCE.
     */
    ASN1Sequence(ASN1EncodableVector elementVector) {
        if (null == elementVector) {
            throw new NullPointerException("'elementVector' cannot be null");
        }

        this.elements = elementVector.takeElements();
    }

    /**
     * Create a SEQUENCE containing an array of objects.
     *
     * @param elements the array of objects to be put in the SEQUENCE.
     */
    ASN1Sequence(ASN1Encodable[] elements) {
        if (Arrays.isNullOrContainsNull(elements)) {
            throw new NullPointerException("'elements' cannot be null, or contain null");
        }

        this.elements = ASN1EncodableVector.cloneElements(elements);
    }

    /**
     * Return an ASN1Sequence from the given object.
     *
     * @param obj the object we want converted.
     * @return an ASN1Sequence instance, or null.
     * @throws IllegalArgumentException if the object cannot be converted.
     */
    public static ASN1Sequence getInstance(Object obj) {
        if (obj == null || obj instanceof ASN1Sequence) {
            return (ASN1Sequence) obj;
        } else if (obj instanceof ASN1Encodable) {
            ASN1Primitive primitive = ((ASN1Encodable) obj).toASN1Primitive();
            if (primitive instanceof ASN1Sequence) {
                return (ASN1Sequence) primitive;
            }
        } else if (obj instanceof byte[]) {
            try {
                return (ASN1Sequence) TYPE.fromByteArray((byte[]) obj);
            } catch (IOException e) {
                throw new IllegalArgumentException("failed to construct sequence from byte[]: " + e.getMessage());
            }
        }

        throw new IllegalArgumentException("unknown object in getInstance: " + obj.getClass().getName());
    }

    /**
     * Return an ASN1 SEQUENCE from a tagged object. There is a special
     * case here, if an object appears to have been explicitly tagged on
     * reading but we were expecting it to be implicitly tagged in the
     * normal course of events it indicates that we lost the surrounding
     * sequence - so we need to add it back (this will happen if the tagged
     * object is a sequence that contains other sequences). If you are
     * dealing with implicitly tagged sequences you really <b>should</b>
     * be using this method.
     *
     * @param taggedObject the tagged object.
     * @param explicit     true if the object is meant to be explicitly tagged,
     *                     false otherwise.
     * @return an ASN1Sequence instance.
     * @throws IllegalArgumentException if the tagged object cannot
     *                                  be converted.
     */
    public static ASN1Sequence getInstance(ASN1TaggedObject taggedObject, boolean explicit) {
        return (ASN1Sequence) TYPE.getContextInstance(taggedObject, explicit);
    }

    public ASN1Encodable[] toArrayInternal() {
        return elements;
    }


    /**
     * Return the object at the sequence position indicated by index.
     *
     * @param index the sequence number (starting at zero) of the object
     * @return the object at the sequence position indicated by index.
     */
    public ASN1Encodable getObjectAt(int index) {
        return elements[index];
    }

    /**
     * Return the number of objects in this sequence.
     *
     * @return the number of objects in this sequence.
     */
    public int size() {
        return elements.length;
    }


    boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1Sequence that)) {
            return false;
        }

        // NOTE: Call size() here (on both) to 'force' a LazyEncodedSequence
        int count = this.size();
        if (that.size() != count) {
            return false;
        }

        for (int i = 0; i < count; ++i) {
            ASN1Primitive p1 = this.elements[i].toASN1Primitive();
            ASN1Primitive p2 = that.elements[i].toASN1Primitive();

            if (p1 != p2 && !p1.asn1Equals(p2)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Change current SEQUENCE object to be encoded as {@link DERSequence}.
     * This is part of Distinguished Encoding Rules form serialization.
     */
    ASN1Primitive toDERObject() {
        return new DERSequence(elements);
    }

    /**
     * Change current SEQUENCE object to be encoded as {@link DLSequence}.
     * This is part of Direct Length form serialization.
     */
    ASN1Primitive toDLObject() {
        return new DLSequence(elements);
    }

    boolean encodeConstructed() {
        return true;
    }

    @NonNull
    public String toString() {
        // NOTE: Call size() here to 'force' a LazyEncodedSequence
        int count = size();
        if (0 == count) {
            return "[]";
        }

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; ; ) {
            sb.append(elements[i]);
            if (++i >= count) {
                break;
            }
            sb.append(", ");
        }
        sb.append(']');
        return sb.toString();
    }

    @NonNull
    public Iterator<ASN1Encodable> iterator() {
        return new Arrays.Iterator<>(elements);
    }

}
