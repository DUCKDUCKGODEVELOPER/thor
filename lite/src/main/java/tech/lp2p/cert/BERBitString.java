package tech.lp2p.cert;

import java.io.IOException;

final class BERBitString extends ASN1BitString {
    private static final int DEFAULT_SEGMENT_LIMIT = 1000;

    private final int segmentLimit;
    private final ASN1BitString[] elements;


    BERBitString(byte[] data, int padBits) {
        super(data, padBits);
        this.elements = null;
        this.segmentLimit = DEFAULT_SEGMENT_LIMIT;
    }

    BERBitString(ASN1BitString[] elements) {
        super(flattenBitStrings(elements));
        this.elements = elements;
        this.segmentLimit = DEFAULT_SEGMENT_LIMIT;
    }

    /**
     * Convert a vector of bit strings into a single bit string
     */
    static byte[] flattenBitStrings(ASN1BitString[] bitStrings) {
        int count = bitStrings.length;
        switch (count) {
            case 0 -> {
                // No bits
                return new byte[]{0};
            }
            case 1 -> {
                return bitStrings[0].contents;
            }
            default -> {
                int last = count - 1, totalLength = 0;
                for (int i = 0; i < last; ++i) {
                    byte[] elementContents = bitStrings[i].contents;
                    if (elementContents[0] != 0) {
                        throw new IllegalArgumentException("only the last nested bitstring can have padding");
                    }

                    totalLength += elementContents.length - 1;
                }

                // Last one can have padding
                byte[] lastElementContents = bitStrings[last].contents;
                byte padBits = lastElementContents[0];
                totalLength += lastElementContents.length;

                byte[] contents = new byte[totalLength];
                contents[0] = padBits;

                int pos = 1;
                for (ASN1BitString bitString : bitStrings) {
                    byte[] elementContents = bitString.contents;
                    int length = elementContents.length - 1;
                    System.arraycopy(elementContents, 1, contents, pos, length);
                    pos += length;
                }

//            assert pos == totalLength;
                return contents;
            }
        }
    }

    boolean encodeConstructed() {
        return null != elements || contents.length > segmentLimit;
    }

    int encodedLength(boolean withTag)
            throws IOException {
        if (!encodeConstructed()) {
            return DLBitString.encodedLength(withTag, contents.length);
        }

        int totalLength = withTag ? 4 : 3;

        if (null != elements) {
            for (ASN1BitString element : elements) {
                totalLength += element.encodedLength(true);
            }
        } else if (contents.length >= 2) {
            int extraSegments = (contents.length - 2) / (segmentLimit - 1);
            totalLength += extraSegments * DLBitString.encodedLength(true, segmentLimit);

            int lastSegmentLength = contents.length - (extraSegments * (segmentLimit - 1));
            totalLength += DLBitString.encodedLength(true, lastSegmentLength);
        } //  else case No bits

        return totalLength;
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        if (!encodeConstructed()) {
            DLBitString.encode(out, withTag, contents, contents.length);
            return;
        }

        out.writeIdentifier(withTag, BERTags.CONSTRUCTED | BERTags.BIT_STRING);
        out.write(0x80);

        if (null != elements) {
            out.writePrimitives(elements);
        } else if (contents.length >= 2) {
            byte pad = contents[0];
            int length = contents.length;
            int remaining = length - 1;
            int segmentLength = segmentLimit - 1;

            while (remaining > segmentLength) {
                DLBitString.encode(out, (byte) 0, contents, length - remaining, segmentLength);
                remaining -= segmentLength;
            }

            DLBitString.encode(out, pad, contents, length - remaining, remaining);
        } //  else case No bits

        out.write(0x00);
        out.write(0x00);
    }
}

