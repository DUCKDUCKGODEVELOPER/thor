package tech.lp2p.dht;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import dht.pb.Dht;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Host;
import tech.lp2p.core.Key;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Routing;
import tech.lp2p.core.Server;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.utils.Utils;


public final class DhtKademlia implements Routing, AutoCloseable {

    private static final String TAG = DhtKademlia.class.getSimpleName();

    @NonNull
    private final DhtRoutingTable dhtRoutingTable;
    @NonNull
    private final Host host;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final ReentrantLock lock = new ReentrantLock();
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public DhtKademlia(@NonNull Host host, @NonNull PeerStore peerStore) {
        this.host = host;
        this.peerStore = peerStore;
        this.dhtRoutingTable = new DhtRoutingTable(ConcurrentHashMap.newKeySet());
    }

    @NonNull
    private static List<DhtPeer> evalClosestPeers(@NonNull Dht.Message pms) {

        List<DhtPeer> dhtPeers = new ArrayList<>();

        for (Dht.Message.Peer entry : pms.getCloserPeersList()) {

            PeerId peerId = PeerId.create(entry.getId().toByteArray());

            Peeraddr resolved = Peeraddrs.reduceToOne(peerId, entry.getAddrsList());
            if (resolved != null) {
                try {
                    dhtPeers.add(DhtPeer.create(resolved, true));
                } catch (Throwable throwable) {
                    Utils.error(TAG, throwable);
                }
            }

        }
        return dhtPeers;
    }

    private void bootstrap() {
        lock.lock();
        try {
            if (dhtRoutingTable.isEmpty()) {
                Peeraddrs peeraddrs = host.bootstrap();
                for (Peeraddr peer : peeraddrs) {
                    try {
                        dhtRoutingTable.addPeer(DhtPeer.create(peer, false));
                    } catch (Throwable throwable) {
                        Utils.error(TAG, throwable);
                    }
                }
                List<Peeraddr> randomPeers = peerStore.randomPeeraddrs(Lite.DHT_TABLE_SIZE);

                for (Peeraddr peeraddr : randomPeers) {
                    try {
                        dhtRoutingTable.addPeer(DhtPeer.create(peeraddr, false));
                    } catch (Throwable throwable) {
                        Utils.error(TAG, throwable);
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Peeraddr> consumer,
                                 @NonNull PeerId peerId) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            Key key = Key.convertKey(peerId.multihash());
            Set<Peeraddr> handled = ConcurrentHashMap.newKeySet();

            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);
            getClosestPeers(done, key, findNodeMessage, peers -> {
                for (DhtPeer dhtPeer : peers) {
                    if (handled.add(dhtPeer.peeraddr())) {
                        consumer.accept(dhtPeer.peeraddr());
                    }
                }
            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
        }

    }

    private void getClosestPeers(@NonNull Cancellable cancellable, @NonNull Key key,
                                 @NonNull Dht.Message message, @NonNull Consumer<List<DhtPeer>> channel)
            throws InterruptedException {

        runQuery(cancellable, key, (ctx1, peer) -> {

            Dht.Message pms = request(ctx1, peer, message);

            List<DhtPeer> dhtPeers = evalClosestPeers(pms);

            channel.accept(dhtPeers);

            return dhtPeers;
        });

    }


    @Override
    public void providers(@NonNull Cancellable cancellable,
                          @NonNull Consumer<Peeraddr> consumer,
                          @NonNull Key key) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            Dht.Message message = DhtService.createGetProvidersMessage(key);

            Set<PeerId> handled = ConcurrentHashMap.newKeySet();
            runQuery(done, key, (ctx, p) -> {

                Dht.Message pms = request(ctx, p, message);

                List<Dht.Message.Peer> list = pms.getProviderPeersList();

                for (Dht.Message.Peer entry : list) {
                    if (entry.getAddrsCount() > 0) {
                        PeerId peerId = PeerId.create(entry.getId().toByteArray());
                        // contains circuit addresses
                        Peeraddr peeraddr = Peeraddrs.reduceToOne(peerId, entry.getAddrsList());
                        if (peeraddr != null) {
                            if (handled.add(peeraddr.peerId())) {
                                consumer.accept(peeraddr);
                            }
                        }
                    }
                }

                return evalClosestPeers(pms);

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
        }
    }

    void addToRouting(@NonNull DhtQueryPeer dhtQueryPeer) {

        DhtPeer dhtPeer = dhtQueryPeer.dhtPeer();
        boolean added = dhtRoutingTable.addPeer(dhtPeer);


        // only replaceable peer are adding
        // (the replaceable peers are initial the routing peers)
        if (added && dhtPeer.replaceable()) {
            peerStore.storePeeraddr(dhtPeer.peeraddr());
        }
    }


    public void provideKey(@NonNull Cancellable cancellable,
                           @NonNull Server server,
                           @NonNull Consumer<Peeraddr> consumer,
                           @NonNull Key key) {


        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();


            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);
            Dht.Message message = DhtService.createAddProviderMessage(server, key);

            Set<DhtPeer> handled = ConcurrentHashMap.newKeySet();

            getClosestPeers(done, key, findNodeMessage, peers -> {

                for (DhtPeer dhtPeer : peers) {
                    if (handled.add(dhtPeer)) {

                        Connection connection = null;
                        try {
                            if (cancellable.isCancelled()) {
                                throw new InterruptedException();
                            }
                            connection = ConnectionBuilder.connect(host, dhtPeer.peeraddr(),
                                    Parameters.create(ALPN.libp2p),
                                    host.certificate(), host.responder(ALPN.libp2p));


                            DhtService.message(connection, message);

                            // success assumed
                            consumer.accept(connection.remotePeeraddr());

                        } catch (Throwable throwable) {
                            Utils.error(TAG, throwable.getClass().getSimpleName());
                        } finally {
                            if (connection != null) {
                                connection.close();
                            }
                        }

                    }
                }

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception here
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
        }
    }


    @NonNull
    private Dht.Message request(@NonNull Cancellable cancellable, @NonNull DhtPeer dhtPeer,
                                @NonNull Dht.Message message)
            throws InterruptedException, ConnectException, TimeoutException {


        if (cancellable.isCancelled()) {
            throw new InterruptedException();
        }

        Connection connection = null;
        try {
            connection = ConnectionBuilder.connect(host, dhtPeer.peeraddr(),
                    Parameters.create(ALPN.libp2p), host.certificate(),
                    host.responder(ALPN.libp2p));
            return DhtService.request(connection, message);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    @NonNull
    @Override
    public Peeraddrs findPeeraddrs(@NonNull Cancellable cancellable, @NonNull PeerId id) {

        Peeraddrs result = new Peeraddrs();
        try {
            AtomicBoolean isFound = new AtomicBoolean(false);
            Cancellable done = () -> cancellable.isCancelled() || isFound.get() || isClosed();
            bootstrap();

            Key key = Key.convertKey(id.multihash());
            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);


            runQuery(done, key, (ctx, peer) -> {

                Dht.Message pms = request(ctx, peer, findNodeMessage);

                List<DhtPeer> dhtPeers = new ArrayList<>();
                List<Dht.Message.Peer> list = pms.getCloserPeersList();
                for (Dht.Message.Peer entry : list) {

                    PeerId peerId = PeerId.create(entry.getId().toByteArray());

                    if (Objects.equals(peerId, id)) {
                        Peeraddrs peeraddrs = Peeraddr.create(peerId, entry.getAddrsList());
                        if (!peeraddrs.isEmpty()) {
                            result.addAll(peeraddrs);
                            isFound.set(true); // done
                            return Collections.emptyList();
                        }
                    } else {
                        Peeraddr resolved = Peeraddrs.reduceToOne(peerId, entry.getAddrsList());
                        if (resolved != null) {
                            try {
                                dhtPeers.add(DhtPeer.create(resolved, true));
                            } catch (Throwable throwable) {
                                Utils.error(TAG, throwable);
                            }
                        }
                    }
                }
                return dhtPeers;

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
        }
        return result;
    }

    private void runQuery(@NonNull Cancellable cancellable, @NonNull Key key,
                          @NonNull QueryFunc queryFn) throws InterruptedException {

        // pick the K closest peers to the key in our Routing table.
        List<DhtQueryPeer> peers = dhtRoutingTable.nearestPeers(key);
        DhtQuery.runQuery(this, cancellable, key, peers, queryFn);

    }

    @NonNull
    public Peeraddrs nearestPeers(@NonNull Key key) {
        Peeraddrs peeraddrs = new Peeraddrs();
        List<DhtQueryPeer> peers = dhtRoutingTable.nearestPeers(key);
        for (DhtQueryPeer peer : peers) {
            peeraddrs.add(peer.dhtPeer().peeraddr());
        }
        return peeraddrs;
    }

    void removeFromRouting(DhtQueryPeer peer, boolean removeBootstrap) {
        boolean result = dhtRoutingTable.removePeer(peer);
        if (result) {
            Utils.debug(TAG, "Remove from routing " + peer);
        }
        if (removeBootstrap) {
            // remove from bootstrap
            peerStore.removePeeraddr(peer.dhtPeer().peeraddr());
        }
    }

    @Override
    public void close() {
        dhtRoutingTable.clear();
        closed.set(true);
    }

    public boolean isClosed() {
        return closed.get();
    }


    public interface QueryFunc {
        @NonNull
        List<DhtPeer> query(@NonNull Cancellable cancellable, @NonNull DhtPeer dhtPeer)
                throws InterruptedException, ConnectException, TimeoutException;
    }
}
