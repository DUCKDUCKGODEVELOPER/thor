package tech.lp2p.dht;

import androidx.annotation.NonNull;

import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;

public record DhtPeer(Key key, Peeraddr peeraddr, boolean replaceable) {

    @NonNull
    public static DhtPeer create(@NonNull Peeraddr peeraddr, boolean replaceable) throws Exception {
        Key key = Key.convertKey(peeraddr.peerId().multihash());
        return new DhtPeer(key, peeraddr, replaceable);
    }

    @NonNull
    public Key key() {
        return key;
    }

    @NonNull
    public Peeraddr peeraddr() {
        return peeraddr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DhtPeer dhtPeer = (DhtPeer) o;
        return key.equals(dhtPeer.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode(); // ok, checked, maybe opt
    }

    @Override
    public boolean replaceable() {
        return replaceable;
    }


}
