package tech.lp2p.dht;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Key;
import tech.lp2p.utils.Utils;


interface DhtQuery {

    String TAG = DhtQuery.class.getSimpleName();

    private static List<DhtQueryPeer> transform(@NonNull Key key, @NonNull List<DhtPeer> dhtPeers) {
        List<DhtQueryPeer> result = new ArrayList<>();
        dhtPeers.forEach(peer -> result.add(DhtQueryPeer.create(peer, key)));
        return result;
    }

    static void runQuery(@NonNull DhtKademlia dht, @NonNull Cancellable cancellable,
                         @NonNull Key key, @NonNull List<DhtQueryPeer> peers,
                         @NonNull DhtKademlia.QueryFunc queryFn) throws InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(Lite.DHT_ALPHA);
        try {
            ConcurrentSkipListSet<DhtQueryPeer> dhtQueryPeers = new ConcurrentSkipListSet<>();
            enhanceSet(dhtQueryPeers, peers);

            iteration(dht, executorService, dhtQueryPeers, cancellable, key, queryFn);

            while (!cancellable.isCancelled()) {
                Thread.sleep(1000); // todo
            }
            Utils.error(TAG, "Termination");
        } finally {
            executorService.shutdown();
            executorService.shutdownNow();
        }
    }

    private static boolean enhanceSet(ConcurrentSkipListSet<DhtQueryPeer> dhtQueryPeers,
                                      List<DhtQueryPeer> peers) {
        // the peers in query update are added to the queryPeers
        boolean enhanceSet = false;
        for (DhtQueryPeer peer : peers) {
            boolean result = dhtQueryPeers.add(peer);  // set initial state to PeerHeard
            if (result) {
                enhanceSet = true; // element was added
            }
        }
        return enhanceSet;
    }


    static void iteration(DhtKademlia dht, ExecutorService executorService,
                          ConcurrentSkipListSet<DhtQueryPeer> dhtQueryPeers, Cancellable cancellable, Key key,
                          DhtKademlia.QueryFunc queryFn) throws InterruptedException {

        if (cancellable.isCancelled()) {
            executorService.shutdown();
            throw new InterruptedException("operation canceled");
        }

        List<DhtQueryPeer> nextPeersToQuery = nextHeardPeers(dhtQueryPeers, Lite.DHT_CONCURRENCY);

        for (DhtQueryPeer dhtQueryPeer : nextPeersToQuery) {
            dhtQueryPeer.setState(DhtPeerState.PeerWaiting);
            try {
                if (!executorService.isShutdown()) {
                    executorService.execute(() -> {
                        try {
                            List<DhtPeer> newDhtPeers = queryFn.query(cancellable, dhtQueryPeer.dhtPeer());
                            boolean enhancedSet = enhanceSet(dhtQueryPeers, transform(key, newDhtPeers));
                            dhtQueryPeer.setState(DhtPeerState.PeerQueried);
                            // query successful, try to add to routing table
                            if (enhancedSet) {
                                // only when the query peer is returning something
                                // it will be added to the routing table
                                dht.addToRouting(dhtQueryPeer);
                            }

                            iteration(dht, executorService, dhtQueryPeers, cancellable,
                                    key, queryFn);

                        } catch (InterruptedException ignore) {
                            executorService.shutdown();
                        } catch (ConnectException connectException) {
                            dht.removeFromRouting(dhtQueryPeer, false);
                            dhtQueryPeer.setState(DhtPeerState.PeerUnreachable);
                        } catch (TimeoutException timeoutException) {
                            dht.removeFromRouting(dhtQueryPeer, true);
                            dhtQueryPeer.setState(DhtPeerState.PeerUnreachable);
                        } catch (Throwable throwable) {
                            Utils.error(TAG, throwable);
                            dht.removeFromRouting(dhtQueryPeer, true);
                            dhtQueryPeer.setState(DhtPeerState.PeerUnreachable);
                        } finally {
                            if (isDone(dhtQueryPeers)) {
                                Utils.debug(TAG, "Query peers done");
                                executorService.shutdown();
                            }
                        }
                    });
                }
            } catch (RejectedExecutionException ignore) {
                // standard exception
            }
        }

    }

    static boolean isDone(ConcurrentSkipListSet<DhtQueryPeer> dhtQueryPeers) {
        for (DhtQueryPeer dhtQueryPeer : dhtQueryPeers) {
            DhtPeerState state = dhtQueryPeer.getState();
            if (state == DhtPeerState.PeerHeard || state == DhtPeerState.PeerWaiting) {
                return false;
            }
        }
        return true;
    }

    static List<DhtQueryPeer> nextHeardPeers(ConcurrentSkipListSet<DhtQueryPeer> dhtQueryPeers, int maxLength) {
        // The peers we query next should be ones that we have only Heard about.
        List<DhtQueryPeer> peers = new ArrayList<>();
        int count = 0;
        for (DhtQueryPeer dhtQueryPeer : dhtQueryPeers) {
            if (dhtQueryPeer.getState() == DhtPeerState.PeerHeard) {
                peers.add(dhtQueryPeer);
                count++;
                if (count == maxLength) {
                    break;
                }
            }
        }
        return peers;

    }

}
