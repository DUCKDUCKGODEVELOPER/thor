package tech.lp2p.dht;


import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import dht.pb.Dht;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Server;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.utils.Utils;

public interface DhtService {

    @NonNull
    static Dht.Message createFindNodeMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    static Dht.Message createGetProvidersMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    static Dht.Message createAddProviderMessage(Server server, Key key) {

        Dht.Message.Builder builder = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.ADD_PROVIDER)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0);

        Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                .setId(ByteString.copyFrom(server.self().multihash()));
        for (Peeraddr peeraddr : server.peeraddrs()) {
            peerBuilder.addAddrs(ByteString.copyFrom(peeraddr.encoded()));
        }
        builder.addProviderPeers(peerBuilder.build());

        return builder.build();
    }


    @NonNull
    static Peeraddrs providers(Connection connection, Key key) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("wrong ALPN usage");
        }

        Dht.Message message = DhtService.createGetProvidersMessage(key);

        byte[] raw = StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.size())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);
        Peeraddrs peeraddrs = new Peeraddrs();

        byte[] data = StreamRequester.data(ByteBuffer.wrap(raw));
        Dht.Message pms = Dht.Message.parseFrom(data);

        List<Dht.Message.Peer> list = pms.getProviderPeersList();
        for (Dht.Message.Peer entry : list) {
            PeerId peerId = PeerId.create(entry.getId().toByteArray());
            Peeraddr resolved = Peeraddrs.reduceToOne(peerId, entry.getAddrsList());
            if (resolved != null) {
                peeraddrs.add(resolved);
            }
        }

        return peeraddrs;
    }

    static Peeraddrs findPeer(Connection connection, Key key) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("wrong ALPN usage");
        }

        Dht.Message message = DhtService.createFindNodeMessage(key);


        byte[] raw = StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.size())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);
        byte[] data = StreamRequester.data(ByteBuffer.wrap(raw));
        Dht.Message pms = Dht.Message.parseFrom(data);

        Peeraddrs peeraddrs = new Peeraddrs();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();
        for (Dht.Message.Peer entry : list) {
            PeerId peerId = PeerId.create(entry.getId().toByteArray());
            Peeraddr resolved = Peeraddrs.reduceToOne(peerId, entry.getAddrsList());
            if (resolved != null) {
                peeraddrs.add(resolved);
            }
        }
        return peeraddrs;
    }

    static void message(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ExecutionException {
        StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.size())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_MESSAGE_TIMEOUT);
    }

    @NonNull
    static Dht.Message request(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ConnectException {

        byte[] raw = StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.size())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);
        try {
            byte[] data = StreamRequester.data(ByteBuffer.wrap(raw));
            return Dht.Message.parseFrom(data);
        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage());
        }

    }

}
