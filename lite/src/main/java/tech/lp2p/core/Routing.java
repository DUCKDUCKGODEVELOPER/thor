package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

public interface Routing {

    void providers(@NonNull Cancellable cancellable,
                   @NonNull Consumer<Peeraddr> consumer,
                   @NonNull Key key);

    @NonNull
    Peeraddrs findPeeraddrs(@NonNull Cancellable cancellable, @NonNull PeerId peerId);

    void findClosestPeers(@NonNull Cancellable cancellable,
                          @NonNull Consumer<Peeraddr> consumer,
                          @NonNull PeerId peerId);

}
