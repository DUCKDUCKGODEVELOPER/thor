package tech.lp2p.core;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.CodedInputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.Objects;

import tech.lp2p.utils.Utils;

// https://github.com/multiformats/multicodec/blob/master/table.csv (see multiaddr)
record Multiaddr(int code, int size, String type) {

    static final Multiaddr IP4 = new Multiaddr(4, 32, "ip4");
    static final Multiaddr IP6 = new Multiaddr(41, 128, "ip6");
    static final Multiaddr UDP = new Multiaddr(273, 16, "udp");
    static final Multiaddr P2P = new Multiaddr(421, -1, "p2p");
    static final Multiaddr QUICV1 = new Multiaddr(461, 0, "quic-v1");

    @NonNull
    static Multiaddr lookup(String name) {
        return switch (name) {
            case "ip4" -> IP4;
            case "ip6" -> IP6;
            case "udp" -> UDP;
            case "p2p" -> P2P;
            case "quic-v1" -> QUICV1;
            default -> throw new IllegalArgumentException("unsupported protocol " + name);
        };
    }

    @Nullable
    static Multiaddr lookup(int code) {
        return switch (code) {
            case 4 -> IP4;
            case 41 -> IP6;
            case 273 -> UDP;
            case 421 -> P2P;
            case 461 -> QUICV1;
            default -> null;
        };
    }


    private static void putUvarint(byte[] buf, long x) {
        int i = 0;
        while (x >= 0x80) {
            buf[i] = (byte) (x | 0x80);
            x >>= 7;
            i++;
        }
        buf[i] = (byte) x;
    }


    static void encodeProtocol(Multiaddr multiaddr, OutputStream out) throws IOException {
        int code = multiaddr.code;
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(code) + 6) / 7];
        putUvarint(varint, code);
        out.write(varint);
    }

    static void encodePart(Integer port, OutputStream outputStream) throws IOException {
        int x = port;
        outputStream.write(new byte[]{(byte) (x >> 8), (byte) x});
    }

    static void encodePart(InetAddress inetAddress, OutputStream outputStream) throws IOException {
        outputStream.write(inetAddress.getAddress());
    }

    static void encodePart(PeerId peerId, OutputStream outputStream) throws IOException {
        byte[] multihash = peerId.multihash();

        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(multihash.length) + 6) / 7];
        putUvarint(varint, multihash.length);
        outputStream.write(varint);
        outputStream.write(multihash);
    }

    static String getString(CodedInputStream in) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            while (!in.isAtEnd()) {
                Multiaddr multiaddr = Multiaddr.lookup(in.readRawVarint32());
                if (multiaddr == null) {
                    throw new RuntimeException("invalid address");
                }
                stringBuilder.append("/");
                stringBuilder.append(multiaddr.type);

                if (multiaddr.size() == 0)
                    continue;

                Object part = multiaddr.readPart(in);
                Objects.requireNonNull(part);
                stringBuilder.append("/");
                if (part instanceof InetAddress inetAddress) {
                    stringBuilder.append(inetAddress.getHostAddress());
                } else {
                    stringBuilder.append(part);
                }
            }
        } catch (Throwable throwable) {
            Utils.error(Multiaddr.class.getSimpleName(), throwable);
        }
        return stringBuilder.toString();
    }

    @Nullable
    static Peeraddr parseAddress(CodedInputStream in, PeerId peerId) {

        boolean isFirst = true;
        InetAddress inetAddress = null;
        int port = 0;
        try {
            while (!in.isAtEnd()) {
                Multiaddr multiaddr = Multiaddr.lookup(in.readRawVarint32());
                if (multiaddr == null) {
                    return null;
                }
                if (isFirst) {
                    // check if is valid and supported
                    if (!multiaddr.type().startsWith("ip")) { // ip4 or ip6
                        return null;
                    }
                    isFirst = false;
                }

                if (multiaddr.size() == 0) {
                    continue;
                }

                Object part = multiaddr.readPart(in);
                Objects.requireNonNull(part);
                if (part instanceof InetAddress) {
                    inetAddress = (InetAddress) part;
                } else if (part instanceof Integer) {
                    port = (Integer) part;
                } else {
                    break;
                }
            }
        } catch (Throwable throwable) {
            Utils.error(Multiaddr.class.getSimpleName(), throwable);
            return null;
        }
        // check if address has a port, when it is not a dnsAddr
        if (port > 0 && inetAddress != null) {
            return new Peeraddr(peerId, inetAddress, port);
        }
        return null;
    }

    void partToStream(String addr, OutputStream outputStream) throws Exception {

        switch (code()) {
            case 4, 41 -> encodePart(InetAddress.getByName(addr), outputStream);
            case 273 -> encodePart(Integer.parseInt(addr), outputStream);
            case 421 -> encodePart(Keys.decodePeerId(addr), outputStream);
            default -> throw new IllegalStateException("Unknown multiaddr payloadType");
        }

    }

    @NonNull
    private Object readPart(CodedInputStream in) throws Exception {
        int sizeForAddress = sizeForAddress(in);
        byte[] buf;
        switch (code()) {
            case 4, 41 -> {
                buf = in.readRawBytes(sizeForAddress);
                return java.net.InetAddress.getByAddress(buf);
            }
            case 273 -> {
                int a = in.readRawByte() & 0xFF;
                int b = in.readRawByte() & 0xFF;
                return (a << 8) | b;
            }
            case 421 -> {
                buf = in.readRawBytes(sizeForAddress);
                return PeerId.create(buf);
            }
        }
        throw new IllegalStateException("Unimplemented protocol payloadType: " + type);
    }

    private int sizeForAddress(CodedInputStream in) throws IOException {
        if (size > 0)
            return size / 8;
        if (size == 0)
            return 0;
        return in.readRawVarint32();
    }
}
