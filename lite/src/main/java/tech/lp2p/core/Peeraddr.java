package tech.lp2p.core;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import tech.lp2p.utils.Utils;

public record Peeraddr(PeerId peerId, InetAddress inetAddress, int port)
        implements Comparable<Peeraddr>, Serializable {

    @NonNull
    public static Peeraddr loopbackPeeraddr(PeerId peerId, int port) {
        return Peeraddr.create(peerId, new InetSocketAddress(
                InetAddress.getLoopbackAddress(), port));
    }

    @NonNull
    public static Peeraddrs peeraddrs(PeerId peerId, int port) {
        Collection<InetAddress> inetSocketAddresses = Network.addresses();
        Peeraddrs result = new Peeraddrs();
        for (InetAddress inetAddress : inetSocketAddresses) {
            result.add(create(peerId, new InetSocketAddress(inetAddress, port)));
        }
        return result;
    }

    @NonNull
    public static Peeraddr create(String address) throws Exception {
        String[] strings = address.split("/");
        if (strings.length == 0) {
            throw new Exception("Address has not a separator");
        }
        String empty = strings[0];
        if (!empty.isEmpty()) {
            throw new Exception("Address should start with separator '/'");
        }
        int size = strings.length;
        int last = size - 1;
        int udp = last - 1;

        PeerId peerId = Keys.decodePeerId(strings[last]); // last element always a peerId
        Objects.requireNonNull(peerId);


        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // skip udp and peerId
        for (int i = 1; i < udp; i++) {
            String part = strings[i];
            Multiaddr multiaddr = Multiaddr.lookup(part);
            Multiaddr.encodeProtocol(multiaddr, byteArrayOutputStream);
            if (multiaddr.size() == 0) continue;

            i++;
            String component = strings[i];
            if (component == null || component.isEmpty())
                throw new Exception("Protocol requires address, but non provided!");

            multiaddr.partToStream(component, byteArrayOutputStream);
        }
        return Objects.requireNonNull(create(peerId, byteArrayOutputStream.toByteArray()));
    }

    @Nullable
    public static Peeraddr create(PeerId peerId, byte[] raw) {
        return Multiaddr.parseAddress(CodedInputStream.newInstance(raw), peerId);
    }

    @NonNull
    public static Peeraddrs create(PeerId peerId, List<ByteString> byteStrings) {
        Peeraddrs peeraddrs = new Peeraddrs();
        for (ByteString entry : byteStrings) {
            Peeraddr peeraddr = Peeraddr.create(peerId, entry.toByteArray());
            if (peeraddr != null) {
                peeraddrs.add(peeraddr);
            }
        }
        return peeraddrs;
    }

    @NonNull
    public static Peeraddr create(PeerId peerId, InetSocketAddress inetSocketAddress) {
        return create(peerId, inetSocketAddress.getAddress(), inetSocketAddress.getPort());
    }

    @NonNull
    public static Peeraddr create(PeerId peerId, InetAddress inetAddress, int port) {
        return new Peeraddr(peerId, inetAddress, port);
    }

    @NonNull
    public InetSocketAddress inetSocketAddress() throws UnknownHostException {
        return new InetSocketAddress(inetAddress(), port());
    }


    @NonNull
    public byte[] encoded() {
        boolean ipv6 = inetAddress instanceof Inet6Address;
        if (!ipv6) {
            throw new IllegalStateException("only ipv6 addresses are supported");
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Multiaddr.encodeProtocol(Multiaddr.IP6, byteArrayOutputStream);
            Multiaddr.encodePart(inetAddress, byteArrayOutputStream);
            Multiaddr.encodeProtocol(Multiaddr.UDP, byteArrayOutputStream);
            Multiaddr.encodePart(port, byteArrayOutputStream);
            Multiaddr.encodeProtocol(Multiaddr.QUICV1, byteArrayOutputStream);

            return byteArrayOutputStream.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    public String host() {
        return inetAddress.getHostName();
    }

    @NonNull
    public String multiaddr() {
        boolean ipv6 = inetAddress instanceof Inet6Address;
        if (!ipv6) {
            throw new IllegalStateException("only ipv6 addresses are supported");
        }
        return Multiaddr.getString(CodedInputStream.newInstance(encoded())) +
                "/" + Multiaddr.P2P.type() + "/" + peerId;
    }

    @Override
    public int compareTo(Peeraddr o) {
        return Comparator.comparing(Peeraddr::peerId)
                .thenComparing(Peeraddr::port)
                .thenComparing((o1, o2) -> Utils.compareUnsigned(
                        o1.inetAddress.getAddress(), o2.inetAddress.getAddress()))
                .compare(this, o);
    }
}
