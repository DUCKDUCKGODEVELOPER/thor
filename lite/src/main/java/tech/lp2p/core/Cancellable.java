package tech.lp2p.core;


public interface Cancellable {

    boolean isCancelled();
}
