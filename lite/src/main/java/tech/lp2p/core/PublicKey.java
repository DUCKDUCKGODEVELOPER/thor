package tech.lp2p.core;

import java.util.Objects;

import tech.lp2p.Lite;

public record PublicKey(byte[] raw) implements java.security.PublicKey {

    public static PublicKey create(byte[] raw) {
        Objects.requireNonNull(raw);
        return new PublicKey(raw);
    }

    @Override
    public String getAlgorithm() {
        return Lite.Ed25519;
    }

    @Override
    public String getFormat() {
        return null;
    }

    @Override
    public byte[] getEncoded() {
        return raw;
    }
}
