package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Arrays;

import tech.lp2p.utils.Base58;
import tech.lp2p.utils.Utils;

public record PeerId(byte[] multihash) implements Comparable<PeerId>, Serializable {


    @NonNull
    public static PeerId create(byte[] multihash) {
        return new PeerId(multihash);
    }


    // Return peerId as cid version 1 (Multicodec.LIBP2P_KEY)
    @NonNull
    public Cid cid() {
        return Cid.createCid(Multicodec.LIBP2P_KEY, multihash);
    }

    // https://docs.ipfs.tech/concepts/content-addressing/#cid-inspector
    // The default for CIDv1 is the case-insensitive base32, but use of the shorter base36 is
    // encouraged for IPNS names to ensure same text representation on subdomains.
    //
    @NonNull
    public String toBase36() {
        return Multibase.encode(Multibase.BASE36, cid().encoded());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeerId peer = (PeerId) o;
        return Arrays.equals(multihash, peer.multihash);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(multihash); // ok, checked, maybe opt
    }


    // https://github.com/libp2p/specs/blob/master/peer-ids/peer-ids.md
    // String representation
    // There are two ways to represent peer IDs in text: as a raw base58btc encoded multihash
    // (e.g., Qm..., 1...) and as a multibase encoded CID (e.g., bafz...). Libp2p is slowly
    // transitioning from the first (legacy) format to the second (new).
    //
    // Implementations MUST support parsing both forms of peer IDs. Implementations SHOULD display
    // peer IDs using the first (raw base58btc encoded multihash) format until the second format
    // is widely supported.
    //
    // Peer IDs encoded as CIDs must be encoded using CIDv1 and must use the libp2p-key multicodec
    // (0x72). By default, such peer IDs SHOULD be encoded in using the base32 multibase
    // (RFC4648, without padding).
    //
    // For reference, CIDs (encoded in text) have the following format
    //
    // <multibase-prefix><cid-version><multicodec><multihash>
    // [Future toBase32() instead of toBase58()]
    @NonNull
    public String toString() {
        return Base58.encode(this.multihash);
    }


    @Override
    public int compareTo(PeerId o) {
        return Utils.compareUnsigned(this.multihash, o.multihash);
    }

}
