package tech.lp2p.core;

import androidx.annotation.VisibleForTesting;

import java.net.DatagramSocket;
import java.util.List;
import java.util.function.Consumer;

public interface Server extends Host {

    void shutdown();

    int port();

    int numConnections();

    DatagramSocket socket();

    void holePunching(Peeraddr peeraddr);

    Identify identify() throws Exception;

    boolean hasReservations();

    Reservations reservations();

    @VisibleForTesting
    Reservation reservation(Peeraddr peeraddr) throws Exception;

    void reservations(Cancellable cancellable);

    Peeraddrs reservationPeeraddrs();

    void provideKey(Cancellable cancellable, Consumer<Peeraddr> consumer, Key key);

    @VisibleForTesting
    Reservation refreshReservation(Reservation reservation) throws Exception;

    @VisibleForTesting
    void closeReservation(Reservation reservation) throws Exception;

    @VisibleForTesting
    List<Connection> connections(PeerId peerId);

    boolean isGated(PeerId peerId);

    @VisibleForTesting
    boolean hasConnection(PeerId peerId);

    record Settings(int port, Limit limit, boolean dhtProtocolEnabled) {
    }
}
