package tech.lp2p.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface BlockStore {

    boolean hasBlock(@NonNull Cid cid);

    @Nullable
    byte[] getBlock(@NonNull Cid cid);

    void deleteBlock(@NonNull Cid cid);

    void storeBlock(@NonNull Cid cid, byte[] data);
}


