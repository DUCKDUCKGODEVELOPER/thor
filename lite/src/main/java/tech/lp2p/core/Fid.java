package tech.lp2p.core;


public record Fid(Cid cid, long size, String name) implements Info {
}
