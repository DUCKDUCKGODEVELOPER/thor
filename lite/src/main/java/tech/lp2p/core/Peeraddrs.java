package tech.lp2p.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

import tech.lp2p.utils.Utils;

public final class Peeraddrs extends ConcurrentSkipListSet<Peeraddr> {

    private static final int REACHABLE_TIMEOUT = 250; // in ms
    private static final String TAG = Peeraddrs.class.getSimpleName();

    @Nullable
    public static Peeraddr reduceToOne(Peeraddrs peeraddrs) {
        if (peeraddrs.isEmpty()) {
            return null;
        }
        return reachable(peeraddrs);
    }

    @Nullable
    public static Peeraddr reduceToOne(@NonNull PeerId peerId,
                                       @NonNull List<ByteString> byteStrings) {
        Peeraddrs peeraddrs = Peeraddr.create(peerId, byteStrings);
        return reachable(peeraddrs);
    }


    private static void reduceLocals(Peeraddrs peeraddrs) {
        if (peeraddrs.size() > 1) {
            removeLoopbacks(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeLinkLocals(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeSiteLocals(peeraddrs);
        }
    }

    @Nullable
    private static Peeraddr reachable(Peeraddrs peeraddrs) {
        if (peeraddrs.isEmpty()) {
            return null;
        }

        reduceLocals(peeraddrs);

        if (peeraddrs.size() == 1) {
            return peeraddrs.first();
        }

        for (Peeraddr resolvedAddr : peeraddrs) {
            if (isReachable(resolvedAddr)) { // only reachable
                return resolvedAddr;
            }
        }
        return null;
    }

    private static void removeLoopbacks(Peeraddrs peeraddrs) {
        for (Peeraddr peeraddr : peeraddrs) {
            if (peeraddrs.size() == 1) {
                return;
            }
            if (peeraddr.inetAddress().isLoopbackAddress()) {
                peeraddrs.remove(peeraddr);
            }
        }
    }

    private static void removeLinkLocals(Peeraddrs peeraddrs) {
        for (Peeraddr peeraddr : peeraddrs) {
            if (peeraddrs.size() == 1) {
                return;
            }
            if (peeraddr.inetAddress().isLinkLocalAddress()) {
                peeraddrs.remove(peeraddr);
            }
        }
    }

    private static void removeSiteLocals(Peeraddrs peeraddrs) {
        for (Peeraddr peeraddr : peeraddrs) {
            if (peeraddrs.size() == 1) {
                return;
            }
            if (peeraddr.inetAddress().isSiteLocalAddress()) {
                peeraddrs.remove(peeraddr);
            }
        }
    }

    private static boolean isReachable(Peeraddr peeraddr) {
        try {
            return peeraddr.inetAddress().isReachable(REACHABLE_TIMEOUT);
        } catch (Throwable throwable) {
            Utils.error(TAG, peeraddr.toString() + " " + throwable.getMessage());
        }
        return false;
    }
}
