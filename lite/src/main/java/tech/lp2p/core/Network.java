package tech.lp2p.core;


import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tech.lp2p.utils.Utils;

public interface Network {
    String TAG = Network.class.getSimpleName();

    static List<InetAddress> siteLocalAddresses() {
        List<InetAddress> inetAddresses = new ArrayList<>();

        try {
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                if (networkInterface.isUp()) {
                    List<InetAddress> addresses =
                            Collections.list(networkInterface.getInetAddresses());
                    for (InetAddress inetAddress : addresses) {
                        if (inetAddress instanceof Inet6Address inet6Address) {
                            if (inetAddress.isSiteLocalAddress()) {
                                inetAddresses.add(inet6Address);
                            }
                        }
                    }
                }
            }
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable); // should not occur
        }
        return inetAddresses;
    }

    static List<InetAddress> addresses() {
        List<InetAddress> inetAddresses = new ArrayList<>();

        try {
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                if (networkInterface.isUp()) {
                    List<InetAddress> addresses =
                            Collections.list(networkInterface.getInetAddresses());
                    for (InetAddress inetAddress : addresses) {
                        if (inetAddress instanceof Inet6Address inet6Address) {
                            inetAddresses.add(inet6Address);
                        }
                    }
                }
            }
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable); // should not occur
        }
        return inetAddresses;
    }

    static List<InetAddress> publicAddresses() {
        List<InetAddress> inetAddresses = new ArrayList<>();

        try {
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                if (networkInterface.isUp()) {
                    List<InetAddress> addresses =
                            Collections.list(networkInterface.getInetAddresses());
                    for (InetAddress inetAddress : addresses) {
                        if (inetAddress instanceof Inet6Address inet6Address) {
                            if (!isLocalAddress(inet6Address)) {
                                inetAddresses.add(inet6Address);
                            }
                        }
                    }
                }
            }
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable); // should not occur
        }
        return inetAddresses;
    }

    static boolean isWellKnownIPv4Translatable(InetAddress inetAddress) {
        if (inetAddress instanceof Inet6Address inet6Address) {
            return Network.isWellKnownIPv4Translatable(inet6Address);
        }
        return false;
    }

    /**
     * Whether the address has the well-known prefix for IPv4 translatable addresses
     * as in rfc 6052 and 6144
     */
    private static boolean isWellKnownIPv4Translatable(Inet6Address inet6Address) {
        byte[] segments = inet6Address.getAddress();
        // 64:ff9b::/96 prefix for auto ipv4/ipv6 translation
        // Segments are [0, 100, -1, -101, 0, 0, 0, 0, 0, 0, 0, 0, x, x, x, x]
        return segments[0] == 0 && segments[1] == 100 && segments[2] == -1 && segments[3] == -101 &&
                segments[4] == 0 && segments[5] == 0 && segments[6] == 0 && segments[7] == 0 &&
                segments[8] == 0 && segments[9] == 0 && segments[10] == 0 && segments[11] == 0;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    static boolean isLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress())
                || (inetAddress.isSiteLocalAddress());
    }

}
