package tech.lp2p.core;

import androidx.annotation.NonNull;

// https://github.com/multiformats/multicodec/blob/master/table.csv (see ipld)
public record Multicodec(int codec) {
    public static final Multicodec RAW = new Multicodec(0x55);    // raw
    public static final Multicodec DAG_PB = new Multicodec(0x70); // dag-pb
    public static final Multicodec LIBP2P_KEY = new Multicodec(0x72); // libp2p-key

    @NonNull
    public static Multicodec get(int codec) {
        return switch (codec) {
            case 0x55 -> RAW;
            case 0x70 -> DAG_PB;
            case 0x72 -> LIBP2P_KEY;
            default -> throw new IllegalArgumentException("unsupported multicodec " + codec);
        };
    }
}

