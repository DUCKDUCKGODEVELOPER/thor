package tech.lp2p.core;

import java.util.Objects;

import tech.lp2p.Lite;

public record Protocol(String name, ALPN alpn, int codec, long size) {

    public static final Protocol MULTISTREAM_PROTOCOL = new Protocol("/multistream/1.0.0", ALPN.libp2p, 1, 0);
    public static final Protocol DHT_PROTOCOL = new Protocol("/ipfs/kad/1.0.0", ALPN.libp2p, 2, Lite.CHUNK_SIZE);
    public static final Protocol IDENTITY_PROTOCOL = new Protocol("/ipfs/id/1.0.0", ALPN.libp2p, 3, 8 * 1024); // signed IDs
    public static final Protocol RELAY_PROTOCOL_HOP = new Protocol("/libp2p/circuit/relay/0.2.0/hop", ALPN.libp2p, 4, 4096);
    public static final Protocol RELAY_PROTOCOL_STOP = new Protocol("/libp2p/circuit/relay/0.2.0/stop", ALPN.libp2p, 5, 4096);

    public static final Protocol LITE_PUSH_PROTOCOL = new Protocol("/lite/push/1.0.0", ALPN.lite, 2, 1024);
    public static final Protocol LITE_PULL_PROTOCOL = new Protocol("/lite/pull/1.0.0", ALPN.lite, 1, 1024);
    public static final Protocol LITE_FETCH_PROTOCOL = new Protocol("/lite/fetch/1.0.0", ALPN.lite, 0, 10 * Lite.CHUNK_SIZE);

    public static Protocol name(ALPN alpn, String protocol) throws Exception {
        if (Objects.requireNonNull(alpn) == ALPN.libp2p) {
            return switch (protocol) {
                case "/multistream/1.0.0" -> MULTISTREAM_PROTOCOL;
                case "/ipfs/kad/1.0.0" -> DHT_PROTOCOL;
                case "/ipfs/id/1.0.0" -> IDENTITY_PROTOCOL;
                case "/libp2p/circuit/relay/0.2.0/hop" -> RELAY_PROTOCOL_HOP;
                case "/libp2p/circuit/relay/0.2.0/stop" -> RELAY_PROTOCOL_STOP;
                default -> throw new Exception("unhandled protocol " + protocol);
            };
        }
        throw new Exception("unhandled protocol " + protocol);

    }

    public static Protocol codec(ALPN alpn, int protocol) throws Exception {
        if (Objects.requireNonNull(alpn) == ALPN.lite) {
            return switch (protocol) {
                case 2 -> LITE_PUSH_PROTOCOL;
                case 1 -> LITE_PULL_PROTOCOL;
                case 0 -> LITE_FETCH_PROTOCOL;
                default -> throw new Exception("unhandled protocol " + protocol);
            };
        }
        throw new Exception("unhandled protocol " + protocol);
    }
}
