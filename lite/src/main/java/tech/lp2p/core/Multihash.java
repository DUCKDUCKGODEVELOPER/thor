package tech.lp2p.core;


import androidx.annotation.NonNull;

import com.google.protobuf.CodedInputStream;

import java.nio.ByteBuffer;

import tech.lp2p.utils.Utils;

// https://github.com/multiformats/multicodec/blob/master/table.csv (see multihash)
public record Multihash(int index, int length) {


    public static final Multihash ID = new Multihash(0, -1);
    public static final Multihash SHA2_256 = new Multihash(0x12, 32);

    public static Multihash lookup(int type) {
        switch (type) {
            case 0 -> {
                return ID;
            }
            case 0x12 -> {
                return SHA2_256;
            }
            default -> throw new IllegalArgumentException("unsupported hashtype " + type);
        }
    }

    public static byte[] create(Multihash type, byte[] hash) {
        if (hash.length != type.length() && type != Multihash.ID)
            throw new IllegalStateException("Incorrect hash length: " + hash.length + " != " + type.length());
        return encoded(type, hash);
    }

    @NonNull
    public static byte[] decode(CodedInputStream codedInputStream) {
        try {
            int type = codedInputStream.readRawVarint32();
            int len = codedInputStream.readRawVarint32();
            Multihash t = Multihash.lookup(type);
            byte[] hash = codedInputStream.readRawBytes(len);
            if (!codedInputStream.isAtEnd()) {
                throw new IllegalStateException("still data available");
            }
            return create(t, hash);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    public static byte[] decode(byte[] raw) {
        return decode(CodedInputStream.newInstance(raw));
    }


    @NonNull
    private static byte[] encoded(Multihash type, byte[] hash) {

        int typeLength = Utils.unsignedVariantSize(type.index());
        int hashLength = Utils.unsignedVariantSize(hash.length);

        ByteBuffer buffer = ByteBuffer.allocate(typeLength + hashLength + hash.length);
        Utils.writeUnsignedVariant(buffer, type.index());
        Utils.writeUnsignedVariant(buffer, hash.length);
        buffer.put(hash);
        return buffer.array();
    }

}