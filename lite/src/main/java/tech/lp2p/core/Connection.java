package tech.lp2p.core;

import androidx.annotation.VisibleForTesting;

public interface Connection {
    ALPN alpn();

    void close();

    Peeraddr remotePeeraddr();

    PeerId remotePeerId();

    boolean isConnected();

    Host host();

    @VisibleForTesting
    boolean hasAttribute(String key);
}
