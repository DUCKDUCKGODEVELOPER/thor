package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.security.KeyPair;


public interface Host {

    @NonNull
    Peeraddrs peeraddrs();

    @NonNull
    Peeraddrs bootstrap();

    @NonNull
    PeerId self();

    @NonNull
    Protocols protocols(@NonNull ALPN alpn);

    @NonNull
    Certificate certificate();

    @NonNull
    Responder responder(@NonNull ALPN alpn);

    @NonNull
    KeyPair keyPair();

    @NonNull
    String agent();

    @NonNull
    Payloads payloads();

    @NonNull
    BlockStore blockStore();
}
