package tech.lp2p.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class TimeoutCancellable implements Cancellable {

    private final long timeout;
    @Nullable
    private final Cancellable cancellable;
    private final long start;

    public TimeoutCancellable(long timeout) {
        this.cancellable = null;
        this.timeout = timeout;
        this.start = System.currentTimeMillis();
    }

    public TimeoutCancellable(@NonNull Cancellable cancellable, long timeout) {
        this.cancellable = cancellable;
        this.timeout = timeout;
        this.start = System.currentTimeMillis();
    }

    @Override
    public boolean isCancelled() {
        if (cancellable != null) {
            return cancellable.isCancelled() || (System.currentTimeMillis() - start) > (timeout * 1000);
        }
        return (System.currentTimeMillis() - start) > (timeout * 1000);
    }

}
