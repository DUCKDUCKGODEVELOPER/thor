package tech.lp2p.core;


public record Envelope(PeerId peerId, Cid cid, Payload payload) {
}
