package tech.lp2p.core;

import java.nio.ByteBuffer;

import tech.lp2p.utils.Utils;

public record Payload(String domain, int type) {

    public static byte[] payloadType(Payload payload) {
        int x = payload.type();
        return new byte[]{(byte) (x >> 8), (byte) x};
    }

    public static int payloadType(byte[] type) {
        int a = type[0] & 0xFF;
        int b = type[1] & 0xFF;
        return (a << 8) | b;
    }

    public static byte[] unsignedEnvelopePayload(Payload payload, Cid cid) {

        byte[] domain = payload.domain().getBytes();
        byte[] type = payloadType(payload);
        byte[] data = cid.encoded();
        int domainLength = Utils.unsignedVariantSize(domain.length);
        int payloadTypeLength = Utils.unsignedVariantSize(type.length);
        int payloadLength = Utils.unsignedVariantSize(data.length);

        ByteBuffer buffer = ByteBuffer.allocate(domain.length + domainLength +
                type.length + payloadTypeLength + payloadLength + data.length);

        Utils.writeUnsignedVariant(buffer, domain.length);
        buffer.put(domain);
        Utils.writeUnsignedVariant(buffer, type.length);
        buffer.put(type);
        Utils.writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        return buffer.array();
    }

}
