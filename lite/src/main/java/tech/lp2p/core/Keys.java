package tech.lp2p.core;


import androidx.annotation.NonNull;

import com.google.crypto.tink.subtle.Ed25519Sign;
import com.google.crypto.tink.subtle.Ed25519Verify;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;

import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.ECPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Objects;

import crypto.pb.Crypto;
import tech.lp2p.utils.Base58;

public interface Keys {


    @NonNull
    static Cid decodeCid(byte[] data) throws Exception {
        if (data[0] == 1) { // can only be cid version 1
            CodedInputStream codedInputStream = CodedInputStream.newInstance(data);
            codedInputStream.readRawVarint32(); // skip version, because it is 1
            int codecType = codedInputStream.readRawVarint32();
            Multicodec multicodec = Multicodec.get(codecType);
            byte[] multihash = Multihash.decode(codedInputStream);
            Objects.requireNonNull(multihash);
            return new Cid(multihash, multicodec.codec());
        } else {
            return Cid.generateCid(Multicodec.DAG_PB, Multihash.decode(data));
        }
    }

    @NonNull
    static Cid decodeCid(@NonNull String name) throws Exception {
        if (name.length() < 2) {
            throw new IllegalStateException("invalid cid");
        }

        if (name.length() == 46 && name.startsWith("Qm")) {
            byte[] multihash = Multihash.decode(Base58.decode(name));
            Objects.requireNonNull(multihash);
            return new Cid(multihash, Multicodec.DAG_PB.codec());
        }

        byte[] raw = Multibase.decode(name);

        CodedInputStream codedInputStream = CodedInputStream.newInstance(raw);

        int version = codedInputStream.readRawVarint32();
        if (version != 1) {
            throw new IllegalStateException("invalid version");
        }
        int codecType = codedInputStream.readRawVarint32();
        Multicodec ipld = Multicodec.get(codecType);

        byte[] multihash = Multihash.decode(codedInputStream);
        Objects.requireNonNull(multihash);
        return new Cid(multihash, ipld.codec());
    }

    @NonNull
    static PeerId decodePeerId(@NonNull String name) throws Exception {

        if (name.startsWith("Qm") || name.startsWith("1")) {
            // base58 encoded sha256 or identity multihash
            return PeerId.create(Base58.decode(name));
        }

        byte[] data = Multibase.decode(name);

        CodedInputStream codedInputStream = CodedInputStream.newInstance(data);
        codedInputStream.readRawVarint32(); // skip version
        codedInputStream.readRawVarint32(); // skip multicodec
        byte[] multihash = Multihash.decode(codedInputStream);
        return PeerId.create(multihash);
    }

    @NonNull
    static Keys extractPublicKey(@NonNull PeerId id) throws Exception {
        CodedInputStream wrap = CodedInputStream.newInstance(id.multihash());
        int version = wrap.readRawVarint32();
        if (version != Multihash.ID.index()) {
            throw new Exception("not supported multicodec");
        }
        int length = wrap.readRawVarint32();
        byte[] data = wrap.readRawBytes(length);
        return unmarshalPublicKey(data);
    }

    static Keys unmarshalPublicKey(byte[] data) throws Exception {
        return unmarshalPublicKey(Crypto.PublicKey.parseFrom(data));
    }

    static Keys unmarshalPublicKey(Crypto.PublicKey publicKey) throws Exception {

        byte[] pubKeyData = publicKey.getData().toByteArray();

        return switch (publicKey.getType()) {
            case RSA -> unmarshalRsaPublicKey(pubKeyData);
            case ECDSA -> unmarshalEcdsaPublicKey(pubKeyData);
            case Secp256k1 -> throw new IllegalStateException(
                    "not supported [jdk does not support bitcoin Secp256k1]");
            case Ed25519 -> unmarshalEd25519PublicKey(pubKeyData);
        };
    }

    // Note: Only Ed25519 support
    static byte[] sign(PrivateKey privateKey, byte[] data) throws Exception {
        Objects.requireNonNull(privateKey);
        Ed25519Sign signer = new Ed25519Sign(privateKey.getEncoded());
        return signer.sign(data);
    }

    // Note: Only Ed25519 support
    static PeerId createPeerId(PublicKey publicKey) throws Exception {
        Ed25519PublicKey pubKey = new Ed25519PublicKey(publicKey.getEncoded());
        Objects.requireNonNull(pubKey);
        return fromPubKey(pubKey);
    }

    @NonNull
    static PeerId fromPublicKey(Crypto.PublicKey publicKey) throws Exception {
        // https://github.com/libp2p/specs/blob/master/peer-ids/peer-ids.md

        // Peer IDs are derived by hashing the encoded public key with multihash. Keys that
        // serialize to more than 42 bytes must be hashed using sha256 multihash, keys that
        // serialize to at most 42 bytes must be hashed using the "identity" multihash multicodec.
        //
        // Specifically, to compute a peer ID of a key:
        //
        // Encode the public key as described in the keys section.
        // If the length of the serialized bytes is less than or equal to 42, compute the
        // "identity" multihash of the serialized bytes. In other words, no hashing is performed,
        // but the multihash format is still followed (byte plus varint plus serialized bytes).
        // The idea here is that if the serialized byte array is short enough, we can fit it in a
        // multihash verbatim without having to condense it using a hash function.
        // If the length is greater than 42, then hash it using the SHA256 multihash.

        byte[] pubKeyBytes = publicKey.toByteArray();

        if (pubKeyBytes.length <= 42) {
            byte[] multihash = Multihash.create(Multihash.ID, pubKeyBytes);
            return PeerId.create(multihash);
        } else {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] multihash = Multihash.create(Multihash.SHA2_256, digest.digest(pubKeyBytes));
            return PeerId.create(multihash);
        }
    }

    @NonNull
    static PeerId fromPubKey(@NonNull Keys keys) throws Exception {
        // https://github.com/libp2p/specs/blob/master/peer-ids/peer-ids.md

        // Peer IDs are derived by hashing the encoded public key with multihash. Keys that
        // serialize to more than 42 bytes must be hashed using sha256 multihash, keys that
        // serialize to at most 42 bytes must be hashed using the "identity" multihash multicodec.
        //
        // Specifically, to compute a peer ID of a key:
        //
        // Encode the public key as described in the keys section.
        // If the length of the serialized bytes is less than or equal to 42, compute the
        // "identity" multihash of the serialized bytes. In other words, no hashing is performed,
        // but the multihash format is still followed (byte plus varint plus serialized bytes).
        // The idea here is that if the serialized byte array is short enough, we can fit it in a
        // multihash verbatim without having to condense it using a hash function.
        // If the length is greater than 42, then hash it using the SHA256 multihash.

        return fromPublicKey(Crypto.PublicKey.newBuilder().setType(keys.getKeyType()).
                setData(ByteString.copyFrom(keys.raw())).build());
    }

    // Note: Only Ed25519 support
    static void verify(byte[] publicKey, byte[] data, byte[] signature) throws Exception {
        // get the publicKey from the other party.
        Ed25519Verify verifier = new Ed25519Verify(publicKey);
        verifier.verify(signature, data);
    }


    static EcdsaPublicKey unmarshalEcdsaPublicKey(byte[] keyBytes) throws Exception {

        KeyFactory keyFactory = KeyFactory.getInstance("ECDSA");
        PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(keyBytes));
        Objects.requireNonNull(publicKey);
        return new EcdsaPublicKey((ECPublicKey) publicKey);
    }

    static Keys unmarshalEd25519PublicKey(byte[] keyBytes) {
        return new Ed25519PublicKey(keyBytes);
    }

    static Keys unmarshalRsaPublicKey(byte[] keyBytes) throws Exception {

        PublicKey publicKey = KeyFactory.getInstance("RSA")
                .generatePublic(new X509EncodedKeySpec(keyBytes));
        return new RsaPublicKey(publicKey);

    }

    void verify(byte[] data, byte[] signature) throws Exception;

    @NonNull
    Crypto.KeyType getKeyType();

    @NonNull
    byte[] raw();

    record EcdsaPublicKey(ECPublicKey publicKey) implements Keys {

        @NonNull
        public byte[] raw() {
            return this.publicKey.getEncoded();
        }

        public void verify(byte[] data, byte[] signature) throws Exception {

            Signature sha256withECDSA = Signature.getInstance(
                    "SHA256withECDSA");
            sha256withECDSA.initVerify(this.publicKey);
            sha256withECDSA.update(data);
            boolean result = sha256withECDSA.verify(signature);
            if (!result) {
                throw new Exception("verify failed");
            }

        }

        @NonNull
        @Override
        public Crypto.KeyType getKeyType() {
            return Crypto.KeyType.ECDSA;
        }


    }

    record Ed25519PublicKey(byte[] publicKey) implements Keys {

        @NonNull
        public byte[] raw() {
            return this.publicKey;
        }

        public void verify(byte[] data, byte[] signature) throws Exception {
            Ed25519Verify verifier = new Ed25519Verify(publicKey);
            verifier.verify(signature, data);
        }

        @NonNull
        @Override
        public Crypto.KeyType getKeyType() {
            return Crypto.KeyType.Ed25519;
        }

    }

    record RsaPublicKey(PublicKey publicKey) implements Keys {

        @NonNull
        public byte[] raw() {
            return this.publicKey.getEncoded();
        }

        public void verify(byte[] data, byte[] signature) throws Exception {

            Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
            sha256withRSA.initVerify(this.publicKey);
            sha256withRSA.update(data);
            boolean result = sha256withRSA.verify(signature);
            if (!result) {
                throw new Exception("verify failed");
            }
        }

        @NonNull
        @Override
        public Crypto.KeyType getKeyType() {
            return Crypto.KeyType.RSA;
        }

    }
}

