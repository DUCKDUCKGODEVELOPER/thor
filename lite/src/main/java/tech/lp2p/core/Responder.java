package tech.lp2p.core;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

import tech.lp2p.lite.LiteErrorCode;
import tech.lp2p.quic.Stream;


public record Responder(@NonNull Protocols protocols) {

    @Nullable
    private Handler protocolHandler(@NonNull Protocol protocol) {
        return protocols.get(protocol);
    }

    public void protocol(Stream stream, Protocol protocol) throws Exception {
        Handler handler = protocolHandler(protocol);
        if (handler != null) {
            handler.protocol(stream);
        } else {
            stream.resetStream(LiteErrorCode.PROTOCOL_NEGOTIATION_FAILED);
        }
    }


    public void data(Stream stream, Protocol protocol, byte[] data) throws Exception {
        Objects.requireNonNull(protocol, "data unknown protocol "
                + stream.connection().remotePeerId());
        Handler handler = protocolHandler(protocol);
        if (handler != null) {
            handler.data(stream, data);
        } else {
            stream.resetStream(LiteErrorCode.PROTOCOL_NEGOTIATION_FAILED);
        }

    }


    public void closed(Stream stream, Protocol protocol) {
        Objects.requireNonNull(protocol, "data unknown protocol "
                + stream.connection().remotePeerId());
        Handler handler = protocolHandler(protocol);
        if (handler != null) {
            handler.closed(stream);
        } else {
            stream.resetStream(LiteErrorCode.PROTOCOL_NEGOTIATION_FAILED);
        }

    }

    public boolean validAlpns(ALPN alpn) {
        for (Protocol protocol : protocols.keySet()) {
            if (protocol.alpn() != alpn) {
                return false;
            }
        }
        return true;
    }
}


