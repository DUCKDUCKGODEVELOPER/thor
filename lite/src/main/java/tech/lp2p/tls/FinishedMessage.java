/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import java.nio.ByteBuffer;

public record FinishedMessage(byte[] verifyData, byte[] raw) implements HandshakeMessage {

    static FinishedMessage createFinishedMessage(byte[] hmac) {
        ByteBuffer buffer = ByteBuffer.allocate(4 + hmac.length);
        buffer.putInt((HandshakeType.finished.value << 24) | hmac.length);
        buffer.put(hmac);
        byte[] raw = buffer.array();
        return new FinishedMessage(hmac, raw);
    }

    public static FinishedMessage parse(ByteBuffer buffer, int length) throws DecodeErrorException {
        buffer.mark();
        int remainingLength = HandshakeMessage.parseHandshakeHeader(buffer, HandshakeType.finished, 4 + 32);
        byte[] verifyData = new byte[remainingLength];
        buffer.get(verifyData);

        buffer.reset();
        byte[] raw = new byte[length];
        buffer.get(raw);

        return new FinishedMessage(verifyData, raw);
    }

    @Override
    public HandshakeType getType() {
        return HandshakeType.finished;
    }

    @Override
    public byte[] getBytes() {
        return raw;
    }

}
