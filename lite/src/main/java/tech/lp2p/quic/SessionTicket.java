package tech.lp2p.quic;

import tech.lp2p.tls.NewSessionTicket;

/**
 * Extension of TLS NewSessionTicket to hold (relevant) QUIC transport parameters too, in order to being able to
 * send 0-RTT packets.
 * <p>
 * <a href="https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-7.3.1">...</a>
 * "Both endpoints store the value of the server transport parameters
 * from a connection and apply them to any 0-RTT packets that are sent
 * in subsequent connections to that peer, except for transport
 * parameters that are explicitly excluded."
 * "A client MUST NOT use remembered values for the following parameters:
 * original_connection_id, preferred_address, stateless_reset_token,
 * ack_delay_exponent and active_connection_id_limit."
 */
record SessionTicket(NewSessionTicket tlsTicket,
                     long maxIdleTimeout,
                     int maxPacketSize,
                     long initialMaxData,
                     long initialMaxStreamDataBidiLocal,
                     long initialMaxStreamDataBidiRemote,
                     long initialMaxStreamDataUni,
                     long initialMaxStreamsBidi,
                     long initialMaxStreamsUni,
                     int maxAckDelay,
                     boolean disableActiveMigration) {


    public static SessionTicket create(NewSessionTicket tlsTicket, TransportParameters serverParameters) {

        return new SessionTicket(tlsTicket, serverParameters.maxIdleTimeout(),
                serverParameters.maxUdpPayloadSize(),
                serverParameters.initialMaxData(),
                serverParameters.initialMaxStreamDataBidiLocal(),
                serverParameters.initialMaxStreamDataBidiRemote(),
                serverParameters.initialMaxStreamDataUni(),
                serverParameters.initialMaxStreamsBidi(),
                serverParameters.initialMaxStreamsUni(),
                serverParameters.maxAckDelay(),
                serverParameters.disableMigration());
    }
}

