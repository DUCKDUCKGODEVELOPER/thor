package tech.lp2p.quic;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.function.Consumer;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Responder;
import tech.lp2p.lite.LiteErrorCode;
import tech.lp2p.utils.Utils;


public record AlpnLibp2pResponder(@NonNull Responder responder, @NonNull State state)
        implements Consumer<StreamData> {

    private static final String TAG = AlpnLibp2pResponder.class.getSimpleName();

    public static AlpnLibp2pResponder create(@NonNull Responder responder) {
        return new AlpnLibp2pResponder(responder, new State(responder));
    }

    private static boolean isProtocol(byte[] data) {
        if (data.length > 2) {
            return data[0] == '/' && data[data.length - 1] == '\n';
        }
        return false;
    }


    private static void iteration(State state, Stream stream, ByteBuffer bytes) throws Exception {

        if (!state.reset) {
            if (state.length() == 0) {
                state.frame = StreamRequester.unsignedVarintReader(bytes);
                state.frameIndex = 0;
                if (state.length() < 0) {
                    state.frame = null;
                    state.frameIndex = 0;
                    throw new Exception("invalid length of < 0 : " + state.length());
                } else {

                    int read = Math.min(state.length(), bytes.remaining());
                    for (int i = 0; i < read; i++) {
                        state.frame[state.frameIndex] = bytes.get();
                        state.frameIndex++;
                    }

                    if (read == state.length()) {
                        byte[] frame = Objects.requireNonNull(state.frame);
                        state.frame = null;
                        state.accept(stream, frame);
                    }

                    // check for a next iteration
                    if (bytes.remaining() > 0) {
                        iteration(state, stream, bytes);
                    }
                }
            } else {
                byte[] frame = Objects.requireNonNull(state.frame);
                int remaining = state.frame.length - state.frameIndex;
                int read = Math.min(remaining, bytes.remaining());
                for (int i = 0; i < read; i++) {
                    state.frame[state.frameIndex] = bytes.get();
                    state.frameIndex++;
                }
                remaining = state.frame.length - state.frameIndex;
                if (remaining == 0) { // frame is full
                    state.frame = null;
                    state.frameIndex++;
                    state.accept(stream, frame);
                }
                // check for a next iteration
                if (bytes.remaining() > 0) {
                    iteration(state, stream, bytes);
                }
            }
        }
    }


    @Override
    public void accept(StreamData rawData) {

        Stream stream = rawData.stream();

        if (rawData.isTerminated()) {
            responder.closed(stream, state.protocol);
            state.reset();
            // nothing to do here, cleaning already done
            return;
        }

        try {
            byte[] data = rawData.data();
            Objects.requireNonNull(data); // can not be null
            if (data.length > 0) {
                iteration(state, rawData.stream(), ByteBuffer.wrap(data));
            }
        } catch (Throwable throwable) {
            stream.resetStream(LiteErrorCode.INTERNAL_ERROR);
            state.reset();
        }

        if (rawData.isFinal()) {
            responder.closed(stream, state.protocol);
            state.reset();
        }
    }


    static class State {

        private final Responder streamResponder;


        Protocol protocol = null;
        byte[] frame = null;
        int frameIndex = 0;
        boolean reset = false;

        State(Responder streamResponder) {
            this.streamResponder = streamResponder;
        }

        void reset() {
            frame = null;
            reset = true;
        }

        public void accept(Stream stream, byte[] frame) throws Exception {
            if (isProtocol(frame)) {
                protocol = Protocol.name(ALPN.libp2p, new String(
                        frame, 0, frame.length - 1));
                Utils.error(TAG, "Protocol " + protocol);
                streamResponder.protocol(stream, protocol);
            } else {
                streamResponder.data(stream, protocol, frame);
            }
        }

        int length() {
            if (frame != null) {
                return frame.length;
            }
            return 0;
        }

        @NonNull
        @Override
        public String toString() {
            return "State{" + "frameIndex=" + frameIndex + ", reset=" + reset + '}';
        }
    }
}
