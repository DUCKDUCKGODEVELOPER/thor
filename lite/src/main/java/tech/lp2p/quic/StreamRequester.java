package tech.lp2p.quic;


import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

import tech.lp2p.core.ALPN;
import tech.lp2p.utils.Utils;

public interface StreamRequester {
    String PEER = "PEER";
    String RELAYED = "RELAYED";
    String STREAM = "STREAM";
    String ADDRS = "ADDRS";
    String TIMER = "TIMER";
    String INITIALIZED = "INITIALIZED";
    String RESERVATION = "RESERVATION";

    @NonNull
    static Stream createStream(@NonNull Connection connection, long delimiter,
                               @NonNull StreamRequester streamRequester)
            throws InterruptedException, TimeoutException {
        if (connection.alpn() == ALPN.libp2p) {
            return connection.createStream(AlpnLibp2pRequester.create(streamRequester), delimiter,
                    true);
        }
        throw new InterruptedException("not supported alpn");
    }

    @NonNull
    static Stream createStream(@NonNull Connection connection, long delimiter)
            throws InterruptedException, TimeoutException {
        return connection.createStream(delimiter, true);
    }

    /**
     * @noinspection BooleanMethodIsAlwaysInverted
     */
    static boolean isProtocol(byte[] data) {
        if (data.length > 2) {
            return data[0] == '/' && data[data.length - 1] == '\n';
        }
        return false;
    }

    static byte[] unsignedVarintReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        if (data.remaining() == 0) {
            return Utils.BYTES_EMPTY;
        }
        return new byte[readUnsignedVariant(data)];
    }

    private static int readUnsignedVariant(ByteBuffer in) {
        int result = 0;
        int cur;
        int count = 0;
        do {
            cur = in.get() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IllegalStateException("invalid unsigned variant sequence");
        }
        return result;
    }

    static byte[] data(ByteBuffer bytes) {

        byte[] frame = unsignedVarintReader(bytes);

        if (frame.length == 0) {
            return frame;
        } else {
            int frameIndex = 0;
            int read = Math.min(frame.length, bytes.remaining());
            for (int i = 0; i < read; i++) {
                frame[frameIndex] = bytes.get();
                frameIndex++;
            }
            if (read == frame.length) {
                if (!isProtocol(frame)) {
                    return frame;
                }
            }
            // check for a next iteration
            if (bytes.remaining() > 0) {
                return data(bytes);
            }
            return Utils.BYTES_EMPTY;
        }
    }

    void throwable(Throwable throwable);

    void data(Stream stream, byte[] data) throws Exception;

    void terminated(Stream stream);

    void fin(Stream stream);

}
