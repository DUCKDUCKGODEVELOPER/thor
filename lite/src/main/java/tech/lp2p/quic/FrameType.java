package tech.lp2p.quic;

public enum FrameType {
    AckFrame, CryptoFrame, StreamFrame, ConnectionCloseFrame,
    DataBlockedFrame, StreamDataBlockedFrame, HandshakeDoneFrame,
    MaxDataFrame, PingFrame, PaddingFrame, NewTokenFrame,
    ResetStreamFrame, MaxStreamDataFrame, MaxStreamsFrame,
    NewConnectionIdFrame, PathChallengeFrame, PathResponseFrame,
    RetireConnectionIdFrame, StopSendingFrame, StreamsBlockedFrame,
    DatagramFrame
}
