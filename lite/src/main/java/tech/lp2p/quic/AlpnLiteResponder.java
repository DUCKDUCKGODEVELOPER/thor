package tech.lp2p.quic;

import androidx.annotation.NonNull;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import lite.pb.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Responder;
import tech.lp2p.lite.LiteErrorCode;


public record AlpnLiteResponder(@NonNull Responder responder) implements Consumer<StreamData> {

    private static final ExecutorService EXECUTORS = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors());

    public static AlpnLiteResponder create(@NonNull Responder responder) {
        return new AlpnLiteResponder(responder);
    }

    @Override
    public void accept(StreamData rawData) {

        Stream stream = rawData.stream();

        if (rawData.isTerminated()) {
            return;
        }

        byte[] data;
        Protocol protocol;
        try {
            Lite.ProtoSelect protoSelect = Lite.ProtoSelect.parseFrom(rawData.data());
            data = protoSelect.getData().toByteArray();
            protocol = Protocol.codec(ALPN.lite, protoSelect.getProtocol().getNumber());
            Objects.requireNonNull(data);
            Objects.requireNonNull(protocol);
        } catch (Throwable throwable) {
            // connection will be closed in case of an exception [intention]
            stream.connection().close(LiteErrorCode.PROTOCOL_NEGOTIATION_FAILED);
            return;
        }
        EXECUTORS.execute(() -> {
            try {
                responder.data(stream, protocol, data);
            } catch (Throwable throwable) {
                // connection will be closed in case of an exception [intention]
                stream.connection().close(LiteErrorCode.INTERNAL_ERROR);
            }
        });

    }
}
