package tech.lp2p.quic;

public abstract class ApplicationProtocolConnectionFactory {

    public abstract void createConnection(String ignoredProtocol,
                                          ServerConnection connection) throws TransportError;
}
