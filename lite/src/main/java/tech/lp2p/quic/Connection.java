package tech.lp2p.quic;

import static tech.lp2p.quic.Level.App;
import static tech.lp2p.quic.Level.Handshake;
import static tech.lp2p.quic.Level.Initial;
import static tech.lp2p.quic.Settings.EMPTY_FRAME_CALLBACK;
import static tech.lp2p.quic.TransportError.Code.CRYPTO_ERROR;
import static tech.lp2p.quic.TransportError.Code.INTERNAL_ERROR;
import static tech.lp2p.utils.Utils.bytesToHex;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Host;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.lite.LiteErrorCode;
import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.tls.DecryptErrorAlert;
import tech.lp2p.tls.ErrorAlert;
import tech.lp2p.tls.TlsEngine;
import tech.lp2p.utils.Utils;


public abstract class Connection extends ConnectionStreams implements tech.lp2p.core.Connection {

    private static final String TAG = Connection.class.getSimpleName();
    protected final AtomicReference<Status> connectionState = new AtomicReference<>(Status.Created);
    protected final AtomicReference<HandshakeState> handshakeState = new AtomicReference<>(HandshakeState.Initial);
    protected final AtomicReference<TransportParameters> remoteTransportParameters =
            new AtomicReference<>();
    protected final DatagramSocket datagramSocket;
    private final long[] largestPacketNumber = new long[Level.LENGTH];
    private final RateLimiter closeFramesSendRateLimiter;
    private final Consumer<byte[]> datagramConsumer;
    private final long flowControlIncrement;  // no concurrency
    private final AtomicLong idleTimeout = new AtomicLong(Settings.MAX_IDLE_TIMEOUT);
    private final AtomicLong lastIdleAction = new AtomicLong(Settings.NOT_DEFINED);
    private final AtomicBoolean enableKeepAlive = new AtomicBoolean(false);
    private final AtomicLong lastPingAction = new AtomicLong(Settings.NOT_DEFINED);
    private final AtomicBoolean enabledIdle = new AtomicBoolean(false);
    private final InetSocketAddress remoteAddress;
    private final Thread requesterThread;
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition waitCondition = lock.newCondition();
    private final Host host;
    private final ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
    private long flowControlMax;  // no concurrency
    private long flowControlLastAdvertised;  // no concurrency
    // Using thread-confinement strategy for concurrency control: only the sender thread created in this class accesses these members
    private volatile boolean shutdownRequester = false;
    @Nullable
    private Consumer<Connection> closeConsumer = null;

    protected Connection(Host host, Version version, Role role, long initialMaxData, int initialRtt,
                         Consumer<byte[]> datagramConsumer, DatagramSocket datagramSocket,
                         InetSocketAddress remoteAddress) {
        super(version, role, initialRtt);
        this.host = host;
        this.datagramConsumer = datagramConsumer;
        this.closeFramesSendRateLimiter = new RateLimiter();
        this.flowControlMax = initialMaxData;
        this.flowControlLastAdvertised = flowControlMax;
        this.flowControlIncrement = flowControlMax / 10;
        this.datagramSocket = datagramSocket;
        this.remoteAddress = remoteAddress;
        this.requesterThread = new Thread(this::run, "sender-loop");
        requesterThread.setDaemon(true);
    }

    private static String determineClosingErrorMessage(FrameReceived.ConnectionCloseFrame closing) {
        if (closing.hasTransportError()) {
            if (closing.hasTlsError()) {
                return "TLS error " + closing.getTlsError() + (closing.hasReasonPhrase() ? ": " + closing.getReasonPhrase() : "");
            } else {
                return "transport error " + closing.getErrorCode() + (closing.hasReasonPhrase() ? ": " + closing.getReasonPhrase() : "");
            }
        } else if (closing.hasApplicationProtocolError()) {
            return "application protocol error " + closing.getErrorCode() + (closing.hasReasonPhrase() ? ": " + closing.getReasonPhrase() : "");
        } else {
            return "";
        }
    }

    private static TransportError quicError(Throwable throwable) {
        if (throwable instanceof ErrorAlert errorAlert) {
            // tlsError evaluate [and maybe in the future to support also ErrorAlert types]
            Utils.error(TAG, errorAlert.toString());
            return new TransportError(CRYPTO_ERROR, errorAlert.getMessage());
        } else if (throwable.getCause() instanceof TransportError) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-20.1
            return ((TransportError) throwable.getCause());
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-20.1
            // "INTERNAL_ERROR (0x1):  The endpoint encountered an internal error and cannot continue with the connection."
            return new TransportError(INTERNAL_ERROR, throwable.getMessage());
        }
    }

    @Override
    @NonNull
    public Host host() {
        return host;
    }

    @NonNull
    public abstract ALPN alpn();

    private void keepAlive() {
        if (enableKeepAlive.get()) {
            long now = System.currentTimeMillis();
            if (now > lastPingAction.get() + Settings.PING_INTERVAL) {
                addRequest(App, Frame.PING, Settings.EMPTY_FRAME_CALLBACK);
                flush();
                lastPingAction.set(System.currentTimeMillis());
            }
        }
    }

    final void enableKeepAlive() {
        lastPingAction.set(System.currentTimeMillis());
        enableKeepAlive.set(true);
    }

    private void disableKeepAlive() {
        enableKeepAlive.set(false);
    }

    public boolean isConnected() {
        return this.connectionState.get().isConnected();
    }

    final void updateConnectionFlowControl(int size) {
        flowControlMax += size;
        if (flowControlMax - flowControlLastAdvertised > flowControlIncrement) {
            addRequest(App, Frame.createMaxDataFrame(flowControlMax), EMPTY_FRAME_CALLBACK);
            flush();
            flowControlLastAdvertised = flowControlMax;
        }
    }

    public Stream createStream(@NonNull Consumer<StreamData> streamDataConsumer,
                               long delimiter, boolean bidirectional)
            throws InterruptedException, TimeoutException {
        return createStream(this, delimiter, stream -> streamDataConsumer, bidirectional);
    }

    public Stream createStream(long delimiter, boolean bidirectional)
            throws InterruptedException, TimeoutException {
        return createStream(this, delimiter, null, bidirectional);
    }

    final void parseAndProcessPackets(long timeReceived, ByteBuffer buffer) {
        try {
            while (buffer.remaining() > 0) {

                PacketReceived packetReceived = null;
                try {
                    packetReceived = parsePacket(buffer);

                    if (packetReceived == null) {
                        return; // when no
                    }

                    processPacket(timeReceived, packetReceived);
                } catch (DecryptErrorAlert decryptErrorAlert) {

                    Utils.error(TAG, "Parsed packet with size " +
                            buffer.position() + "; " + buffer.remaining() + " bytes left.");
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-12.2
                    // "if decryption fails (...), the receiver (...) MUST attempt to process the remaining packets."
                    int nrOfPacketBytes = buffer.position();
                    if (nrOfPacketBytes == 0) {
                        // Nothing could be made out of it, so the whole datagram will be discarded
                        nrOfPacketBytes = buffer.remaining();
                    }
                    if (checkForStatelessResetToken(buffer)) {
                        if (enterDrainingState()) {
                            Utils.debug(TAG, "Entering draining state because stateless reset was received");
                        } else {
                            Utils.error(TAG, "Received stateless reset");
                        }
                    } else {
                        Utils.error(TAG, role.name() + " Discarding packet (" + nrOfPacketBytes +
                                " bytes) that cannot be decrypted (" +
                                decryptErrorAlert + ") ");
                    }
                    // TODO [Future low] maybe some other packages might be well
                    return; // There is no point in trying to parse the rest of the datagram.
                } catch (TransportError transportError) {
                    immediateCloseWithError(packetReceived.level(), transportError);
                    return;// There is no point in trying to parse the rest of the datagram.
                }

                if (buffer.position() == 0) {
                    // If parsing (or an attempt to parse a) packet does not advance the buffer, there is no point in going on.
                    break;
                }
            }
        } finally {
            // Processed all packets in the datagram, so not expecting more.
            flush();
        }

    }

    boolean checkForStatelessResetToken(ByteBuffer data) {
        return false;
    }

    @Nullable
    private Level parseLongHeaderLevel(byte flags, ByteBuffer data) {
        final int MIN_LONGHEADERPACKET_LENGTH = 1 + 4 + 1 + 1;
        if (1 + data.remaining() < MIN_LONGHEADERPACKET_LENGTH) {
            Utils.error(TAG, "packet too short to be valid QUIC long header packet");
            return null;
        }
        int version = data.getInt();

        // https://tools.ietf.org/html/draft-ietf-quic-transport-16#section-17.4:
        // "A Version Negotiation packet ... will appear to be a packet using the long header, but
        //  will be identified as a Version Negotiation packet based on the
        //  Version field having a value of 0."
        if (version == 0) {
            Utils.error(TAG, "Version Negotiation packet is intentionally not supported");
            return null;
        }

        boolean matchingVersion = Version.parse(version).equals(version());
        if (!matchingVersion) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-5.2
            // "... packets are discarded if they indicate a different protocol version than that of the connection..."
            Utils.error(TAG, "Version does not match version of the connection");
            return null;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.5
        // "An Initial packet uses long headers with a payloadType value of 0x0."
        if ((flags & 0xf0) == 0xc0) {  // 1100 0000
            return Initial;

        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.7
        // "A Retry packet uses a long packet header with a payloadType value of 0x3"
        else if ((flags & 0xf0) == 0xf0) {  // 1111 0000
            // Retry packet....
            Utils.error(TAG, "Retry packet is intentionally not supported");
            return null;
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.6
        // "A Handshake packet uses long headers with a payloadType value of 0x2."
        else if ((flags & 0xf0) == 0xe0) {  // 1110 0000
            return Handshake;
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.2
        // "|  0x1 | 0-RTT Protected | Section 12.1 |"
        else if ((flags & 0xf0) == 0xd0) {  // 1101 0000
            // 0-RTT Protected
            // "It is used to carry "early"
            //   data from the client to the server as part of the first flight, prior
            //   to handshake completion."

            // When such a packet arrives, consider it to be caused by network corruption, so
            Utils.error(TAG, "network corruption detected");
            return null;

        } else {
            Utils.error(TAG, "Should not happen, all cases should be covered above");
            return null;
        }
    }

    @Nullable
    protected PacketReceived parsePacket(ByteBuffer data) throws DecryptErrorAlert {

        if (data.remaining() < 2) {
            Utils.error(TAG, "packet too short to be a valid QUIC packet");
            return null;
        }
        int posFlags = data.position();
        byte flags = data.get();

        if ((flags & 0x40) != 0x40) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.2
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.3
            // "Fixed Bit:  The next bit (0x40) of byte 0 is set to 1.  Packets
            //      containing a zero value for this bit are not valid packets in this
            //      version and MUST be discarded."
            Utils.error(TAG, "The next bit (0x40) of byte 0 is set to 1");
            return null;
        }


        Level level = null;
        if ((flags & 0x80) == 0x80) {
            // Long header packet
            level = parseLongHeaderLevel(flags, data);
        } else {
            // Short header packet
            if ((flags & 0xc0) == 0x40) {
                level = App;
            }
        }

        // check if the level is not discarded (level can be null, to avoid expensive
        // exception handling
        if (level == null || isDiscarded(level)) {
            return null;
        }

        try {
            PacketHeader packetHeader = switch (level) {
                case Initial -> PacketParser.parseInitialPacketHeader(
                        data, flags, posFlags, version());
                case Handshake -> PacketParser.parseHandshakePackageHeader(
                        data, flags, posFlags, version());
                case App -> PacketParser.parseShortPacketHeader(
                        data, flags, posFlags, version(), sourceCidLength());
            };


            Keys keys = remoteSecrets(packetHeader.level());


            if (keys == null) {
                // Could happen when, due to packet reordering, the first short header packet arrives
                // before handshake is finished.
                // https://tools.ietf.org/html/draft-ietf-quic-tls-18#section-5.7
                // "Due to reordering and loss, protected packets might be received by an
                //   endpoint before the final TLS handshake messages are received."
                throw new DecryptErrorAlert(level.name());
            }

            long lpn = largestPacketNumber[packetHeader.level().ordinal()];

            PacketReceived packetReceived = PacketParser.parse(packetHeader,
                    data, keys, lpn, remoteTransportParameters.get());

            if (packetReceived.hasUpdatedKeys()) {
                // update the secrets keys here (updated was set)
                remoteSecrets(packetHeader.level(), packetReceived.updated());

                // now update own secrets
                Keys oldKeys = ownSecrets(packetHeader.level());
                Keys newKeys = Keys.computeKeyUpdate(packetHeader.version(), oldKeys);
                ownSecrets(packetHeader.level(), newKeys);

            }

            if (packetReceived.packetNumber() > largestPacketNumber[packetReceived.level().ordinal()]) {
                largestPacketNumber[packetReceived.level().ordinal()] = packetReceived.packetNumber();
            }
            return packetReceived;
        } catch (DecodeErrorException decodeErrorException) {
            Utils.error(TAG, "Ignore Packet DecodeErrorException " +
                    decodeErrorException.getClass().getSimpleName());
            return null;
        }
    }

    final void process(FrameReceived.AckFrame ackFrame, PacketReceived packetReceived, long timeReceived) {
        process(ackFrame, packetReceived.level(), timeReceived);
    }

    protected void processFrames(PacketReceived packetReceived, long timeReceived) {
        for (FrameReceived frameReceived : packetReceived.frames()) {
            switch (frameReceived.type()) {
                case PaddingFrame, NewTokenFrame, PingFrame -> {
                    // Padding will not be evaluated
                    // NewTokenFrame frames will be supported someday in the future
                    // PingFrame are intentionally left empty (nothing to do on receiving ping: will be acknowledged like any other ack-eliciting frame)
                }
                case AckFrame ->
                        process((FrameReceived.AckFrame) frameReceived, packetReceived, timeReceived);
                case ConnectionCloseFrame ->
                        process((FrameReceived.ConnectionCloseFrame) frameReceived, packetReceived);
                case CryptoFrame ->
                        process((FrameReceived.CryptoFrame) frameReceived, packetReceived);
                case MaxDataFrame -> process((FrameReceived.MaxDataFrame) frameReceived);
                case MaxStreamDataFrame ->
                        process((FrameReceived.MaxStreamDataFrame) frameReceived);
                case MaxStreamsFrame -> process((FrameReceived.MaxStreamsFrame) frameReceived);
                case NewConnectionIdFrame ->
                        process((FrameReceived.NewConnectionIdFrame) frameReceived, packetReceived);
                case PathChallengeFrame ->
                        process((FrameReceived.PathChallengeFrame) frameReceived);
                case ResetStreamFrame -> process((FrameReceived.ResetStreamFrame) frameReceived);
                case RetireConnectionIdFrame ->
                        process((FrameReceived.RetireConnectionIdFrame) frameReceived, packetReceived);
                case StopSendingFrame -> process((FrameReceived.StopSendingFrame) frameReceived);
                case StreamFrame -> process((FrameReceived.StreamFrame) frameReceived);
                case HandshakeDoneFrame ->
                        process((FrameReceived.HandshakeDoneFrame) frameReceived, packetReceived);
                case DatagramFrame ->
                        process((FrameReceived.DatagramFrame) frameReceived, packetReceived);
                default -> Utils.error(TAG, "Not handled " + frameReceived.type().name());
            }
        }
    }

    /**
     * Returns the length of the connection ID's this endpoint uses (and generates).
     *
     * @return length of the connection ID
     */
    abstract int sourceCidLength();

    // https://www.rfc-editor.org/rfc/rfc9000.html#name-path_challenge-frames
    // "The recipient of this frame MUST generate a PATH_RESPONSE frame (...) containing the same Data value."
    abstract void process(PacketReceived packetReceived, long time) throws TransportError;

    final void processPacket(long timeReceived, PacketReceived packetReceived) throws TransportError {

        if (!connectionState.get().closingOrDraining()) {
            process(packetReceived, timeReceived);

            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-13.1
            // "A packet MUST NOT be acknowledged until packet protection has been successfully removed and all frames
            //  contained in the packet have been processed."
            // "Once the packet has been fully processed, a receiver acknowledges receipt by sending one or more ACK
            //  frames containing the packet number of the received packet."
            packetReceived(packetReceived, timeReceived);
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // "An endpoint restarts its idle timer when a packet from its peer is received and processed successfully."
            packetIdleProcessed();
        } else if (connectionState.get().isClosing()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.1
            // "An endpoint in the closing state sends a packet containing a CONNECTION_CLOSE frame in response
            //  to any incoming packet that it attributes to the connection."
            handlePacketInClosingState(packetReceived);
        }
    }

    final void process(FrameReceived.DatagramFrame datagramFrame, PacketReceived packetReceived) {

        if (datagramFrame.datagram().length > Settings.MAX_DATAGRAM_FRAME_SIZE) {
            immediateCloseWithError(Level.App,
                    new TransportError(TransportError.Code.PROTOCOL_VIOLATION,
                            "Datagram exceeds mas datagram frame size"));
            return;
        }

        // The DATAGRAM frame shares the same security properties as the rest of the data
        // transmitted within a QUIC connection, and the security considerations of [RFC9000]
        // apply accordingly. All application data transmitted with the DATAGRAM frame,
        // like the STREAM frame, MUST be protected either by 0-RTT or 1-RTT keys.
        //
        // Application protocols that allow DATAGRAM frames to be sent in 0-RTT
        // require a profile that defines acceptable use of 0-RTT; see Section 5.6 of [RFC9001].
        //
        // The use of DATAGRAM frames might be detectable by an adversary on path that is
        // capable of dropping packets. Since DATAGRAM frames do not use transport-level
        // retransmission, connections that use DATAGRAM frames might be distinguished from other
        // connections due to their different response to packet loss.

        // -> currently only 1-RTT keys are supported, check here if packetReceived is
        // on the encryptionLevel App, when not close connection
        if (packetReceived.level() != App) {
            immediateCloseWithError(Level.App, new TransportError(TransportError.Code.PROTOCOL_VIOLATION,
                    "Datagram only supported on App level"));
        } else {

            // When a QUIC endpoint receives a valid DATAGRAM frame, it SHOULD deliver the data to the
            // application immediately, as long as it is able to process the frame and can
            // store the contents in memory.
            datagramConsumer.accept(datagramFrame.datagram());
        }
    }

    abstract void process(FrameReceived.HandshakeDoneFrame ignoredHandshakeDoneFrame, PacketReceived ignoredPacketReceived);

    abstract void process(FrameReceived.RetireConnectionIdFrame retireConnectionIdFrame, PacketReceived packetReceived);

    abstract void process(FrameReceived.NewConnectionIdFrame newConnectionIdFrame, PacketReceived ignoredPacketReceived);

    final void process(FrameReceived.CryptoFrame cryptoFrame, PacketReceived packetReceived) {
        try {
            getCryptoStream(packetReceived.level()).add(cryptoFrame);
        } catch (Throwable throwable) {
            immediateCloseWithError(packetReceived.level(), quicError(throwable));
        }
    }

    final void process(FrameReceived.ConnectionCloseFrame connectionCloseFrame, PacketReceived packetReceived) {
        handlePeerClosing(connectionCloseFrame, packetReceived.level());
    }

    final void process(FrameReceived.MaxDataFrame maxDataFrame) {
        processMaxDataFrame(maxDataFrame);
    }

    final void process(FrameReceived.MaxStreamDataFrame maxStreamDataFrame) {
        try {
            processMaxStreamDataFrame(maxStreamDataFrame);
        } catch (TransportError transportError) {
            immediateCloseWithError(Level.App, transportError);
        }
    }

    final void process(FrameReceived.MaxStreamsFrame maxStreamsFrame) {
        processMaxStreamsFrame(maxStreamsFrame);
    }

    final void process(FrameReceived.PathChallengeFrame pathChallengeFrame) {
        Frame response = Frame.createPathResponseFrame(pathChallengeFrame.data());
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retransmission-of-informati
        // "Responses to path validation using PATH_RESPONSE frames are sent just once."
        addRequest(App, response, EMPTY_FRAME_CALLBACK);
    }

    final void process(FrameReceived.ResetStreamFrame resetStreamFrame) {
        processResetStreamFrame(resetStreamFrame);
    }

    final void process(FrameReceived.StopSendingFrame stopSendingFrame) {
        processStopSendingFrame(stopSendingFrame);
    }

    final void process(FrameReceived.StreamFrame streamFrame) {
        try {
            processStreamFrame(this, streamFrame);
        } catch (TransportError transportError) {
            immediateCloseWithError(Level.App, transportError);
        }
    }

    /**
     * @noinspection ExtractMethodRecommender
     */
    final void determineIdleTimeout(long maxIdleTimout, long peerMaxIdleTimeout) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
        // "If a max_idle_timeout is specified by either peer in its transport parameters (Section 18.2), the
        //  connection is silently closed and its state is discarded when it remains idle for longer than the minimum
        //  of both peers max_idle_timeout values."
        long idleTimeout = Long.min(maxIdleTimout, peerMaxIdleTimeout);
        if (idleTimeout == 0) {
            // Value of 0 is the same as not specified.
            idleTimeout = Long.max(maxIdleTimout, peerMaxIdleTimeout);
        }
        if (idleTimeout != 0) {
            // Initialise the idle timer that will take care of (silently) closing connection if idle longer than idle timeout
            setIdleTimeout(idleTimeout);
        } else {
            // Both or 0 or not set: [Note: this does not occur within this application]
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-18.2
            // "Idle timeout is disabled when both endpoints omit this transport parameter or specify a value of 0."
            setIdleTimeout(Long.MAX_VALUE);
        }


    }

    private void silentlyCloseConnection(long idleTime) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.1
        // "If a max_idle_timeout is specified by either peer (...), the connection is silently closed and its state is
        //  discarded when it remains idle for longer than the minimum of both peers max_idle_timeout values."
        clearRequests();
        Utils.error(TAG, "Idle timeout: silently closing connection after " + idleTime +
                " ms of inactivity (" + bytesToHex(activeSourceCid()) + ")");
        terminate();
    }

    /**
     * Immediately closes the connection (with or without error) and enters the "closing state".
     * Connection close frame with indicated error (or "NO_ERROR") is send to peer and after 3 x PTO,
     * the closing state is ended and all connection state is discarded.
     * If this method is called outside received-message-processing, post-processing actions
     * (including flushing the sender) should be performed by the caller.
     *
     * @param level The level that should be used for sending the connection close frame
     */

    protected void immediateCloseWithError(Level level, TransportError transportError) {
        if (connectionState.get().closingOrDraining()) {
            Utils.debug(TAG, "Immediate close ignored because already closing");
            return;
        }

        disableKeepAlive();

        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        // "An endpoint sends a CONNECTION_CLOSE frame (Section 19.19) to terminate the connection immediately."
        clearRequests(); // all outgoing messages are cleared -> purpose send connection close
        addRequest(level, Frame.createConnectionCloseFrame(transportError), EMPTY_FRAME_CALLBACK);
        flush();

        // "After sending a CONNECTION_CLOSE frame, an endpoint immediately enters the closing state;"
        connectionState.set(Status.Closing);


        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.3
        // "An endpoint that has not established state, such as a server that detects an error in an Initial packet,
        //  does not enter the closing state."
        if (level == Initial) {
            terminate(); // immediately terminate
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
            // "The closing and draining connection states exist to ensure that connections close cleanly and that
            //  delayed or reordered packets are properly discarded. These states SHOULD persist for at least three
            //  times the current Probe Timeout (PTO) interval"
            int pto = getPto();
            scheduleTerminate(pto);
        }
    }

    private void scheduleTerminate(int pto) {
        // TODO [Future urgent] replace by virtual threads
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.schedule(this::terminate, 3L * pto, TimeUnit.MILLISECONDS);
        scheduler.shutdown();
    }

    private void handlePacketInClosingState(PacketReceived packetReceived) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.2
        // "An endpoint MAY enter the draining state from the closing state if it receives a CONNECTION_CLOSE frame,
        //  which indicates that the peer is also closing or draining."

        if (PacketReceived.anyMatch(packetReceived, FrameType.ConnectionCloseFrame)) {
            connectionState.set(Status.Draining);
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.1
            // "An endpoint in the closing state sends a packet containing a CONNECTION_CLOSE frame in response to any
            //  incoming packet that it attributes to the connection."
            // "An endpoint SHOULD limit the rate at which it generates packets in the closing state."
            closeFramesSendRateLimiter.execute(() -> addRequest(
                    packetReceived.level(),
                    Frame.createConnectionCloseFrame(),
                    EMPTY_FRAME_CALLBACK
            ));
            // No flush necessary, as this method is called while processing a received packet.
        }
    }

    private void handlePeerClosing(FrameReceived.ConnectionCloseFrame closing, Level level) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.2
        // "The draining state is entered once an endpoint receives a CONNECTION_CLOSE frame, which indicates that its
        //  peer is closing or draining."
        if (!connectionState.get().closingOrDraining()) {  // Can occur due to race condition (both peers closing simultaneously)
            if (closing.hasError()) {
                Utils.error(TAG, "Connection closed by peer "
                        + remotePeerId() + " with " +
                        determineClosingErrorMessage(closing));
            } else {
                Utils.debug(TAG, "Peer is closing");
            }
            clearRequests();

            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.2
            // "An endpoint that receives a CONNECTION_CLOSE frame MAY send a single packet containing a CONNECTION_CLOSE
            //  frame before entering the draining state, using a CONNECTION_CLOSE frame and a NO_ERROR code if appropriate.
            //  An endpoint MUST NOT send further packets."
            addRequest(level, Frame.createConnectionCloseFrame(), EMPTY_FRAME_CALLBACK);
            // No flush necessary, as this method is called while processing a received packet.

            drain();
        }
    }

    private void drain() {
        connectionState.set(Status.Draining);
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        // "The closing and draining connection states exist to ensure that connections close cleanly and that
        //  delayed or reordered packets are properly discarded. These states SHOULD persist for at least three
        //  times the current Probe Timeout (PTO) interval"
        int pto = getPto();
        scheduleTerminate(pto);
    }

    private boolean enterDrainingState() {
        if (connectionState.get().closingOrDraining()) {
            return false;
        } else {
            clearRequests();
            drain();
            return true;
        }
    }

    /**
     * Closes the connection by discarding all connection state. Do not call directly, should be called after
     * closing state or draining state ends.
     */
    void terminate() {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        // "Once its closing or draining state ends, an endpoint SHOULD discard all connection state."
        shutdownRequester();
        super.terminate(); // cleanup
        connectionState.set(Status.Closed);
        if (closeConsumer != null) {
            closeConsumer.accept(this); // should be done before clearing attributes
            closeConsumer = null; // not required anymore
        }
        attributes.clear();
    }

    public void setCloseConsumer(@Nullable Consumer<Connection> closeConsumer) {
        this.closeConsumer = closeConsumer;
    }

    public final void close() {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        immediateCloseWithError(App, new TransportError(TransportError.Code.NO_ERROR, ""));
    }

    public final void close(LiteErrorCode liteErrorCode) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        immediateCloseWithError(App, new TransportError(
                liteErrorCode.code(), liteErrorCode.message()));
    }

    public final Version version() {
        return version;
    }

    @NonNull
    abstract TlsEngine tlsEngine();

    /**
     * Returns the connection ID of this connection. During handshake, this is a fixed ID, that is generated by this
     * endpoint. After handshaking, this is one of the active connection ID's; if there are multiple active connection
     * ID's, which one is returned is not determined (but this method should always return the same until it is not
     * active anymore). Note that after handshaking, the connection ID (of this endpoint) is not used for sending
     * packets (short header packets only contain the destination connection ID), only for routing received packets.
     */
    @NonNull
    abstract byte[] activeSourceCid();

    /**
     * Returns the current peer connection ID, i.e. the connection ID this endpoint uses as destination connection id
     * when sending packets.
     * During handshake this is a fixed ID generated by the peer (except for the first Initial packets send by the client).
     * After handshaking, there can be multiple active connection ID's supplied by the peer; which one is current (thus,
     * is being used when sending packets) is determined by the implementation.
     */
    @NonNull
    abstract byte[] activeDestinationCid();

    @Nullable
    public X509Certificate remoteCertificate() {
        return tlsEngine().getRemoteCertificate();
    }

    public void sendDatagram(byte[] datagram) throws TransportError {
        // When an application sends a datagram over a QUIC connection, QUIC will generate a
        // new DATAGRAM frame and send it in the first available packet. This frame
        // SHOULD be sent as soon as possible (as determined by factors like congestion control,
        // described below) and MAY be coalesced with other frames.

        // Although DATAGRAM frames are not retransmitted upon loss detection,
        // they are ack-eliciting ([RFC9002]).
        addRequest(App, Frame.createDatagramFrame(datagram), EMPTY_FRAME_CALLBACK);
        flush();

    }

    @NonNull
    public abstract PeerId remotePeerId();

    abstract void abortConnection(Throwable error);

    private void setIdleTimeout(long idleTimeoutInMillis) {
        if (!enabledIdle.getAndSet(true)) {
            lastIdleAction.set(System.currentTimeMillis());
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // To avoid excessively small idle timeout periods, endpoints MUST increase the idle timeout period
            // to be at least three times the current Probe Timeout (PTO)
            idleTimeout.set(Math.max(idleTimeoutInMillis, 3L * getPto()));
        }
    }

    private void checkIdle() {
        if (enabledIdle.get()) {
            long now = System.currentTimeMillis();
            if (now > (lastIdleAction.get() + idleTimeout.get())) {
                enabledIdle.set(false);
                silentlyCloseConnection(idleTimeout.get());
            }
        }
    }

    private void packetIdleProcessed() {
        if (enabledIdle.get()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // "An endpoint restarts its idle timer when a packet from its peer is received and processed successfully."
            lastIdleAction.set(System.currentTimeMillis());

        }
    }

    private void packetIdleSent(Packet packet, long sendTime) {
        if (enabledIdle.get()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // "An endpoint also restarts its idle timer when sending an ack-eliciting packet if no other ack-eliciting
            //  packets have been sent since last receiving and processing a packet. "
            if (Packet.isAckEliciting(packet)) {
                lastIdleAction.set(sendTime);
            }
        }
    }

    public final InetSocketAddress remoteAddress() {
        return remoteAddress;
    }

    final void startRequester() {
        requesterThread.start();
    }

    final void shutdownRequester() {
        // Stopped should have be called before.
        // Stop cannot be called here (again),
        // because it would drop ConnectionCloseFrame still waiting to be sent.

        shutdownRequester = true;
        requesterThread.interrupt();
    }

    private void run() {

        try {
            // Determine whether this loop must be ended _before_ composing packets, to avoid race conditions with
            // items being queued just after the packet assembler (for that level) has executed.
            while (!shutdownRequester) {

                sendIfAny();

                keepAlive(); // only happens when enabled
                checkIdle(); // only happens when enabled

                lock.lock();
                try {
                    long timeout = determineMinimalDelay();
                    if (timeout > 0) {
                        boolean result = waitCondition.await(timeout, TimeUnit.MILLISECONDS);
                        if (Utils.isDebug()) {
                            if (!result) {
                                Utils.info(TAG, "Lock has timeout " + timeout);
                            }
                        }
                    }
                } finally {
                    lock.unlock();
                }


            }
        } catch (Throwable fatalError) {
            if (!shutdownRequester) {
                abortConnection(fatalError);
            } else {
                Utils.debug(TAG, "Ignoring " + fatalError + " because sender is shutting down.");
            }
        }
    }

    private void sendIfAny() throws IOException {
        List<SendItem> items;
        do {
            items = assemblePacket();
            if (!items.isEmpty()) {
                send(items);
            }
        }
        while (!items.isEmpty());
    }

    final void flush() {
        lock.lock();
        try {
            waitCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    private long determineMinimalDelay() {
        int delay = nextDelayedSendTime();
        if (delay >= 0) {
            return delay;
        } else {
            return Settings.MAX_IDLE_RESOLUTION;
        }
    }

    private void send(List<SendItem> itemsToSend) throws IOException {

        for (SendItem item : itemsToSend) {
            Packet packet = item.packet();
            Keys keys = ownSecrets(packet.level());
            if (keys != null) { // keys can be discard in between
                byte[] packetData = packet.generatePacketBytes(keys);

                DatagramPacket datagram = new DatagramPacket(packetData, packetData.length,
                        remoteAddress.getAddress(), remoteAddress.getPort());

                long timeSent = System.currentTimeMillis();
                datagramSocket.send(datagram);

                packetSent(item.packet(), timeSent,
                        packetData.length, item.packetLostCallback());
                packetIdleSent(item.packet(), timeSent);
            }
        }
    }


    public void setAttribute(String key, Object value) {
        attributes.put(key, value);
    }

    /**
     * @noinspection unused
     */
    @Nullable
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    public boolean hasAttribute(String key) {
        return attributes.containsKey(key);
    }

    /**
     * @noinspection unused
     */
    public void removeAttribute(String key) {
        attributes.remove(key);
    }

    private List<SendItem> assemblePacket() {
        byte[] srcCid = activeSourceCid();
        byte[] destCid = activeDestinationCid();
        return assemble((int) remainingCwnd(), srcCid, destCid);
    }

    @NonNull
    public Peeraddr remotePeeraddr() {
        return Peeraddr.create(remotePeerId(), remoteAddress());
    }

    public enum Status {
        Created,
        Handshaking,
        Connected,
        Closing,
        Draining,
        Closed,
        Failed;

        @SuppressWarnings("BooleanMethodIsAlwaysInverted")
        boolean closingOrDraining() {
            return this == Closing || this == Draining;
        }

        boolean isClosing() {
            return this == Closing;
        }

        boolean isConnected() {
            return this == Connected;
        }

        public boolean isClosed() {
            return this == Closed;
        }
    }
}
