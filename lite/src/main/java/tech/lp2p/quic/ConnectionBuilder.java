package tech.lp2p.quic;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Host;
import tech.lp2p.core.Network;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Responder;
import tech.lp2p.lite.LiteTrustManager;
import tech.lp2p.tls.CipherSuite;
import tech.lp2p.utils.Utils;

public final class ConnectionBuilder {

    private static final String TAG = ConnectionBuilder.class.getSimpleName();
    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);


    @NonNull
    public static Connection connect(@NonNull Host host,
                                     @NonNull Peeraddr address,
                                     @NonNull Parameters parameters,
                                     @NonNull Certificate certificate,
                                     @NonNull Responder responder)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;

        DatagramSocket socket;
        try {
            socket = new DatagramSocket();
        } catch (SocketException socketException) {
            throw new ConnectException(socketException.getMessage());
        }

        try {
            ClientConnection connection = ConnectionBuilder.connection(host, address,
                    parameters, certificate, responder, socket);
            run = true;
            return connection;
        } catch (Throwable throwable) {
            if (Utils.isError()) {
                Utils.error(TAG, throwable.getClass().getSimpleName());
            }
            throw throwable;
        } finally {
            if (Utils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                Utils.error(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    public static Connection connect(@NonNull Host host,
                                     @NonNull Peeraddr address,
                                     @NonNull Parameters parameters,
                                     @NonNull Certificate certificate,
                                     @NonNull Responder responder,
                                     @NonNull DatagramSocket datagramSocket)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;

        try {
            ClientConnection connection = ConnectionBuilder.connection(host,
                    address, parameters, certificate, responder, datagramSocket);

            run = true;
            return connection;
        } catch (Throwable throwable) {
            if (Utils.isError()) {
                Utils.error(TAG, throwable.getClass().getSimpleName());
            }
            throw throwable;
        } finally {
            if (Utils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                Utils.error(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    private static ClientConnection connection(@NonNull Host host,
                                               @NonNull Peeraddr address,
                                               @NonNull Parameters parameters,
                                               @NonNull Certificate certificate,
                                               @NonNull Responder responder,
                                               @NonNull DatagramSocket datagramSocket)
            throws ConnectException, InterruptedException, TimeoutException {

        String serverName = address.host();


        InetSocketAddress inetSocketAddress;
        try {
            inetSocketAddress = address.inetSocketAddress();
        } catch (UnknownHostException unknownHostException) {
            throw new ConnectException(unknownHostException.getMessage());
        }
        Objects.requireNonNull(inetSocketAddress); // not possible

        if (Utils.isDebug()) {
            Utils.error(TAG, "isWellKnownIPv4Translatable " +
                    Network.isWellKnownIPv4Translatable(inetSocketAddress.getAddress()));
        }
        int initialRtt = Settings.INITIAL_RTT;
        if (Network.isLocalAddress(inetSocketAddress.getAddress())) {
            initialRtt = 100;
        }

        if (!responder.validAlpns(parameters.alpn())) {
            throw new ConnectException("invalid alpn configuration");
        }
        Function<Stream, Consumer<StreamData>> streamDataConsumer = null;
        if (parameters.alpn() == ALPN.libp2p) {
            streamDataConsumer = quicStream -> AlpnLibp2pResponder.create(responder);
        }
        if (parameters.alpn() == ALPN.lite) {
            streamDataConsumer = quicStream -> AlpnLiteResponder.create(responder);
        }
        Objects.requireNonNull(streamDataConsumer, "Alpn not handled");

        return ClientConnection.connect(host, parameters.alpn(), serverName,
                address.peerId(), inetSocketAddress,
                Version.QUIC_version_1, parameters.initialMaxData(), initialRtt,
                List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                new LiteTrustManager(address.peerId()), certificate.x509Certificate(), certificate.key(),
                datagramSocket, datagram -> Utils.error(TAG, Arrays.toString(datagram)),
                streamDataConsumer,
                parameters.keepAlive(), Lite.CONNECT_TIMEOUT);

    }
}
