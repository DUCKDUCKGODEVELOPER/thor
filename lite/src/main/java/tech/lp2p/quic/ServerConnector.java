package tech.lp2p.quic;

import static tech.lp2p.utils.Utils.bytesToHex;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Host;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.tls.TlsServerEngineFactory;
import tech.lp2p.utils.Utils;


/**
 * Listens for QUIC connections on a given port. Requires server certificate and corresponding private key.
 */
public final class ServerConnector implements ServerConnectionRegistry {
    private static final String TAG = ServerConnector.class.getSimpleName();
    private static final int MINIMUM_LONG_HEADER_LENGTH = 1 + 4 + 1 + 1;
    private final TlsServerEngineFactory tlsEngineFactory;
    private final Receiver receiver;
    private final List<Integer> supportedVersionIds;
    private final DatagramSocket serverSocket;
    private final X509TrustManager trustManager;
    private final Map<ConnectionSource, ServerConnectionProxy> connections = new ConcurrentHashMap<>();
    private final ApplicationProtocolRegistry applicationProtocolRegistry =
            new ApplicationProtocolRegistry();
    private final Map<ALPN, Function<Stream, Consumer<StreamData>>> streamDataConsumers;
    private final Host host;
    @Nullable
    private Consumer<Peeraddr> closedConsumer;

    public ServerConnector(Host host, DatagramSocket socket, X509TrustManager trustManager,
                           byte[] certificateFile, byte[] certificateKeyFile,
                           List<Version> supportedVersions,
                           Map<ALPN, Function<Stream, Consumer<StreamData>>> streamDataConsumers) {
        this.host = host;
        this.serverSocket = socket;
        this.trustManager = trustManager;
        this.tlsEngineFactory = TlsServerEngineFactory.createTlsServerEngineFactory(
                certificateFile, certificateKeyFile);
        this.supportedVersionIds = supportedVersions.stream().map(
                Version::getId).collect(Collectors.toList());
        this.streamDataConsumers = streamDataConsumers;
        this.receiver = new Receiver(socket, new DatagramPacketConsumer(), new ThrowableConsumer());
    }

    @NonNull
    public Host host() {
        return host;
    }

    public void setClosedConsumer(@Nullable Consumer<Peeraddr> closedConsumer) {
        this.closedConsumer = closedConsumer;
    }


    public int numConnections() {
        Set<ServerConnection> treeSet = new LinkedHashSet<>();
        for (ServerConnectionProxy conn : connections.values()) {
            if (conn instanceof ServerConnection serverConnection) {
                treeSet.add(serverConnection);
            }
        }
        return treeSet.size();
    }

    public void shutdown() {
        try {
            connections.values().forEach(ServerConnectionProxy::terminate);
            receiver.shutdown();
            serverSocket.close();
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
        } finally {
            connections.clear();
        }
    }

    public void registerApplicationProtocol(String protocol, ApplicationProtocolConnectionFactory protocolConnectionFactory) {
        applicationProtocolRegistry.registerApplicationProtocol(protocol, protocolConnectionFactory);
    }

    /**
     * @noinspection unused
     */
    public Set<String> getRegisteredApplicationProtocols() {
        return applicationProtocolRegistry.getRegisteredApplicationProtocols();
    }

    public void start() {
        receiver.start();
    }

    private void process(long timeReceived, DatagramPacket datagramPacket) {
        ByteBuffer data = ByteBuffer.wrap(datagramPacket.getData(), 0, datagramPacket.getLength());
        int flags = data.get();
        data.rewind();
        if ((flags & 0b1100_0000) == 0b1100_0000) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "Header Form:  The most significant bit (0x80) of byte 0 (the first byte) is set to 1 for long headers."
            processLongHeaderPacket((InetSocketAddress) datagramPacket.getSocketAddress(), data, timeReceived);
        } else if ((flags & 0b1100_0000) == 0b0100_0000) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "Header Form:  The most significant bit (0x80) of byte 0 is set to 0 for the short header.
            processShortHeaderPacket((InetSocketAddress) datagramPacket.getSocketAddress(), data, timeReceived);
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "The next bit (0x40) of byte 0 is set to 1. Packets containing a zero value for this bit are not valid
            //  packets in this version and MUST be discarded."
            Utils.debug(TAG, "Local port " + serverSocket.getLocalPort() +
                    String.format(" Invalid Quic packet (flags: %02x) is discarded", flags));
        }
    }

    private void processLongHeaderPacket(InetSocketAddress clientAddress, ByteBuffer data, long received) {
        if (data.remaining() >= MINIMUM_LONG_HEADER_LENGTH) {
            data.position(1);
            int version = data.getInt();


            data.position(5);
            int dcidLength = data.get() & 0xff;
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "In QUIC version 1, this value MUST NOT exceed 20. Endpoints that receive a version 1 long header with a
            //  value larger than 20 MUST drop the packet. In order to properly form a Version Negotiation packet,
            //  servers SHOULD be able to read longer connection IDs from other QUIC versions."
            if (dcidLength > 20) {
                if (initialWithUnspportedVersion(data, version)) {
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-6
                    // "A server sends a Version Negotiation packet in response to each packet that might initiate a new connection;"
                    Utils.error(TAG, "initialWithUnspportedVersion not supported");
                }
                Utils.error(TAG, "Local port " + serverSocket.getLocalPort() +
                        " Ignore connection from  " + clientAddress);

                return;
            }
            if (data.remaining() >= dcidLength + 1) {  // after dcid at least one byte scid length
                byte[] dcid = new byte[dcidLength];
                data.get(dcid);

                int scidLength = data.get() & 0xff;
                if (data.remaining() >= scidLength) {
                    byte[] scid = new byte[scidLength];
                    data.get(scid);
                    data.rewind();


                    ServerConnectionProxy connection = isExistingConnection(dcid);
                    if (connection == null) {
                        Utils.debug(TAG,
                                String.format("Original destination connection id: %s (scid: %s)",
                                        bytesToHex(dcid),
                                        bytesToHex(scid)));
                        if (mightStartNewConnection(version, dcid) &&
                                isExistingConnection(dcid) == null) {
                            connection = createNewConnection(version, clientAddress, dcid);
                        } else if (initialWithUnspportedVersion(data, version)) {
                            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-6
                            // "A server sends a Version Negotiation packet in response to each packet that might initiate a new connection;"
                            // NOTE NOT SUPPORTED
                            Utils.error(TAG, "initialWithUnspportedVersion not supported");
                        }
                    }
                    if (connection != null) {
                        connection.parsePackets(received, data);
                    }
                }
            }
        }
    }

    private void processShortHeaderPacket(InetSocketAddress clientAddress, ByteBuffer data, long received) {
        byte[] dcid = new byte[Settings.DEFAULT_CID_LENGTH];
        data.position(1);
        data.get(dcid);
        data.rewind();

        ServerConnectionProxy connection = isExistingConnection(dcid);
        if (connection != null) {
            connection.parsePackets(received, data);
        } else {
            Utils.error(TAG, "Local port " + serverSocket.getLocalPort() +
                    " Discarding short header " + bytesToHex(dcid) +
                    " packet addressing non existent connection " + clientAddress.getPort());
        }
    }

    private boolean mightStartNewConnection(int version, byte[] dcid) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-7.2
        // "This Destination Connection ID MUST be at least 8 bytes in length."
        if (dcid.length >= 8) {
            return supportedVersionIds.contains(version);
        } else {
            return false;
        }
    }

    private boolean initialWithUnspportedVersion(ByteBuffer packetBytes, int version) {
        packetBytes.rewind();
        int type = (packetBytes.get() & 0x30) >> 4;
        if (PacketParser.isInitial(type, Version.parse(version))) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-14.1
            // "A server MUST discard an Initial packet that is carried in a UDP
            //   datagram with a payload that is smaller than the smallest allowed
            //   maximum datagram size of 1200 bytes. "
            if (packetBytes.limit() >= 1200) {
                return !supportedVersionIds.contains(version);
            }
        }
        return false;
    }

    private ServerConnectionProxy createNewConnection(int versionValue, InetSocketAddress clientAddress, byte[] dcid) {
        Version version = Version.parse(versionValue);
        ServerConnectionProxy connectionCandidate = new ServerConnectionCandidate(
                this, trustManager,
                serverSocket, tlsEngineFactory,
                applicationProtocolRegistry, this,
                streamDataConsumers,
                version, clientAddress, dcid);
        // Register new connection now with the original connection id, as retransmitted initial packets with the
        // same original dcid might be received, which should _not_ lead to another connection candidate)

        connections.put(new ConnectionSource(dcid), connectionCandidate);
        return connectionCandidate;
    }

    void removeConnection(ServerConnection connection) {
        for (ConnectionIdInfo info : connection.sourceConnectionIds()) {
            Objects.requireNonNull(info);
            connections.remove(new ConnectionSource(info.getConnectionId()));
        }
        connections.remove(new ConnectionSource(connection.getOriginalDestinationConnectionId()));

        if (!connection.isClosed()) {
            Utils.error(TAG, "Removed connection with dcid " +
                    Utils.bytesToHex(connection.getOriginalDestinationConnectionId()) +
                    " that is not closed...");
        }

        if (closedConsumer != null) {
            closedConsumer.accept(connection.remotePeeraddr());
        }
    }

    @Nullable
    private ServerConnectionProxy isExistingConnection(byte[] dcid) {
        return getConnections(dcid);
    }


    @Override
    public void registerConnection(ServerConnectionProxy connection, byte[] connectionId) {
        connections.put(new ConnectionSource(connectionId), connection);
    }

    @Override
    public void deregisterConnection(ServerConnectionProxy connection, byte[] connectionId) {
        boolean removed = removeConnection(connection, connectionId);
        if (!removed && containsKey(connectionId)) {
            Utils.error(TAG, "Connection " + connection + " not removed, because "
                    + getConnections(connectionId) + " is registered for "
                    + Utils.bytesToHex(connectionId));
        }
    }

    private boolean containsKey(byte[] connectionId) {
        return connections.containsKey(new ConnectionSource(connectionId));
    }

    private boolean removeConnection(ServerConnectionProxy connection, byte[] connectionId) {
        return connections.remove(new ConnectionSource(connectionId), connection);
    }

    @Nullable
    private ServerConnectionProxy getConnections(byte[] connectionId) {
        return connections.get(new ConnectionSource(connectionId));
    }


    @NonNull
    public Collection<Connection> getConnections(PeerId peerId) {
        Set<Connection> connectionSet = new HashSet<>();
        for (ServerConnectionProxy conn : connections.values()) {
            if (conn instanceof ServerConnection serverConnection) {
                if (serverConnection.isConnected()) {
                    if (Objects.equals(serverConnection.remotePeerId(), peerId)) {
                        connectionSet.add(serverConnection);
                    }
                }
            }
        }
        return connectionSet;
    }

    @Override
    public void registerAdditionalConnectionId(byte[] currentConnectionId, byte[] newConnectionId) {
        ServerConnectionProxy connection = getConnections(currentConnectionId);
        if (connection != null) {
            registerConnection(connection, newConnectionId);
        } else {
            Utils.error(TAG, "Cannot add additional cid to non-existing connection " +
                    Utils.bytesToHex(currentConnectionId));
        }
    }

    @Override
    public void deregisterConnectionId(byte[] connectionId) {
        connections.remove(new ConnectionSource(connectionId));
    }

    private static final class ThrowableConsumer implements Consumer<Throwable> {

        @Override
        public void accept(Throwable throwable) {
            Utils.error(TAG, throwable);
        }
    }

    private record ConnectionSource(byte[] dcid) {

        @Override
        public int hashCode() {
            return Arrays.hashCode(dcid); // ok, checked, maybe opt
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ConnectionSource) {
                return Arrays.equals(this.dcid, ((ConnectionSource) obj).dcid);
            } else {
                return false;
            }
        }
    }

    private final class DatagramPacketConsumer implements Consumer<DatagramPacket> {

        @Override
        public void accept(DatagramPacket datagramPacket) {
            try {
                process(System.currentTimeMillis(), datagramPacket);
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable);
            }
        }
    }
}
