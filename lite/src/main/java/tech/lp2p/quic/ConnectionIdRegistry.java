package tech.lp2p.quic;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.concurrent.ConcurrentSkipListSet;

class ConnectionIdRegistry {

    /**
     * Maps sequence number to connection ID (info)
     */
    private final ConcurrentSkipListSet<ConnectionIdInfo> connectionIds = new ConcurrentSkipListSet<>();

    ConnectionIdRegistry(ConnectionIdInfo init) {
        connectionIds.add(init);
    }

    static byte[] generateCid(int cidLength) {
        byte[] connectionId = new byte[cidLength];
        new SecureRandom().nextBytes(connectionId);
        return connectionId;
    }

    void initialConnectionId(byte[] connectionId) {
        connectionIds.first().connectionId(connectionId);
    }

    void initialStatelessResetToken(byte[] statelessResetToken) {
        connectionIds.first().statelessResetToken(statelessResetToken);
    }

    int getMaxSequenceNr() {
        int maxSequenceNr = 0;
        for (ConnectionIdInfo cidInfo : connectionIds) {
            if (cidInfo.getSequenceNumber() > maxSequenceNr) {
                maxSequenceNr = cidInfo.getSequenceNumber();
            }
        }

        return maxSequenceNr;
    }

    @Nullable
    byte[] retireCid(int sequenceNr) {
        ConnectionIdInfo cidInfo = getConnectionIdInfo(sequenceNr);
        if (cidInfo != null) {
            if (cidInfo.getConnectionIdStatus().active()) {
                cidInfo.setStatus(ConnectionIdStatus.RETIRED);
                return cidInfo.getConnectionId();
            }
        }
        return null;
    }

    @Nullable
    ConnectionIdInfo getConnectionIdInfo(int sequenceNr) {
        for (ConnectionIdInfo cidInfo : connectionIds) {
            if (cidInfo.getSequenceNumber() == sequenceNr) {
                return cidInfo;
            }
        }
        return null;
    }


    byte[] getInitial() {
        return connectionIds.first().getConnectionId();
    }

    /**
     * Get an active connection ID. There can be multiple active connection IDs, this method returns an arbitrary one.
     *
     * @return an active connection ID or null if non is active (which should never happen).
     */
    @NonNull
    byte[] getActive() {
        for (ConnectionIdInfo info : connectionIds) {
            if (info.getConnectionIdStatus().active()) {
                return info.getConnectionId();
            }
        }
        throw new IllegalStateException("no active connection id");
    }


    int getActiveCids() {
        int active = 0;
        for (ConnectionIdInfo info : connectionIds) {
            if (info.getConnectionIdStatus().active()) {
                active++;
            }
        }
        return active;
    }


    Collection<ConnectionIdInfo> connectionIds() {
        return connectionIds;
    }

}

