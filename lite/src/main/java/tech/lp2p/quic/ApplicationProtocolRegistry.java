package tech.lp2p.quic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

final class ApplicationProtocolRegistry {
    private final Map<String, ApplicationProtocolConnectionFactory> registeredFactories = new HashMap<>(); // no concurrency


    Optional<String> selectSupportedApplicationProtocol(List<String> protocols) {
        Set<String> intersection = new HashSet<>(registeredFactories.keySet());
        intersection.retainAll(protocols);
        return intersection.stream().findFirst();
    }

    void startApplicationProtocolConnection(String protocol, ServerConnection connection) throws TransportError {
        Objects.requireNonNull(registeredFactories.get(protocol)).createConnection(protocol, connection);
    }

    void registerApplicationProtocol(String protocol, ApplicationProtocolConnectionFactory factory) {
        registeredFactories.put(protocol, factory);
    }

    Set<String> getRegisteredApplicationProtocols() {
        return registeredFactories.keySet();
    }

}
