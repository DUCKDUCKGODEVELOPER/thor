package tech.lp2p.quic;

import static tech.lp2p.quic.TransportError.Code.CONNECTION_ID_LIMIT_ERROR;
import static tech.lp2p.quic.TransportError.Code.FRAME_ENCODING_ERROR;
import static tech.lp2p.quic.TransportError.Code.PROTOCOL_VIOLATION;
import static tech.lp2p.utils.Utils.bytesToHex;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import tech.lp2p.utils.Utils;

/**
 * Manages the collections of connection ID's for the connection, both for this (side of the) connection and the peer's.
 * <p>
 */
final class ConnectionIdManager {

    private static final String TAG = ConnectionIdManager.class.getSimpleName();

    private final int cidLength;
    private final ServerConnectionRegistry connectionRegistry;
    private final SendRequestQueue sendRequestQueue;
    private final Consumer<TransportError> transportError;
    private final ConnectionSourceIdRegistry cidSourceRegistry;
    private final ConnectionDestinationIdRegistry cidDestinationRegistry;
    private final byte[] initialSourceCid;
    private final byte[] initialDestinationCid;
    private final byte[] originalDestinationCid;
    /**
     * The maximum numbers of connection IDs this endpoint can use; determined by the TP
     * supplied by the peer
     */
    private final AtomicInteger remoteCidLimit = new AtomicInteger(Settings.ACTIVE_CONNECTION_ID_LIMIT);
    /**
     * The maximum number of peer connection IDs this endpoint is willing to maintain;
     * advertised in TP sent by this endpoint
     */
    private final int activeCidLimit;


    /**
     * Creates a connection ID manager for server role.
     */
    ConnectionIdManager(int cidLength, byte[] initialClientCid, byte[] originalDestinationCid,
                        int activeCidLimit, ServerConnectionRegistry connectionRegistry,
                        SendRequestQueue sendRequestQueue,
                        Consumer<TransportError> transportError) {
        this.originalDestinationCid = originalDestinationCid;
        this.cidLength = cidLength;
        this.activeCidLimit = activeCidLimit;
        this.connectionRegistry = connectionRegistry;
        this.sendRequestQueue = sendRequestQueue;
        this.transportError = transportError;
        this.cidSourceRegistry = new ConnectionSourceIdRegistry(cidLength);
        this.initialSourceCid = cidSourceRegistry.getInitial();

        if (initialClientCid != null && initialClientCid.length != 0) {
            this.cidDestinationRegistry = new ConnectionDestinationIdRegistry(initialClientCid);
            this.initialDestinationCid = initialClientCid;
        } else {
            // If peer (client) uses zero-length connection ID, it cannot change, so a registry is not needed.
            this.cidDestinationRegistry = null;
            this.initialDestinationCid = Utils.BYTES_EMPTY;
        }
    }

    /**
     * Creates a connection ID manager for client role.
     */
    ConnectionIdManager(int cidLength, int activeCidLimit, SendRequestQueue sendRequestQueue,
                        Consumer<TransportError> transportError) {
        this.activeCidLimit = activeCidLimit;
        this.sendRequestQueue = sendRequestQueue;
        this.cidSourceRegistry = new ConnectionSourceIdRegistry(cidLength);
        this.cidLength = cidLength;
        this.initialSourceCid = cidSourceRegistry.getInitial();
        this.transportError = transportError;

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-negotiating-connection-ids
        // "When an Initial packet is sent by a client (...), the client populates the Destination Connection ID field
        //  with an unpredictable value. This Destination Connection ID MUST be at least 8 bytes in length."
        this.originalDestinationCid = ConnectionIdRegistry.generateCid(8);

        this.cidDestinationRegistry = new ConnectionDestinationIdRegistry(originalDestinationCid);
        this.initialDestinationCid = originalDestinationCid;

        this.connectionRegistry = new ConnectionServerConnectionRegistry();
    }

    void handshakeFinished() {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "An endpoint SHOULD ensure that its peer has a sufficient number of available and unused connection IDs."
        // "The initial connection ID issued by an endpoint is sent in the Source Connection ID field of the long
        //  packet header (Section 17.2) during the handshake."
        int max = remoteCidLimit.get();
        for (int i = 1; i < max; i++) {
            sendNewCid();
        }
    }

    void process(FrameReceived.NewConnectionIdFrame frame) {

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
        // "An endpoint that is sending packets with a zero-length Destination Connection ID MUST treat receipt of a
        //  NEW_CONNECTION_ID frame as a connection error of payloadType PROTOCOL_VIOLATION."
        if (cidDestinationRegistry == null) {
            transportError.accept(new TransportError(PROTOCOL_VIOLATION,
                    "new connection id frame not allowed when using zero-length connection ID"));
            return;
        }

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
        // "Receiving a value in the Retire Prior To field that is greater than that in the Sequence Number field MUST
        //  be treated as a connection error of payloadType FRAME_ENCODING_ERROR."
        if (frame.retirePriorTo() > frame.sequenceNr()) {
            transportError.accept(new TransportError(FRAME_ENCODING_ERROR,
                    "exceeding active connection id limit"));
            return;
        }
        ConnectionIdInfo cidInfo = cidDestinationRegistry.getConnectionIdInfo(frame.sequenceNr());
        if (cidInfo == null) {

            boolean added = cidDestinationRegistry.registerNewConnectionId(frame.sequenceNr(),
                    frame.connectionId(), frame.statelessResetToken());
            if (!added) {
                // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
                // "An endpoint that receives a NEW_CONNECTION_ID frame with a sequence number smaller than the Retire Prior To
                //  field of a previously received NEW_CONNECTION_ID frame MUST send a corresponding RETIRE_CONNECTION_ID
                //  frame that retires the newly received connection ID, "
                sendRetireCid(frame.sequenceNr());
            }
        } else if (!Arrays.equals(cidInfo.getConnectionId(), frame.connectionId())) {
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
            // "... or if a sequence number is used for different connection IDs, the endpoint MAY treat that receipt as a
            //  connection error of payloadType PROTOCOL_VIOLATION."
            transportError.accept(new TransportError(PROTOCOL_VIOLATION,
                    "different cids or same sequence number"));
            return;
        }
        if (frame.retirePriorTo() > 0) {
            List<Integer> retired = cidDestinationRegistry.retireAllBefore(frame.retirePriorTo());
            retired.forEach(this::sendRetireCid);
        }
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "After processing a NEW_CONNECTION_ID frame and adding and retiring active connection IDs, if the number of
        //  active connection IDs exceeds the value advertised in its active_connection_id_limit transport parameter, an
        //  endpoint MUST close the connection with an error of payloadType CONNECTION_ID_LIMIT_ERROR."
        if (cidDestinationRegistry.getActiveCids() > activeCidLimit) {
            transportError.accept(new TransportError(CONNECTION_ID_LIMIT_ERROR,
                    "exceeding active connection id limit"));
        }
    }

    void process(FrameReceived.RetireConnectionIdFrame frame, byte[] destinationConnectionId) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames
        // "Receipt of a RETIRE_CONNECTION_ID frame containing a sequence number greater than any previously sent to the
        //  peer MUST be treated as a connection error of payloadType PROTOCOL_VIOLATION."
        if (frame.sequenceNumber() > cidSourceRegistry.getMaxSequenceNr()) {
            transportError.accept(new TransportError(PROTOCOL_VIOLATION,
                    "invalid connection ID sequence number"));
            return;
        }
        int sequenceNr = frame.sequenceNumber();
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames
        // "The sequence number specified in a RETIRE_CONNECTION_ID frame MUST NOT refer to the
        //  Destination Connection ID field of the packet in which the frame is contained.
        //  The peer MAY treat this as a connection error of payloadType PROTOCOL_VIOLATION."
        if (Arrays.equals(Objects.requireNonNull(
                        cidSourceRegistry.getConnectionIdInfo(sequenceNr)).getConnectionId(),
                destinationConnectionId)) {
            transportError.accept(new TransportError(PROTOCOL_VIOLATION,
                    "cannot retire current connection ID"));
            return;
        }

        byte[] retiredCid = cidSourceRegistry.retireCid(sequenceNr);
        // If not retired already
        if (retiredCid != null) {
            connectionRegistry.deregisterConnectionId(retiredCid);
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
            // "An endpoint SHOULD supply a new connection ID when the peer retires a connection ID."
            if (cidSourceRegistry.getActiveCids() < remoteCidLimit.get()) {
                sendNewCid();
            }
        }
    }

    /**
     * Register the active connection ID limit of the peer (as received by this endpoint as TP active_connection_id_limit)
     * and determine the maximum number of peer connection ID's this endpoint is willing to maintain.
     * "This is an integer value specifying the maximum number of connection IDs from the peer that an endpoint is
     * willing to store.", so it puts an upper bound to the number of connection IDs this endpoint can generate.
     */
    void remoteCidLimit(int peerCidLimit) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "An endpoint MUST NOT provide more connection IDs than the peer's limit."
        remoteCidLimit.set(peerCidLimit);
    }

    /**
     * Generate, register and send a new connection ID (that identifies this endpoint).
     */
    void sendNewCid() {
        ConnectionIdInfo cidInfo = cidSourceRegistry.generateNew(cidLength);
        connectionRegistry.registerAdditionalConnectionId(cidSourceRegistry.getActive(),
                cidInfo.getConnectionId());
        sendRequestQueue.addRequest(Frame.createNewConnectionIdFrame(cidInfo.getSequenceNumber(),
                0, cidInfo.getConnectionId()), this::retransmitFrame);
    }

    private void retransmitFrame(Frame frame) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retransmission-of-informati
        // "New connection IDs are sent in NEW_CONNECTION_ID frames and retransmitted if the packet containing them is
        //  lost. Retransmissions of this frame carry the same sequence number value."
        sendRequestQueue.addRequest(frame, this::retransmitFrame);
    }

    /**
     * Send a retire connection ID frame, that informs the peer the given connection ID will not be used by this
     * endpoint anymore for addressing the peer.
     */
    private void sendRetireCid(Integer seqNr) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retransmission-of-informati
        // "Likewise, retired connection IDs are sent in RETIRE_CONNECTION_ID frames and retransmitted if the packet
        //  containing them is lost."
        sendRequestQueue.addRequest(Frame.createRetireConnectionsIdFrame(seqNr), this::retransmitFrame);
    }


    public Collection<ConnectionIdInfo> sourceConnectionIds() {
        return cidSourceRegistry.connectionIds();
    }


    byte[] initialDestinationCid() {
        if (cidDestinationRegistry != null) {
            return cidDestinationRegistry.getInitial();
        } else {
            return Utils.BYTES_EMPTY;
        }
    }

    /**
     * Returns the (peer's) connection ID that is currently used by this endpoint to address the peer.
     */
    @NonNull
    byte[] activeDestinationCid() {
        if (cidDestinationRegistry != null) {
            return cidDestinationRegistry.getActive();
        } else {
            return Utils.BYTES_EMPTY;
        }
    }

    /**
     * Retrieves the initial connection used by this endpoint. This is the value that the endpoint included in the
     * Source Connection ID field of the first Initial packet it sends/send for the connection.
     *
     * @return the initial connection id
     */
    byte[] initialSourceCid() {
        return initialSourceCid;
    }

    /**
     * Returns the original destination connection ID, i.e. the connection ID the client used as destination in its
     * very first initial packet.
     */
    byte[] originalDestinationCid() {
        return originalDestinationCid;
    }

    /**
     * Validates the given connection ID equals the initial peer connection ID.
     *
     * @return true if given connection ID equals the initial peer connection ID, false otherwise.
     */
    boolean validateInitialDestinationCid(byte[] connectionId) {
        return Arrays.equals(connectionId, initialDestinationCid);
    }

    /**
     * Registers that the given connection is used by the peer (as destination connection ID) to send messages to this
     * endpoint.
     *
     * @param connectionId the connection ID used
     */
    void registerCidInUse(byte[] connectionId) {

        if (cidSourceRegistry.registerUsedConnectionId(connectionId)) {
            // New connection id, not used before.
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
            // "If an endpoint provided fewer connection IDs than the peer's active_connection_id_limit, it MAY supply
            //  a new connection ID when it receives a packet with a previously unused connection ID."
            if (cidSourceRegistry.getActiveCids() < remoteCidLimit.get()) {
                sendNewCid();
            }
        }
    }


    /**
     * Registers the initial connection ID issued by the peer (server). Used in client role only.
     */
    void registerInitialRemoteCid(byte[] connectionId) {
        cidDestinationRegistry.initialConnectionId(connectionId);
    }

    /**
     * Registers the stateless reset token for the initial connection ID. Used in client role only.
     */
    void initialStatelessResetToken(byte[] statelessResetToken) {
        cidDestinationRegistry.initialStatelessResetToken(statelessResetToken);
    }

    /**
     * Determines whether the given token is a stateless reset token
     */
    boolean isStatelessResetToken(byte[] data) {
        return cidDestinationRegistry.isStatelessResetToken(data);
    }

    /**
     * Returns the length of the connection ID (or connection ID's) of this endpont.
     */
    int getCidLength() {
        return cidLength;
    }

    /**
     * Returns the connection ID that this endpoint considers as "current".
     * Note that in QUIC, there is no such thing as a "current" connection ID, there are only active and retired
     * connection ID's. The peer can use any time any active connection ID.
     */
    @NonNull
    byte[] activeSourceCid() {
        return cidSourceRegistry.getActive();
    }


    // nothing to do here, it is for the client
    private static class ConnectionServerConnectionRegistry implements ServerConnectionRegistry {
        @Override
        public void registerConnection(ServerConnectionProxy connection, byte[] connectionId) {
            Utils.error(TAG, "registerConnection " + bytesToHex(connectionId));
        }

        @Override
        public void deregisterConnection(ServerConnectionProxy connection, byte[] connectionId) {
            Utils.error(TAG, "deregisterConnection " + bytesToHex(connectionId));
        }

        @Override
        public void registerAdditionalConnectionId(byte[] currentConnectionId, byte[] newConnectionId) {
            Utils.error(TAG, "registerAdditionalConnectionId " + bytesToHex(newConnectionId));
        }

        @Override
        public void deregisterConnectionId(byte[] connectionId) {
            Utils.error(TAG, "deregisterConnectionId " + bytesToHex(connectionId));
        }
    }
}
