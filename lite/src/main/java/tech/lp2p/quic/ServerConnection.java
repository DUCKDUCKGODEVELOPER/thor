package tech.lp2p.quic;

import static tech.lp2p.quic.Connection.Status.Connected;
import static tech.lp2p.quic.Level.App;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.PeerId;
import tech.lp2p.tls.ApplicationLayerProtocolNegotiationExtension;
import tech.lp2p.tls.CertificateMessage;
import tech.lp2p.tls.CertificateRequestMessage;
import tech.lp2p.tls.CertificateVerifyMessage;
import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.tls.EncryptedExtensions;
import tech.lp2p.tls.ErrorAlert;
import tech.lp2p.tls.Extension;
import tech.lp2p.tls.FinishedMessage;
import tech.lp2p.tls.MissingExtensionAlert;
import tech.lp2p.tls.NewSessionTicket;
import tech.lp2p.tls.NoApplicationProtocolAlert;
import tech.lp2p.tls.ServerHello;
import tech.lp2p.tls.ServerMessageSender;
import tech.lp2p.tls.TlsEngine;
import tech.lp2p.tls.TlsServerEngine;
import tech.lp2p.tls.TlsServerEngineFactory;
import tech.lp2p.tls.TlsStatusEventHandler;
import tech.lp2p.utils.Utils;


public final class ServerConnection extends Connection implements ServerConnectionProxy {

    private final TlsServerEngine tlsEngine;
    private final ApplicationProtocolRegistry applicationProtocolRegistry;
    private final ServerConnector serverConnector;
    private final ConnectionIdManager connectionIdManager;
    private final AtomicLong bytesReceived = new AtomicLong(0L);
    private final AtomicReference<ALPN> negotiatedApplicationProtocol =
            new AtomicReference<>(ALPN.libp2p);
    private final Map<ALPN, Function<Stream, Consumer<StreamData>>> streamDataConsumers;
    @Nullable
    private PeerId remotePeerId;

    ServerConnection(ServerConnector serverConnector, Version version, X509TrustManager trustManager,
                     DatagramSocket serverSocket, InetSocketAddress initialClientAddress,
                     byte[] peerCid, byte[] originalDcid,
                     TlsServerEngineFactory tlsServerEngineFactory,
                     ApplicationProtocolRegistry applicationProtocolRegistry,
                     ServerConnectionRegistry connectionRegistry,
                     Consumer<byte[]> datagramConsumer,
                     Map<ALPN, Function<Stream, Consumer<StreamData>>> streamDataConsumers) {
        super(serverConnector.host(), version, Role.Server, Settings.INITIAL_MAX_DATA,
                Settings.SERVER_INITIAL_RTT, datagramConsumer, serverSocket, initialClientAddress);
        this.streamDataConsumers = streamDataConsumers;
        this.applicationProtocolRegistry = applicationProtocolRegistry;
        this.serverConnector = serverConnector;
        this.tlsEngine = tlsServerEngineFactory.createServerEngine(
                trustManager, new CryptoMessageSender(), new StatusEventHandler());

        initializeCryptoStreams(tlsEngine);
        computeInitialKeys(originalDcid);


        Consumer<TransportError> transportError = (error) ->
                immediateCloseWithError(Level.App, error);
        this.connectionIdManager = new ConnectionIdManager(Settings.DEFAULT_CID_LENGTH,
                peerCid, originalDcid, Settings.ACTIVE_CONNECTION_ID_LIMIT, connectionRegistry,
                getSendRequestQueue(Level.App), transportError);

        startRequester();

    }

    @NonNull
    @Override
    public ALPN alpn() {
        return negotiatedApplicationProtocol.get();
    }

    @NonNull
    public PeerId remotePeerId() {
        return Objects.requireNonNull(remotePeerId); // invoke function when connected
    }

    @Override
    void abortConnection(Throwable error) {
        terminate();
    }

    @NonNull
    @Override
    TlsEngine tlsEngine() {
        return tlsEngine;
    }

    @Override
    protected int sourceCidLength() {
        return connectionIdManager.initialSourceCid().length;
    }


    @NonNull
    @Override
    byte[] activeSourceCid() {
        return connectionIdManager.initialSourceCid();
    }

    @NonNull
    @Override
    public byte[] activeDestinationCid() {
        return connectionIdManager.activeDestinationCid();
    }


    private void sendHandshakeDone(Frame frame) {
        addRequest(App, frame, this::sendHandshakeDone);
    }


    void parseAndProcessPackets(long timeReceived, ByteBuffer data, PacketReceived packetReceived) {
        if (PacketParser.isInitial(data) && data.limit() < 1200) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14.1
            // "A server MUST discard an Initial packet that is carried in a UDP datagram with a payload that is smaller
            //  than the smallest allowed maximum datagram size of 1200 bytes."
            return;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8
        // "Therefore, after receiving packets from an address that is not yet validated, an endpoint MUST limit the
        //  amount of data it sends to the unvalidated address to three times the amount of data received from that address."
        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8.1
        // "For the purposes of avoiding amplification prior to address validation, servers MUST count all of the
        //  payload bytes received in datagrams that are uniquely attributed to a single connection. This includes
        //  datagrams that contain packets that are successfully processed and datagrams that contain packets that
        //  are all discarded."
        bytesReceived.getAndAdd(data.remaining());

        // TODO [Future low] implement amplification [not yet implemented, for faster execution]

        if (packetReceived != null) {
            try {
                processPacket(timeReceived, packetReceived);
            } catch (TransportError transportError) {
                immediateCloseWithError(packetReceived.level(), transportError);
            } finally {
                flush();
            }
        } else {
            parseAndProcessPackets(timeReceived, data);
        }
    }


    @Override
    void process(PacketReceived packetReceived, long time) {
        switch (packetReceived.level()) {
            case Handshake -> processHandshake(packetReceived, time);
            case Initial -> processInitial(packetReceived, time);
            case App -> processShortHeader(packetReceived, time);
        }
    }

    @Override
    void process(FrameReceived.HandshakeDoneFrame ignoredHandshakeDoneFrame,
                 PacketReceived ignoredPacketReceived) {
        throw new IllegalStateException("Server Connection does not process a HandshakeDoneFrame");
    }

    private void processInitial(PacketReceived packetReceived, long time) {
        processFrames(packetReceived, time);
    }


    private void processShortHeader(PacketReceived packetReceived, long time) {
        connectionIdManager.registerCidInUse(packetReceived.destinationConnectionId());
        processFrames(packetReceived, time);
    }

    private void processHandshake(PacketReceived packetReceived, long time) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8.1
        // "In particular, receipt of a packet protected with Handshake keys confirms that the peer successfully processed
        //  an Initial packet. Once an endpoint has successfully processed a Handshake packet from the peer, it can consider
        //  the peer address to have been validated."


        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2.2.1
        // "A server stops sending and processing Initial packets when it receives its first Handshake packet. "
        discard(Level.Initial);  // Only discards when not yet done.

        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.1
        // (-> a server MUST discard Initial keys when it first successfully processes a Handshake packet)
        // 4.9.1. Discarding Initial Keys
        // Packets protected with Initial secrets (Section 5.2) are not authenticated,
        // meaning that an attacker could spoof packets with the intent to disrupt a connection.
        // To limit these attacks, Initial packet protection keys are discarded more aggressively
        // than other keys.
        //
        // The successful use of Handshake packets indicates that no more Initial packets need to
        // be exchanged, as these keys can only be produced after receiving all CRYPTO frames from
        // Initial packets. Thus, a client MUST discard Initial keys when it first sends a
        // Handshake packet and a server MUST discard Initial keys when it first successfully
        // processes a Handshake packet. Endpoints MUST NOT send Initial packets after this point.
        //
        // This results in abandoning loss recovery state for the Initial encryption level and
        // ignoring any outstanding Initial packets.
        discardInitialKeys();


        processFrames(packetReceived, time);
    }


    @Override
    public void process(FrameReceived.NewConnectionIdFrame newConnectionIdFrame, PacketReceived packetReceived) {
        connectionIdManager.process(newConnectionIdFrame);
    }

    @Override
    public void process(FrameReceived.RetireConnectionIdFrame retireConnectionIdFrame, PacketReceived packetReceived) {
        connectionIdManager.process(retireConnectionIdFrame, packetReceived.destinationConnectionId());
    }

    @NonNull
    @Override
    Function<Stream, Consumer<StreamData>> getStreamDataConsumer() {
        return Objects.requireNonNull(streamDataConsumers.get(alpn()));
    }

    @Override
    public void terminate() {

        // TODO [Future medium] not yet fully implemented, check what is missing

        super.terminate();
        serverConnector.removeConnection(this);
    }

    private void validateAndProcess(TransportParameters remoteTransportParameters) throws DecodeErrorException {

        if (remoteTransportParameters.maxUdpPayloadSize() < 1200) {
            throw new DecodeErrorException("maxUdpPayloadSize transport parameter is invalid");
        }
        if (remoteTransportParameters.ackDelayExponent() > 20) {
            throw new DecodeErrorException("ackDelayExponent transport parameter is invalid");
        }
        if (remoteTransportParameters.maxAckDelay() > 16384) { // 16384 = 2^14 ()
            throw new DecodeErrorException("maxAckDelay value of 2^14 or greater are invalid.");
        }
        if (remoteTransportParameters.activeConnectionIdLimit() < 2) {
            throw new DecodeErrorException("activeConnectionIdLimit transport parameter is invalid");
        }
        if (!connectionIdManager.validateInitialDestinationCid(
                remoteTransportParameters.initialSourceConnectionId())) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-7.3
            // "An endpoint MUST treat absence of the initial_source_connection_id transport parameter from either
            //  endpoint (...) as a connection error of payloadType TRANSPORT_PARAMETER_ERROR."
            // "An endpoint MUST treat the following as a connection error of payloadType TRANSPORT_PARAMETER_ERROR or
            //  PROTOCOL_VIOLATION: a mismatch between values received from a peer in these transport parameters and the
            //  value sent in the corresponding Destination or Source Connection ID fields of Initial packets."
            throw new DecodeErrorException("Validation connection ids failed");
        }

        determineIdleTimeout(Settings.MAX_IDLE_TIMEOUT, remoteTransportParameters.maxIdleTimeout());

        connectionIdManager.remoteCidLimit(remoteTransportParameters.activeConnectionIdLimit());

        init(remoteTransportParameters.initialMaxData(),
                remoteTransportParameters.initialMaxStreamDataBidiLocal(),
                remoteTransportParameters.initialMaxStreamDataBidiRemote(),
                remoteTransportParameters.initialMaxStreamDataUni());

        setInitialMaxStreamsBidi(remoteTransportParameters.initialMaxStreamsBidi());
        setInitialMaxStreamsUni(remoteTransportParameters.initialMaxStreamsUni());

        remoteMaxAckDelay = remoteTransportParameters.maxAckDelay();
    }


    public boolean isClosed() {
        return connectionState.get().isClosed();
    }

    byte[] getOriginalDestinationConnectionId() {
        return connectionIdManager.originalDestinationCid();
    }

    @Override
    public void parsePackets(long timeReceived, ByteBuffer data) {
        parseAndProcessPackets(timeReceived, data, null);
    }

    Collection<ConnectionIdInfo> sourceConnectionIds() {
        return connectionIdManager.sourceConnectionIds();
    }

    @NonNull
    @Override
    public String toString() {
        return "ServerConnection[" + Utils.bytesToHex(connectionIdManager.originalDestinationCid()) + "]";
    }

    public void setRemotePeerId(@NonNull PeerId remotePeerId) {
        this.remotePeerId = remotePeerId;
    }

    private class StatusEventHandler implements TlsStatusEventHandler {

        @Override
        public void earlySecretsKnown() {
        }

        @Override
        public void handshakeSecretsKnown() {
            computeHandshakeSecrets(tlsEngine, tlsEngine.getSelectedCipher());
        }

        @Override
        public void handshakeFinished() {
            computeApplicationSecrets(tlsEngine, tlsEngine.getSelectedCipher());

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.1.2
            // "the TLS handshake is considered confirmed at the server when the handshake completes"
            discard(Level.Handshake);

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
            // "An endpoint MUST discard its handshake keys when the TLS handshake is confirmed"
            // 4.9.2. Discarding Handshake Keys
            // An endpoint MUST discard its handshake keys when the TLS handshake is confirmed
            // (Section 4.1.2).
            discardHandshakeKeys();


            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
            // "The server MUST send a HANDSHAKE_DONE frame as soon as it completes the handshake."
            sendHandshakeDone(Frame.HANDSHAKE_DONE);


            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.Confirmed)) {
                    return HandshakeState.Confirmed;
                }
                return handshakeState;
            });

            if (state == HandshakeState.Confirmed) {
                handshakeStateChangedEvent(HandshakeState.Confirmed);
            } else {
                throw new IllegalStateException("Handshake server state cannot be set to Confirmed");
            }

            try {
                applicationProtocolRegistry.startApplicationProtocolConnection(
                        negotiatedApplicationProtocol.get().name(),
                        ServerConnection.this);

                connectionIdManager.handshakeFinished();

                connectionState.set(Connected);
            } catch (TransportError transportError) {
                immediateCloseWithError(Level.App, transportError);
            }

        }

        @Override
        public void newSessionTicketReceived(NewSessionTicket ticket) {
        }

        @Override
        public void extensionsReceived(Extension[] extensions) throws ErrorAlert {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-8.1
            // "Unless another mechanism is used for agreeing on an application protocol, endpoints MUST use ALPN for this purpose."
            Extension alpnExtension = null;
            for (Extension ext : extensions) {
                if (ext instanceof ApplicationLayerProtocolNegotiationExtension) {
                    alpnExtension = ext;
                    break;
                }

            }
            if (alpnExtension != null) {
                // "When using ALPN, endpoints MUST immediately close a connection (...) if an application protocol is not negotiated."
                String[] requestedProtocols = ((ApplicationLayerProtocolNegotiationExtension) alpnExtension).protocols();
                Optional<String> applicationProtocol = applicationProtocolRegistry.selectSupportedApplicationProtocol(
                        Arrays.asList(requestedProtocols));
                applicationProtocol
                        .map(protocol -> {
                            // Add negotiated protocol to TLS response (Encrypted Extensions message)
                            tlsEngine.addServerExtensions(
                                    ApplicationLayerProtocolNegotiationExtension.create(protocol));
                            return protocol;
                        })
                        .map(selectedProtocol -> negotiatedApplicationProtocol.updateAndGet(s -> ALPN.valueOf(selectedProtocol)))
                        .orElseThrow(() -> new NoApplicationProtocolAlert(Arrays.asList(requestedProtocols)));
            } else {
                throw new MissingExtensionAlert("missing application layer protocol negotiation extension");
            }

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-8.2
            // "endpoints that receive ClientHello or EncryptedExtensions messages without the quic_transport_parameters extension
            //  MUST close the connection with an error of payloadType 0x16d (equivalent to a fatal TLS missing_extension alert"
            TransportParametersExtension tpExtension = null;
            for (Extension ext : extensions) {
                if (ext instanceof TransportParametersExtension) {
                    tpExtension = (TransportParametersExtension) ext;
                    break;
                }

            }

            if (tpExtension != null) {
                validateAndProcess(tpExtension.getTransportParameters());
                remoteTransportParameters.set(tpExtension.getTransportParameters());

            } else {
                throw new MissingExtensionAlert("missing quic transport parameters extension");
            }

            TransportParameters.VersionInformation versionInformation = null;
            if (version.isV2()) {
                Version[] otherVersions = {Version.QUIC_version_2, Version.QUIC_version_1};
                versionInformation = new TransportParameters.VersionInformation(
                        Version.QUIC_version_2, otherVersions);
            }

            TransportParameters serverTransportParams = TransportParameters.createServer(
                    connectionIdManager.originalDestinationCid(),
                    connectionIdManager.initialSourceCid(),
                    Settings.INITIAL_MAX_DATA, Settings.ACTIVE_CONNECTION_ID_LIMIT,
                    versionInformation);


            TlsServerEngine.setSelectedApplicationLayerProtocol(negotiatedApplicationProtocol.get().name());
            tlsEngine.addServerExtensions(TransportParametersExtension.create(
                    version, serverTransportParams, Role.Server));

        }
    }

    private class CryptoMessageSender implements ServerMessageSender {
        @Override
        public void send(ServerHello sh) {
            CryptoStream cryptoStream = getCryptoStream(Level.Initial);
            cryptoStream.write(sh);
        }

        @Override
        public void send(EncryptedExtensions ee) {
            getCryptoStream(Level.Handshake).write(ee);
        }

        @Override
        public void send(CertificateRequestMessage cm) {
            getCryptoStream(Level.Handshake).write(cm);
        }

        @Override
        public void send(CertificateMessage cm) {
            getCryptoStream(Level.Handshake).write(cm);
        }

        @Override
        public void send(CertificateVerifyMessage cv) {
            getCryptoStream(Level.Handshake).write(cv);
        }

        @Override
        public void send(FinishedMessage finished) {
            CryptoStream cryptoStream = getCryptoStream(Level.Handshake);
            cryptoStream.write(finished);
        }

    }
}
