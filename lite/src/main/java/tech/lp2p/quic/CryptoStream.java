package tech.lp2p.quic;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.tls.ErrorAlert;
import tech.lp2p.tls.Extension;
import tech.lp2p.tls.HandshakeMessage;
import tech.lp2p.tls.HandshakeType;
import tech.lp2p.tls.ProtectionKeysType;
import tech.lp2p.tls.TlsEngine;
import tech.lp2p.tls.TlsMessageParser;
import tech.lp2p.utils.Utils;


final class CryptoStream {
    private static final String TAG = CryptoStream.class.getSimpleName();
    private final Version quicVersion;
    private final ProtectionKeysType tlsProtectionType;
    private final Role peerRole;
    private final TlsEngine tlsEngine;
    private final SendRequestQueue sendRequestQueue;
    private final TlsMessageParser tlsMessageParser;
    private final Queue<ByteBuffer> sendQueue = new ConcurrentLinkedDeque<>();
    private final AtomicInteger dataToSendOffset = new AtomicInteger(0);
    private final AtomicInteger sendStreamSize = new AtomicInteger(0);
    private final SortedSet<FrameReceived.CryptoFrame> frames = new TreeSet<>(); // no concurrency
    private boolean msgSizeRead = false; // no concurrency
    private int msgSize; // no concurrency
    private byte msgType; // no concurrency
    private long processedToOffset = 0; // no concurrency

    CryptoStream(Version quicVersion, Level level, Role role,
                 TlsEngine tlsEngine, SendRequestQueue sendRequestQueue) {
        this.quicVersion = quicVersion;
        this.peerRole = role.other();
        this.tlsEngine = tlsEngine;
        this.sendRequestQueue = sendRequestQueue;

        this.tlsProtectionType =
                level == Level.Handshake ? ProtectionKeysType.Handshake :
                        level == Level.App ? ProtectionKeysType.Application :
                                ProtectionKeysType.None;

        this.tlsMessageParser = new TlsMessageParser(this::quicExtensionsParser);
    }

    public void add(FrameReceived.CryptoFrame cryptoFrame) throws ErrorAlert {
        try {
            if (addFrame(cryptoFrame)) {
                long availableBytes = bytesAvailable();
                // Because the stream may not have enough bytes available to read the whole message, but enough to
                // read the size, the msg size read must be remembered for the next invocation of this method.
                // So, when this method is called, either one of the following cases holds:
                // - msg size was read last time, but not the message
                // - no msg size and (thus) no msg was read last time (or both where read, leading to the same state)
                // The boolean msgSizeRead is used to differentiate these two cases.
                while (msgSizeRead && availableBytes >= msgSize || !msgSizeRead && availableBytes >= 4) {
                    if (!msgSizeRead) {
                        // Determine message length (a TLS Handshake message starts with 1 byte payloadType and 3 bytes length)
                        ByteBuffer buffer = ByteBuffer.allocate(4);
                        read(buffer);
                        msgType = buffer.get(0);
                        buffer.put(0, (byte) 0);  // Mask 1st byte as it contains the TLS handshake msg payloadType
                        buffer.flip();
                        msgSize = buffer.getInt();
                        msgSizeRead = true;
                        availableBytes -= 4;
                    }
                    if (availableBytes >= msgSize) {
                        ByteBuffer msgBuffer = ByteBuffer.allocate(4 + msgSize);
                        msgBuffer.putInt(msgSize);
                        msgBuffer.put(0, msgType);
                        int read = read(msgBuffer);
                        availableBytes -= read;
                        msgSizeRead = false;

                        msgBuffer.flip();

                        tlsMessageParser.parseAndProcessHandshakeMessage(msgBuffer,
                                tlsEngine, tlsProtectionType);

                        if (msgBuffer.hasRemaining()) {
                            throw new RuntimeException();  // Must be programming error
                        }
                    }
                }
            } else {
                Utils.debug(TAG, "Discarding " + cryptoFrame +
                        ", because stream already parsed to " + readOffset());
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Extension quicExtensionsParser(ByteBuffer buffer, HandshakeType ignoredContext) throws DecodeErrorException {
        buffer.mark();
        int extensionType = buffer.getShort();
        buffer.reset();
        if (TransportParametersExtension.isCodepoint(quicVersion, extensionType & 0xffff)) {
            return TransportParametersExtension.parse(quicVersion, buffer, peerRole);
        } else {
            return null;
        }
    }


    public void write(HandshakeMessage message) {
        write(message.getBytes());
    }

    private void write(byte[] data) {
        sendQueue.add(ByteBuffer.wrap(data));
        sendStreamSize.getAndAdd(data.length);
        sendRequestQueue.addRequest(this::sendFrame, 10, this::retransmitCrypto);  // Caller should flush sender.
    }

    private Frame sendFrame(int maxSize) {
        int leftToSend = sendStreamSize.get() - dataToSendOffset.get();
        int bytesToSend = Integer.min(leftToSend, maxSize - 10);
        if (bytesToSend == 0) {
            return null;
        }
        if (bytesToSend < leftToSend) {
            // Need (at least) another frame to send all data. Because current method is the sender callback, flushing sender is not necessary.
            sendRequestQueue.addRequest(this::sendFrame, 10, this::retransmitCrypto);
        }

        byte[] frameData = new byte[bytesToSend];
        int frameDataOffset = 0;
        while (frameDataOffset < bytesToSend && !sendQueue.isEmpty()) {
            ByteBuffer buffer = sendQueue.peek();
            if (buffer != null) {
                int bytesToCopy = Integer.min(bytesToSend - frameDataOffset, buffer.remaining());
                buffer.get(frameData, frameDataOffset, bytesToCopy);
                if (buffer.remaining() == 0) {
                    sendQueue.poll();
                }
                frameDataOffset += bytesToCopy;
            }
        }

        Frame frame = Frame.createCryptoFrame(dataToSendOffset.get(), frameData);
        dataToSendOffset.getAndAdd(bytesToSend);
        return frame;
    }

    private void retransmitCrypto(Frame cryptoFrame) {
        sendRequestQueue.addRequest(cryptoFrame, this::retransmitCrypto);
    }

    /**
     * Add a stream frame to this stream. The frame can contain any number of bytes positioned anywhere in the stream;
     * the read method will take care of returning stream bytes in the right order, without gaps.
     *
     * @return true if the frame is adds bytes to this stream; false if the frame does not add bytes to the stream
     * (because the frame is a duplicate or its stream bytes where already received with previous frames).
     */
    private boolean addFrame(FrameReceived.CryptoFrame frame) {
        if (frame.getUpToOffset() > processedToOffset) {
            frames.add(frame);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the number of bytes that can be read from this stream.
     */
    private int bytesAvailable() {
        if (frames.isEmpty()) {
            return 0;
        } else {
            int available = 0;
            long countedUpTo = processedToOffset;

            for (FrameReceived.CryptoFrame nextFrame : frames) {
                if (nextFrame.offset() <= countedUpTo) {
                    if (nextFrame.getUpToOffset() > countedUpTo) {
                        available += (int) (nextFrame.getUpToOffset() - countedUpTo);
                        countedUpTo = nextFrame.getUpToOffset();
                    }
                } else {
                    break;
                }
            }
            return available;
        }

    }


    /**
     * Read a much as possible bytes from the stream (limited by the size of the given buffer or the number of bytes
     * available on the stream). If no byte is available because the end of the stream has been reached, the value -1 is returned.
     * Does not block: returns 0 when no bytes can be read.
     */
    private int read(ByteBuffer buffer) {

        if (frames.isEmpty()) {
            return 0;
        } else {
            int read = 0;
            long readUpTo = processedToOffset;
            Iterator<FrameReceived.CryptoFrame> iterator = frames.iterator();

            while (iterator.hasNext() && buffer.remaining() > 0) {
                FrameReceived.CryptoFrame nextFrame = iterator.next();
                if (nextFrame.offset() <= readUpTo) {
                    if (nextFrame.getUpToOffset() > readUpTo) {
                        long available = nextFrame.offset() - readUpTo + nextFrame.length();
                        int bytesToRead = (int) Long.min(buffer.limit() - buffer.position(), available);
                        buffer.put(nextFrame.payload(), (int) (readUpTo - nextFrame.offset()), bytesToRead);
                        readUpTo += bytesToRead;
                        read += bytesToRead;
                    }
                } else {
                    break;
                }
            }

            processedToOffset += read;
            removeParsedFrames();
            return read;
        }

    }


    private void removeParsedFrames() {
        Iterator<FrameReceived.CryptoFrame> iterator = frames.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getUpToOffset() <= processedToOffset) {
                iterator.remove();
            } else {
                break;
            }
        }
    }

    /**
     * Returns the position in the stream up to where stream bytes are read.
     */
    private long readOffset() {
        return processedToOffset;
    }


    public void cleanup() {
        frames.clear();
        sendQueue.clear();
    }
}
