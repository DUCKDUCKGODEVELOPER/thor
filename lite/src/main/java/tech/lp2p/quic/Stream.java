package tech.lp2p.quic;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.MessageLite;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.lite.LiteErrorCode;
import tech.lp2p.utils.Utils;


public final class Stream {

    private static final float RECEIVER_MAX_DATA_INCREMENT_FACTOR = 0.10f;
    private static final String TAG = Stream.class.getSimpleName();
    // Minimum stream frame size: frame payloadType (1), stream id (1..8), offset (1..8), length (1..2), data (1...)
    // Note that in practice stream id and offset will seldom / never occupy 8 bytes, so the minimum leaves more room for data.
    private static final int MIN_FRAME_SIZE = 1 + 8 + 8 + 2 + 1;

    private final int streamId;
    private final StreamFlowControl streamFlowControl;
    private final long receiverMaxDataIncrement;

    private final ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
    // not required to be thread safe, only invoked from the receiver thread
    private final SortedSet<FrameReceived.StreamFrame> frames = new TreeSet<>(); // no concurrency

    // Send request queued indicates whether a request to send a stream frame is queued with the sender.
    // Is used to avoid multiple requests being queued.
    // Thread safety: read/set by caller and by sender thread, so must be guarded by lock
    private final ReentrantLock sendRequestLock = new ReentrantLock();
    // Send queue contains stream bytes to send in order. The position of the first byte buffer in the queue determines the next byte(s) to send.
    private final Queue<ByteBuffer> sendQueue = new ConcurrentLinkedDeque<>();
    //private final int maxBufferSize;
    private final AtomicInteger bufferedBytes;
    // Reset indicates whether the OutputStream has been reset.
    private final AtomicBoolean reset = new AtomicBoolean(false);
    private final AtomicBoolean terminate = new AtomicBoolean(false);
    private final FrameDataSupplier frameDataSupplier = new FrameDataSupplier();
    private final FrameLostConsumer frameLostConsumer = new FrameLostConsumer();
    @Nullable
    private final Consumer<StreamData> streamDataConsumer;
    private final SendRequestQueue sendRequestQueue;
    private final Connection connection;
    private final CountDownLatch requestFinishCondition = new CountDownLatch(1);
    private final CountDownLatch responseFinishCondition = new CountDownLatch(1);
    private final boolean requestResponseStyle;
    private final ByteArrayOutputStream response = new ByteArrayOutputStream();
    private final long delimiter;
    // meaning that no more bytes can be written by caller.
    private volatile boolean isFinal = false;
    // Current offset is the offset of the next byte in the stream that will be sent.
    // Thread safety: only used by sender thread, no concurrency
    private long currentOffset = 0; // no concurrency
    private volatile long resetErrorCode = 0L;
    private volatile boolean allDataReceived = false;
    private long receiverFlowControlLimit; // no concurrency
    private long lastCommunicatedMaxData; // no concurrency
    private long processedToOffset = 0; // no concurrency
    // Stream offset at which the stream was last blocked, for detecting the first time stream is blocked at a certain offset.
    private long blockedOffset; // no concurrency

    public Stream(Connection connection, long delimiter, int streamId,
                  @Nullable Function<Stream, Consumer<StreamData>> streamDataConsumer) {
        this.delimiter = delimiter;
        this.connection = connection;
        this.sendRequestQueue = connection.getSendRequestQueue(Level.App);
        this.streamId = streamId;
        this.requestResponseStyle = streamDataConsumer == null;
        this.streamFlowControl = new StreamFlowControl(connection,
                connection.determineInitialMaxStreamData(this));
        this.bufferedBytes = new AtomicInteger();
        this.receiverFlowControlLimit = connection.getInitialMaxStreamDataBidiRemote();
        this.lastCommunicatedMaxData = receiverFlowControlLimit;
        this.receiverMaxDataIncrement = (long) (receiverFlowControlLimit * RECEIVER_MAX_DATA_INCREMENT_FACTOR);
        if (streamDataConsumer != null) {
            this.streamDataConsumer = streamDataConsumer.apply(this);
        } else {
            this.streamDataConsumer = null;
        }
    }

    /**
     * @noinspection unused
     */
    public boolean isInitiator() {
        return streamId % 2 == 0;
    }

    public void add(FrameReceived.StreamFrame frame) {
        boolean added = addFrame(frame);
        if (added) {
            broadcast(); // this blocks the parsing of further packets
        }
    }

    public void increaseMaxStreamDataAllowed(long maxStreamData) {
        boolean streamWasBlocked = streamFlowControl.increaseMaxStreamDataAllowed(maxStreamData);
        if (streamWasBlocked) {
            unblock();
        }
    }

    private void publishMessage() {
        try {
            if (streamDataConsumer != null) {
                streamDataConsumer.accept(new StreamData(this,
                        response.toByteArray(), true, false));
            } else {
                responseFinishCondition.countDown();
            }
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
        }
    }

    private void broadcast() {

        int bytesRead = 0;

        Iterator<FrameReceived.StreamFrame> iterator = frames.iterator();
        boolean isFinal = false;
        while (iterator.hasNext()) {
            FrameReceived.StreamFrame nextFrame = iterator.next();

            if (nextFrame.offset() <= processedToOffset) {
                long upToOffset = nextFrame.offsetLength();
                if (upToOffset >= processedToOffset) {

                    bytesRead += nextFrame.length();

                    if (!requestResponseStyle) {
                        try {
                            Objects.requireNonNull(streamDataConsumer, "No data consumer");
                            streamDataConsumer.accept(new StreamData(this,
                                    nextFrame.streamData(), nextFrame.isFinal(), false));
                        } catch (Throwable throwable) {
                            Utils.error(TAG, throwable);
                        }
                    } else {
                        try {
                            response.write(nextFrame.streamData());
                        } catch (Throwable throwable) {
                            Utils.error(TAG, throwable);
                        }
                    }
                    processedToOffset = nextFrame.offsetLength();


                    if (nextFrame.isFinal()) {
                        isFinal = true;
                    }

                    iterator.remove(); // done not needed anymore
                }
            } else {
                break;
            }
        }

        if (bytesRead > 0) {
            updateAllowedFlowControl(bytesRead);
        }

        // case when to much data is send
        if (processedToOffset > delimiter) {
            Utils.error(TAG, "To much data send " + processedToOffset + " max " + delimiter);
            resetStream(TransportError.Code.APPLICATION_ERROR.value());
            return;
        }

        if (frames.isEmpty()) {
            this.allDataReceived = isFinal;
            if (isFinal) {
                if (requestResponseStyle) {
                    publishMessage();
                }
            }
        }
    }

    private void updateAllowedFlowControl(int bytesRead) {
        // Slide flow control window forward (with as much bytes as are read)
        receiverFlowControlLimit += bytesRead;
        connection.updateConnectionFlowControl(bytesRead);
        // Avoid sending flow control updates with every single read; check diff with last send max data
        if (receiverFlowControlLimit - lastCommunicatedMaxData > receiverMaxDataIncrement) {
            sendRequestQueue.addRequest(
                    Frame.createMaxStreamDataFrame(streamId, receiverFlowControlLimit), this::retransmitMaxData);
            connection.flush();
            lastCommunicatedMaxData = receiverFlowControlLimit;
        }
    }

    private void retransmitMaxData(Frame lostFrame) {
        sendRequestQueue.addRequest(
                Frame.createMaxStreamDataFrame(streamId, receiverFlowControlLimit), this::retransmitMaxData);
        Utils.error(TAG, "Retransmitted max stream data, because lost frame " + lostFrame);
    }

    public boolean isUnidirectional() {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-23#section-2.1
        // "The second least significant bit (0x2) of the stream ID distinguishes
        //   between bidirectional streams (with the bit set to 0) and
        //   unidirectional streams (with the bit set to 1)."
        return (streamId & 0x0002) == 0x0002;
    }

    public boolean isClientInitiatedBidirectional() {
        // "Client-initiated streams have even-numbered stream IDs (with the bit set to 0)"
        return (streamId & 0x0003) == 0x0000;
    }

    public boolean isServerInitiatedBidirectional() {
        // "server-initiated streams have odd-numbered stream IDs"
        return (streamId & 0x0003) == 0x0001;
    }


    /**
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-operations-on-streams">...</a>
     * "reset the stream (abrupt termination), resulting in a RESET_STREAM frame (Section 19.4) if the stream was
     * not already in a terminal state."
     */
    public void resetStream(LiteErrorCode errorCode) {
        resetStream(errorCode.code());
    }

    void resetStream(long errorCode) {
        if (!reset.getAndSet(true)) {
            resetErrorCode = errorCode;
            sendRequestQueue.addRequest(this::createResetFrame,
                    Frame.getMaximumResetStreamFrameSize(streamId, resetErrorCode),
                    this::retransmitResetFrame);
            connection.flush();
        }
        terminate(errorCode);
    }

    @NonNull
    @Override
    public String toString() {
        return "Stream " + streamId;
    }

    /**
     * Terminates the receiving input stream (abruptly). Is called when peer sends a RESET_STREAM frame
     * <p>
     * This method is intentionally package-protected, as it should only be called by the StreamManager class.
     */
    void terminate(long errorCode) {

        Utils.info(TAG, "Terminate (reset) Stream " + errorCode);
        if (streamDataConsumer != null) {
            if (!terminate.getAndSet(true)) {
                streamDataConsumer.accept(new StreamData(this, Utils.BYTES_EMPTY,
                        false, true));
            }
        }
        terminate();
    }

    /**
     * @noinspection unused
     */
    public void stopLoading(int errorCode) {
        // Note that QUIC specification does not define application protocol error codes.
        // By absence of an application specified error code, the arbitrary code 0 is used.
        if (!allDataReceived) {
            sendRequestQueue.addRequest(Frame.createStopSendingFrame(streamId, errorCode)
                    , this::retransmitStopInput);
            connection.flush();
        }
    }

    private void retransmitStopInput(Frame lostFrame) {
        if (!allDataReceived) {
            sendRequestQueue.addRequest(lostFrame, this::retransmitStopInput);
        }
    }

    void terminate() {
        reset.compareAndSet(false, true);
        responseFinishCondition.countDown();
        requestFinishCondition.countDown();
        sendQueue.clear();
        streamFlowControl.unregister();
        attributes.clear();
    }

    // this is a non blocking response, usually invoked on a responder handler
    public void writeOutput(byte[] data, boolean isFinal) {
        this.isFinal = isFinal;
        sendQueue.add(ByteBuffer.wrap(data));
        bufferedBytes.getAndAdd(data.length);
        sendNotification();
    }

    public void response(@NonNull MessageLite message, int timeout)
            throws TimeoutException, InterruptedException {
        this.isFinal = true;
        byte[] data = message.toByteArray();
        sendQueue.add(ByteBuffer.wrap(data));
        bufferedBytes.getAndAdd(data.length);
        sendNotification();
        try {
            boolean finished = requestFinishCondition.await(timeout, TimeUnit.SECONDS);
            if (!finished) {
                resetStream(TransportError.Code.APPLICATION_ERROR.value());
                throw new TimeoutException("Request timed out after " + timeout + " s");
            } else {
                resetStream(TransportError.Code.NO_ERROR.value());
            }
        } catch (InterruptedException interruptedException) {
            resetStream(TransportError.Code.APPLICATION_ERROR.value());
            Utils.error(TAG, "InterruptedException " + interruptedException.getMessage());
            throw interruptedException;
        }
    }

    // info: writes the fin marker to the output stream (output stream will be closed)
    public void response(int timeout) throws TimeoutException, InterruptedException {
        this.isFinal = true;
        byte[] data = Utils.BYTES_EMPTY;
        sendQueue.add(ByteBuffer.wrap(data));
        bufferedBytes.getAndAdd(data.length);
        sendNotification();

        try {
            boolean finished = requestFinishCondition.await(timeout, TimeUnit.SECONDS);
            if (!finished) {
                resetStream(TransportError.Code.APPLICATION_ERROR.value());
                throw new TimeoutException("Request timed out after " + timeout + " s");
            } else {
                resetStream(TransportError.Code.NO_ERROR.value());
            }
        } catch (InterruptedException interruptedException) {
            resetStream(TransportError.Code.APPLICATION_ERROR.value());
            Utils.error(TAG, "InterruptedException " + interruptedException.getMessage());
            throw interruptedException;
        }
    }

    // this is a blocking request with the given timeout [fin is written, no more writing data allowed]
    public byte[] request(byte[] data, int timeout) throws TimeoutException, InterruptedException {
        this.isFinal = true;
        sendQueue.add(ByteBuffer.wrap(data));
        bufferedBytes.getAndAdd(data.length);
        sendNotification();

        try {
            boolean finished = responseFinishCondition.await(timeout, TimeUnit.SECONDS);
            if (!finished) {
                resetStream(TransportError.Code.APPLICATION_ERROR.value());
                throw new TimeoutException("Response timed out after " + timeout + " s");
            } else {
                resetStream(TransportError.Code.NO_ERROR.value());
            }
        } catch (InterruptedException interruptedException) {
            resetStream(TransportError.Code.APPLICATION_ERROR.value());
            Utils.error(TAG, "InterruptedException " + interruptedException.getMessage());
            throw interruptedException;
        }
        Objects.requireNonNull(response, "No response defined");
        return response.toByteArray();

    }

    public Connection connection() {
        return this.connection;
    }


    public void setAttribute(String key, Object value) {
        attributes.put(key, value);
    }

    @Nullable
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    public boolean hasAttribute(String key) {
        return attributes.containsKey(key);
    }

    /**
     * @noinspection unused
     */
    public void removeAttribute(String key) {
        attributes.remove(key);
    }

    private void sendNotification() {
        sendRequestLock.lock();
        try {
            sendRequestQueue.addRequest(frameDataSupplier, MIN_FRAME_SIZE, frameLostConsumer);
            connection.flush();
        } finally {
            sendRequestLock.unlock();
        }
    }

    private Frame sendFrame(int maxFrameSize) {
        if (reset.get()) {
            return null;
        }

        if (!sendQueue.isEmpty()) {
            long flowControlLimit = streamFlowControl.getFlowControlLimit();

            if (flowControlLimit < 0) {
                return null;
            }

            int maxBytesToSend = bufferedBytes.get();

            if (flowControlLimit > currentOffset || maxBytesToSend == 0) {
                int nrOfBytes = 0;
                int dummyFrameLength = Frame.frameLength(streamId,
                        currentOffset, 0);

                maxBytesToSend = Integer.min(maxBytesToSend, maxFrameSize - dummyFrameLength - 1);  // Take one byte extra for length field var int
                int maxAllowedByFlowControl = (int) (streamFlowControl.increaseFlowControlLimit(
                        currentOffset + maxBytesToSend) - currentOffset);
                if (maxAllowedByFlowControl < 0) {
                    return null;
                }
                maxBytesToSend = Integer.min(maxAllowedByFlowControl, maxBytesToSend);

                byte[] dataToSend = new byte[maxBytesToSend];
                boolean finalFrame = false;


                boolean enter = false;
                while (nrOfBytes < maxBytesToSend && !sendQueue.isEmpty()) {
                    enter = true;
                    ByteBuffer bufferFuture = sendQueue.peek();
                    if (bufferFuture != null) {
                        int position = nrOfBytes;
                        if (bufferFuture.remaining() <= maxBytesToSend - nrOfBytes) {
                            // All bytes remaining in buffer will fit in stream frame
                            nrOfBytes += bufferFuture.remaining();
                            bufferFuture.get(dataToSend, position,
                                    bufferFuture.remaining());
                            ByteBuffer poll = sendQueue.poll();
                            Objects.requireNonNull(poll);
                        } else {
                            // Just part of the buffer will fit in (and will fill up) the stream frame
                            bufferFuture.get(dataToSend, position, maxBytesToSend - nrOfBytes);
                            nrOfBytes = maxBytesToSend;  // Short form of: nrOfBytes += (maxBytesToSend - nrOfBytes)
                        }
                    }
                }

                if (!enter && !sendQueue.isEmpty() && this.isFinal) {
                    ByteBuffer bufferFuture = sendQueue.peek();
                    if (bufferFuture != null) {
                        if (bufferFuture.capacity() == 0) {
                            sendQueue.poll();
                        }
                    }
                }

                if (sendQueue.isEmpty()) {
                    finalFrame = this.isFinal;
                }

                if (nrOfBytes == 0 && !finalFrame) {
                    // Nothing to send really
                    return null;
                }

                bufferedBytes.getAndAdd(-1 * nrOfBytes);

                if (nrOfBytes < maxBytesToSend) {
                    // This can happen when not enough data is buffer to fill a stream frame, or length field is 1 byte (instead of 2 that was counted for)
                    dataToSend = Arrays.copyOfRange(dataToSend, 0, nrOfBytes);
                }

                Frame streamFrame = Frame.createStreamFrame(
                        streamId, currentOffset, dataToSend, finalFrame);

                currentOffset += nrOfBytes;


                if (!sendQueue.isEmpty()) {
                    sendNotification();
                }

                if (finalFrame) {
                    // Done! Retransmissions may follow, but don't need flow control.
                    streamFlowControl.unregister();
                    requestFinishCondition.countDown();
                }

                return streamFrame;
            } else {
                // So flowControlLimit <= currentOffset
                // Check if this condition hasn't been handled before
                if (currentOffset != blockedOffset) {
                    // Not handled before, remember this offset, so this isn't executed twice for the same offset
                    blockedOffset = currentOffset;
                    // And let peer know
                    // https://www.rfc-editor.org/rfc/rfc9000.html#name-data-flow-control
                    // "A sender SHOULD send a STREAM_DATA_BLOCKED or DATA_BLOCKED frame to indicate to the receiver
                    //  that it has data to write but is blocked by flow control limits."
                    sendRequestQueue.addRequest(this::sendBlockReason, Frame.getStreamDataBlockedFrameMaxSize(streamId),
                            this::retransmitSendBlockReason);
                    connection.flush();
                }
            }
        }
        return null;
    }

    public void unblock() {
        // Stream might have been blocked (or it might have filled the flow control window exactly),
        // queue send request and let sendFrame method determine whether there is more to send or not.
        sendRequestQueue.addRequest(frameDataSupplier, MIN_FRAME_SIZE, frameLostConsumer);  // No need to flush, as this is called while processing received message
    }

    /**
     * Sends StreamDataBlockedFrame or DataBlockedFrame to the peer, provided the blocked condition is still true.
     */
    private Frame sendBlockReason(int ignoredMaxFrameSize) {

        // Retrieve actual block reason; could be "none" when an update has been received in the meantime.
        BlockReason blockReason = streamFlowControl.getFlowControlBlockReason();
        return switch (blockReason) {
            case STREAM_DATA_BLOCKED -> Frame.createStreamDataBlockedFrame(streamId, currentOffset);
            case DATA_BLOCKED -> Frame.createDataBlockedFrame(streamFlowControl.maxDataAllowed());
            default -> null;
        };
    }

    private void retransmitSendBlockReason(Frame ignoredFrame) {
        sendRequestQueue.addRequest(this::sendBlockReason, Frame.getStreamDataBlockedFrameMaxSize(streamId),
                this::retransmitSendBlockReason);
        connection.flush();
    }

    private void retransmitStreamFrame(Frame frame) {
        if (!reset.get()) {
            sendRequestQueue.addRequest(frame, frameLostConsumer);
            Utils.error(TAG, "Retransmitted lost stream frame " + frame);
        }
    }

    private Frame createResetFrame(int ignoredMaxFrameSize) { // why ignoredMaxFrameSize
        return Frame.createResetStreamFrame(streamId, resetErrorCode, currentOffset);
    }

    private void retransmitResetFrame(Frame frame) {
        sendRequestQueue.addRequest(frame, this::retransmitResetFrame);
    }

    /**
     * Add a stream frame to this stream. The frame can contain any number of bytes positioned anywhere in the stream;
     * the read method will take care of returning stream bytes in the right order, without gaps.
     *
     * @return true if the frame is adds bytes to this stream; false if the frame does not add bytes to the stream
     * (because the frame is a duplicate or its stream bytes where already received with previous frames).
     */
    private boolean addFrame(FrameReceived.StreamFrame frame) {
        if (frame.offset() >= processedToOffset) {
            return frames.add(frame);
        } else {
            return false;
        }
    }

    public void writeFin() {
        writeOutput(Utils.BYTES_EMPTY, true);
    }

    public enum BlockReason {

        DATA_BLOCKED,
        STREAM_DATA_BLOCKED,
        NOT_BLOCKED
    }

    private static final class StreamFlowControl {
        private static final long UNREGISTER = -1;
        private final Connection connection;

        // The maximum amount of data that a stream would be allowed to send (to the peer), ignoring possible connection limit
        private volatile long maxStreamDataAllowed;
        // The maximum amount of data that is already assigned to a stream (i.e. already sent, or upon being sent)
        private volatile long maxStreamDataAssigned;

        StreamFlowControl(Connection connection, long initMaxStreamDataAllowed) {
            this.connection = connection;
            this.maxStreamDataAllowed = initMaxStreamDataAllowed;
            this.maxStreamDataAssigned = 0L;

        }

        public boolean increaseMaxStreamDataAllowed(long maxStreamData) {
            boolean streamWasBlocked = false;
            if (maxStreamDataAllowed != UNREGISTER) {
                // If frames are received out of order, the new max can be smaller than the current value.
                if (maxStreamData > maxStreamDataAllowed) {
                    if (maxStreamDataAssigned != UNREGISTER) {
                        streamWasBlocked = maxStreamDataAssigned == maxStreamDataAllowed
                                && connection.maxDataAssigned() != connection.maxDataAllowed();
                        maxStreamDataAllowed = maxStreamData;
                    }
                }
            }
            return streamWasBlocked;
        }


        /**
         * Returns the maximum flow control limit for the given stream, if it was requested now. Note that this limit
         * cannot be used to send data on the stream, as the flow control credits are not yet reserved.
         */
        private long getFlowControlLimit() {
            long currentMasStreamDataAssigned = maxStreamDataAssigned;
            long currentMaxStreamDataAllowed = maxStreamDataAllowed;
            if (currentMasStreamDataAssigned != UNREGISTER && currentMaxStreamDataAllowed != UNREGISTER) {
                return currentMasStreamDataAssigned + currentStreamCredits(
                        currentMasStreamDataAssigned, currentMaxStreamDataAllowed);
            }
            return UNREGISTER;

        }


        /**
         * Returns the maximum possible flow control limit for the given stream, taking into account both stream and connection
         * flow control limits. Note that the returned limit is not yet reserved for use by this stream!
         */
        private long currentStreamCredits(long maxStreamDataAssigned, long maxStreamDataAllowed) {
            long maxStreamIncrement = maxStreamDataAllowed - maxStreamDataAssigned;
            long maxPossibleDataIncrement = connection.maxDataAllowed() - connection.maxDataAssigned();
            if (maxStreamIncrement > maxPossibleDataIncrement) {
                maxStreamIncrement = maxPossibleDataIncrement;
            }
            return maxStreamIncrement;
        }

        /**
         * Request to increase the flow control limit for the indicated stream to the indicated value. Whether this is
         * possible depends on whether the stream flow control limit allows this and whether the connection flow control
         * limit has enough "unused" credits.
         *
         * @return the new flow control limit for the stream: the offset of the last byte sent on the stream may not past this limit.
         */
        private long increaseFlowControlLimit(long requestedLimit) {

            long currentMasStreamDataAssigned = maxStreamDataAssigned;
            long currentMaxStreamDataAllowed = maxStreamDataAllowed;

            if (currentMasStreamDataAssigned != UNREGISTER && currentMaxStreamDataAllowed != UNREGISTER) {
                long possibleStreamIncrement = currentStreamCredits(
                        currentMasStreamDataAssigned, currentMaxStreamDataAllowed);
                long requestedIncrement = requestedLimit - currentMasStreamDataAssigned;
                long proposedStreamIncrement = Long.min(requestedIncrement, possibleStreamIncrement);

                if (requestedIncrement < 0) {
                    throw new IllegalArgumentException();
                }

                connection.addMaxDataAssigned(proposedStreamIncrement);
                maxStreamDataAssigned = currentMasStreamDataAssigned + proposedStreamIncrement;

                return maxStreamDataAssigned;
            }
            return UNREGISTER;
        }

        public void unregister() {
            maxStreamDataAllowed = UNREGISTER;
            maxStreamDataAssigned = UNREGISTER;
        }

        /**
         * Returns the current connection flow control limit (maxDataAllowed).
         *
         * @return current connection flow control limit (maxDataAllowed)
         */
        public long maxDataAllowed() {
            return connection.maxDataAllowed();
        }


        /**
         * Returns the reason why a given stream is blocked, which can be due that the stream flow control limit is reached
         * or the connection data limit is reached.
         */
        BlockReason getFlowControlBlockReason() {
            if (maxStreamDataAssigned == maxStreamDataAllowed) {
                return BlockReason.STREAM_DATA_BLOCKED;
            }
            if (connection.maxDataAllowed() == connection.maxDataAssigned()) {
                return BlockReason.DATA_BLOCKED;
            }

            return BlockReason.NOT_BLOCKED;
        }

    }

    private final class FrameDataSupplier implements Function<Integer, Frame> {

        @Override
        public Frame apply(Integer integer) {
            return sendFrame(integer);
        }
    }

    private final class FrameLostConsumer implements Consumer<Frame> {

        @Override
        public void accept(Frame frame) {
            retransmitStreamFrame(frame);
        }
    }

}
