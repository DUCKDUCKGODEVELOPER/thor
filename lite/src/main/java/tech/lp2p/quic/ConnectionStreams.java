package tech.lp2p.quic;

import static tech.lp2p.quic.TransportError.Code.STREAM_LIMIT_ERROR;
import static tech.lp2p.quic.TransportError.Code.STREAM_STATE_ERROR;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.utils.Utils;

abstract class ConnectionStreams extends ConnectionFlow {
    private static final String TAG = ConnectionStreams.class.getSimpleName();
    private final Map<Integer, Stream> streams = new ConcurrentHashMap<>();
    private final ReentrantLock streamLock = new ReentrantLock();
    private final Semaphore openBidirectionalStreams;
    private final Semaphore openUnidirectionalStreams;
    private final AtomicLong maxOpenStreamIdUni;
    private final AtomicLong maxOpenStreamIdBidi;
    private final AtomicBoolean maxOpenStreamsUniUpdateQueued = new AtomicBoolean(false);
    private final AtomicBoolean maxOpenStreamsBidiUpdateQueued = new AtomicBoolean(false);
    private final AtomicInteger nextStreamId = new AtomicInteger(0);
    private final AtomicLong maxStreamsAcceptedByPeerBidi = new AtomicLong(0);
    private final AtomicLong maxStreamsAcceptedByPeerUni = new AtomicLong(0);
    private final MaxStreamsUpdateBidiFunction maxStreamsUpdateBidiFunction = new MaxStreamsUpdateBidiFunction();
    private final MaxStreamsUpdateUniFunction maxStreamsUpdateUniFunction = new MaxStreamsUpdateUniFunction();
    private final MaxStreamsRetransmitBidi maxStreamsRetransmitBidi = new MaxStreamsRetransmitBidi();
    private final MaxStreamsRetransmitUni maxStreamsRetransmitUni = new MaxStreamsRetransmitUni();


    protected ConnectionStreams(Version version, Role role, int initialRtt) {
        super(version, role, initialRtt);

        this.maxOpenStreamIdUni = new AtomicLong(computeMaxStreamId(
                Settings.MAX_STREAMS_UNI, role.other(), false));
        this.maxOpenStreamIdBidi = new AtomicLong(computeMaxStreamId(
                Settings.MAX_STREAMS_BIDI, role.other(), true));
        this.openBidirectionalStreams = new Semaphore(0);
        this.openUnidirectionalStreams = new Semaphore(0);
    }

    private static long computeMaxStreamId(long maxStreams, Role peerRole, boolean bidirectional) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-4.6
        // "Only streams with a stream ID less than (max_stream * 4 + initial_stream_id_for_type) can be opened; "
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-2.1
        //  | 0x0  | Client-Initiated, Bidirectional  |
        long maxStreamId = maxStreams * 4;
        //  | 0x1  | Server-Initiated, Bidirectional  |
        if (peerRole == Role.Server && bidirectional) {
            maxStreamId += 1;
        }
        //  | 0x2  | Client-Initiated, Unidirectional |
        if (peerRole == Role.Client && !bidirectional) {
            maxStreamId += 2;
        }
        //  | 0x3  | Server-Initiated, Unidirectional |
        if (peerRole == Role.Client && bidirectional) {
            maxStreamId += 3;
        }
        return maxStreamId;
    }

    private static boolean isUni(int streamId) {
        return streamId % 4 > 1;
    }

    private static boolean isBidi(int streamId) {
        return streamId % 4 < 2;
    }


    Stream createStream(Connection connection, long delimiter,
                        @Nullable Function<Stream, Consumer<StreamData>> streamDataConsumer,
                        boolean bidirectional) throws InterruptedException, TimeoutException {

        streamLock.lock();
        try {
            boolean acquired;

            if (bidirectional) {
                acquired = openBidirectionalStreams.tryAcquire(Settings.STREAM_TIMEOUT, TimeUnit.SECONDS);
            } else {
                acquired = openUnidirectionalStreams.tryAcquire(Settings.STREAM_TIMEOUT, TimeUnit.SECONDS);
            }
            if (!acquired) {
                throw new TimeoutException("Timeout for acquire a stream");
            }

            int streamId = generateStreamId(bidirectional);
            Stream stream = new Stream(connection, delimiter, streamId, streamDataConsumer);
            streams.put(streamId, stream);
            return stream;
        } finally {
            streamLock.unlock();
        }
    }

    private int generateStreamId(boolean bidirectional) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-2.1:
        // "0x0  | Client-Initiated, Bidirectional"
        // "0x1  | Server-Initiated, Bidirectional"
        int id = (nextStreamId.getAndIncrement() << 2) + (role == Role.Client ? 0x00 : 0x01);
        if (!bidirectional) {
            // "0x2  | Client-Initiated, Unidirectional |"
            // "0x3  | Server-Initiated, Unidirectional |"
            id += 0x02;
        }
        return id;
    }

    @NonNull
    abstract Function<Stream, Consumer<StreamData>> getStreamDataConsumer();

    void processStreamFrame(Connection connection, FrameReceived.StreamFrame frame) throws TransportError {
        int streamId = frame.streamId();
        Stream stream = streams.get(streamId);
        if (stream != null) {
            stream.add(frame);
            // This implementation maintains a fixed maximum number of open streams, so when the peer closes a stream
            // it is allowed to open another.
            if (frame.isFinal() && isPeerInitiated(streamId)) {
                increaseMaxOpenStreams(streamId);
            }
        } else {
            if (isPeerInitiated(streamId)) {
                if (isUni(streamId) && streamId < maxOpenStreamIdUni.get() ||
                        isBidi(streamId) && streamId < maxOpenStreamIdBidi.get()) {
                    stream = new Stream(connection, Long.MAX_VALUE, streamId, getStreamDataConsumer());
                    streams.put(streamId, stream);
                    stream.add(frame);
                    if (frame.isFinal()) {
                        increaseMaxOpenStreams(streamId);
                    }
                } else {
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-19.11
                    // "An endpoint MUST terminate a connection with a STREAM_LIMIT_ERROR error if a peer opens more
                    //  streams than was permitted."
                    throw new TransportError(STREAM_LIMIT_ERROR,
                            "peer opens more streams than was permitted");
                }
            } else {
                Utils.error(TAG, "Receiving frame for non-existent stream " + streamId);
            }
        }
    }


    void processMaxStreamDataFrame(FrameReceived.MaxStreamDataFrame frame) throws TransportError {

        int streamId = frame.streamId();


        long maxStreamData = frame.maxData();

        Stream stream = streams.get(streamId);
        if (stream != null) {
            stream.increaseMaxStreamDataAllowed(maxStreamData);
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-33#section-19.10
            // "Receiving a MAX_STREAM_DATA frame for a locally-initiated stream that has not yet been created MUST
            //  be treated as a connection error of payloadType STREAM_STATE_ERROR."
            if (locallyInitiated(streamId)) {
                throw new TransportError(STREAM_STATE_ERROR,
                        "Receiving a MAX_STREAM_DATA frame for a locally-initiated " +
                                "stream that has not yet been created");
            }
        }
    }

    private boolean locallyInitiated(int streamId) {
        if (role == Role.Client) {
            return streamId % 2 == 0;
        } else {
            return streamId % 2 == 1;
        }
    }

    void processMaxDataFrame(FrameReceived.MaxDataFrame maxDataFrame) {

        // If frames are received out of order, the new max can be smaller than the current value.
        long currentMaxDataAllowed = maxDataAllowed();
        if (maxDataFrame.maxData() > currentMaxDataAllowed) {
            boolean maxDataWasReached = currentMaxDataAllowed == maxDataAssigned();
            maxDataAllowed(maxDataFrame.maxData());
            if (maxDataWasReached) {
                for (Stream stream : streams.values()) {
                    stream.unblock();
                }
            }
        }
    }

    void processStopSendingFrame(FrameReceived.StopSendingFrame stopSendingFrame) {

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-solicited-state-transitions
        // "A STOP_SENDING frame requests that the receiving endpoint send a RESET_STREAM frame."
        Stream stream = streams.get(stopSendingFrame.streamId());
        if (stream != null) {
            // "An endpoint SHOULD copy the error code from the STOP_SENDING frame to the RESET_STREAM frame it sends, ..."
            stream.resetStream(stopSendingFrame.errorCode());
        }
    }

    void processResetStreamFrame(FrameReceived.ResetStreamFrame resetStreamFrame) {

        Stream stream = streams.get(resetStreamFrame.streamId());
        if (stream != null) {
            // "An endpoint SHOULD copy the error code from the STOP_SENDING frame to the RESET_STREAM frame it sends, ..."
            stream.terminate(resetStreamFrame.errorCode());

            streams.remove(resetStreamFrame.streamId());
        }
    }

    private void increaseMaxOpenStreams(int streamId) {

        if (isUni(streamId)) {
            maxOpenStreamIdUni.getAndAdd(4);
            if (!maxOpenStreamsUniUpdateQueued.getAndSet(true)) {
                getSendRequestQueue(Level.App).addRequest(maxStreamsUpdateUniFunction, 9,
                        maxStreamsRetransmitUni);  // Flush not necessary, as this method is called while processing received message.
            }
        } else {
            maxOpenStreamIdBidi.getAndAdd(4);
            if (!maxOpenStreamsBidiUpdateQueued.getAndSet(true)) {
                getSendRequestQueue(Level.App).addRequest(maxStreamsUpdateBidiFunction, 9,
                        maxStreamsRetransmitBidi);  // Flush not necessary, as this method is called while processing received message.
            }
        }

    }

    private Frame createMaxStreamsUpdateUni() {
        maxOpenStreamsUniUpdateQueued.set(false);
        // largest streamId < maxStreamId; e.g. client initiated: max-id = 6, server initiated: max-id = 7 => max streams = 1,
        return Frame.createMaxStreamsFrame(maxOpenStreamIdUni.get() / 4, false);
    }

    private Frame createMaxStreamsUpdateBidi() {
        maxOpenStreamsBidiUpdateQueued.set(false);

        // largest streamId < maxStreamId; e.g. client initiated: max-id = 4, server initiated: max-id = 5 => max streams = 1,
        return Frame.createMaxStreamsFrame(maxOpenStreamIdBidi.get() / 4, true);
    }

    private void retransmitMaxStreamsUni(Frame ignoredFrame) {
        getSendRequestQueue(Level.App).addRequest(createMaxStreamsUpdateUni(),
                maxStreamsRetransmitUni);

    }

    private void retransmitMaxStreamsBidi(Frame ignoredFrame) {
        getSendRequestQueue(Level.App).addRequest(createMaxStreamsUpdateBidi(),
                maxStreamsRetransmitBidi);
    }

    private boolean isPeerInitiated(int streamId) {
        return streamId % 2 == (role == Role.Client ? 1 : 0);
    }

    void processMaxStreamsFrame(FrameReceived.MaxStreamsFrame frame) {
        if (frame.appliesToBidirectional()) {
            long streamsAcceptedByPeerBidi = maxStreamsAcceptedByPeerBidi.get();
            if (frame.maxStreams() > streamsAcceptedByPeerBidi) {
                int increment = (int) (frame.maxStreams() - streamsAcceptedByPeerBidi);
                maxStreamsAcceptedByPeerBidi.set(frame.maxStreams());
                openBidirectionalStreams.release(increment);
            }
        } else {
            long streamsAcceptedByPeerUni = maxStreamsAcceptedByPeerUni.get();
            if (frame.maxStreams() > streamsAcceptedByPeerUni) {
                int increment = (int) (frame.maxStreams() - streamsAcceptedByPeerUni);
                maxStreamsAcceptedByPeerUni.set(frame.maxStreams());
                openUnidirectionalStreams.release(increment);
            }
        }
    }

    /**
     * Set initial max bidirectional streams that the peer will accept.
     */
    void setInitialMaxStreamsBidi(long initialMaxStreamsBidi) {
        if (initialMaxStreamsBidi >= maxStreamsAcceptedByPeerBidi.get()) {
            Utils.debug(TAG, "Initial max bidirectional stream: " + initialMaxStreamsBidi);
            maxStreamsAcceptedByPeerBidi.set(initialMaxStreamsBidi);
            if (initialMaxStreamsBidi > Integer.MAX_VALUE) {
                Utils.error(TAG, "Server initial max streams bidirectional is larger " +
                        "than supported; limiting to " + Integer.MAX_VALUE);
                initialMaxStreamsBidi = Integer.MAX_VALUE;
            }
            openBidirectionalStreams.release((int) initialMaxStreamsBidi);
        } else {
            Utils.error(TAG, "Attempt to reduce value of initial_max_streams_bidi from "
                    + maxStreamsAcceptedByPeerBidi + " to " + initialMaxStreamsBidi + "; ignoring.");
        }
    }

    /**
     * Set initial max unidirectional streams that the peer will accept.
     */
    void setInitialMaxStreamsUni(long initialMaxStreamsUni) {
        if (initialMaxStreamsUni >= maxStreamsAcceptedByPeerUni.get()) {

            maxStreamsAcceptedByPeerUni.set(initialMaxStreamsUni);
            if (initialMaxStreamsUni > Integer.MAX_VALUE) {
                initialMaxStreamsUni = Integer.MAX_VALUE;
            }
            openUnidirectionalStreams.release((int) initialMaxStreamsUni);
        } else {
            Utils.error(TAG, "Attempt to reduce value of initial_max_streams_uni from "
                    + maxStreamsAcceptedByPeerUni + " to " + initialMaxStreamsUni + "; ignoring.");
        }
    }

    void terminate() {
        super.terminate();
        streams.values().forEach(Stream::terminate);
        streams.clear();
    }

    private class MaxStreamsRetransmitBidi implements Consumer<Frame> {
        @Override
        public void accept(Frame frame) {
            retransmitMaxStreamsBidi(frame);
        }
    }

    private class MaxStreamsRetransmitUni implements Consumer<Frame> {
        @Override
        public void accept(Frame frame) {
            retransmitMaxStreamsUni(frame);
        }
    }

    private class MaxStreamsUpdateUniFunction implements Function<Integer, Frame> {
        @Override
        public Frame apply(Integer integer) {
            return createMaxStreamsUpdateUni();
        }
    }

    private class MaxStreamsUpdateBidiFunction implements Function<Integer, Frame> {

        @Override
        public Frame apply(Integer integer) {
            return createMaxStreamsUpdateBidi();
        }
    }

}

