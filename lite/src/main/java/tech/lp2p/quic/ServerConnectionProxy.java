package tech.lp2p.quic;

import java.nio.ByteBuffer;


public interface ServerConnectionProxy {


    void parsePackets(long timeReceived, ByteBuffer data);


    void terminate();
}
