package tech.lp2p.ident;

import identify.pb.IdentifyOuterClass;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Host;
import tech.lp2p.core.Protocol;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class IdentifyHandler implements Handler {

    private final Host host;

    public IdentifyHandler(Host host) {
        this.host = host;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        IdentifyOuterClass.Identify identify = IdentifyService.identify(
                host.keyPair(), host.agent(),
                host.protocols(ALPN.libp2p).keySet(), host.peeraddrs());
        stream.writeOutput(Utils.encode(identify,
                Protocol.MULTISTREAM_PROTOCOL, Protocol.IDENTITY_PROTOCOL), true);
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        throw new Exception("should not be invoked");
    }

    @Override
    public void closed(Stream stream) {
        // nothing to do here
    }

}
