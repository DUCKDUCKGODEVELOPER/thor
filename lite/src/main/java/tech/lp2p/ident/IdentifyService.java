package tech.lp2p.ident;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.security.KeyPair;
import java.util.Objects;
import java.util.Set;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.utils.Utils;

public interface IdentifyService {
    String PROTOCOL_VERSION = "ipfs/0.1.0";

    @NonNull
    static IdentifyOuterClass.Identify identify(@NonNull KeyPair keyPair,
                                                @NonNull String agent,
                                                @NonNull Set<Protocol> protocols,
                                                @NonNull Peeraddrs peeraddrs) {

        Crypto.PublicKey cryptoKey = Crypto.PublicKey.newBuilder()
                .setType(Crypto.KeyType.Ed25519)
                .setData(ByteString.copyFrom(keyPair.getPublic().getEncoded()))
                .build();

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(agent)
                .setPublicKey(ByteString.copyFrom(cryptoKey.toByteArray()))
                .setProtocolVersion(PROTOCOL_VERSION);

        if (!peeraddrs.isEmpty()) {
            for (Peeraddr peeraddr : peeraddrs) {
                builder.addListenAddrs(ByteString.copyFrom(peeraddr.encoded()));
            }
        }

        for (Protocol protocol : protocols) {
            if (protocol.alpn() == ALPN.libp2p) {
                builder.addProtocols(protocol.name());
            }
        }
        return builder.build();
    }

    @NonNull
    static Identify identify(Connection connection) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("ALPN must be libp2p");
        }

        byte[] raw = StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.IDENTITY_PROTOCOL.size())
                .request(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.IDENTITY_PROTOCOL), Lite.CONNECT_TIMEOUT);

        byte[] data = StreamRequester.data(ByteBuffer.wrap(raw));
        IdentifyOuterClass.Identify identify = IdentifyOuterClass.Identify.parseFrom(data);

        return identify(identify, connection.remotePeerId());
    }

    @NonNull
    static Identify identify(IdentifyOuterClass.Identify identify, PeerId expected)
            throws Exception {

        String agent = identify.getAgentVersion();


        Keys pk = Keys.unmarshalPublicKey(identify.getPublicKey().toByteArray());
        PeerId peerId = Keys.fromPubKey(pk);

        if (!Objects.equals(peerId, expected)) {
            throw new Exception("PeerId is not what is expected");
        }


        String[] protocols = new String[identify.getProtocolsCount()];
        identify.getProtocolsList().toArray(protocols);

        Peeraddrs peeraddrs = Peeraddr.create(peerId, identify.getListenAddrsList());
        Peeraddr[] addresses = new Peeraddr[peeraddrs.size()];
        peeraddrs.toArray(addresses);

        return new Identify(peerId, agent, addresses, protocols);
    }

}
