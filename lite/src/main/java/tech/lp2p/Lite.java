package tech.lp2p;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.google.crypto.tink.subtle.Ed25519Sign;
import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Host;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Info;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Multicodec;
import tech.lp2p.core.Network;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.PrivateKey;
import tech.lp2p.core.Progress;
import tech.lp2p.core.PublicKey;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Reservations;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagInputStream;
import tech.lp2p.dag.DagResolver;
import tech.lp2p.dag.DagService;
import tech.lp2p.dag.DagStream;
import tech.lp2p.dht.DhtService;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.lite.LiteFetchManager;
import tech.lp2p.lite.LiteHost;
import tech.lp2p.lite.LiteProgressStream;
import tech.lp2p.lite.LiteReader;
import tech.lp2p.lite.LiteReaderStream;
import tech.lp2p.lite.LiteServer;
import tech.lp2p.lite.LiteService;
import tech.lp2p.lite.LiteSession;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.relay.RelayDialer;
import tech.lp2p.relay.RelayService;


public final class Lite {

    public static final int CHUNK_SIZE = 262144;
    public static final int GRACE_PERIOD = 15;
    public static final int DHT_TABLE_SIZE = 200;
    public static final int DHT_ALPHA = 50;
    public static final int DHT_CONCURRENCY = 5;
    public static final int DHT_REQUEST_TIMEOUT = 5; // in seconds
    public static final int DHT_MESSAGE_TIMEOUT = 5; // in seconds
    public static final int DEFAULT_REQUEST_TIMEOUT = 1; // in seconds
    public static final int CONNECT_TIMEOUT = 5; // in seconds
    public static final int RELAY_CONNECT_TIMEOUT = 30; // in seconds
    public static final String Ed25519 = "Ed25519";

    private static final long LIMIT_BYTES = 1024;
    private static final int LIMIT_DURATION = 15 * 50; // in seconds [15 min]
    @NonNull
    private final LiteHost host;

    public Lite(@NonNull Settings settings) throws Exception {
        KeyPair keyPair = settings.keyPair();
        Objects.requireNonNull(keyPair);
        if (!Objects.equals(keyPair.getPublic().getAlgorithm(), Ed25519)) {
            throw new IllegalArgumentException("Ed22519 public key expected");
        }
        if (!Objects.equals(keyPair.getPrivate().getAlgorithm(), Ed25519)) {
            throw new IllegalArgumentException("Ed22519 private key expected");
        }
        String agent = settings.agent();
        Objects.requireNonNull(agent);
        Peeraddrs bootstrap = settings.bootstrap();
        Objects.requireNonNull(bootstrap);
        Payloads payloads = settings.payloads();
        Objects.requireNonNull(payloads);
        this.host = new LiteHost(keyPair, bootstrap, agent, payloads);
    }


    // Note: Only Ed25519 support
    public static KeyPair generateKeyPair() throws Exception {
        Ed25519Sign.KeyPair keyPair = Ed25519Sign.KeyPair.newKeyPair();
        return new KeyPair(PublicKey.create(keyPair.getPublicKey()),
                new PrivateKey(keyPair.getPrivateKey()));
    }


    public static void pushEnvelope(@NonNull Session session,
                                    @NonNull Connection connection,
                                    @NonNull Envelope envelope) throws Exception {
        LiteService.pushEnvelope(session, connection, envelope);
    }

    @NonNull
    public static Envelope createEnvelope(@NonNull Host host,
                                          @NonNull Payload payload,
                                          @NonNull Cid cid) {
        return new Envelope(host.self(), cid, payload);
    }

    @NonNull
    public static String fetchText(@NonNull Connection connection,
                                   @NonNull Cid cid,
                                   @NonNull Progress progress) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(connection, outputStream, cid, progress);
            return outputStream.toString();
        }
    }

    @NonNull
    public static String fetchText(@NonNull Session session, @NonNull Cid cid) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid);
            return outputStream.toString();
        }
    }

    @NonNull
    public static byte[] fetchData(@NonNull Connection connection,
                                   @NonNull Cid cid,
                                   @NonNull Progress progress) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(connection, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public static byte[] fetchData(@NonNull Session session, @NonNull Cid cid) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid);
            return outputStream.toByteArray();
        }
    }


    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public static void fetchToOutputStream(@NonNull Connection connection,
                                           @NonNull OutputStream outputStream,
                                           @NonNull Cid cid,
                                           @NonNull Progress progress) throws Exception {

        long totalRead = 0L;
        int remember = 0;

        LiteReader liteReader = LiteReader.getReader(connection, cid);
        long size = liteReader.getSize();

        do {


            ByteString buffer = liteReader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();

            if (size > 0) {
                int percent = (int) ((totalRead * 100.0f) / size);
                if (remember < percent) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }

        } while (true);
    }

    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public static void fetchToOutputStream(@NonNull Session session,
                                           @NonNull OutputStream outputStream,
                                           @NonNull Cid cid)
            throws Exception {

        LiteReader liteReader = LiteReader.getReader(session, cid);
        do {
            ByteString buffer = liteReader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());
        } while (true);
    }

    @NonNull
    public static Identify identify(@NonNull Server server) throws Exception {
        return server.identify();
    }

    @NonNull
    public static Peeraddrs reservationPeeraddrs(@NonNull Server server) {
        return server.reservationPeeraddrs();
    }

    @Nullable
    public static Envelope pullEnvelope(@NonNull Session session,
                                        @NonNull Connection connection,
                                        @NonNull Payload payload) throws Exception {
        return LiteService.pullEnvelope(session, connection, payload);
    }

    @NonNull
    public static Fid storeFile(@NonNull Session session, @NonNull File file) throws Exception {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            return storeInputStream(session, file.getName(), inputStream);
        }
    }

    // data is limited to Lite.CHUNK_SIZE
    @NonNull
    public static Raw storeData(@NonNull Session session, byte[] data) throws Exception {
        return DagService.createRaw(session, data);
    }

    // data is limited to Lite.CHUNK_SIZE
    @NonNull
    public static Raw storeText(@NonNull Session session, @NonNull String data) throws Exception {
        return DagService.createRaw(session, data.getBytes());
    }

    @NonNull
    public static Fid storeInputStream(@NonNull Session session,
                                       @NonNull String name,
                                       @NonNull InputStream inputStream,
                                       @NonNull Progress progress, long size) throws Exception {

        return DagStream.readInputStream(session, name,
                new DagInputStream(inputStream, progress, size));

    }

    @NonNull
    public static Fid storeInputStream(@NonNull Session session,
                                       @NonNull String name,
                                       @NonNull InputStream inputStream)
            throws Exception {
        return DagStream.readInputStream(session, name, new DagInputStream(inputStream, 0));
    }


    @NonNull
    public static InputStream getInputStream(@NonNull Connection connection, @NonNull Cid cid)
            throws Exception {
        LiteReader liteReader = LiteReader.getReader(connection, cid);
        return new LiteReaderStream(liteReader);
    }


    @NonNull
    public static InputStream getInputStream(@NonNull Session session, @NonNull Cid cid)
            throws Exception {
        LiteReader liteReader = LiteReader.getReader(session, cid);
        return new LiteReaderStream(liteReader);
    }

    @NonNull
    public static InputStream getInputStream(@NonNull Connection connection,
                                             @NonNull Cid cid,
                                             @NonNull Progress progress)
            throws Exception {
        LiteReader liteReader = LiteReader.getReader(connection, cid);
        return new LiteProgressStream(liteReader, progress);

    }

    public static void fetchToFile(@NonNull Session session, @NonNull File file, @NonNull Cid cid)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(session, fileOutputStream, cid);
        }
    }

    public static void fetchToFile(@NonNull Connection connection,
                                   @NonNull File file,
                                   @NonNull Cid cid,
                                   @NonNull Progress progress) throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(connection, fileOutputStream, cid, progress);
        }
    }

    // has the session block storage the cid block
    public static boolean hasBlock(@NonNull Session session, @NonNull Cid cid) {
        return session.blockStore().hasBlock(cid);
    }


    // remove the cid block (add all links blocks recursively) from the session block storage
    public static void removeBlocks(@NonNull Session session, @NonNull Cid cid) throws Exception {
        DagStream.removeBlocks(session, cid);
    }

    // returns all blocks of the cid from the session block storage,
    // If the cid block contains links, also the links cid blocks are returned (recursive)
    @NonNull
    public static List<Cid> blocks(@NonNull Session session, @NonNull Cid cid) throws Exception {
        return DagStream.blocks(session, cid);
    }


    @NonNull
    public static Dir removeFromDirectory(@NonNull Session session, @NonNull Dir dir,
                                          @NonNull Dir child) throws Exception {
        return DagStream.removeFromDirectory(session, dir.cid(), child);
    }

    @NonNull
    public static Dir removeFromDirectory(@NonNull Session session, @NonNull Dir dir,
                                          @NonNull Fid child) throws Exception {
        return DagStream.removeFromDirectory(session, dir.cid(), child);
    }

    @NonNull
    public static Dir addToDirectory(@NonNull Session session, @NonNull Dir dir,
                                     @NonNull Dir child) throws Exception {
        return DagStream.addToDirectory(session, dir.cid(), child);
    }

    @NonNull
    public static Dir addToDirectory(@NonNull Session session, @NonNull Dir dir,
                                     @NonNull Fid child) throws Exception {
        return DagStream.addToDirectory(session, dir.cid(), child);
    }

    @NonNull
    public static Dir updateDirectory(@NonNull Session session, @NonNull Dir dir,
                                      @NonNull Fid child) throws Exception {
        return DagStream.updateDirectory(session, dir.cid(), child);
    }

    @NonNull
    public static Dir updateDirectory(@NonNull Session session, @NonNull Dir dir,
                                      @NonNull Dir child) throws Exception {
        return DagStream.updateDirectory(session, dir.cid(), child);
    }

    // Note: children can only be of Type Fid and Dir
    @NonNull
    public static Dir createDirectory(@NonNull Session session,
                                      @NonNull String name,
                                      @NonNull List<Info> children) throws Exception {
        return DagStream.createDirectory(session, name, children);
    }

    @NonNull
    public static Dir createEmptyDirectory(@NonNull Session session,
                                           @NonNull String name) throws Exception {
        return DagStream.createEmptyDirectory(session, name);
    }

    @NonNull
    public static Dir renameDirectory(@NonNull Session session,
                                      @NonNull Dir directory,
                                      @NonNull String name) throws Exception {
        return DagStream.renameDirectory(session, directory.cid(), name);
    }

    @NonNull
    public static Info info(@NonNull Session session, @NonNull Cid cid) throws Exception {
        return DagStream.info(session.blockStore(), cid);
    }

    public static boolean hasChild(@NonNull Connection connection,
                                   @NonNull Dir dir,
                                   @NonNull String name) throws Exception {
        LiteFetchManager fetchManager = new LiteFetchManager(connection);
        return DagStream.hasChild(fetchManager, dir, name);
    }

    public static boolean hasChild(@NonNull Session session, @NonNull Dir dir, @NonNull String name)
            throws Exception {
        return DagStream.hasChild(session.blockStore(), dir, name);
    }


    @NonNull
    public static List<Raw> raws(@NonNull Session session, @NonNull Fid fid) throws Exception {
        return DagStream.raws(session.blockStore(), fid);
    }

    @NonNull
    public static List<Info> childs(@NonNull Session session, @NonNull Dir dir) throws Exception {
        return DagStream.childs(session.blockStore(), dir);
    }

    public static Key createKey(@NonNull Cid cid) throws Exception {
        return Key.convertKey(cid.multihash());
    }

    public static void provideKey(@NonNull Server server,
                                  @NonNull Key key,
                                  @NonNull Consumer<Peeraddr> consumer,
                                  @NonNull Cancellable cancellable) {
        server.provideKey(cancellable, consumer, key);
    }

    public static void providers(@NonNull Session session,
                                 @NonNull Key key,
                                 @NonNull Consumer<Peeraddr> consumer,
                                 @NonNull Cancellable cancellable) {
        session.providers(cancellable, consumer, key);
    }

    @NonNull
    public static Peeraddrs findPeeraddrs(@NonNull Session session, @NonNull PeerId peerId,
                                          @NonNull Cancellable cancellable) {
        return session.findPeeraddrs(cancellable, peerId);
    }

    public static void findClosestPeers(@NonNull Session session, @NonNull PeerId peerId,
                                        @NonNull Consumer<Peeraddr> consumer,
                                        @NonNull Cancellable cancellable) {
        session.findClosestPeers(cancellable, consumer, peerId);
    }

    // no timeout is set, it is set internally to Lite.CONNECT_TIMEOUT 5 sec
    // this function is can be used for direct connection (not relayed)
    @NonNull
    public static Connection dial(@NonNull Session session,
                                  @NonNull Peeraddr peeraddr,
                                  @NonNull Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {
        return ConnectionBuilder.connect(session, peeraddr, parameters,
                session.certificate(), session.responder(parameters.alpn()));
    }

    // no timeout is set, it is set internally to Lite.CONNECT_TIMEOUT 5 sec
    // this function is can be used for relayed connection
    @NonNull
    public static Connection dial(@NonNull Session session, @NonNull PeerId peerId,
                                  @NonNull Parameters parameters, @NonNull Cancellable cancellable)
            throws ExecutionException, InterruptedException, TimeoutException {
        return RelayDialer.connection(session, peerId, parameters, cancellable);
    }

    @NonNull
    public static Identify identify(Connection connection) throws Exception {
        return IdentifyService.identify(connection);
    }

    @NonNull
    public static Parameters connectionParameters(ALPN alpn) {
        return Parameters.create(alpn);
    }

    @NonNull
    public static Cid decodeCid(@NonNull String cid) throws Exception {
        return Keys.decodeCid(cid);
    }

    @NonNull
    public static PeerId decodePeerId(@NonNull String pid) throws Exception {
        return Keys.decodePeerId(pid);
    }

    // Utility function, resolves a root Cid object till the path of links is reached
    @NonNull
    public static Info resolvePath(@NonNull Connection connection,
                                   @NonNull Cid root,
                                   @NonNull List<String> path) throws Exception {
        LiteFetchManager fetchManager = new LiteFetchManager(connection);
        return DagResolver.resolvePath(fetchManager, root, path);
    }

    @NonNull
    public static Info resolvePath(@NonNull Session session,
                                   @NonNull Cid root,
                                   @NonNull List<String> path) throws Exception {
        return DagResolver.resolvePath(session.blockStore(), root, path);
    }

    // return true, when it has reservations
    public static boolean hasReservations(@NonNull Server server) {
        return server.hasReservations();
    }

    // this function returns all the valid reservations
    @NonNull
    public static Reservations reservations(@NonNull Server server) {
        return server.reservations();
    }


    // this function does the reservation [it is bound to a server]
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    @NonNull
    public static Reservations reservations(@NonNull Server server,
                                            @NonNull Cancellable cancellable) {
        server.reservations(cancellable);
        return server.reservations();
    }

    // this function does the reservation [it is bound to a server]
    // Note: only reservation of version 2 is supported
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    // static relays are marked as Kind.STATIC, where limited relays are marked
    // as Kind.LIMITED (in the Reservation class)
    // Note: this function is used for testing (though it still adds it to the internal
    // reservation list, when succeeds)
    @VisibleForTesting
    @NonNull
    static Reservation reservation(@NonNull Server server,
                                   @NonNull Peeraddr peeraddr) throws Exception {
        return server.reservation(peeraddr);
    }

    @VisibleForTesting
    @NonNull
    static Reservation refreshReservation(@NonNull Server server,
                                          @NonNull Reservation reservation) throws Exception {
        return server.refreshReservation(reservation);
    }

    @VisibleForTesting
    static void closeReservation(@NonNull Server server,
                                 @NonNull Reservation reservation) throws Exception {
        server.closeReservation(reservation);
    }

    @VisibleForTesting
    @NonNull
    static Connection connection(@NonNull Session session,
                                 @NonNull Peeraddr relayPeeraddr,
                                 @NonNull PeerId target,
                                 @NonNull Parameters parameters) throws Exception {
        Connection connection = Lite.dial(session, relayPeeraddr,
                Lite.connectionParameters(ALPN.libp2p));
        try {
            return RelayService.hopConnect(session, relayPeeraddr, target, parameters);
        } finally {
            connection.close();
        }
    }


    public static Settings createSettings(@NonNull KeyPair keyPair,
                                          @NonNull Peeraddrs bootstrap,
                                          @NonNull String agent,
                                          @NonNull Payloads payloads) {
        return new Settings(keyPair, bootstrap, agent, payloads);
    }

    @NonNull
    public static Cid rawCid(@NonNull String string) throws Exception {
        return Cid.generateCid(Multicodec.RAW, string.getBytes(StandardCharsets.UTF_8));
    }

    // returns the closer peers to the given peerId
    @NonNull
    public static Peeraddrs findPeer(Connection connection, PeerId peerId) throws Exception {
        Key key = Key.convertKey(peerId.multihash());
        return DhtService.findPeer(connection, key);
    }

    @NonNull
    public static Peeraddrs providers(Connection connection, Key key) throws Exception {
        return DhtService.providers(connection, key);
    }

    @NonNull
    public static Server.Settings createServerSettings(int port) {
        return new Server.Settings(port, new Limit(LIMIT_BYTES, LIMIT_DURATION), false);
    }

    @NonNull
    public static Server.Settings createServerSettings(int port, Limit limit,
                                                       boolean dhtProtocolEnabled) {
        return new Server.Settings(port, limit, dhtProtocolEnabled);
    }

    @NonNull
    public static Peeraddr createPeeraddr(PeerId peerId, InetAddress inetAddress, int port) {
        return Peeraddr.create(peerId, inetAddress, port);
    }

    @NonNull
    public static Peeraddr createPeeraddr(String peerId, String inetAddress, int port) {
        try {
            return Peeraddr.create(decodePeerId(peerId), InetAddress.getByName(inetAddress), port);
        } catch (Throwable throwable) {
            throw new IllegalArgumentException(throwable);
        }
    }

    @VisibleForTesting
    @NonNull
    public static List<Connection> connections(Server server, PeerId peerId) {
        return server.connections(peerId);
    }


    // Reservation feature is only possible when a public Inet6Address is available
    public static boolean reservationFeaturePossible() {
        return !Network.publicAddresses().isEmpty();
    }

    @NonNull
    public KeyPair keyPair() {
        return host.keyPair();
    }

    @NonNull
    public PeerId self() {
        return host.self();
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore, @NonNull PeerStore peerStore) {
        return new LiteSession(blockStore, peerStore, host);
    }

    @NonNull
    public Server startServer(@NonNull Server.Settings settings,
                              @NonNull BlockStore blockStore,
                              @NonNull PeerStore peerStore,
                              @NonNull Consumer<Peeraddr> connectionGain,
                              @NonNull Consumer<Peeraddr> connectionLost,
                              @NonNull Consumer<Reservation> reservationGain,
                              @NonNull Consumer<Reservation> reservationLost,
                              @NonNull Function<PeerId, Boolean> isGated,
                              @NonNull Function<Payload, Envelope> envelopeSupplier,
                              @NonNull Consumer<Envelope> envelopeConsumer) {


        return LiteServer.startServer(settings, host, blockStore, peerStore,
                connectionGain, connectionLost, reservationGain, reservationLost, isGated,
                envelopeSupplier, envelopeConsumer);
    }

    public record Settings(@NonNull KeyPair keyPair,
                           @NonNull Peeraddrs bootstrap,
                           @NonNull String agent,
                           @NonNull Payloads payloads) {
    }
}
