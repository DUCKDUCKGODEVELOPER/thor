package tech.lp2p.relay;

import androidx.annotation.NonNull;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Session;
import tech.lp2p.quic.Connection;
import tech.lp2p.utils.Utils;

public class RelayDialer {
    private static final String TAG = RelayDialer.class.getSimpleName();

    @NonNull
    public static Connection connection(Session session, PeerId peerId,
                                        Parameters parameters, Cancellable cancellable)
            throws ExecutionException, InterruptedException, TimeoutException {

        AtomicBoolean abort = new AtomicBoolean(false);
        Cancellable cancel = () -> cancellable.isCancelled() || abort.get();
        CompletableFuture<Connection> connectionCompletableFuture = new CompletableFuture<>();

        session.findClosestPeers(cancel, peeraddr -> {
            try {
                // step by step connecting (though not fast)
                if (cancel.isCancelled()) {
                    return;
                }
                Utils.error(TAG, "Try " + peeraddr.toString());
                Connection connection = (tech.lp2p.quic.Connection)
                        RelayService.hopConnect(session, peeraddr, peerId, parameters);
                connectionCompletableFuture.complete(connection);
                abort.set(true);

                // abort other stuff
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable);
            }
        }, peerId);

        while (!cancel.isCancelled()) {
            Thread.sleep(1000); // todo
        }

        return connectionCompletableFuture.get(1, TimeUnit.SECONDS);
    }
}
