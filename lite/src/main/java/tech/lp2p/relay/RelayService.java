package tech.lp2p.relay;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import holepunch.pb.Holepunch;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Session;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.utils.Utils;

public interface RelayService {

    @NonNull
    static Connection hopConnect(@NonNull Session session,
                                 @NonNull Peeraddr relayAddress,
                                 @NonNull PeerId target,
                                 @NonNull Parameters parameters) throws Exception {
        Connection connection = ConnectionBuilder.connect(session,
                relayAddress, Parameters.create(ALPN.libp2p),
                session.certificate(), session.responder(ALPN.libp2p));
        try {
            RelayInfo relayInfo = createRelayPunchInfo(session, target, parameters);
            return hopConnect(session, connection, relayInfo);
        } finally {
            connection.close();
        }
    }

    @NonNull
    static RelayInfo createRelayPunchInfo(@NonNull Session session,
                                          @NonNull PeerId target,
                                          @NonNull Parameters parameters)
            throws Exception {
        DatagramSocket socket = new DatagramSocket();

        Peeraddrs peeraddrs = Peeraddr.peeraddrs(session.self(), socket.getLocalPort());
        if (peeraddrs.isEmpty()) {
            throw new ConnectException("Hole punching not possible [abort]");
        }
        return new RelayInfo(target, peeraddrs, socket, parameters);
    }


    @NonNull
    private static Connection hopConnect(@NonNull Session session,
                                         @NonNull Connection connection,
                                         @NonNull RelayInfo relayInfo) throws Exception {
        try {
            return connectHopRequest(session, connection, relayInfo)
                    .get(Lite.RELAY_CONNECT_TIMEOUT, TimeUnit.SECONDS);

        } catch (Exception exception) {
            relayInfo.socket().close();
            throw exception;
        }
    }

    static Reservation hopReserve(Connection connection, PeerId self) throws Exception {

        Circuit.Peer.Builder peerBuilder = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(self.multihash()));

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setPeer(peerBuilder.build())
                .setType(Circuit.HopMessage.Type.RESERVE).build();


        byte[] raw = StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                Protocol.RELAY_PROTOCOL_HOP.size()).request(
                Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.RELAY_PROTOCOL_HOP), Lite.CONNECT_TIMEOUT);

        byte[] data = StreamRequester.data(ByteBuffer.wrap(raw));
        Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);

        if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
            throw new Exception("NO RESERVATION STATUS " + connection.remotePeeraddr());
        }
        if (msg.getStatus() != Circuit.Status.OK) {
            throw new Exception("RESERVATION STATUS " + msg.getStatus().toString() + " " +
                    connection.remotePeeraddr());
        }
        if (!msg.hasReservation()) {
            throw new Exception("NO RESERVATION " + connection.remotePeeraddr());
        }
        Circuit.Reservation reserve = msg.getReservation();
        Limit limit = limit(msg);

        return new Reservation(connection.remotePeeraddr(), limit, reserve.getExpire());
    }


    static CompletableFuture<Connection> connectHopRequest(@NonNull Session session,
                                                           @NonNull Connection connection,
                                                           @NonNull RelayInfo relayInfo) {


        CompletableFuture<Connection> done = new CompletableFuture<>();

        try {
            Circuit.Peer.Builder builder = Circuit.Peer.newBuilder()
                    .setId(ByteString.copyFrom(relayInfo.target().multihash()));

            Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                    .setType(Circuit.HopMessage.Type.CONNECT)
                    .setPeer(builder.build())
                    .build();

            StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                    Protocol.RELAY_PROTOCOL_HOP.size(), new ConnectRequest(
                            done, session, relayInfo)).writeOutput(
                    Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                            Protocol.RELAY_PROTOCOL_HOP), false);

        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
        }
        return done;
    }

    static Limit limit(Circuit.HopMessage hopMessage) {
        long limitData = 0;
        int limitDuration = 0;

        if (hopMessage.hasLimit()) {
            Circuit.Limit limit = hopMessage.getLimit();
            limitData = limit.getData();
            limitDuration = limit.getDuration();
        }

        return new Limit(limitData, limitDuration);
    }


    record ConnectRequest(CompletableFuture<Connection> done, Session session,
                          RelayInfo relayInfo) implements StreamRequester {


        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void fin(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream finished before message"));
            }
            stream.removeAttribute(INITIALIZED);
        }


        @Override
        public void terminated(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
            stream.removeAttribute(INITIALIZED);
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {

            if (stream.hasAttribute(INITIALIZED)) {
                try {
                    SyncInfo syncInfo = sendSync(stream, data);

                    // Next, B wait for rtt/2
                    // the creating of a connection should not block, it will otherwise block
                    // all reading of the underlying port
                    Thread.sleep(syncInfo.rtt() / 2);


                    Executors.newSingleThreadExecutor().execute(() -> {
                        try {
                            Parameters parameters = relayInfo.parameters();
                            done.complete(ConnectionBuilder.connect(session,
                                    syncInfo.peeraddr(), parameters,
                                    session.certificate(),
                                    session.responder(parameters.alpn()),
                                    relayInfo.socket()));
                        } catch (Throwable throwable) {
                            throwable(throwable);
                        }
                    });
                } catch (Throwable throwable) {
                    throwable(throwable);
                }

            } else {
                try {
                    Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);
                    Objects.requireNonNull(msg);


                    if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                        throwable(new Exception("Malformed message"));
                        return;
                    }

                    if (msg.getStatus() != Circuit.Status.OK) {
                        throwable(new Exception("No reservation reason " + msg.getStatus().name()));
                        return;
                    }

                    Limit limit = RelayService.limit(msg);
                    Objects.requireNonNull(limit);
                    // Note the limit will not be checked even it is a non limited relay
                    // always a hole punch will be done

                    initializeConnect(stream);
                    stream.setAttribute(INITIALIZED, true);
                } catch (Throwable throwable) {
                    throwable(throwable);
                }
            }
        }


        @NonNull
        private SyncInfo sendSync(Stream stream, byte[] data) throws Exception {

            Long timer = (Long) stream.getAttribute(StreamRequester.TIMER); // timer set earlier
            Objects.requireNonNull(timer, "Timer not set on stream");
            long rtt = System.currentTimeMillis() - timer;

            // B receives the Connect message from A
            Holepunch.HolePunch msg = Holepunch.HolePunch.parseFrom(data);
            Objects.requireNonNull(msg, "Message is not defined");

            if (msg.getType() != Holepunch.HolePunch.Type.CONNECT) {
                throw new Exception("[A] send wrong message connect payloadType, abort");
            }

            Peeraddrs peeraddrs = Peeraddr.create(relayInfo.target(), msg.getObsAddrsList());

            if (peeraddrs.isEmpty()) {
                throw new Exception("[A] send no observed addresses, abort");
            }

            Holepunch.HolePunch response = Holepunch.HolePunch.newBuilder().
                    setType(Holepunch.HolePunch.Type.SYNC).build();
            stream.writeOutput(Utils.encode(response), false);

            Peeraddr peeraddr = Peeraddrs.reduceToOne(peeraddrs);
            Objects.requireNonNull(peeraddr);
            return new SyncInfo(peeraddr, rtt);
        }

        void initializeConnect(Stream stream) {
            Holepunch.HolePunch.Builder builder = Holepunch.HolePunch.newBuilder()
                    .setType(Holepunch.HolePunch.Type.CONNECT);

            for (Peeraddr peeraddr : relayInfo.peeraddrs()) {
                builder.addObsAddrs(ByteString.copyFrom(peeraddr.encoded()));
            }

            Holepunch.HolePunch message = builder.build();
            stream.setAttribute(StreamRequester.TIMER, System.currentTimeMillis());
            stream.writeOutput(Utils.encode(message), false);

        }

        record SyncInfo(Peeraddr peeraddr, long rtt) {
        }


    }


    record RelayInfo(@NonNull PeerId target, @NonNull Peeraddrs peeraddrs,
                     @NonNull DatagramSocket socket, @NonNull Parameters parameters) {

    }
}