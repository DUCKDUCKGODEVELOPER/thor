package tech.lp2p.utils;

import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import lite.pb.Lite;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Protocol;


public interface Utils {

    byte[] BYTES_EMPTY = new byte[0];
    X509Certificate[] CERTIFICATES_EMPTY = new X509Certificate[0];

    static byte[] concat(byte[]... chunks) throws IllegalStateException {
        int length = 0;
        for (byte[] chunk : chunks) {
            if (length > Integer.MAX_VALUE - chunk.length) {
                throw new IllegalStateException("exceeded size limit");
            }
            length += chunk.length;
        }
        byte[] res = new byte[length];
        int pos = 0;
        for (byte[] chunk : chunks) {
            System.arraycopy(chunk, 0, res, pos, chunk.length);
            pos += chunk.length;
        }
        return res;
    }

    static String bytesToHex(byte[] data, int length) {
        String digits = "0123456789abcdef";
        StringBuilder buffer = new StringBuilder();

        for (int i = 0; i != length; i++) {
            int v = data[i] & 0xff;

            buffer.append(digits.charAt(v >> 4));
            buffer.append(digits.charAt(v & 0xf));
        }

        return buffer.toString();
    }

    static String bytesToHex(byte[] data) {
        return bytesToHex(data, data.length);
    }


    static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NonNull
    static DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }


    static int compareUnsigned(byte[] a, byte[] b) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return Arrays.compareUnsigned(a, b);
        } else {
            return compareUnsignedFallback(a, b);
        }
    }


    private static int compareUnsignedFallback(byte[] a, byte[] b) {
        int minLength = Math.min(a.length, b.length);
        for (int i = 0; i + 7 < minLength; i += 8) {
            long la = Byte.toUnsignedLong(a[i]) << 56 |
                    Byte.toUnsignedLong(a[i + 1]) << 48 |
                    Byte.toUnsignedLong(a[i + 2]) << 40 |
                    Byte.toUnsignedLong(a[i + 3]) << 32 |
                    Byte.toUnsignedLong(a[i + 4]) << 24 |
                    Byte.toUnsignedLong(a[i + 5]) << 16 |
                    Byte.toUnsignedLong(a[i + 6]) << 8 |
                    Byte.toUnsignedLong(a[i + 7]);
            long lb = Byte.toUnsignedLong(b[i]) << 56 |
                    Byte.toUnsignedLong(b[i + 1]) << 48 |
                    Byte.toUnsignedLong(b[i + 2]) << 40 |
                    Byte.toUnsignedLong(b[i + 3]) << 32 |
                    Byte.toUnsignedLong(b[i + 4]) << 24 |
                    Byte.toUnsignedLong(b[i + 5]) << 16 |
                    Byte.toUnsignedLong(b[i + 6]) << 8 |
                    Byte.toUnsignedLong(b[i + 7]);

            if (la != lb)
                return Long.compareUnsigned(la, lb);

        }


        for (int i = 0; i < minLength; i++) {
            int ia = Byte.toUnsignedInt(a[i]);
            int ib = Byte.toUnsignedInt(b[i]);
            if (ia != ib)
                return Integer.compare(ia, ib);
        }

        return a.length - b.length;
    }

    /**
     * Returns the values from each provided array combined into a single array. For example, {@code
     * concat(new int[] {a, b}, new int[] {}, new int[] {c}} returns the array {@code {a, b, c}}.
     *
     * @param arrays zero or more {@code int} arrays
     * @return a single array containing all the values from the source arrays, in order
     */
    static int[] concat(int[]... arrays) {
        int length = 0;
        for (int[] array : arrays) {
            length += array.length;
        }
        int[] result = new int[length];
        int pos = 0;
        for (int[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }


    static int unsignedVariantSize(int value) {
        int remaining = value >> 7;
        int count = 0;
        while (remaining != 0) {
            remaining >>= 7;
            count++;
        }
        return count + 1;
    }

    static void writeUnsignedVariant(ByteBuffer out, int value) {
        int remaining = value >>> 7;
        while (remaining != 0) {
            out.put((byte) ((value & 0x7f) | 0x80));
            value = remaining;
            remaining >>>= 7;
        }
        out.put((byte) (value & 0x7f));
    }


    static byte[] encode(@NonNull MessageLite message) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    /**
     * @noinspection unused
     */
    static byte[] encode(byte[] data) {
        int dataLength = unsignedVariantSize(data.length);
        ByteBuffer buffer = ByteBuffer.allocate(dataLength + data.length);
        writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        return buffer.array();
    }

    static Lite.ProtoSelect.Protocol protocol(@NonNull Protocol protocol) {
        return switch (protocol.codec()) {
            case 0 -> Lite.ProtoSelect.Protocol.Fetch;
            case 1 -> Lite.ProtoSelect.Protocol.Pull;
            case 2 -> Lite.ProtoSelect.Protocol.Push;
            default -> throw new IllegalStateException();
        };
    }

    static byte[] createMessage(@NonNull Protocol protocol, @NonNull MessageLite messageLite) {

        return Lite.ProtoSelect.newBuilder()
                .setProtocol(protocol(protocol))
                .setData(ByteString.copyFrom(messageLite.toByteArray()))
                .build().toByteArray();
    }

    static byte[] encodeProtocols(Protocol... protocols) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (Protocol protocol : protocols) {
                byte[] data = protocol.name().getBytes(StandardCharsets.UTF_8);
                int length = data.length + 1; // 1 is "\n"
                int dataLength = unsignedVariantSize(length);
                ByteBuffer buffer = ByteBuffer.allocate(dataLength);
                writeUnsignedVariant(buffer, length);
                buf.write(buffer.array());
                buf.write(data);
                buf.write('\n');
            }
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    static byte[] encode(@NonNull MessageLite message, Protocol... protocols) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (Protocol protocol : protocols) {
                byte[] data = protocol.name().getBytes(StandardCharsets.UTF_8);
                int length = data.length + 1; // 1 is "\n"
                int dataLength = unsignedVariantSize(length);
                ByteBuffer buffer = ByteBuffer.allocate(dataLength);
                writeUnsignedVariant(buffer, length);
                buf.write(buffer.array());
                buf.write(data);
                buf.write('\n');
            }
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    @SuppressWarnings("UnusedReturnValue")
    static long copy(InputStream source, OutputStream sink) throws IOException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return source.transferTo(sink);
        } else {
            long nread = 0L;
            byte[] buf = new byte[4096];
            int n;
            while ((n = source.read(buf)) > 0) {
                sink.write(buf, 0, n);
                nread += n;
            }
            return nread;
        }
    }

    static void copy(@NonNull InputStream source, @NonNull OutputStream sink,
                     @NonNull Progress progress, long size) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[4096];
        int remember = 0;
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;

            if (size > 0) {
                int percent = (int) ((nread * 100.0f) / size);
                if (percent > remember) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }
        }
    }

    @SuppressWarnings("SameReturnValue")
    static boolean isDebug() {
        return false;
    }

    @SuppressWarnings("SameReturnValue")
    static boolean isError() {
        return true;
    }


    static void info(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            Log.w(tag, message);
        }
    }

    static void debug(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            Log.d(tag, message);
        }
    }

    static void error(@Nullable final String tag, @Nullable String message) {
        if (isError()) {
            Log.e(tag, Objects.requireNonNullElse(message, "No error message defined"));
        }
    }

    static void error(final String tag, @Nullable Throwable throwable) {
        if (isError()) {
            if (throwable != null) {
                Log.e(tag, throwable.getLocalizedMessage(), throwable);
            } else {
                Log.e(tag, "no throwable");
            }
        }
    }
}
