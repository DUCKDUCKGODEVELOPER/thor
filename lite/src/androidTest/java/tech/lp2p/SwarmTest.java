package tech.lp2p;


import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.utils.Utils;

public class SwarmTest {

    private static final String TAG = SwarmTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void dialPeerSwarm() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!Lite.hasReservations(server)) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        // now we check if we can dial ourself
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {

            Parameters parameters = Lite.connectionParameters(ALPN.lite);
            Connection connection = Lite.dial(dummySession, lite.self(),
                    parameters, new TimeoutCancellable(120)); // find me

            assertNotNull(connection);

            Envelope entry = Lite.pullEnvelope(dummySession, connection, TestEnv.CID);
            assertNotNull(entry);
        }


    }

}
