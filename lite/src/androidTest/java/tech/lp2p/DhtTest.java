package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class DhtTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void providers() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);
        Dummy dummy = Dummy.getInstance(context);
        Connection connection = null;
        try (Session dummySession = dummy.createSession()) {

            PeerId host = lite.self();
            assertNotNull(host);
            Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

            Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
            connection = Lite.dial(dummySession, peeraddr, parameters);

            Cid cid = Lite.rawCid("test");
            Key key = Lite.createKey(cid);

            // should return empty list (server does not support GET_PROVIDERS)
            Peeraddrs providers = Lite.providers(connection, key);
            assertNotNull(providers);
            assertTrue(providers.isEmpty());

        } finally {
            if (connection != null) {
                connection.close();

                Thread.sleep(3000);
                assertEquals(server.numConnections(), 0);
            }
        }
    }

    @Test
    public void findPeer() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {

            PeerId host = lite.self();
            assertNotNull(host);
            Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

            Parameters parameters = Lite.connectionParameters(ALPN.libp2p);

            Connection connection = null;
            try {
                connection = Lite.dial(dummySession, peeraddr, parameters);
                PeerId peerId = TestEnv.random();

                Peeraddrs peeraddrs = Lite.findPeer(connection, peerId);
                assertTrue(peeraddrs.isEmpty()); // peerId not inside the DHT
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } finally {
            Thread.sleep(3000);
            assertEquals(server.numConnections(), 0);
        }
    }
}
