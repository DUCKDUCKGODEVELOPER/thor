package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;
import java.util.Set;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;


public class HolePunchDirectTest {
    private static final String TAG = HolePunchDirectTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_holepunch() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);

        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!Lite.hasReservations(server)) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        Set<Reservation> reservations = Lite.reservations(server);

        for (Reservation reservation : reservations) {

            PeerId relayID = reservation.peerId();
            assertNotNull(relayID);
            Peeraddr relay = reservation.peeraddr();
            assertNotNull(relay);

            assertTrue(reservation.limitData() > 0);
            assertTrue(reservation.limitDuration() > 0);


            Utils.error(TAG, reservation.toString());

            Dummy dummy = Dummy.getInstance(context);
            try (Session dummySession = dummy.createSession()) {
                Connection connection = null;
                try {
                    connection = Lite.connection(dummySession, reservation.peeraddr(),
                            lite.self(), Lite.connectionParameters(ALPN.lite));
                    Objects.requireNonNull(connection);

                    Cid rawCid = Lite.rawCid("moin elf");
                    Envelope data = Lite.createEnvelope(dummySession, TestEnv.RAW, rawCid);


                    Lite.pushEnvelope(dummySession, connection, data);
                    Thread.sleep(2000);

                    try {
                        Envelope envelope = TestEnv.PUSHED.get();
                        assertArrayEquals(envelope.cid().encoded(), rawCid.encoded());
                    } catch (Exception e) {
                        fail();
                    }

                } catch (Throwable throwable) {
                    Utils.error(TAG, throwable.getMessage());
                } finally {
                    if (connection != null) {
                        connection.close();
                    }
                }


            } finally {
                dummy.clearDatabase();
            }

        }

        assertTrue(Lite.hasReservations(server));
    }

}
