package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagService;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

public class AlpnTest {


    private static final String TAG = AlpnTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void alpn_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numConnections(), 0);

        try (Session session = lite.createSession(BLOCKS.getInstance(context),
                TestEnv.PEERS)) {
            Dummy dummy = Dummy.getInstance(context);

            Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

            try (Session dummySession = dummy.createSession()) {


                byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

                Fid fid = TestEnv.createContent(session, "random.bin", input);
                assertNotNull(fid);

                byte[] cmp = Lite.fetchData(session, fid.cid());
                assertArrayEquals(input, cmp);

                List<Cid> cids = Lite.blocks(session, fid.cid());
                assertNotNull(cids);

                Parameters parameters = Parameters.create(ALPN.lite, false);
                Connection connection = Lite.dial(dummySession, peeraddr, parameters);
                Objects.requireNonNull(connection);

                Thread.sleep(1000);

                assertEquals(server.numConnections(), 1);


                // simple push test
                Cid cid = Lite.rawCid("moin");
                Envelope data = Lite.createEnvelope(dummySession, TestEnv.RAW, cid);
                Lite.pushEnvelope(dummySession, connection, data);

                Thread.sleep(1000);
                assertArrayEquals(TestEnv.PUSHED.get().cid().encoded(), cid.encoded());


                // simple pull test
                Envelope envelope = Lite.pullEnvelope(dummySession, connection, TestEnv.CID);
                assertNotNull(envelope);
                PeerId from = envelope.peerId();
                assertEquals(from, lite.self());
                Cid entry = envelope.cid();
                assertNotNull(entry);
                assertEquals(entry, DagService.createEmptyDirectory(""));


                //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
                byte[] output = Lite.fetchData(connection, fid.cid(),
                        progress -> Utils.info(TAG, String.valueOf(progress)));
                assertArrayEquals(input, output);

                connection.close();

                Thread.sleep(1000);

            } finally {
                dummy.clearDatabase();
            }

            Thread.sleep(3000);
            assertEquals(server.numConnections(), 0);
        }

    }

}
