package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;


public class CatTest {

    private static final String TAG = CatTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void cat_not_exist() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            Cid cid = Lite.decodeCid("QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nt");
            try {
                Lite.fetchData(session, cid);
                fail();
            } catch (Exception ignore) {
                //
            }
        }
    }


    @Test
    public void cat_test_local() throws Exception {


        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            Raw local = Lite.storeText(session, "Moin Moin Moin");
            assertNotNull(local);

            byte[] content = Lite.fetchData(session, local.cid());

            assertNotNull(content);
        }
    }

}