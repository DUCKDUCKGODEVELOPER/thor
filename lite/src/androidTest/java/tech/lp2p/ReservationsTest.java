package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;


public class ReservationsTest {
    private static final String TAG = ReservationsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_reservations() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!Lite.hasReservations(server)) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        for (Peeraddr ma : Lite.reservationPeeraddrs(server)) {
            Utils.debug(TAG, ma.toString());
        }

        try (Session ignored = lite.createSession(BLOCKS.getInstance(context),
                TestEnv.PEERS)) {

            int timeInMinutes = 1; // make higher for long run


            // test 1 minutes
            for (int i = 0; i < timeInMinutes; i++) {
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));

                Set<Reservation> reservations = Lite.reservations(server);
                for (Reservation reservation : reservations) {
                    Utils.debug(TAG, "Expire in minutes " + reservation.expireInMinutes()
                            + " " + reservation);
                    assertNotNull(reservation.peeraddr());
                    assertNotNull(reservation.peerId());
                }
            }
        }
    }

}
