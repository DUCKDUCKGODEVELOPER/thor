package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

public class DownloadTest {
    private static final String TAG = DownloadTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_client_server_download() throws Exception {

        Dummy dummy = Dummy.getInstance(context);

        Lite lite = TestEnv.getTestInstance(context);

        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        assertEquals(server.numConnections(), 0);

        // create dummy session
        int maxNumberBytes = 100 * 1000 * 1000; // 100 MB

        Fid cid;
        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            AtomicInteger counter = new AtomicInteger(0);
            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            cid = Lite.storeInputStream(session, "random.bin", new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });

        }
        assertNotNull(cid);
        long start = System.currentTimeMillis();

        PeerId host = lite.self();
        TestCase.assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());


        File file = TestEnv.createCacheFile(context);

        try (Dummy.BlockStoreCache blockStore = Dummy.BlockStoreCache.createInstance(context)) {
            try (Session dummySession = dummy.createSession(blockStore)) {
                Parameters parameters = Parameters.create(ALPN.lite, false);
                Connection connection = Lite.dial(dummySession, peeraddr, parameters);
                Objects.requireNonNull(connection);

                //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
                Lite.fetchToFile(connection, file, cid.cid(), new TestEnv.DummyProgress());
                connection.close();
            }

        }
        assertEquals(file.length(), maxNumberBytes);

        long end = System.currentTimeMillis();
        Utils.info(TAG, "Time for downloading " + (end - start) / 1000 +
                "[s]" + " " + file.length() / 1000000 + " [MB]");
        file.deleteOnExit();
        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);
    }

}
