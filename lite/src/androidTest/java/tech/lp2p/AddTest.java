package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.test.core.app.ApplicationProvider;

import com.google.protobuf.ByteString;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import tech.lp2p.lite.LiteReader;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

public class AddTest {

    private static final String TAG = AddTest.class.getSimpleName();
    private static final Context context = ApplicationProvider.getApplicationContext();


    @NonNull
    public static File createCacheFile() throws IOException {
        return File.createTempFile("temp", ".io.ipfs.cid", context.getCacheDir());
    }

    @Test(expected = Exception.class)
    public void add_and_remove() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            String content = "Hallo dfsadf";
            Raw raw = Lite.storeText(session, content);
            Cid text = raw.cid();
            assertNotNull(text);
            assertTrue(Lite.hasBlock(session, text));
            Lite.removeBlocks(session, text);
            assertFalse(Lite.hasBlock(session, text));

            Lite.fetchText(session, text); // closed exception expected
        }
    }

    @Test
    public void add_dir() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            Dir dir = Lite.createEmptyDirectory(session, "Moin");
            assertNotNull(dir);
            Info info = Lite.info(session, dir.cid());
            assertTrue(info instanceof Dir);
            Dir infoDir = (Dir) info;
            assertEquals(infoDir.name(), "Moin");
            assertEquals(info.name(), "Moin");

            dir = Lite.renameDirectory(session, dir, "Hallo");
            assertNotNull(dir);
            info = Lite.info(session, dir.cid());
            assertTrue(info instanceof Dir);
            infoDir = (Dir) info;
            assertEquals(infoDir.name(), "Hallo");
            assertEquals(info.name(), "Hallo");


            String content = "Hallo";
            Fid text = TestEnv.createContent(session, "text.txt", content.getBytes());
            assertNotNull(text);

            byte[] data = Lite.fetchData(session, text.cid());
            assertEquals(content, new String(data));

            dir = Lite.addToDirectory(session, dir, text);
            assertNotNull(dir);
            assertTrue(dir.size() > 0);

            boolean exists = Lite.hasChild(session, dir, "text.txt");
            assertTrue(exists);

            Info resolved = Lite.resolvePath(session, dir.cid(), List.of("text.txt"));
            assertNotNull(resolved);
            assertEquals(resolved.cid(), text.cid());


            exists = Lite.hasChild(session, dir, "text2.txt");
            assertFalse(exists);

            List<Info> childs = Lite.childs(session, dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 1);

            dir = Lite.removeFromDirectory(session, dir, text);
            assertNotNull(dir);

            childs = Lite.childs(session, dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 0);
        }
    }

    @Test
    public void update_dir() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            Dir dir = Lite.createEmptyDirectory(session, "a");
            assertNotNull(dir);
            assertEquals(dir.size(), 0);

            String fileName = "test.txt";

            // TEST 1
            String content = "Hallo";
            Fid text = TestEnv.createContent(session, fileName, content.getBytes());
            assertNotNull(text);

            dir = Lite.addToDirectory(session, dir, text);
            assertNotNull(dir);
            assertEquals(dir.size(), content.length());

            List<Info> childs = Lite.childs(session, dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).cid(), text.cid());

            // TEST 2
            String contentNew = "Hallo Moin";
            Fid cmp = TestEnv.createContent(session, fileName, contentNew.getBytes());
            Cid textNew = cmp.cid();
            assertNotNull(textNew);

            dir = Lite.updateDirectory(session, dir, cmp);
            assertNotNull(dir);
            assertEquals(dir.size(), contentNew.length());

            childs = Lite.childs(session, dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).cid(), textNew);
        }
    }

    @Test
    public void create_dir() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {

            String content1 = "Hallo 1";
            Fid text1 = TestEnv.createContent(session, "b.txt", content1.getBytes());
            assertNotNull(text1);

            String content2 = "Hallo 12";
            Fid text2 = TestEnv.createContent(session, "a.txt", content2.getBytes());
            assertNotNull(text2);


            Dir dir = Lite.createDirectory(session, "zeit", List.of(text1, text2));
            assertNotNull(dir);

            List<Info> childs = Lite.childs(session, dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 2);

            assertEquals(childs.get(0).cid().toString(), text1.cid().toString());
            assertEquals(childs.get(1).cid().toString(), text2.cid().toString());
        }
    }

    @Test
    public void add_wrap_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            int packetSize = 1000;
            long maxData = 1000;
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            Utils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = Lite.storeFile(session, inputFile);
            assertNotNull(fid);

            List<Raw> links = Lite.raws(session, fid);
            assertNotNull(links);
            assertEquals(links.size(), 4);

            byte[] bytes = Lite.fetchData(session, fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);


        }
    }

    @Test
    public void add_dir_test() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {

            File inputFile = new File(context.getCacheDir(), UUID.randomUUID().toString());
            assertTrue(inputFile.createNewFile());
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < 10; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(1000);
                    outputStream.write(randomBytes);
                }
            }

            Fid fid = Lite.storeFile(session, inputFile);
            assertNotNull(fid);

            List<Raw> links = Lite.raws(session, fid);
            assertNotNull(links);

            assertEquals(links.size(), 0);
        }
    }


    @Test
    public void add_test() throws Exception {

        int packetSize = 1000;
        long maxData = 1000;
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            Utils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = Lite.storeFile(session, inputFile);
            assertNotNull(fid);

            List<Raw> links = Lite.raws(session, fid);
            assertNotNull(links);
            assertEquals(links.size(), 4);
            Raw link = links.get(0);
            assertNotEquals(link.cid(), fid.cid());

            byte[] bytes = Lite.fetchData(session, fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);

        }
    }


    @Test
    public void add_wrap_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();


            Utils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = Lite.storeFile(session, inputFile);
            assertNotNull(fid);

            List<Raw> links = Lite.raws(session, fid);
            assertNotNull(links);
            assertEquals(links.size(), 0);

            byte[] bytes = Lite.fetchData(session, fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);
        }

    }

    @Test
    public void add_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            File inputFile = createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            Utils.debug(TAG, "Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = Lite.storeFile(session, inputFile);
            assertNotNull(fid);

            List<Raw> links = Lite.raws(session, fid);
            assertNotNull(links);
            assertEquals(links.size(), 0);

            byte[] bytes = Lite.fetchData(session, fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);

        }
    }


    @Test
    public void test_inputStream() throws Exception {


        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            String text = "moin zehn";
            Raw cid = Lite.storeText(session, text);
            assertTrue(Lite.hasBlock(session, cid.cid()));

            byte[] bytes = Lite.fetchData(session, cid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, text.length());

            InputStream stream = Lite.getInputStream(session, cid.cid());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Utils.copy(stream, outputStream);
            assertEquals(text, outputStream.toString());
        }
    }


    @Test
    public void test_inputStreamBig() throws Exception {


        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            byte[] text = TestEnv.getRandomBytes((Lite.CHUNK_SIZE * 2) - 50);
            Fid fid = TestEnv.createContent(session, "random.bin", text);

            byte[] bytes = Lite.fetchData(session, fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, text.length);


            InputStream stream = Lite.getInputStream(session, fid.cid());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Utils.copy(stream, outputStream);
            assertArrayEquals(text, outputStream.toByteArray());

        }
    }

    @Test
    public void test_reader() throws Exception {


        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            String text = "0123456789 jjjjjjjj";
            Raw cid = Lite.storeText(session, text);
            assertTrue(Lite.hasBlock(session, cid.cid()));

            LiteReader liteReader = LiteReader.getReader(session, cid.cid());
            liteReader.seek(0);
            ByteString buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            assertEquals(text, new String(buffer.toByteArray()));

            int pos = 11;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(text.substring(pos), stream.toString());

            pos = 5;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(text.substring(pos), stream.toString());
        }
    }

    @Test
    public void test_readerBig() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            byte[] text = TestEnv.getRandomBytes((Lite.CHUNK_SIZE * 2) - 50);
            Fid fid = TestEnv.createContent(session, "random.bin", text);

            assertTrue(Lite.hasBlock(session, fid.cid()));

            LiteReader liteReader = LiteReader.getReader(session, fid.cid());
            liteReader.seek(0);
            ByteString buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            assertEquals(Lite.CHUNK_SIZE, buffer.size());
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            assertEquals(Lite.CHUNK_SIZE - 50, buffer.size());

            int pos = Lite.CHUNK_SIZE + 50;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());

            assertEquals(Lite.CHUNK_SIZE - 100, stream.size());

            pos = Lite.CHUNK_SIZE - 50;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(50, stream.size());
        }
    }

    @Test
    public void directoryTest() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);

        // prepare data
        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {

            Fid fid = TestEnv.createContent(session, "index.txt", "Moin Moin".getBytes());
            assertNotNull(fid);


            Dir a = Lite.createEmptyDirectory(session, "a");
            assertNotNull(a);
            Dir b = Lite.createEmptyDirectory(session, "b");
            assertNotNull(b);
            b = Lite.addToDirectory(session, b, fid);
            assertNotNull(b);
            a = Lite.addToDirectory(session, a, b);
            assertNotNull(a);

            assertEquals(a.size(), fid.size()); // test if same size

            // now change the "index.txt"
            fid = TestEnv.createContent(session, "index.txt", "Moin Moin Moin".getBytes());
            assertNotNull(fid);

            b = Lite.updateDirectory(session, b, fid);
            assertNotNull(b);
            a = Lite.updateDirectory(session, a, b);
            assertNotNull(a);

            assertEquals(a.size(), fid.size()); // test if same size


            a = Lite.removeFromDirectory(session, a, b);
            assertNotNull(a);

            assertEquals(a.size(), 0); // test if same size

        }
    }
}
