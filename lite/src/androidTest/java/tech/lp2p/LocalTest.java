package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagService;
import tech.lp2p.store.BLOCKS;


public class LocalTest {


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void own_service_connect() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {

            Parameters parameters = Parameters.create(ALPN.lite, false);
            Connection connection = Lite.dial(session, peeraddr, parameters);
            assertNotNull(connection);


            Envelope envelope = Lite.pullEnvelope(session, connection, TestEnv.CID);
            assertNotNull(envelope);
            PeerId from = envelope.peerId();
            assertEquals(from, lite.self());
            Cid entry = envelope.cid();
            assertNotNull(entry);
            assertEquals(entry, DagService.createEmptyDirectory(""));

            connection.close();
        }
        Thread.sleep(3000);
        assertFalse(server.hasConnection(lite.self()));

    }
}
