package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class IdleTimeoutTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void idle_timeout_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        Dummy dummy = Dummy.getInstance(context);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        try (Session dummySession = dummy.createSession()) {

            Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
            Connection connection = Lite.dial(dummySession, peeraddr, parameters);
            Objects.requireNonNull(connection);


            Identify info = Lite.identify(connection);
            assertNotNull(info);
            assertEquals(info.agent(), TestEnv.AGENT);

            // check if connection is connected
            assertTrue(connection.isConnected());

            assertEquals(server.connections(dummy.self()).size(), 1); // also server is alive

            connection.close();

            Thread.sleep(1000);

            // check if connection is not connected
            assertFalse(connection.isConnected());

            Thread.sleep(3000);
            assertFalse(server.hasConnection(dummy.self()));

        }

    }


    @Test
    public void idle_timeout_with_keep_alive_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Dummy dummy = Dummy.getInstance(context);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        try (Session dummySession = dummy.createSession()) {

            Parameters parameters = Parameters.create(ALPN.libp2p, true);
            Connection connection = Lite.dial(dummySession, peeraddr, parameters);
            Objects.requireNonNull(connection);

            Identify info = Lite.identify(connection);
            assertNotNull(info);
            assertEquals(info.agent(), TestEnv.AGENT);

            // check if connection is connected
            assertTrue(connection.isConnected());

            assertEquals(server.connections(dummy.self()).size(), 1); // also server is alive

            connection.close();

            Thread.sleep(3000);
            assertFalse(server.hasConnection(dummy.self()));
        }

    }

}
