package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Reservations;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.utils.Utils;

public class RelayTest {
    private static final String TAG = RelayTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test(expected = Exception.class)
    public void permissionDeniedReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        try {
            Lite.reservation(server, relayPeeraddr); // exception expected
            fail();
        } finally {
            Thread.sleep(3000); // necessary so that the server is able to remove connection
            assertEquals(server.numConnections(), 0);
        }
    }


    @Test(expected = Exception.class)
    public void permissionDeniedConnection() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        try (Session session = lite.createSession(
                new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore())) {

            Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
            Lite.connection(session, relayPeeraddr, lite.self(), parameters); // exception expected
            fail();
        } finally {
            Thread.sleep(3000); // necessary so that the server is able to remove connection
            assertEquals(server.numConnections(), 0);
        }

    }

    @Test
    public void positiveReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            // check if server has connection
            List<Connection> connections = server.connections(aliceServer.self());
            assertNotNull(connections);
            assertEquals(connections.size(), 1);
            Connection connection = connections.get(0);
            assertNotNull(connection);
            assertTrue(connection.hasAttribute(StreamRequester.RELAYED));

            Lite bob = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                    new Peeraddrs(), TestEnv.AGENT, new Payloads()));

            try (Session bobSession = bob.createSession(
                    new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore())) {

                Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
                Connection bobToAlice = Lite.connection(bobSession, relayPeeraddr,
                        alice.self(), parameters);
                assertNotNull(bobToAlice);

                Peeraddr aliceRemote = bobToAlice.remotePeeraddr();
                assertNotNull(aliceRemote);
                assertEquals(aliceRemote.port(), alicePort);

                Identify identify = Lite.identify(bobToAlice);
                assertNotNull(identify);
                assertEquals(identify.peerId(), alice.self());
                assertEquals(identify.peerId(), bobToAlice.remotePeerId());

                assertTrue(bobToAlice.isConnected());
                bobToAlice.close();

            }

            reservations = Lite.reservations(aliceServer);
            assertFalse(reservations.isEmpty()); // should not be empty


            Lite.closeReservation(aliceServer, reservation);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty
        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);

    }


    @Test
    public void bobDeniedReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));


        List<PeerId> aliceGated = new ArrayList<>();
        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                aliceGated::contains,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            // check if server has connection
            List<Connection> connections = server.connections(aliceServer.self());
            assertNotNull(connections);
            assertEquals(connections.size(), 1);
            Connection connection = connections.get(0);
            assertNotNull(connection);
            assertTrue(connection.hasAttribute(StreamRequester.RELAYED));

            Lite bob = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                    new Peeraddrs(), TestEnv.AGENT, new Payloads()));

            aliceGated.add(bob.self()); // bob is now gated for alice

            try (Session bobSession = bob.createSession(
                    new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore())) {

                Parameters parameters = Lite.connectionParameters(ALPN.libp2p);

                boolean failed = false;
                try {
                    // this will fail
                    Lite.connection(bobSession, relayPeeraddr, alice.self(), parameters);
                } catch (Throwable expected) {
                    failed = true;
                }
                assertTrue(failed);

            }

            reservations = Lite.reservations(aliceServer);
            assertFalse(reservations.isEmpty()); // should not be empty


            Lite.closeReservation(aliceServer, reservation);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty
        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);

    }


    @Test
    public void reservationRefused() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);

            Thread.sleep(3000);
            boolean failed = false;
            try {
                Lite.reservation(aliceServer, relayPeeraddr);
            } catch (Throwable expected) {
                failed = true;
            }
            assertTrue(failed);

            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);
            assertEquals(reservations.iterator().next(), reservation);

            Lite.closeReservation(aliceServer, reservation);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty

        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);

    }


    @Test
    public void refreshReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            Reservation refreshed = Lite.refreshReservation(aliceServer, reservation);
            assertNotEquals(reservation, refreshed);

            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);
            assertEquals(reservations.iterator().next(), refreshed);

            Lite.closeReservation(aliceServer, refreshed);
            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 0);

        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);
    }


    @Test
    public void reservationLost() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            // get the connection with the peerId from alice from the lite server and close it
            PeerId alicePeerId = alice.self();
            assertNotNull(alicePeerId);
            List<Connection> connections = Lite.connections(server, alicePeerId);
            assertNotNull(connections);
            assertEquals(connections.size(), 1);
            Connection connection = connections.get(0);
            assertNotNull(connection);
            connection.close();

            Thread.sleep(3000);
            assertEquals(server.numConnections(), 0);

            // now check that the reservation is not returned by the aliceServer
            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 0);

        } finally {
            aliceServer.shutdown();
        }
    }


    @Test
    public void twoReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        int bobPort = Utils.nextFreePort();
        Lite bob = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));
        serverSettings = Lite.createServerSettings(bobPort);
        Server bobServer = bob.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to bobServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from bobServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservationAlice = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservationAlice);
            assertEquals(relayPeeraddr, reservationAlice.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            Reservation reservationBob = Lite.reservation(bobServer, relayPeeraddr);
            assertNotNull(reservationBob);
            assertEquals(relayPeeraddr, reservationBob.peeraddr());

            reservations = Lite.reservations(bobServer);
            assertEquals(reservations.size(), 1);

            // server has two connections now
            assertEquals(server.numConnections(), 2);


            // now closing
            Lite.closeReservation(aliceServer, reservationAlice);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty


            Lite.closeReservation(bobServer, reservationBob);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty

        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);

    }


    @Test(expected = ExecutionException.class)
    public void limitedReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        assertNotNull(lite);
        int port = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(port,
                new Limit(10, 10), true); // limit should fail for reservation

        Server server = lite.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to server "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from server "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettingsAlice = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettingsAlice, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            Lite bob = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                    new Peeraddrs(), TestEnv.AGENT, new Payloads()));

            try (Session bobSession = bob.createSession(
                    new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore())) {

                Parameters parameters = Lite.connectionParameters(ALPN.libp2p);

                // this should fail
                Lite.connection(bobSession, relayPeeraddr, alice.self(), parameters);
                fail(); // should never reach this point
            } finally {
                Thread.sleep(3000);
                assertEquals(server.numConnections(), 1);

                // reservation is still valid
                reservations = Lite.reservations(aliceServer);
                assertFalse(reservations.isEmpty()); // should not be empty

                Lite.closeReservation(aliceServer, reservation);
                reservations = Lite.reservations(aliceServer);
                assertTrue(reservations.isEmpty()); // should be empty


                Thread.sleep(3000);
                assertEquals(server.numConnections(), 0);
            }

        } finally {
            server.shutdown();
            aliceServer.shutdown();
        }
    }

    @Test(expected = TimeoutException.class)
    public void serverShutdown() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        assertNotNull(lite);
        int port = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(port,
                new Limit(10, 10), true); // limit should fail for reservation

        Server server = lite.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to server "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from server "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettingsAlice = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettingsAlice, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {
            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);

            server.shutdown(); // server shutdown

            // this is only to make the test run (the shutdown is not detected by the
            // reservation connection (because shutdown does not send anything)
            // but an refresh of the reservation will help
            Lite.refreshReservation(aliceServer, reservation);
            fail(); // should never reach this point

        } finally {
            Reservations reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty

            aliceServer.shutdown();
        }
    }

}
