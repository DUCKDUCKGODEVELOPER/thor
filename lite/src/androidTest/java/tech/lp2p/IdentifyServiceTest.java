package tech.lp2p;


import static org.junit.Assert.assertNotNull;

import junit.framework.TestCase;

import org.junit.Test;

import tech.lp2p.core.Identify;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.utils.Utils;

public class IdentifyServiceTest {
    private static final String TAG = IdentifyServiceTest.class.getSimpleName();


    @Test
    public void identify_test() throws Exception {

        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        Identify info = Lite.identify(server);
        assertNotNull(info);

        assertNotNull(info.agent());
        assertNotNull(info.peerId());

        Peeraddr[] list = info.peeraddrs();
        assertNotNull(list);
        for (Peeraddr addr : list) {
            Utils.info(TAG, addr.toString());
        }

    }
}
