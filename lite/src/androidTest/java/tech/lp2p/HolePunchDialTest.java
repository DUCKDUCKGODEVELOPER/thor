package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.utils.Utils;

public class HolePunchDialTest {

    private static final String TAG = HolePunchDialTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_holepunch() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);

        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!Lite.hasReservations(server)) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {

            Connection connection = Lite.dial(dummySession, lite.self(),
                    Lite.connectionParameters(ALPN.libp2p),
                    new TimeoutCancellable(120));
            assertNotNull(connection);

            Identify info = Lite.identify(connection);
            assertNotNull(info);
            Utils.error(TAG, info.toString());

            connection.close();

        }
        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);
    }

}