package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import record.pb.EnvelopeOuterClass;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Session;
import tech.lp2p.lite.LiteService;
import tech.lp2p.store.BLOCKS;

public class EnvelopeTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void envelope_test() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context),
                TestEnv.PEERS)) {

            Cid cid = Lite.rawCid("moin");
            Envelope envelope = Lite.createEnvelope(session, TestEnv.RAW, cid);
            assertNotNull(envelope);
            assertEquals(envelope.peerId(), session.self());

            EnvelopeOuterClass.Envelope transformed = LiteService.createEnvelope(
                    session, envelope);
            assertNotNull(transformed);

            Envelope cmp = LiteService.createEnvelope(session, transformed.toByteArray());
            assertNotNull(cmp);

            assertEquals(cmp, envelope);
        }
    }
}
