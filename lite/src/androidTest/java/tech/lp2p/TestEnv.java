package tech.lp2p;

import static org.junit.Assert.assertNotNull;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyPair;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.dag.DagService;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

final class TestEnv {
    public static final Payload RAW = new Payload("raw-record", 10);
    public static final Payload CID = new Payload("cid-record", 20);
    public static final String AGENT = "lite/1.0.0/";
    public static final AtomicReference<Envelope> PUSHED = new AtomicReference<>();


    public static final Peeraddrs BOOTSTRAP = new Peeraddrs();
    public static final PeerStore PEERS = new PeerStore() {
        private final Set<Peeraddr> peers = ConcurrentHashMap.newKeySet();

        @Override
        public List<Peeraddr> randomPeeraddrs(int limit) {
            return peers.stream().limit(limit).toList();
        }

        @Override
        public void storePeeraddr(@NonNull Peeraddr peeraddr) {
            peers.add(peeraddr);
        }

        @Override
        public void removePeeraddr(@NonNull Peeraddr peeraddr) {
            peers.remove(peeraddr);
        }
    };
    private static final String TAG = TestEnv.class.getSimpleName();
    @NonNull
    private static final AtomicReference<Server> SERVER = new AtomicReference<>();
    @NonNull
    private static final ReentrantLock reserve = new ReentrantLock();
    private static volatile Lite INSTANCE = null;
    private static Lite.Settings SETTINGS;

    static {
        try {
            // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
            BOOTSTRAP.add(Lite.createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    "104.131.131.82", 4001));
            Payloads payloads = new Payloads();
            payloads.put(RAW.type(), RAW);
            payloads.put(CID.type(), CID);
            SETTINGS = Lite.createSettings(Lite.generateKeyPair(), BOOTSTRAP, AGENT, payloads);
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
        }
    }

    @NonNull
    public static Lite getInstance(@NonNull Lite.Settings settings) throws Exception {
        if (INSTANCE == null) {
            synchronized (Lite.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Lite(settings);
                }
            }
        }
        return INSTANCE;
    }

    public static PeerId random() {
        try {
            KeyPair keys = Lite.generateKeyPair();
            return Keys.createPeerId(keys.getPublic());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    static File createCacheFile(Context context) throws IOException {
        return File.createTempFile("temp", ".cid", context.getCacheDir());
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        new Random().nextBytes(bytes);
        return bytes;
    }

    public static Fid createContent(Session session, String name, byte[] data) throws Exception {
        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return Lite.storeInputStream(session, name, inputStream);
        }
    }


    public static boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return false;
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
        return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    @Nullable
    public static Server getServer() {
        return SERVER.get();
    }

    public static Lite getTestInstance(@NonNull Context context) throws Exception {
        reserve.lock();
        try {
            Objects.requireNonNull(SETTINGS);
            Lite lite = TestEnv.getInstance(SETTINGS);

            BLOCKS.getInstance(context).clear(); // clears the default blockStore

            if (SERVER.get() == null) {

                Server server = lite.startServer(Lite.createServerSettings(5001),
                        BLOCKS.getInstance(context), PEERS,
                        peeraddr -> Utils.debug(TAG, "Incoming connection : "
                                + peeraddr.toString()),
                        peeraddr -> Utils.debug(TAG, "Closing connection : "
                                + peeraddr.toString()),
                        reservationGain -> Utils.error(TAG, "Reservation gain " +
                                reservationGain.toString()),
                        reservationLost -> Utils.error(TAG, "Reservation lost " +
                                reservationLost.toString()),
                        peerId -> {
                            Utils.debug(TAG, "Peer Gated : " + peerId.toString());
                            return false;
                        },
                        payload -> {
                            try {
                                if (Objects.equals(payload, TestEnv.CID)) {
                                    Cid cid = DagService.createEmptyDirectory("");
                                    return Lite.createEnvelope(Objects.requireNonNull(getServer()),
                                            TestEnv.CID, cid);
                                } else {
                                    return null;
                                }
                            } catch (Throwable throwable) {
                                Utils.error(TAG, throwable);
                            }
                            return null;
                        }, PUSHED::set);

                // just to get coverage of shutdown method
                Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));
                SERVER.set(server);

                if (Lite.reservationFeaturePossible()) {

                    Set<Reservation> reservations = Lite.reservations(
                            server, new TimeoutCancellable(120));

                    for (Reservation reservation : reservations) {
                        Utils.error(TAG, reservation.toString());
                    }

                    assertNotNull(server.socket());

                    Peeraddrs peeraddrs = Lite.reservationPeeraddrs(server);
                    for (Peeraddr addr : peeraddrs) {
                        Utils.error(TAG, "Reservation Address " + addr.toString());
                    }

                }
            }

            return lite;
        } finally {
            reserve.unlock();
        }
    }

    public static class DummyBlockStore implements BlockStore {
        @Override
        public boolean hasBlock(@NonNull Cid cid) {
            return false;
        }

        @Nullable
        @Override
        public byte[] getBlock(@NonNull Cid cid) {
            return null;
        }


        @Override
        public void deleteBlock(@NonNull Cid cid) {

        }

        @Override
        public void storeBlock(@NonNull Cid cid, @NonNull byte[] block) {

        }

    }

    public static class DummyProgress implements Progress {
        @Override
        public void setProgress(int progress) {
            Utils.info(TAG, "Progress " + progress);
        }
    }

    public static class DummyPeerStore implements PeerStore {
        @Override
        public List<Peeraddr> randomPeeraddrs(int limit) {
            return Collections.emptyList();
        }


        @Override
        public void storePeeraddr(@NonNull Peeraddr peeraddr) {

        }

        @Override
        public void removePeeraddr(@NonNull Peeraddr peeraddr) {

        }
    }


}
