package tech.lp2p;


import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

public class FindClosestPeersTest {
    private static final String TAG = FindClosestPeersTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void find_closest_peers() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Set<Peeraddr> found = ConcurrentHashMap.newKeySet();

        try (Session session = lite.createSession(BLOCKS.getInstance(context),
                TestEnv.PEERS)) {

            Lite.findClosestPeers(session, lite.self(), found::add,
                    new TimeoutCancellable(() -> found.size() > 10, 30));

            for (Peeraddr peeraddr : found) {
                Utils.error(TAG, peeraddr.toString());
            }

            assertFalse(found.isEmpty());
        }
    }
}
