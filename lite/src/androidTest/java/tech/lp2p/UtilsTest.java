package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;
import java.net.InetAddress;
import java.security.KeyPair;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

import crypto.pb.Crypto;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Multibase;
import tech.lp2p.core.Multicodec;
import tech.lp2p.core.Multihash;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.PublicKey;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagService;
import tech.lp2p.dht.DhtQueryPeer;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.RoomConverters;
import tech.lp2p.utils.Utils;


public class UtilsTest {
    private static final String TAG = UtilsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void testBlockStoreCache() throws Exception {
        Dummy.BlockStoreCache a = Dummy.BlockStoreCache.createInstance(context);
        Dummy.BlockStoreCache b = Dummy.BlockStoreCache.createInstance(context);

        Assert.assertNotNull(a.getName());
        Assert.assertNotNull(b.getName());
        assertNotEquals(a.getName(), b.getName());

        DagService.DirectoryNode directory = DagService.createDirectory("");

        byte[] bytes = directory.node().toByteArray();
        Cid cid = Cid.generateCid(Multicodec.DAG_PB, bytes);
        a.storeBlock(cid, bytes);

        assertTrue(a.hasBlock(cid));
        assertFalse(b.hasBlock(cid));
        a.close();
        assertFalse(a.hasBlock(cid));
        b.close();
    }


    @Test
    public void testAddress() throws Exception {
        PeerId peerId = TestEnv.random();
        String address = "/ip6/2804:d41:432f:3f00:ccbd:8e0d:a023:376b/udp/4001/quic-v1/p2p/" + peerId;
        Peeraddr peeraddr = Peeraddr.create(address);
        Assert.assertNotNull(peeraddr);

        Peeraddr cmp = Peeraddr.create(peerId, peeraddr.encoded());
        Assert.assertNotNull(cmp);
        TestCase.assertEquals(peeraddr.peerId(), cmp.peerId());
        TestCase.assertEquals(peeraddr.multiaddr(), cmp.multiaddr());
        TestCase.assertEquals(peeraddr, cmp);
    }


    @Test
    public void peerId_random() {
        PeerId peerId = TestEnv.random();
        byte[] bytes = RoomConverters.toArray(peerId);
        TestCase.assertEquals(RoomConverters.fromArrayToPeerId(bytes), peerId);
    }

    @Test
    public void decode_name() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        // test of https://docs.ipfs.io/how-to/address-ipfs-on-web/#http-gateways
        PeerId test = Lite.decodePeerId("QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
        byte[] multihash = Multihash.decode(test.multihash());
        String base36 = Multibase.encode(Multibase.BASE36,
                Cid.createCid(Multicodec.LIBP2P_KEY, multihash).encoded());
        TestCase.assertEquals("k2k4r8jl0yz8qjgqbmc2cdu5hkqek5rj6flgnlkyywynci20j0iuyfuj", base36);

        Cid cid = Lite.decodeCid("QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");

        TestCase.assertEquals(Cid.createCid(cid.getCodec(), cid.multihash()).toString(),
                "bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi");

        TestCase.assertEquals(lite.self(), Lite.decodePeerId(lite.self().toBase36()));
    }

    @Test
    public void network() throws Exception {

        int port = 4001;

        Lite lite = TestEnv.getTestInstance(context);

        List<InetAddress> siteLocalAddresses = Network.siteLocalAddresses();
        Assert.assertNotNull(siteLocalAddresses);
        Utils.info(TAG, siteLocalAddresses.toString());

        Peeraddr ma = Peeraddr.loopbackPeeraddr(lite.self(), port);
        Assert.assertNotNull(ma);
        Utils.info(TAG, ma.toString());


    }

    @Test
    public void cat_utils() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        PeerId peerId = lite.self();


        Key a = Key.convertKey(peerId.multihash());
        Key b = Key.convertKey(peerId.multihash());


        BigInteger dist = DhtQueryPeer.distance(a, b);
        TestCase.assertEquals(dist.longValue(), 0L);


        PeerId randrom = TestEnv.random();

        Key r1 = Key.convertKey(randrom.multihash());

        BigInteger distCmp = DhtQueryPeer.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        Cid cid = Lite.rawCid("time");
        Assert.assertNotNull(cid);

    }


    @Test
    public void test_tink_ed25519() throws Exception {
        KeyPair keyPair = Lite.generateKeyPair();

        PeerId peerId = Keys.createPeerId(keyPair.getPublic());

        byte[] msg = "moin moin".getBytes();
        byte[] signature = Keys.sign(keyPair.getPrivate(), msg);

        Base64.Encoder encoder = Base64.getEncoder();
        String privateKeyAsString = encoder.encodeToString(keyPair.getPrivate().getEncoded());
        Assert.assertNotNull(privateKeyAsString);
        String publicKeyAsString = encoder.encodeToString(keyPair.getPublic().getEncoded());
        Assert.assertNotNull(publicKeyAsString);

        Base64.Decoder decoder = Base64.getDecoder();

        byte[] privateKey = decoder.decode(privateKeyAsString);
        Assert.assertNotNull(privateKey);
        byte[] publicKey = decoder.decode(publicKeyAsString);


        PeerId peerIdCmp = Keys.createPeerId(PublicKey.create(publicKey));

        TestCase.assertEquals(peerId, peerIdCmp);

        Keys.verify(publicKey, msg, signature);

    }

    @Test
    public void cidV1() throws Exception {

        Cid cid = Lite.rawCid("moin welt");
        Assert.assertNotNull(cid);
        Assert.assertNotNull(cid.multihash());

        Cid cmp = RoomConverters.fromArrayToCid(cid.encoded());
        Assert.assertNotNull(cmp);
        TestCase.assertEquals(cmp, cid);
        assertArrayEquals(cmp.multihash(), cid.multihash());

        // automatic conversion to version 1 and base32
        Cid v0 = Lite.decodeCid("QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");
        TestCase.assertEquals(v0.toString(), "bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi");

    }

    @Test
    public void peer_ids_test() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        //  -- Peer ID (sha256) encoded as a CID (inspect)
        PeerId peerId = Lite.decodePeerId("bafzbeie5745rpv2m6tjyuugywy4d5ewrqgqqhfnf445he3omzpjbx5xqxe");
        assertNotNull(peerId);

        // -- Peer ID (sha256) encoded as a raw base58btc multihash
        peerId = Lite.decodePeerId("QmYyQSo1c1Ym7orWxLYvCrM2EmxFTANf8wXmmE7DWjhx5N");
        assertNotNull(peerId);

        //  -- Peer ID (ed25519, using the "identity" multihash) encoded as a raw base58btc multihash
        peerId = Lite.decodePeerId("12D3KooWD3eckifWpRn9wQpMG9R9hX3sD158z7EqHWmweQAJU5SA");
        assertNotNull(peerId);

        peerId = Lite.decodePeerId(lite.self().toBase36());
        assertNotNull(peerId);
        assertEquals(peerId, lite.self());
    }


    @Test
    public void test_extractPublicKey() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Keys key = Keys.extractPublicKey(lite.self());
        assertNotNull(key);
        assertEquals(key.getKeyType(), Crypto.KeyType.Ed25519);

    }

    @Test
    public void test_keys() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        KeyPair keyPair = lite.keyPair();
        assertNotNull(keyPair.getPrivate());
        assertNotNull(keyPair.getPublic());
    }

    @Test
    public void listenAddresses() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Peeraddr[] result = Lite.identify(server).peeraddrs();
        assertNotNull(result);
        for (Peeraddr ma : result) {
            Utils.debug(TAG, "Listen Address : " + ma.toString());
        }


        Identify identify = Lite.identify(server);
        Objects.requireNonNull(identify);

        assertNotNull(identify.peerId());
        assertTrue(identify.hasRelayHop());
        assertEquals(identify.agent(), TestEnv.AGENT);
        assertEquals(identify.peerId(), lite.self());

        String[] protocols = identify.protocols();
        for (String protocol : protocols) {
            assertNotNull(protocol);
        }

        Utils.debug(TAG, identify.toString());
    }


    @Test
    public void streamTest() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(
                BLOCKS.getInstance(context), TestEnv.PEERS)) {
            String test = "Moin";
            Raw cid = Lite.storeText(session, test);
            assertNotNull(cid);
            byte[] bytes = Lite.fetchData(session, cid.cid());
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));


            try {
                Cid fault = Lite.decodeCid(lite.self().toString());
                Lite.fetchData(session, fault);
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }

    }

    @Test
    public void textProgressTest() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(
                BLOCKS.getInstance(context), TestEnv.PEERS)) {
            String test = "Moin bla bla";
            Raw cid = Lite.storeText(session, test);
            assertNotNull(cid);

            byte[] bytes = Lite.fetchData(session, cid.cid());
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));

            String text = Lite.fetchText(session, cid.cid());
            assertNotNull(text);
            assertEquals(test, text);

        }

    }

    @Test
    public void test_timeout_cat() throws Exception {

        Cid notValid = Lite.decodeCid("QmaFuc7VmzwT5MAx3EANZiVXRtuWtTwALjgaPcSsZ2Jdip");
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(
                BLOCKS.getInstance(context), TestEnv.PEERS)) {
            try {
                Lite.fetchData(session, notValid);
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }
    }


    @Test
    public void test_add_cat() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(
                BLOCKS.getInstance(context), TestEnv.PEERS)) {
            byte[] content = TestEnv.getRandomBytes(400000);

            Fid cid = TestEnv.createContent(session, "random.bin", content);

            byte[] fileContents = Lite.fetchData(session, cid.cid());
            assertNotNull(fileContents);
            assertEquals(content.length, fileContents.length);
            assertEquals(new String(content), new String(fileContents));

            Lite.removeBlocks(session, cid.cid());
        }
    }

    @Test
    public void test_ls_small() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(
                BLOCKS.getInstance(context), TestEnv.PEERS)) {
            Raw cid = Lite.storeText(session, "hallo");
            assertNotNull(cid);
        }
    }
}
