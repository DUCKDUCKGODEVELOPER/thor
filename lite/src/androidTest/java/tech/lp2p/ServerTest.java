package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;


public class ServerTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void server_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(peeraddr);

        try (Session session = lite.createSession(
                BLOCKS.getInstance(context), TestEnv.PEERS)) {
            String text = "Hallo das ist ein Test";
            Raw cid = Lite.storeText(session, text);
            assertNotNull(cid);

            Dummy dummy = Dummy.getInstance(context);


            try (Session dummySession = dummy.createSession()) {

                PeerId host = lite.self();
                assertNotNull(host);

                Parameters parameters = Parameters.create(ALPN.lite, false);
                Connection connection = Lite.dial(dummySession, peeraddr, parameters);
                Objects.requireNonNull(connection);


                // simple push test
                Cid nsToCid = Lite.rawCid("moin moin");
                Envelope data =
                        Lite.createEnvelope(dummySession, TestEnv.RAW, nsToCid);

                Lite.pushEnvelope(dummySession, connection, data);
                Thread.sleep(1000);
                assertArrayEquals(TestEnv.PUSHED.get().cid().encoded(), nsToCid.encoded());

                String cmpText = Lite.fetchText(connection, cid.cid(), new TestEnv.DummyProgress());
                assertEquals(text, cmpText);


                connection.close();
            } finally {
                dummy.clearDatabase();
            }
        }

    }


    @Test
    public void server_multiple_dummy_conn() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(
                lite.self(), server.port());

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            byte[] input = TestEnv.getRandomBytes(50000);

            Raw cid = Lite.storeData(session, input);
            assertNotNull(cid);

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;
            int timeSeconds = 200;
            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {

                    try {
                        Dummy dummy = Dummy.getInstance(context);


                        try (Session dummySession = dummy.createSession()) {

                            PeerId serverPeer = lite.self();
                            assertNotNull(serverPeer);
                            PeerId client = dummy.self();
                            assertNotNull(client);


                            Parameters parameters = Parameters.create(ALPN.lite, false);
                            Connection connection = Lite.dial(dummySession, peeraddr, parameters);
                            assertNotNull(connection);


                            byte[] output = Lite.fetchData(connection, cid.cid(),
                                    new TestEnv.DummyProgress());
                            assertArrayEquals(input, output);

                            connection.close();

                            finished.incrementAndGet();
                        } finally {
                            dummy.clearDatabase();
                        }

                    } catch (Throwable throwable) {
                        Utils.error(ServerTest.class.getSimpleName(), throwable);
                        fail();
                    }
                });
            }

            executors.shutdown();

            assertTrue(executors.awaitTermination(timeSeconds, TimeUnit.SECONDS));
            assertEquals(finished.get(), instances);
        }

    }
}