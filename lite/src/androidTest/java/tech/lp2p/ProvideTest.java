package tech.lp2p;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

public class ProvideTest {
    private static final String TAG = ProvideTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_resolve_provide() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }


        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {

            byte[] data = TestEnv.getRandomBytes(100);
            Raw cid = Lite.storeData(session, data);
            assertNotNull(cid);

            long start = System.currentTimeMillis();

            Set<Peeraddr> providers = ConcurrentHashMap.newKeySet();
            Key key = Lite.createKey(cid.cid());
            Lite.provideKey(server, key, providers::add,
                    new TimeoutCancellable(60));

            Utils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());


            Set<Peeraddr> peeraddrs = ConcurrentHashMap.newKeySet();
            Lite.providers(session, key, peeraddrs::add,
                    new TimeoutCancellable(60));

            assertFalse(peeraddrs.isEmpty());
        }
    }


    @Test
    public void test_host_provide() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            PeerId self = lite.self();

            Cid cid = Lite.decodeCid(self.toBase36());
            assertNotNull(cid);

            boolean result = Arrays.equals(cid.multihash(), self.multihash());
            assertTrue(result);

            long start = System.currentTimeMillis();

            Set<Peeraddr> providers = ConcurrentHashMap.newKeySet();
            Key key = Lite.createKey(cid);
            Lite.provideKey(server, key, providers::add,
                    new TimeoutCancellable(60));

            Utils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());


            Set<Peeraddr> peeraddrs = ConcurrentHashMap.newKeySet();
            Lite.providers(session, key, peeraddrs::add,
                    new TimeoutCancellable(60));

            assertFalse(peeraddrs.isEmpty());
        }
    }
}
