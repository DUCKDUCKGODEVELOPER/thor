package tech.lp2p;


import static junit.framework.TestCase.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

public class ProvidesTest {
    private static final String TAG = ProvidesTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void providers_empty_bytes() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            Cid cid = Lite.storeData(session, Utils.BYTES_EMPTY).cid();

            long time = System.currentTimeMillis();

            Set<Peeraddr> provs = ConcurrentHashMap.newKeySet();
            Key key = Lite.createKey(cid);

            Lite.providers(session, key, provs::add,
                    new TimeoutCancellable(() -> provs.size() > 10, 60));

            assertFalse(provs.isEmpty());

            for (Peeraddr prov : provs) {
                Utils.info(TAG, "Provider " + prov);
            }
            Utils.debug(TAG, "Time Providers : " + (System.currentTimeMillis() - time) + " [ms]");
        }
    }
}
