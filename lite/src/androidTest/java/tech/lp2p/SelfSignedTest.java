package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.security.cert.X509Certificate;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.PeerId;
import tech.lp2p.utils.Utils;


public class SelfSignedTest {
    private static final String TAG = SelfSignedTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void certificateTest() throws Exception {

        Utils.debug(TAG, Certificate.integersToString(Certificate.PREFIXED_EXTENSION_ID));


        assertNotNull(Certificate.getLiteExtension());

        Lite lite = TestEnv.getTestInstance(context);

        X509Certificate cert = Certificate.createCertificate(lite.keyPair()).x509Certificate();
        assertNotNull(cert);

        PeerId peerId = Certificate.extractPeerId(cert);
        assertNotNull(peerId);
        assertEquals(peerId, lite.self());

    }

}
