package tech.lp2p;

import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Fid;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.utils.Utils;

public class TimeTest {
    private static final String TAG = TimeTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_adding_performance() throws Exception {


        Lite lite = TestEnv.getTestInstance(context);

        long start = System.currentTimeMillis();
        // create dummy session
        int maxNumberBytes = 100 * 1000 * 1000; // 100 MB
        try (Session session = lite.createSession(
                new TestEnv.DummyBlockStore(), TestEnv.PEERS)) {
            AtomicInteger counter = new AtomicInteger(0);
            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            Fid cid = Lite.storeInputStream(session, "random.bin", new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });
            assertNotNull(cid);
        }
        long end = System.currentTimeMillis();
        Utils.debug(TAG, "Time for hashing " + (end - start) / 1000 + "[s]");


        start = System.currentTimeMillis();
        try (Session session = lite.createSession(BLOCKS.getInstance(context), TestEnv.PEERS)) {
            AtomicInteger counter = new AtomicInteger(0);
            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            Fid cid = Lite.storeInputStream(session, "test.bin", new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });
            assertNotNull(cid);
        }
        end = System.currentTimeMillis();
        Utils.debug(TAG, "Time for hashing and storing " + (end - start) / 1000 + "[s]");
    }

}
